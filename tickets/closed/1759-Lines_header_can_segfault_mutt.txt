Ticket:  1759
Status:  closed
Summary: Lines header can segfault mutt

Reporter: <agriffis@gentoo.org>
Owner:    mutt-dev

Opened:       2004-01-15 03:49:05 UTC
Last Updated: 2005-08-10 01:33:43 UTC

Priority:  minor
Component: mutt
Keywords:  patch

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.5.5.1i
Severity: normal

-- Please type your report below this line
Switch on "edit_headers" in your muttrc, write an email containing a
Lines header, for example

    | From: me@myplace.org
    | To: you@yourplace.org
    | Lines: 10

Save, exit the editor and see the segfault:

    | "/tmp/mutt-agriffis-23229-1" 4L, 41C written
    | Segmentation fault

-- System Information
System Version: Linux kaf.zk3.dec.com 2.4.21-alpha-r1 #1 Mon Jul 14 14:04:04 EDT 2003 alpha EV67  GNU/Linux

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/lib/gcc-lib/alphaev67-unknown-linux-gnu/3.3.2/specs
Configured with: /usr/local/tmp/portage/gcc-3.3.2-r5/work/gcc-3.3.2/configure --prefix=/usr --bindir=/usr/alphaev67-unknown-linux-gnu/gcc-bin/3.3 --includedir=/usr/lib/gcc-lib/alphaev67-unknown-linux-gnu/3.3.2/include --datadir=/usr/share/gcc-data/alphaev67-unknown-linux-gnu/3.3 --mandir=/usr/share/gcc-data/alphaev67-unknown-linux-gnu/3.3/man --infodir=/usr/share/gcc-data/alphaev67-unknown-linux-gnu/3.3/info --enable-shared --host=alphaev67-unknown-linux-gnu --target=alphaev67-unknown-linux-gnu --with-system-zlib --enable-languages=c,c++,f77,objc --enable-threads=posix --enable-long-long --disable-checking --enable-cstdio=stdio --enable-clocale=generic --enable-__cxa_atexit --enable-version-specific-runtime-libs --with-gxx-include-dir=/usr/lib/gcc-lib/alphaev67-unknown-linux-gnu/3.3.2/include/g++-v3 --with-local-prefix=/usr/local --enable-shared --enable-nls --without-included-gettext --disable-multilib
Thread model: posix
gcc version 3.3.2 20031218 (Gentoo Linux 3.3.2-r5, propolice-3.3-7)

- CFLAGS
-Wall -pedantic -Wall -pedantic -mcpu=ev67 -O2 -pipe -mieee

-- Mutt Version Information

Mutt 1.5.5.1i (2003-11-05)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.21-alpha-r1 (alpha) [using ncurses 5.3]
Compile options:
-DOMAIN
-DEBUG
+HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
-USE_FCNTL  +USE_FLOCK
+USE_POP  +USE_IMAP  +IMAP_EDIT_THREADS  -USE_GSS  +USE_SSL  -USE_SASL  -USE_SASL2  
-HAVE_REGCOMP  +USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +COMPRESSED  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  -HAVE_LIBIDN  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="Maildir"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc/mutt"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.

patch-1.5.5.1.rr.compressed.1
patch-1.5.3.cd.edit_threads.9.5


>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2004-01-15 03:49:05 UTC <agriffis@gentoo.org>
* Added comment:
{{{
Package: mutt
Version: 1.5.5.1i
Severity: normal

-- Please type your report below this line
Switch on "edit_headers" in your muttrc, write an email containing a
Lines header, for example

   | From: me@myplace.org
   | To: you@yourplace.org
   | Lines: 10

Save, exit the editor and see the segfault:

   | "/tmp/mutt-agriffis-23229-1" 4L, 41C written
   | Segmentation fault

-- System Information
System Version: Linux kaf.zk3.dec.com 2.4.21-alpha-r1 #1 Mon Jul 14 14:04:04 EDT 2003 alpha EV67  GNU/Linux

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/lib/gcc-lib/alphaev67-unknown-linux-gnu/3.3.2/specs
Configured with: /usr/local/tmp/portage/gcc-3.3.2-r5/work/gcc-3.3.2/configure --prefix=/usr --bindir=/usr/alphaev67-unknown-linux-gnu/gcc-bin/3.3 --includedir=/usr/lib/gcc-lib/alphaev67-unknown-linux-gnu/3.3.2/include --datadir=/usr/share/gcc-data/alphaev67-unknown-linux-gnu/3.3 --mandir=/usr/share/gcc-data/alphaev67-unknown-linux-gnu/3.3/man --infodir=/usr/share/gcc-data/alphaev67-unknown-linux-gnu/3.3/info --enable-shared --host=alphaev67-unknown-linux-gnu --target=alphaev67-unknown-linux-gnu --with-system-zlib --enable-languages=c,c++,f77,objc --enable-threads=posix --enable-long-long --disable-checking --enable-cstdio=stdio --enable-clocale=generic --enable-__cxa_atexit --enable-version-specific-runtime-libs --with-gxx-include-dir=/usr/lib/gcc-lib/alphaev67-unknown-linux-gnu/3.3.2/include/g++-v3 --with-local-prefix=/usr/local --enable-shared --enable-nls --without-included-gettext --disable-multilib
Thread model: posix
gcc version 3.3.2 20031218 (Gentoo Linux 3.3.2-r5, propolice-3.3-7)

- CFLAGS
-Wall -pedantic -Wall -pedantic -mcpu=ev67 -O2 -pipe -mieee

-- Mutt Version Information

Mutt 1.5.5.1i (2003-11-05)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.21-alpha-r1 (alpha) [using ncurses 5.3]
Compile options:
-DOMAIN
-DEBUG
+HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
-USE_FCNTL  +USE_FLOCK
+USE_POP  +USE_IMAP  +IMAP_EDIT_THREADS  -USE_GSS  +USE_SSL  -USE_SASL  -USE_SASL2  
-HAVE_REGCOMP  +USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +COMPRESSED  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  -HAVE_LIBIDN  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="Maildir"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc/mutt"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.

patch-1.5.5.1.rr.compressed.1
patch-1.5.3.cd.edit_threads.9.5
}}}

--------------------------------------------------------------------------------
2004-06-03 12:16:28 UTC Alain Bench <veronatif@free.fr>
* Added attachment patch-1.5.6.ab.bug_1759.1
* Added attachment patch-1.5.6.ab.bug_1759.2.1
* Added comment:
patch-1.5.6.ab.bug_1759.1

* Added comment:
{{{
tags 1759 patch
thanks


Hello Aron, and thanks for your Mutt support.

   The report seems to not have reached mutt-dev list, found it only on
the BTS at <URL:http://bugs.guug.de/db/17/1759.html>.

On Wednesday, January 14, 2004 at 4:49:05 PM -0500, Aron Griffis wrote:

> Switch on "edit_headers" in your muttrc, write an email containing a
> Lines header, for example
>| From: me@myplace.org
>| To: you@yourplace.org
>| Lines: 10
> Save, exit the editor and see the segfault:
>| "/tmp/mutt-agriffis-23229-1" 4L, 41C written
>| Segmentation fault

   Confirmed. On February 13 2002 in early 1.5 a change was made to
parse.c:mutt_parse_rfc822_line() to deal with wrong negative values in
Lines header. This change lack a pair of curly brackets around a
conditional block, making Mutt shamelessly use a NULL pointer. The
attached patch fixes this.


Bye!	Alain.
-- 
Give your computer's unused idle processor cycles to a scientific goal:
The Folding@home project at <URL:http://folding.stanford.edu/>.
}}}

--------------------------------------------------------------------------------
2004-06-03 12:16:29 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
tags 1759 patch
thanks


Hello Aron, and thanks for your Mutt support.

   The report seems to not have reached mutt-dev list, found it only on
the BTS at <URL:http://bugs.guug.de/db/17/1759.html>.

On Wednesday, January 14, 2004 at 4:49:05 PM -0500, Aron Griffis wrote:

> Switch on "edit_headers" in your muttrc, write an email containing a
> Lines header, for example
>| From: me@myplace.org
>| To: you@yourplace.org
>| Lines: 10
> Save, exit the editor and see the segfault:
>| "/tmp/mutt-agriffis-23229-1" 4L, 41C written
>| Segmentation fault

   Confirmed. On February 13 2002 in early 1.5 a change was made to
parse.c:mutt_parse_rfc822_line() to deal with wrong negative values in
Lines header. This change lack a pair of curly brackets around a
conditional block, making Mutt shamelessly use a NULL pointer. The
attached patch fixes this.


Bye!	Alain.
-- 
Give your computer's unused idle processor cycles to a scientific goal:
The Folding@home project at <URL:http://folding.stanford.edu/>.
}}}

--------------------------------------------------------------------------------
2005-08-10 19:33:43 UTC brendan
* Added comment:
{{{
Fixed in CVS.
}}}

* resolution changed to fixed
* status changed to closed
