Ticket:  3180
Status:  closed
Summary: mutt does not set the From: header by default

Reporter: antonio@dyne.org
Owner:    mutt-dev

Opened:       2009-02-01 00:30:30 UTC
Last Updated: 2009-06-12 11:44:59 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
Hi guys, 
I'm forwarding a bug opened to the debian team, we have the intention to close the bug because for us it is not a problem but the person who cut the bug keeps reopening it.

What I'm looking here is a *clear* response from you about this behavior, do you want to add the From header by default or do you want to let the MTA manage it?

This is the first bug report, done 8 years ago (sic).

---

With the default configuration of mutt and exim, bounced messages may end
up with no From: headers.  This can be reproduced by sending a message while
saving a copy in =sent.  If that copy is then bounced, then mutt doesn't
add a From: header since the default Muttrc doesn't do so, and exim doesn't
add one since Resent-From is present.

Something's got to give here.

---

All the rest of the bug and its discussion is on http://bugs.debian.org/93268

I look forward to your reply.

Cheers
Antonio

--------------------------------------------------------------------------------
2009-06-12 11:44:59 UTC Rocco Rutte <pdmef@gmx.net>
* Added comment:
(In [ed206ad41108]) Warn before bouncing messages without From: header. Closes #3180.

* resolution changed to fixed
* status changed to closed
