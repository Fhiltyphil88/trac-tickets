Ticket:  3671
Status:  closed
Summary: warning installing mutt-1.5.22 (release) on OS X 10.9

Reporter: william
Owner:    kevin8t8

Opened:       2013-12-05 23:59:53 UTC
Last Updated: 2015-07-02 18:19:43 UTC

Priority:  trivial
Component: build
Keywords:  

--------------------------------------------------------------------------------
Description:
I get the following error (not sure if it's of concern, but thought I'd submit a bug just in case).

{{{
/Users/wby/mutt-1.5.22/missing: Unknown `--is-lightweight' option
Try `/Users/wby/mutt-1.5.22/missing --help' for more information}}}

I ran configure as:

jazz% ./configure --with-hcache --enable-imap  --enable-debug

I will attache config.log

--------------------------------------------------------------------------------
2013-12-06 00:00:05 UTC william
* Added attachment config.log

--------------------------------------------------------------------------------
2013-12-06 00:01:16 UTC william
* Added comment:
ps - I fixed the mistake ({{{--enable-hcache}}} instead of {{{--with-hcache}}}), but I don't think that's related.

--------------------------------------------------------------------------------
2015-06-28 17:35:44 UTC kevin8t8
* Added comment:
This is nothing too serious, but does show a problem with the release process.

Looking at the 1.5.22 and 1.5.23 release tarballs, the "missing" script is older than the version of automake used (in part) to generate the configure script.  Newer automake includes a revised version of the "missing" script, and the "--is-lightweight" flag is used by configure to make sure "missing" is the newer version.

The problem is that "autoreconf --install" won't reinstall the automake tools (e.g. missing, compile, depcomp, install-sh) if they are already there.  Also, even "make maintainer-clean" doesn't clean up those files.

So if the release is generated from an older clone of the source, the old version of those tools will end up in the tarball; while the configure script will be rebuilt using the latest versions in (e.g.) /usr/share/aclocal/missing.m4.

I suggest going forward, we always make a pristine clone and generate the releases off of that.

* owner changed to kevin8t8
* status changed to assigned

--------------------------------------------------------------------------------
2015-06-28 19:02:42 UTC kevin8t8
* Added comment:
Alternatively, "autoreconf --install --force" seems to update the tools too.  I'm attaching a proposed patch to the build-release script which adds "--add-missing --copy --force-missing" to the automake invocation.

--------------------------------------------------------------------------------
2015-06-28 19:03:02 UTC kevin8t8
* Added attachment release-fix.patch

--------------------------------------------------------------------------------
2015-07-02 18:19:43 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [67d945e380744c2db20adc57c75e5fd915de15db]:
{{{
#!CommitTicketReference repository="" revision="67d945e380744c2db20adc57c75e5fd915de15db"
Fix build-release to always update automake tools. (closes #3671)

1.5.22 and 1.5.23 include a version of "missing" older than the version
of automake/aclocal used to generate configure.

Fix the build-release script to always copy and force update of the
automake tools.
}}}

* resolution changed to fixed
* status changed to closed
