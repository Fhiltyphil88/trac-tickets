Ticket:  1262
Status:  closed
Summary: IMAP: messages flagged to old when synchronizing

Reporter: <Vincent.Lefevre@loria.fr>
Owner:    mutt-dev

Opened:       2002-07-02 01:10:18 UTC
Last Updated: 2007-02-27 21:14:50 UTC

Priority:  minor
Component: IMAP
Keywords:  IMAP

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.5.1-current-20020627i
Severity: normal

-- Please type your report below this line

When I synchronize an IMAP folder, new messages are automatically set to
old. This behavior is incorrect: this should happen only when quitting
the folder, like with FS folders.

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /users/spaces/logiciels/gcc-3.0/linux/lib/gcc-lib/i686-pc-linux-gnu/3.0/specs
Configured with: ../gcc-3.0/configure --prefix=/users/spaces/logiciels/gcc-3.0 --exec-prefix=/users/spaces/logiciels/gcc-3.0/linux
Thread model: single
gcc version 3.0

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.5.1-current-20020627i (2002-05-02)
Copyright (C) 1996-2001 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.2.17 (i686) [using ncurses 5.2]
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  -USE_SETGID  -USE_DOTLOCK  -DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_GSS  +USE_SSL  -USE_SASL  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+HAVE_PGP  +HAVE_SMIME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  +LOCALES_HACK  -HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/users/spaces/logiciels/ispell/linux/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH=""
PKGDATADIR="/users/spaces/lefevre/share/mutt"
SYSCONFDIR="/users/spaces/lefevre/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.


Received: (at submit) by bugs.guug.de; 23 Mar 2001 05:06:11 +0000
From hugo@larve.lcs.mit.edu  Fri Mar 23 06:06:06 2001
Received: from smtp01.mrf.mail.rcn.net (smtp01.mrf.mail.rcn.net [207.172.4.60])
	by sigtrap.guug.de (8.9.3/8.9.3/Debian/GNU) with ESMTP id GAA09771
	for <submit@bugs.guug.de>; Fri, 23 Mar 2001 06:06:06 +0100
Received: from 208-59-178-77.c3-0.smr-ubr1.sbo-smr.ma.cable.rcn.com ([208.59.178.77] helo=larve.lcs.mit.edu)
	by smtp01.mrf.mail.rcn.net with esmtp (Exim 3.16 #5)
	id 14gJlw-0000aN-00 ; Fri, 23 Mar 2001 00:06:00 -0500
Received: by larve.lcs.mit.edu (Postfix, from userid 1000)
	id 7018219; Fri, 23 Mar 2001 00:05:48 -0500 (EST)
From: Hugo Haas <hugo@larve.net>
Subject: mutt-1.3.15i: save-message does not preserve read status with IMAP folders
To: submit@bugs.guug.de
Message-Id: <20010323050548.7018219@larve.lcs.mit.edu>
Date: Fri, 23 Mar 2001 00:05:48 -0500 (EST)
Sender: hugo@larve.lcs.mit.edu

Package: mutt
Version: 1.3.15i
Severity: normal

-- Please type your report below this line

Hi.

I noticed the following problem:
1) open an IMAP folder.
2) read a new message; the message is now marked as read in the index.
3) save the message to another IMAP folder.
4) go to this folder: the saved message appears as unread.

With mbox folders, the message appears as read. Basically, the updated
status of the message should be propagated to the saved instance.

Regards,

Hugo

-- Mutt Version Information

Mutt 1.3.15i (2001-02-12)
Copyright (C) 1996-2000 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.2.18 [using ncurses 5.0]
Compile options:
-DOMAIN
+DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
-USE_POP  +USE_IMAP  -USE_GSS  -USE_SSL  -USE_SASL  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+HAVE_PGP  +BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
-ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
SHAREDIR="/usr/local/mutt-1.3.15/lib/mutt"
SYSCONFDIR="/usr/local/mutt-1.3.15/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.


>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2007-02-08 13:18:45 UTC rado
* Added comment:
{{{
Move to IMAP category, add Hugo Haas <hugo@larve.net> to notify-list.

Has imap_peek of a more recent mutt (1.5.13+) any influence on this?
}}}

--------------------------------------------------------------------------------
2007-02-09 05:48:36 UTC Vincent Lefevre <vincent@vinc17.org>
* Added comment:
{{{
On 2007-02-07 20:18:45 +0100, Rado Smiljanic wrote:
> Synopsis: IMAP: messages flagged to old when synchronizing
>=20
> **** Comment added by rado on Wed, 07 Feb 2007 20:18:45 +0100 ****
>  Move to IMAP category, add Hugo Haas <hugo@larve.net> to notify-list.
>=20
> Has imap_peek of a more recent mutt (1.5.13+) any influence on this?

Yes, this bug no longer occurs. It can be closed.

--=20
Vincent Lef=E8vre <vincent@vinc17.org> - Web: <http://www.vinc17.org/>
100% accessible validated (X)HTML - Blog: <http://www.vinc17.org/blog/>
Work: CR INRIA - computer arithmetic / Arenaire project (LIP, ENS-Lyon)
}}}

--------------------------------------------------------------------------------
2007-02-28 15:14:50 UTC rado
* Added comment:
{{{
On 2007-02-07 20:18:45 +0100, Rado Smiljanic wrote:
fixed as reported by OP, re-open if others still have problems.
}}}

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2007-02-28 15:14:51 UTC rado
* Added comment:
{{{
close per OP feedback.
}}}
