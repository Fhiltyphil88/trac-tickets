Ticket:  2452
Status:  closed
Summary: spurious space added to my_hdr Message-ID:

Reporter: Alain Bench <veronatif@free.fr> (Alain Bench)
Owner:    mutt-dev

Opened:       2006-08-27 10:53:37 UTC
Last Updated: 2007-11-15 11:39:57 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{

My dear ALL,

    When one declares a msgid with my_hdr, then compose a message, a
parasite space is added between the colon and the content of the field.
Example this command in muttrc or interactively:

| my_hdr Message-ID: <nice@example.com>

    ...is stored correctly by Mutt (Tamo's <status-commands>):

| my_hdr
|     Message-ID: <nice@example.com>

    ...but produces 2 spaces in composed mails:

| Message-ID:  <nice@example.com>

    This breaks locating message and threading with some other fragile
software. Apparently no other field is touched. Mutt 1.2.5i had no
problem, Mutts 1.4 until CVS have it. Noted by Troy Piggins on cmm:
Thank you Troy.


Alain.
>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2007-11-15 11:39:57 UTC pdmef
* Added comment:
(In [8c5357e2b31c]) Properly parse and validate Message-IDs from my_hdrs. Closes #2452.

* resolution changed to fixed
* status changed to closed
