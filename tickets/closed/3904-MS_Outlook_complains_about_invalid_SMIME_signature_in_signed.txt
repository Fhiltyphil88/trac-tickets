Ticket:  3904
Status:  closed
Summary: MS Outlook complains about invalid S/MIME signature in signed + encrypted message

Reporter: cooler
Owner:    mutt-dev

Opened:       2016-12-23 18:07:38 UTC
Last Updated: 2017-01-01 04:02:04 UTC

Priority:  major
Component: crypto
Keywords:  patch

--------------------------------------------------------------------------------
Description:
When I use the gpgme backend and send a signed + encrypted S/MIME message to an MS
Outlook user, the signature is reported as invalid. The signature is 
reported as valid if the message is only signed but not encrypted. When I use the
openssl backend, the signature is reported as valid in both cases.

When mutt uses the gpgme backend, <CR> is added at the end of the
lines of the message text before calculating the detached signature. When the
message text and the detached signature are encrypted, no <CR> is added at the
end of the lines of the message text. It appears that in case the 
message is also encrypted, MS Outlook does not add <CR> before verifying
the signature.

The attached patch adds <CR> to the mail body before encrypting the
data. This solved the problem for me.

I don't know what the S/MIME standard says on this but as mutt+openssl 
already adds <CR> to the mail body before encrypting wouldn't it be 
consistent to do the same with mutt+gpgme?

--------------------------------------------------------------------------------
2016-12-23 18:08:30 UTC cooler
* Added attachment mutt-1.7.2-gpgme-add-carriagereturn-before-encryption.patch

--------------------------------------------------------------------------------
2016-12-31 05:02:24 UTC kevin8t8
* Added comment:
Thanks for the bug report and patch.  I haven't spent much time with the s/mime gpgme backend, so I've been taking a closer look at it.

I'm not fully clear on what the "right thing" is either, but the openssl binary does appear to be converting to cr/lf when encrypting.  So it seems safe to make this change for the gpgme backend too.

I'll just test this a bit tomorrow and if it looks good, push it.  Thanks again.

--------------------------------------------------------------------------------
2017-01-01 04:02:04 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [changeset:"2bc2ec9ac664d7c17946097c5d50cbe42f477fb4" 6896:2bc2ec9ac664]:
{{{
#!CommitTicketReference repository="" revision="2bc2ec9ac664d7c17946097c5d50cbe42f477fb4"
Canonicalize line endings for GPGME S/MIME encryption. (closes #3904)

This matches the behavior for S/MIME classic mode: OpenSSL converts
the line endings to cr/lf before encrypting.  Although Mutt always
canonicalizes the line endings before verifying the signature, some
clients do not do this for encrypted messages.

Thanks to cooler for the patch!
}}}

* resolution changed to fixed
* status changed to closed
