Ticket:  3149
Status:  closed
Summary: Missing #undef's in mbyte.h leads to bad compilation with --without-wc-funcs

Reporter: vinc17
Owner:    mutt-dev

Opened:       2009-01-06 12:15:25 UTC
Last Updated: 2009-03-09 11:05:16 UTC

Priority:  major
Component: build
Keywords:  patch

--------------------------------------------------------------------------------
Description:
Mutt 1.5.19 hangs under Mac OS X 10.4.11 with the combination of configure options --with-regex --without-wc-funcs (as the Mac OS X wc functions are buggy) and //TRANSLIT in $charset.

I've built Mutt with:
{{{
$ ./configure --with-mailpath=/Users/vinc17/Mail \
    --enable-debug --with-regex --without-wc-funcs
$ make mutt
}}}

When running Mutt under gdb and typing Ctrl-\ to interrupt Mutt, I get:
{{{
Program received signal SIGQUIT, Quit.
__tolower (wc=65) at mbyte.c:359
359         return (0 <= wc && wc < 256) ? tolower (wc) : wc;
(gdb) bt
#0  __tolower (wc=65) at mbyte.c:359
#1  0x000874b4 in regcomp (preg=0x505ad0, pattern=0x8b450 "^[^,]*", cflags=3) at regex.c:5585
#2  0x00024edc in mutt_restore_default (p=0x99cb8) at init.c:1608
#3  0x0002a728 in mutt_init (skip_sys_rc=0, commands=0x0) at init.c:3029
#4  0x00036d74 in main (argc=5, argv=0xbfffe178) at main.c:738
}}}

--------------------------------------------------------------------------------
2009-01-06 12:18:32 UTC vinc17
* Added comment:
I forgot an example of muttrc file to reproduce the bug:
{{{
set charset=ISO-8859-1//TRANSLIT
color index yellow black ~s.
}}}

--------------------------------------------------------------------------------
2009-01-06 12:28:53 UTC vinc17
* Added comment:
Output of "sample <pid> 10 10":
{{{
Analysis of sampling pid 12881 every 10.000000 milliseconds
Call graph:
    1001 Thread_1007
      1001 start
        1001 _start
          1001 main
            1001 mutt_init
              1001 mutt_restore_default
                1001 regcomp
                  1001 __tolower
                    1001 __tolower

Total number in stack (recursive counted multiple, when >=5):

Sort by top of stack, same collapsed (when >= 5):
        __tolower        1001
}}}

--------------------------------------------------------------------------------
2009-01-06 13:58:13 UTC vinc17
* Added comment:
I can see with gcc -E that

wint_t towlower (wint_t wc)

in mbyte.c gets compiled as

wint_t __tolower(wint_t wc)

This is incorrect. Hence strange behavior. mbyte.h misses more #undef's.

* summary changed to Missing #undef's in mbyte.h leads to bad compilation with --without-wc-funcs

--------------------------------------------------------------------------------
2009-01-06 13:59:03 UTC vinc17
* Added attachment mbyte.diff
* Added comment:
patch adding missing #undef's

--------------------------------------------------------------------------------
2009-01-06 20:45:05 UTC brendan
* component changed to charset
* milestone changed to 1.6

--------------------------------------------------------------------------------
2009-01-06 20:46:15 UTC brendan
* component changed to build

--------------------------------------------------------------------------------
2009-01-16 13:46:09 UTC pdmef
* keywords changed to patch

--------------------------------------------------------------------------------
2009-02-20 13:20:25 UTC pdmef
* Added comment:
Just for the record: I can't even get it compile on 10.5.6 since wchar.h from mutt.h includes _wctype.h defining these functions as inline.

--------------------------------------------------------------------------------
2009-02-20 13:48:09 UTC pdmef
* Added attachment 3149.diff

--------------------------------------------------------------------------------
2009-02-20 13:51:22 UTC pdmef
* Added comment:
Attached a patch which defines _DONT_USE_CTYPE_INLINE_ to make _wctype.h not inline the functions to unbreak compilation. This is also defined in regex.c. Furthermore it changes the inclusion logic for wchar.h and wctype.h to the one mutt.h uses.

I think the regex.c change is unrelated to this ticket and should be committed anyway. I think we also need the mbyte.c change.

Can you please retry with that and your patch attached? I don't have any problem with these on 10.5.6.

--------------------------------------------------------------------------------
2009-02-21 01:52:18 UTC vinc17
* Added comment:
Mutt doesn't hang either with both patches.

--------------------------------------------------------------------------------
2009-03-09 11:05:14 UTC Rocco Rutte <pdmef@gmx.net>
* Added comment:
(In [f3a33b77dc90]) Unbreak compilation with --without-wc-funcs on OS X 10.5.*, see #3149.

--------------------------------------------------------------------------------
2009-03-09 11:05:16 UTC Vincent Lefevre <vincent@vinc17.org>
* Added comment:
(In [4ce562b7f5d7]) Unbreak compilation on OS X with --with-regex/--without-wc-funcs. Closes #3149.

* resolution changed to fixed
* status changed to closed
