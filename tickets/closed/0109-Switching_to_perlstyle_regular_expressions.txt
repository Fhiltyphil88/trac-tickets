Ticket:  109
Status:  closed
Summary: Switching to perl-style regular expressions?

Reporter: Andreas Metzler <ametzler@logic.univie.ac.at>
Owner:    mutt-dev

Opened:       2000-05-09 08:38:53 UTC
Last Updated: 2006-09-21 22:57:39 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.0.1
Severity: wishlist

Hello!
Wouldn't it be nice to switch to Perl-style regular expressions, using
libpcre2 (Philip Hazel's Perl Compatible Regular Expression library)?
         cu andreas

>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2005-08-07 14:20:10 UTC brendan
* Added comment:
{{{
change-request
}}}

--------------------------------------------------------------------------------
2006-09-22 16:57:39 UTC paul
* Added comment:
{{{
It's been 6 years - given the resistance to just renaming
the variables, I don't think changing the regexps used is
ever going to happen. Submitter or maintainers can re-open
if they disagree with me...
}}}

* resolution changed to fixed
* status changed to closed
