Ticket:  2545
Status:  closed
Summary: Cannot open message with pgp key attachment

Reporter: lhecking@users.sourceforge.net
Owner:    mutt-dev

Opened:       2006-11-02 17:07:55 UTC
Last Updated: 2008-07-14 02:17:27 UTC

Priority:  major
Component: crypto
Keywords:  

--------------------------------------------------------------------------------
Description:
Trying to open a message with a PGP key attachment yields
"Could not copy message".
I can open both message parts separately from the
Attachments menu.
{{{
  I     1 <no description>                   [text/plain, 7bit, us-ascii, 0.2K]
  A     2 xyzfoo.key                         [applica/pgp-keys, base64, 1.5K]
}}}
I reported this on July 17th for 1.5.12 on mutt-dev, but it
must have slipped through the cracks ...
>How-To-Repeat:
Export a key from the key ring into a file, attach it to an
email. Send to yourself (or postpone), then open.


--------------------------------------------------------------------------------
2007-04-01 22:43:55 UTC brendan
* component changed to crypto
* Updated description:
Trying to open a message with a PGP key attachment yields
"Could not copy message".
I can open both message parts separately from the
Attachments menu.
{{{
  I     1 <no description>                   [text/plain, 7bit, us-ascii, 0.2K]
  A     2 xyzfoo.key                         [applica/pgp-keys, base64, 1.5K]
}}}
I reported this on July 17th for 1.5.12 on mutt-dev, but it
must have slipped through the cracks ...
>How-To-Repeat:
Export a key from the key ring into a file, attach it to an
email. Send to yourself (or postpone), then open.
* milestone changed to 1.6

--------------------------------------------------------------------------------
2007-04-01 22:46:56 UTC brendan
* Added comment:
I can't reproduce this here with tip. Can you?

--------------------------------------------------------------------------------
2007-04-30 00:03:55 UTC brendan
* Added comment:
Cannot reproduce; got no followup.

* resolution changed to worksforme
* status changed to closed

--------------------------------------------------------------------------------
2007-04-30 09:20:19 UTC Lars Hecking
* Added comment:
{{{
Mutt writes:
> #2545: Cannot open message with pgp key attachment
> 
> Changes (by brendan):
> 
>   * status:  new => closed
>   * resolution:  => worksforme
> 
> Comment:
> 
>  Cannot reproduce; got no followup.

 Sorry, must have gotten lost in the volume of mutt-dev emails.

 This is still broken as of 1.5.15. Is there something like a nightly snapshot
 I can download? I haven't really worked with mercurial yet.

 Btw, is group-reply broken in newer mutts? I group-replied to this message

| Sender: fleas@mutt.org
| X-Trac-Version: 0.10.3
| Cc: mutt-dev@mutt.org
| X-Mailer: Trac 0.10.3, by Edgewall Software
| To: lhecking@users.sourceforge.net, brendan@kublai.com
| X-Trac-Project: Mutt
| Date: Mon, 30 Apr 2007 00:03:55 -0000

 but only got 

|    To: fleas@mutt.org

 and re-added the other addresses manually.
}}}

--------------------------------------------------------------------------------
2007-04-30 14:23:52 UTC Lars Hecking
* Added comment:
{{{
Lars Hecking writes:
> Mutt writes:
> > #2545: Cannot open message with pgp key attachment
> > 
> > Changes (by brendan):
> > 
> >   * status:  new => closed
> >   * resolution:  => worksforme
> > 
> > Comment:
> > 
> >  Cannot reproduce; got no followup.
> 
>  Sorry, must have gotten lost in the volume of mutt-dev emails.
> 
>  This is still broken as of 1.5.15. Is there something like a nightly snapshot
>  I can download? I haven't really worked with mercurial yet.
 
 Thanks, Rado.

 Yes, this is still broken in tip.

 I can add application/pgp-keys to auto_view to make this go away, but it
 doesn't really help:

[-- application/pgp-keys is unsupported  --]
}}}

--------------------------------------------------------------------------------
2007-04-30 15:35:42 UTC brendan
* resolution changed to 
* status changed to reopened

--------------------------------------------------------------------------------
2008-05-18 04:02:53 UTC brendan
* Added comment:
I think I've figured out how to reproduce this: export the key without ascii armoring. I believe that's illegal according to rfc 3156, but the error handling is no good. See also #2912.

--------------------------------------------------------------------------------
2008-05-18 16:34:23 UTC Derek Martin
* Added comment:
{{{
> I think I've figured out how to reproduce this: export the key
> without ascii armoring. I believe that's illegal according to rfc
> 3156, but the error handling is no good.

Here I feel compeled to point out a couple of things:

1. Jon Postel's "Robustness Principle": be conservative in what you
   do, be liberal in what you accept from others.

2. RFC 3156 is listed as a *PROPOSED* standard as of May 2008,
   according to:

      http://rfc.net/std1.html#s3.1.
   
   It is not (yet) an official standard.

3. This RFC (#3156) states: 

      A MIME body part of the content type "application/pgp-keys"
      contains ASCII-armored transferable Public Key Packets as
      defined in [1], section 10.1.

      [...]

      References

	 [1]   Callas, J., Donnerhacke, L., Finney, H. and R. Thayer,
	 "OpenPGP Message Format", RFC 2440, November 1998.

   However, RFC 2440 makes no mention that the Public Key Packets
   must, or even should be ASCII-armored.

4. RFC 2440 is also not an official standard.  In fact, STD1 doesn't
   even list it as a proposed standard, nor does the RFC list that it
   is obsoleted by a superceding RFC.
      

RFCs are guidelines, not laws.  Even if the proposed standards were
official, all software should obey the robustness principle, wherever
it is reasonable to do so.  Is it reasonable to assume that some MUAs
were written to comply with RFC 2440, and attach non-ASCII-armored key
data, given that the RFC does not madate it be so?  Seems reasonable
to me...  Mutt should accept key data in this form.
}}}

--------------------------------------------------------------------------------
2008-06-26 05:43:53 UTC brendan
* Added comment:
(In [e2780a423d96]) Make mutt_copy_message distinguish between fatal and non-fatal errors.
Non-fatal errors should prevent moving messages, since they indicate
data loss. But mutt should still attempt to display them, since being
able to see some attachments is better than nothing.
Also stop printing out non-PGP material in application/pgp
attachments. Closes #2545, #2912.

* resolution changed to fixed
* status changed to closed
