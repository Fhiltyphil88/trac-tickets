Ticket:  3777
Status:  closed
Summary: S/MIME clear w/o effect when enabling PGP/MIME

Reporter: btuue
Owner:    kevin8t8

Opened:       2015-09-08 14:22:49 UTC
Last Updated: 2015-10-01 07:43:15 UTC

Priority:  trivial
Component: crypto
Keywords:  

--------------------------------------------------------------------------------
Description:
Recently, I observed an unexpected behavior while changing the crypto from S/MIME to PGP/MIME.


To reproduce:

Write a mail, save it and get to the compose screen. As I use 
{{{
set crypt_autosign = yes
}}}
the mail is set to be signed via S/MIME.

{{{
Security: Sign (S/MIME)
 sign as: a12345678.0
}}}

To make it plain-text, hit S (smime-menu) and c (clear):
{{{
Security: None
}}}

Then when hitting P (pgp-menu) mutt asks:
{{{
S/MIME already selected. Clear & continue ?  ([yes]/no):
}}}

Hmm, that's wrong here, because the mail is intentionally chosen to be plain-text already.

My mutt version is 
{{{
Mutt 1.5.24+2 (2f0d516fc638) (2015-08-30)
}}}


I chose priority to be trivial, because it is just a weird behavior and can be overstepped by saying 'yes' to clear S/MIME again.




--------------------------------------------------------------------------------
2015-09-09 01:14:42 UTC kevin8t8
* Added comment:
This was a side-effect from the oppenc patch series ((c)lear only clears out the encrypt and sign flags, not the S/MIME mode flag).

However, the transition between S/MIME and PGP can definitely be handled better.  There's no need to prompt if encrypt, sign, and oppenc aren't set.

* owner changed to kevin8t8
* status changed to assigned

--------------------------------------------------------------------------------
2015-10-01 07:43:15 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [9de2f1c6da87c8bbff07e11177697072e9bf5533]:
{{{
#!CommitTicketReference repository="" revision="9de2f1c6da87c8bbff07e11177697072e9bf5533"
Improve prompt when switching between PGP and S/MIME. (closes #3777)

Only prompt when encrypt or sign is enabled.

Also, improve oppenc to run and refresh the status when switching.
}}}

* resolution changed to fixed
* status changed to closed
