Ticket:  700
Status:  closed
Summary: Incorrect bouncing of messages with two From_ headers

Reporter: Vsevolod Volkov <vvv@colocall.net>
Owner:    me

Opened:       2001-07-20 11:37:25 UTC
Last Updated: 2013-01-15 03:44:46 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: /usr/local/mutt/bin/mutt
Severity: normal

-- Please type your report below this line

mutt does not removes second From_ header when bouncing. This
causes sendmail to treat header of source message as body.
Example. Source header was:

From mail-troubles@colocall.net  Wed Jul 11 18:36:56 2001
>From nobody  Wed Jul 11 18:36:56 2001
Return-Path: <mail-troubles@colocall.net>
.......

After bouncing we have:

From vvv  Thu Jul 19 17:34:42 2001
Return-Path: <vvv@colocall.net>
Received: (from vvv@localhost)
        by colocall.net (8.11.1/8.11.1) id f6JEYgc72612
        for vvv@colocall.net; Thu, 19 Jul 2001 17:34:42 +0300 (EEST)
        (envelope-from vvv)
Date: Thu, 19 Jul 2001 17:34:42 +0300 (EEST)
From: Vsevolod Volkov <vvv@colocall.net>
Message-Id: <200107191434.f6JEYgc72612@colocall.net>

>From nobody  Wed Jul 11 18:36:56 2001
Return-Path: <mail-troubles@colocall.net>
.......

To fix this, mutt should remove >From_ while copying header before
bouncing.

-- Mutt Version Information

Use /usr/local/mutt/bin/mutt


>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2001-08-09 05:36:59 UTC Vsevolod Volkov <vvv@mutt.org.ua>
* Added comment:
{{{
On Thu, Jul 19, 2001 at 05:37:25PM +0300, Vsevolod Volkov wrote:
VV> Package: mutt
VV> Version: /usr/local/mutt/bin/mutt
VV> Severity: normal

VV> -- Please type your report below this line

VV> mutt does not removes second From_ header when bouncing. This
VV> causes sendmail to treat header of source message as body.
VV> Example. Source header was:

VV> From nobody  Wed Jul 11 18:36:56 2001
VV> Return-Path: <mail-troubles@colocall.net>
VV> .......

VV> After bouncing we have:

VV> From vvv  Thu Jul 19 17:34:42 2001
VV> Return-Path: <vvv@colocall.net>
VV> Received: (from vvv@localhost)
VV>         by colocall.net (8.11.1/8.11.1) id f6JEYgc72612
VV>         for vvv@colocall.net; Thu, 19 Jul 2001 17:34:42 +0300 (EEST)
VV>         (envelope-from vvv)
VV> Date: Thu, 19 Jul 2001 17:34:42 +0300 (EEST)
VV> From: Vsevolod Volkov <vvv@colocall.net>
VV> Message-Id: <200107191434.f6JEYgc72612@colocall.net>

VV> >From nobody  Wed Jul 11 18:36:56 2001
VV> Return-Path: <mail-troubles@colocall.net>
VV> .......

VV> To fix this, mutt should remove >From_ while copying header before
VV> bouncing.

Here is additional information from man procmail:

=== cut: man procmail ===
      When in explicit delivery mode, procmail will  generate  a
      leading  `From  '  line if none is present.  If one is al-
      ready present procmail will leave it intact.  If  procmail
      is  not invoked with one of the following user or group ids
      : root, daemon, uucp, mail, x400,  network,  list,  slist,
      lists,  news, majordom or majordomo, but still has to gen-
      erate or accept a new `From ' line, it  will  generate  an
      additional `>From ' line to help distinguish fake mails.
=== end cut ===

-- 
Vsevolod Volkov (VV-RIPE)
mailto:vvv@mutt.org.ua
http://mutt.org.ua/download
}}}

--------------------------------------------------------------------------------
2005-10-29 13:59:08 UTC ab
* Added comment:
{{{
deduppe despam unform
}}}

--------------------------------------------------------------------------------
2010-08-08 22:27:39 UTC me
* Updated description:
{{{
Package: mutt
Version: /usr/local/mutt/bin/mutt
Severity: normal

-- Please type your report below this line

mutt does not removes second From_ header when bouncing. This
causes sendmail to treat header of source message as body.
Example. Source header was:

From mail-troubles@colocall.net  Wed Jul 11 18:36:56 2001
>From nobody  Wed Jul 11 18:36:56 2001
Return-Path: <mail-troubles@colocall.net>
.......

After bouncing we have:

From vvv  Thu Jul 19 17:34:42 2001
Return-Path: <vvv@colocall.net>
Received: (from vvv@localhost)
        by colocall.net (8.11.1/8.11.1) id f6JEYgc72612
        for vvv@colocall.net; Thu, 19 Jul 2001 17:34:42 +0300 (EEST)
        (envelope-from vvv)
Date: Thu, 19 Jul 2001 17:34:42 +0300 (EEST)
From: Vsevolod Volkov <vvv@colocall.net>
Message-Id: <200107191434.f6JEYgc72612@colocall.net>

>From nobody  Wed Jul 11 18:36:56 2001
Return-Path: <mail-troubles@colocall.net>
.......

To fix this, mutt should remove >From_ while copying header before
bouncing.

-- Mutt Version Information

Use /usr/local/mutt/bin/mutt


>How-To-Repeat:
>Fix:
}}}
* milestone changed to 1.6
* owner changed to me
* status changed to accepted

--------------------------------------------------------------------------------
2013-01-15 03:44:46 UTC me
* Added comment:
This was fixed by [fff9534bed5b].

* Updated description:
{{{
Package: mutt
Version: /usr/local/mutt/bin/mutt
Severity: normal

-- Please type your report below this line

mutt does not removes second From_ header when bouncing. This
causes sendmail to treat header of source message as body.
Example. Source header was:

From mail-troubles@colocall.net  Wed Jul 11 18:36:56 2001
>From nobody  Wed Jul 11 18:36:56 2001
Return-Path: <mail-troubles@colocall.net>
.......

After bouncing we have:

From vvv  Thu Jul 19 17:34:42 2001
Return-Path: <vvv@colocall.net>
Received: (from vvv@localhost)
        by colocall.net (8.11.1/8.11.1) id f6JEYgc72612
        for vvv@colocall.net; Thu, 19 Jul 2001 17:34:42 +0300 (EEST)
        (envelope-from vvv)
Date: Thu, 19 Jul 2001 17:34:42 +0300 (EEST)
From: Vsevolod Volkov <vvv@colocall.net>
Message-Id: <200107191434.f6JEYgc72612@colocall.net>

>From nobody  Wed Jul 11 18:36:56 2001
Return-Path: <mail-troubles@colocall.net>
.......

To fix this, mutt should remove >From_ while copying header before
bouncing.

-- Mutt Version Information

Use /usr/local/mutt/bin/mutt


>How-To-Repeat:
>Fix:
}}}
* resolution changed to fixed
* status changed to closed
