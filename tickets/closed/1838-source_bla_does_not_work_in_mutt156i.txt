Ticket:  1838
Status:  closed
Summary: source "~/bla|" does not work in mutt-1.5.6i

Reporter: "Jakob, Michael" <Michael.Jakob@epcos.com>
Owner:    mutt-dev

Opened:       2004-03-26 02:30:21 UTC
Last Updated: 2005-07-26 07:37:06 UTC

Priority:  minor
Component: mutt
Keywords:  fixed patch

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.5.6i
Severity: normal

-- Please type your report below this line
Reading configuration commands from a pipe does no longer work in mutt-1.5.6.
In function source_rc() (line 1361, init.c), a stat() test has been added that
should be skipped if the file is a pipe:

--- orig/mutt-1.5.6/init.c	Sun Feb  1 19:21:00 2004
+++ mutt-1.5.6/init.c	Thu Mar 25 09:22:53 2004
@@ -1369,16 +1369,20 @@
   size_t buflen;
   pid_t pid;
   struct stat s;
+  int len = mutt_strlen (rcfile);
 
-  if (stat (rcfile, &s) < 0)
+  if (rcfile[len-1] != '|')
   {
-    snprintf (err->data, err->dsize, _("%s: stat: %s"), rcfile, strerror (errno));
-    return (-1);
-  }
-  if (!S_ISREG (s.st_mode))
-  {
-    snprintf (err->data, err->dsize, _("%s: not a regular file"), rcfile);
-    return (-1);
+    if (stat (rcfile, &s) < 0)
+    {
+      snprintf (err->data, err->dsize, _("%s: stat: %s"), rcfile, strerror (errno));
+      return (-1);
+    }
+    if (!S_ISREG (s.st_mode))
+    {
+      snprintf (err->data, err->dsize, _("%s: not a regular file"), rcfile);
+      return (-1);
+    }
   }
 
   if ((f = mutt_open_read (rcfile, &pid)) == NULL)

-- System Information
System Version: Linux ophelia 2.4.18-64GB-SMP #1 SMP Mon Mar 24 14:37:49 GMT 2003 i686 unknown
SuSE Release: SuSE Linux 7.3 (i386)

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/lib/gcc-lib/i486-suse-linux/2.95.3/specs
gcc version 2.95.3 20010315 (SuSE)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.5.6i (2004-02-01)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.18-64GB-SMP (i686) [using ncurses 5.2] [using libiconv 1.7]
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_GSS  -USE_SSL  -USE_SASL  -USE_SASL2  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  -HAVE_LIBIDN  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/usr/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.


Received: (at submit) by bugs.guug.de; 20 May 2004 21:55:38 +0000
From juergen.salk@highx.de Thu May 20 23:55:36 2004
Received: from mailout07.sul.t-online.com ([194.25.134.83])
	by trithemius.gnupg.org with esmtp (Exim 3.35 #1 (Debian))
	id 1BQvVo-0006rE-00
	for <submit@bugs.guug.de>; Thu, 20 May 2004 23:55:36 +0200
Received: from sfwd01.aul.t-online.de 
	by mailout07.sul.t-online.com with smtp 
	id 1BQvYm-0003HD-00; Thu, 20 May 2004 23:58:40 +0200
Received: from charlotte.highx.de (bNQtWEZ-8eRV73i8bS+ZDMlpDO7AGkJHZpwSqmDif-snWH3vGsfhQ0@[80.145.94.10]) by afwd01.sul.t-online.com
	with esmtp id 1BQvYd-1OGxLU0; Thu, 20 May 2004 23:58:31 +0200
Received: by charlotte.highx.de (Postfix, from userid 500)
	id B9D5473ACB; Thu, 20 May 2004 23:55:08 +0200 (CEST)
From: juergen.salk@gmx.de
Subject: mutt-1.5.6i: Bug in source_rc()?
To: submit@bugs.guug.de
Message-Id: <20040520215508.B9D5473ACB@charlotte.highx.de>
Date: Thu, 20 May 2004 23:55:08 +0200 (CEST)
X-ID: bNQtWEZ-8eRV73i8bS+ZDMlpDO7AGkJHZpwSqmDif-snWH3vGsfhQ0
X-Spam-Status: No, hits=-4.1 required=4.0
	tests=BAYES_10,NO_REAL_NAME,PATCH_UNIFIED_DIFF
	version=2.55
X-Spam-Level: 
X-Spam-Checker-Version: SpamAssassin 2.55 (1.174.2.19-2003-05-19-exp)

Package: mutt
Version: 1.5.6i
Severity: important

-- Please type your report below this line

Hi,

i can't seem to source input from an external command in mutt-1.5.6.

I have something like

  source "~/.mutt/scripts/chkmbox|"

in my ~/.muttrc and mutt complains that "~/.mutt/scripts/chkmbox|"
does not exist. This is because stat(rcfile, &s) is called
in source_rc() without stripping the trailing pipe sign.

For what it's worth, here is a patch that seemed to fix the problem
for me.

--- init.c.orig 2004-05-20 16:25:26.581558456 +0200
+++ init.c      2004-05-20 17:13:02.064459072 +0200
@@ -1369,17 +1369,30 @@
   size_t buflen;
   pid_t pid;
   struct stat s;
+  char *rcstat = NULL;
+  int len = mutt_strlen(rcfile);

-  if (stat (rcfile, &s) < 0)
+  rcstat = safe_strdup (rcfile);
+
+  if (rcstat[len - 1] == '|')
+  {
+    /* strip last character, if it is a pipe (|) */
+    rcstat[len - 1] = 0;
+  }
+
+  if (stat (rcstat, &s) < 0)
   {
-    snprintf (err->data, err->dsize, _("%s: stat: %s"), rcfile, strerror (errno));
+    snprintf (err->data, err->dsize, _("%s: stat: %s"), rcstat, strerror (errno));
+    FREE(&rcstat);
     return (-1);
   }
   if (!S_ISREG (s.st_mode))
   {
-    snprintf (err->data, err->dsize, _("%s: not a regular file"), rcfile);
+    snprintf (err->data, err->dsize, _("%s: not a regular file"), rcstat);
+    FREE(&rcstat);
     return (-1);
   }
+  FREE(&rcstat);

   if ((f = mutt_open_read (rcfile, &pid)) == NULL)
   {


Regards - Juergen



-- System Information
System Version: Linux charlotte 2.6.4-54.5-default #1 Fri May 7 21:43:10 UTC 2004 i686 i686 i386 GNU/Linux
SuSE Release: SuSE Linux 9.1 (i586)

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/lib/gcc-lib/i586-suse-linux/3.3.3/specs
Configured with: ../configure --enable-threads=posix --prefix=/usr --with-local-prefix=/usr/local --infodir=/usr/share/info --mandir=/usr/share/man --enable-languages=c,c++,f77,objc,java,ada --disable-checking --libdir=/usr/lib --enable-libgcj --with-gxx-include-dir=/usr/include/g++ --with-slibdir=/lib --with-system-zlib --enable-shared --enable-__cxa_atexit i586-suse-linux
Thread model: posix
gcc version 3.3.3 (SuSE Linux)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.5.6i (2004-02-01)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.6.4-54.5-default (i686) [using ncurses 5.4]
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
-USE_POP  -USE_IMAP  -USE_GSS  -USE_SSL  -USE_SASL  -USE_SASL2  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +COMPRESSED  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  -HAVE_LIBIDN  +HAVE_GETSID  -HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/local/mutt-1.5.6/share/mutt"
SYSCONFDIR="/usr/local/mutt-1.5.6/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.

patch-1.5.6.rr.compressed.1


>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2004-03-26 02:30:21 UTC "Jakob, Michael" <Michael.Jakob@epcos.com>
* Added comment:
{{{
Package: mutt
Version: 1.5.6i
Severity: normal

-- Please type your report below this line
Reading configuration commands from a pipe does no longer work in mutt-1.5.6.
In function source_rc() (line 1361, init.c), a stat() test has been added that
should be skipped if the file is a pipe:

--- orig/mutt-1.5.6/init.c	Sun Feb  1 19:21:00 2004
+++ mutt-1.5.6/init.c	Thu Mar 25 09:22:53 2004
@@ -1369,16 +1369,20 @@
  size_t buflen;
  pid_t pid;
  struct stat s;
+  int len = mutt_strlen (rcfile);

-  if (stat (rcfile, &s) < 0)
+  if (rcfile[len-1] != '|')
  {
-    snprintf (err->data, err->dsize, _("%s: stat: %s"), rcfile, strerror (errno));
-    return (-1);
-  }
-  if (!S_ISREG (s.st_mode))
-  {
-    snprintf (err->data, err->dsize, _("%s: not a regular file"), rcfile);
-    return (-1);
+    if (stat (rcfile, &s) < 0)
+    {
+      snprintf (err->data, err->dsize, _("%s: stat: %s"), rcfile, strerror (errno));
+      return (-1);
+    }
+    if (!S_ISREG (s.st_mode))
+    {
+      snprintf (err->data, err->dsize, _("%s: not a regular file"), rcfile);
+      return (-1);
+    }
  }

  if ((f = mutt_open_read (rcfile, &pid)) == NULL)

-- System Information
System Version: Linux ophelia 2.4.18-64GB-SMP #1 SMP Mon Mar 24 14:37:49 GMT 2003 i686 unknown
SuSE Release: SuSE Linux 7.3 (i386)

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/lib/gcc-lib/i486-suse-linux/2.95.3/specs
gcc version 2.95.3 20010315 (SuSE)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.5.6i (2004-02-01)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.18-64GB-SMP (i686) [using ncurses 5.2] [using libiconv 1.7]
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_GSS  -USE_SSL  -USE_SASL  -USE_SASL2  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  -HAVE_LIBIDN  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/usr/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.
}}}

--------------------------------------------------------------------------------
2004-04-07 03:03:36 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
# no followups
close 1460
# not an upstream bug
close 1830
tags 1229 stable
merge 1235 1708 1815
tags 1652 patch
tags 1834 patch
tags 1838 patch
tags 1845 patch
}}}

--------------------------------------------------------------------------------
2004-05-21 10:43:45 UTC David Yitzchak Cohen <lists+mutt_bugs_submit@bigfatdave.com>
* Added comment:
{{{
On Thu, May 20, 2004 at 05:55:08PM EDT, juergen.salk@gmx.de wrote:

> Hi,
> 
> i can't seem to source input from an external command in mutt-1.5.6.
> 
> I have something like
> 
>   source "~/.mutt/scripts/chkmbox|"
> 
> in my ~/.muttrc and mutt complains that "~/.mutt/scripts/chkmbox|"
> does not exist.

The bug was fixed in CVS only a few days after 1.5.6i was released.
Either update to CVS, or get my local Mutt version [1] (up-to-date CVS
with two patches - 1 for updating es.po and 1 for undoing locale in
muttbug to make life good for devs who have to read your bug reports
and don't know Greek or Russian or whatever; full diff of my version
against CVS available [2]).

> This is because stat(rcfile, &s) is called
> in source_rc() without stripping the trailing pipe sign.

Well, that's not really a very good way of fixing it, since the only
interpreter that knows for sure whether a particular command is valid
or not is the shell itself.  In other words, no stat(2)ing should be
done at all if it's a command.  The patch in CVS does just that.

> For what it's worth, here is a patch that seemed to fix the problem
> for me.

Well, it fixes your particular case, but leaves many other cases broken.
Just get CVS and be happy :-)

> --- init.c.orig 2004-05-20 16:25:26.581558456 +0200
> +++ init.c      2004-05-20 17:13:02.064459072 +0200
> @@ -1369,17 +1369,30 @@

> -  if (stat (rcfile, &s) < 0)
> +  rcstat = safe_strdup (rcfile);
> +
> +  if (rcstat[len - 1] == '|')
> +  {
> +    /* strip last character, if it is a pipe (|) */
> +    rcstat[len - 1] = 0;
> +  }

Notice that you're repeating the functionality already found in
mutt_open_read() (in muttlib.c):

 if (path[len - 1] == '|')
 {
   /* read from a pipe */

   char *s = safe_strdup (path);

   s[len - 1] = 0;
   mutt_endwin (NULL);
   *thepid = mutt_create_filter (s, NULL, &f, NULL);
   FREE (&s);
 }

> +  if (stat (rcstat, &s) < 0)

>    if (!S_ISREG (s.st_mode))

These checks are better done in mutt_open_read(), which you're about to
call anyway:

>    if ((f = mutt_open_read (rcfile, &pid)) == NULL)
>    {

Thanks for taking the initiative to fix bugs, rather than simply reverting
back to 1.5.5.1i :-)

- Dave


> -- System Information
> System Version: Linux charlotte 2.6.4-54.5-default #1 Fri May 7 21:43:10 UTC 2004 i686 i686 i386 GNU/Linux
> SuSE Release: SuSE Linux 9.1 (i586)
> 
> -- Build environment information
> 
> (Note: This is the build environment installed on the system
> muttbug is run on.  Information may or may not match the environment
> used to build mutt.)
> 
> - gcc version information
> gcc
> Reading specs from /usr/lib/gcc-lib/i586-suse-linux/3.3.3/specs
> Configured with: ../configure --enable-threads=posix --prefix=/usr --with-local-prefix=/usr/local --infodir=/usr/share/info --mandir=/usr/share/man --enable-languages=c,c++,f77,objc,java,ada --disable-checking --libdir=/usr/lib --enable-libgcj --with-gxx-include-dir=/usr/include/g++ --with-slibdir=/lib --with-system-zlib --enable-shared --enable-__cxa_atexit i586-suse-linux
> Thread model: posix
> gcc version 3.3.3 (SuSE Linux)
> 
> - CFLAGS
> -Wall -pedantic -g -O2
> 
> -- Mutt Version Information
> 
> Mutt 1.5.6i (2004-02-01)
> Copyright (C) 1996-2002 Michael R. Elkins and others.
> Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
> Mutt is free software, and you are welcome to redistribute it
> under certain conditions; type `mutt -vv' for details.
> 
> System: Linux 2.6.4-54.5-default (i686) [using ncurses 5.4]
> Compile options:
> -DOMAIN
> -DEBUG
> -HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  
> +USE_FCNTL  -USE_FLOCK
> -USE_POP  -USE_IMAP  -USE_GSS  -USE_SSL  -USE_SASL  -USE_SASL2  
> +HAVE_REGCOMP  -USE_GNU_REGEX  
> +HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
> +HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
> +CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
> +ENABLE_NLS  -LOCALES_HACK  +COMPRESSED  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
> +HAVE_ICONV  -ICONV_NONTRANS  -HAVE_LIBIDN  +HAVE_GETSID  -HAVE_GETADDRINFO  
> ISPELL="/usr/bin/ispell"
> SENDMAIL="/usr/sbin/sendmail"
> MAILPATH="/var/mail"
> PKGDATADIR="/usr/local/mutt-1.5.6/share/mutt"
> SYSCONFDIR="/usr/local/mutt-1.5.6/etc"
> EXECSHELL="/bin/sh"
> -MIXMASTER
> To contact the developers, please mail to <mutt-dev@mutt.org>.
> To report a bug, please use the flea(1) utility.
> 
> patch-1.5.6.rr.compressed.1

-- 
Uncle Cosmo, why do they call this a word processor?
It's simple, Skyler.  You've seen what food processors do to food, right?

Please visit this link:
http://rotter.net/israel
}}}

--------------------------------------------------------------------------------
2004-05-21 16:55:08 UTC juergen.salk@gmx.de
* Added comment:
{{{
Package: mutt
Version: 1.5.6i
Severity: important

-- Please type your report below this line

Hi,

i can't seem to source input from an external command in mutt-1.5.6.

I have something like

 source "~/.mutt/scripts/chkmbox|"

in my ~/.muttrc and mutt complains that "~/.mutt/scripts/chkmbox|"
does not exist. This is because stat(rcfile, &s) is called
in source_rc() without stripping the trailing pipe sign.

For what it's worth, here is a patch that seemed to fix the problem
for me.

--- init.c.orig 2004-05-20 16:25:26.581558456 +0200
+++ init.c      2004-05-20 17:13:02.064459072 +0200
@@ -1369,17 +1369,30 @@
  size_t buflen;
  pid_t pid;
  struct stat s;
+  char *rcstat = NULL;
+  int len = mutt_strlen(rcfile);

-  if (stat (rcfile, &s) < 0)
+  rcstat = safe_strdup (rcfile);
+
+  if (rcstat[len - 1] == '|')
+  {
+    /* strip last character, if it is a pipe (|) */
+    rcstat[len - 1] = 0;
+  }
+
+  if (stat (rcstat, &s) < 0)
  {
-    snprintf (err->data, err->dsize, _("%s: stat: %s"), rcfile, strerror (errno));
+    snprintf (err->data, err->dsize, _("%s: stat: %s"), rcstat, strerror (errno));
+    FREE(&rcstat);
    return (-1);
  }
  if (!S_ISREG (s.st_mode))
  {
-    snprintf (err->data, err->dsize, _("%s: not a regular file"), rcfile);
+    snprintf (err->data, err->dsize, _("%s: not a regular file"), rcstat);
+    FREE(&rcstat);
    return (-1);
  }
+  FREE(&rcstat);

  if ((f = mutt_open_read (rcfile, &pid)) == NULL)
  {


Regards - Juergen



-- System Information
System Version: Linux charlotte 2.6.4-54.5-default #1 Fri May 7 21:43:10 UTC 2004 i686 i686 i386 GNU/Linux
SuSE Release: SuSE Linux 9.1 (i586)

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/lib/gcc-lib/i586-suse-linux/3.3.3/specs
Configured with: ../configure --enable-threads=posix --prefix=/usr --with-local-prefix=/usr/local --infodir=/usr/share/info --mandir=/usr/share/man --enable-languages=c,c++,f77,objc,java,ada --disable-checking --libdir=/usr/lib --enable-libgcj --with-gxx-include-dir=/usr/include/g++ --with-slibdir=/lib --with-system-zlib --enable-shared --enable-__cxa_atexit i586-suse-linux
Thread model: posix
gcc version 3.3.3 (SuSE Linux)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.5.6i (2004-02-01)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.6.4-54.5-default (i686) [using ncurses 5.4]
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
-USE_POP  -USE_IMAP  -USE_GSS  -USE_SSL  -USE_SASL  -USE_SASL2  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +COMPRESSED  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  -HAVE_LIBIDN  +HAVE_GETSID  -HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/local/mutt-1.5.6/share/mutt"
SYSCONFDIR="/usr/local/mutt-1.5.6/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.

patch-1.5.6.rr.compressed.1
}}}

--------------------------------------------------------------------------------
2004-05-23 16:56:32 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
# no bug but config error
close 1832
# no bug report
close 1866
# locale problem
close 1868
# bug in compat patch, not Mutt
close 1869
merge 1838 1884
tags 1838 fixed
tags 1867 patch
severity 1870 wishlist
merge 1870 1022
}}}

--------------------------------------------------------------------------------
2004-05-23 16:56:33 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
# no bug but config error
close 1832
# no bug report
close 1866
# locale problem
close 1868
# bug in compat patch, not Mutt
close 1869
merge 1838 1884
tags 1838 fixed
tags 1867 patch
severity 1870 wishlist
merge 1870 1022
}}}

--------------------------------------------------------------------------------
2004-05-23 16:56:34 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
# no bug but config error
close 1832
# no bug report
close 1866
# locale problem
close 1868
# bug in compat patch, not Mutt
close 1869
merge 1838 1884
tags 1838 fixed
tags 1867 patch
severity 1870 wishlist
merge 1870 1022
}}}

--------------------------------------------------------------------------------
2004-05-23 16:56:35 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
# no bug but config error
close 1832
# no bug report
close 1866
# locale problem
close 1868
# bug in compat patch, not Mutt
close 1869
merge 1838 1884
tags 1838 fixed
tags 1867 patch
severity 1870 wishlist
merge 1870 1022
}}}

--------------------------------------------------------------------------------
2005-07-27 01:37:06 UTC brendan
* Added comment:
{{{
Fixed in CVS.
}}}

* resolution changed to fixed
* status changed to closed
