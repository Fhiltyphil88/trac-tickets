Ticket:  3955
Status:  closed
Summary: regex checking crashes on long lines with UTF-8 locale

Reporter: thorsten
Owner:    mutt-dev

Opened:       2017-07-12 18:10:12 UTC
Last Updated: 2017-07-14 22:22:35 UTC

Priority:  major
Component: user interface
Keywords:  regex

--------------------------------------------------------------------------------
Description:
The regex
{{{
color body brightblue default "[[:space:]][^[:space:]]+\\*"
}}}
lets mutt crash when viewing the attached mail. The mail was composed with mutt and has a plain-text attachment with a very long line (with spaces and stars).

For locales ''C'' and ''en_US'', mutt does not crash, but with ''en_US.UTF-8'' it crashes:
{{{
LC_ALL=en_US.UTF-8 ./mutt -F /dev/null \
 -e 'color body brightblue default "[[:space:]][^[:space:]]+\\*"' \
 -e 'push <display-message>' -f crash.mbox > /dev/null
}}}



I can reproduce the crash with the released 1.8.3 and with todays hg tip 952e4fcf1e2b (`default` branch) compiled with
{{{
./configure --enable-debug  --with-regex && make
}}}
The backtrace is the following:
{{{
$ LC_ALL=en_US.UTF-8 gdb -q ./mutt
Reading symbols from ./mutt...done.
(gdb) r -F /dev/null \
 -e 'color body brightblue default "[[:space:]][^[:space:]]+\\*"' \
 -e 'push <display-message>' -f crash.mbox > /dev/null
Starting program: /home/thorsten/git/mutt/mutt -F /dev/null  -e 'color body brightblue default "[[:space:]][^[:space:]]+\\*"'  -e 'push <display-message>' -f crash.mbox > /dev/null
[Thread debugging using libthread_db enabled]
Using host libthread_db library "/usr/lib/libthread_db.so.1".

Program received signal SIGSEGV, Segmentation fault.
                                                    0x0000000000481815 in re_match_2_internal (bufp=bufp@entry=0x7fffffffb010, string1=<optimized out>, string1@entry=0x0, size1=size1@entry=0, 
    string2=string2@entry=0x94cf70 "(equal_c SAT (some rr (and (some rr (and (some rr (and (all rr (or (all rr (or (not p3) (or (not p2) (or (all rr (or (some rr (and p6 (and (some rr (and *BOTTOM* (and (not p3) (and p2 (and p4 (and p4 "..., size2=size2@entry=1827888, pos=pos@entry=8, regs=0x7fffffffaff0, 
    stop=1827888) at regex.c:4652
4652	          PUSH_FAILURE_POINT (p + mcnt, d, -2);
(gdb) bt
#0  0x0000000000481815 in re_match_2_internal (bufp=bufp@entry=0x7fffffffb010, string1=<optimized out>, string1@entry=0x0, size1=size1@entry=0, 
    string2=string2@entry=0x94cf70 "(equal_c SAT (some rr (and (some rr (and (some rr (and (all rr (or (all rr (or (not p3) (or (not p2) (or (all rr (or (some rr (and p6 (and (some rr (and *BOTTOM* (and (not p3) (and p2 (and p4 (and p4 "..., size2=size2@entry=1827888, pos=pos@entry=8, regs=0x7fffffffaff0, 
    stop=1827888) at regex.c:4652
#1  0x0000000000486626 in re_search_2 (bufp=bufp@entry=0x7fffffffb010, string1=string1@entry=0x0, size1=size1@entry=0, 
    string2=string2@entry=0x94cf70 "(equal_c SAT (some rr (and (some rr (and (some rr (and (all rr (or (all rr (or (not p3) (or (not p2) (or (all rr (or (some rr (and p6 (and (some rr (and *BOTTOM* (and (not p3) (and p2 (and p4 (and p4 "..., size2=size2@entry=1827888, startpos=<optimized out>, 
    startpos@entry=0, range=1827880, regs=0x7fffffffaff0, stop=1827888) at regex.c:3586
#2  0x0000000000486b16 in re_search (regs=0x7fffffffaff0, range=<optimized out>, startpos=0, size=<optimized out>, 
    string=0x94cf70 "(equal_c SAT (some rr (and (some rr (and (some rr (and (all rr (or (all rr (or (not p3) (or (not p2) (or (all rr (or (some rr (and p6 (and (some rr (and *BOTTOM* (and (not p3) (and p2 (and p4 (and p4 "..., bufp=0x7fffffffb010) at regex.c:3457
#3  regexec (preg=preg@entry=0x785e30, 
    string=string@entry=0x94cf70 "(equal_c SAT (some rr (and (some rr (and (some rr (and (all rr (or (all rr (or (not p3) (or (not p2) (or (all rr (or (some rr (and p6 (and (some rr (and *BOTTOM* (and (not p3) (and p2 (and p4 (and p4 "..., nmatch=nmatch@entry=1, pmatch=pmatch@entry=0x7fffffffb130, 
    eflags=eflags@entry=0) at regex.c:5675
#4  0x0000000000446e56 in resolve_types (q_classify=2, force_redraw=0x7fffffffb8c0, q_level=0x7fffffffb8cc, QuoteList=0x7fffffffb8d0, 
    last=<optimized out>, n=37, lineInfo=<optimized out>, raw=<optimized out>, 
    buf=0x94cf70 "(equal_c SAT (some rr (and (some rr (and (some rr (and (all rr (or (all rr (or (not p3) (or (not p2) (or (all rr (or (some rr (and p6 (and (some rr (and *BOTTOM* (and (not p3) (and p2 (and p4 (and p4 "...) at pager.c:879
#5  display_line (f=0x78cbe0, last_pos=last_pos@entry=0x7fffffffb8d8, lineInfo=lineInfo@entry=0x7fffffffb978, n=37, last=last@entry=0x7fffffffb8b4, 
    max=max@entry=0x7fffffffb8b0, flags=66, QuoteList=0x7fffffffb8d0, q_level=0x7fffffffb8cc, force_redraw=0x7fffffffb8c0, SearchRE=0x7fffffffb910, 
    pager_window=0x78d860) at pager.c:1369
#6  0x000000000044778a in pager_menu_redraw (pager_menu=0x78d880) at pager.c:1807
#7  0x00000000004481e1 in mutt_pager (banner=banner@entry=0x0, fname=fname@entry=0x7fffffffbea0 "/tmp/mutt-faui8thorsten-1000-28734-7744702671431236879", 
    flags=flags@entry=66, extra=extra@entry=0x7fffffffbe70) at pager.c:1991
#8  0x0000000000412359 in mutt_display_message (cur=0x78a9f0) at commands.c:214
#9  0x000000000041d373 in mutt_index_menu () at curs_main.c:1337
#10 0x0000000000405323 in main (argc=<optimized out>, argv=0x7fffffffd728, environ=<optimized out>) at main.c:1252
(gdb) 

}}}
You can find the `core.28734` file attached.

This bug sounds related to https://dev.mutt.org/trac/ticket/3850, however this is on archlinux and I have the configuration option --with-regex.



--------------------------------------------------------------------------------
2017-07-12 18:17:24 UTC thorsten
* Added comment:
Due to size restrictions, the files are uploaded elsewhere:
The mail/mbox: http://thorsten-wissmann.de/p/crash.mbox (1.9MB) [[BR]]
The core file: http://thorsten-wissmann.de/p/core.28734 (12MB)

Thank you for your help and your wonderful MUA :-)

--------------------------------------------------------------------------------
2017-07-12 22:33:03 UTC kevin8t8
* Added comment:
alloca() is segfaulting.  In the common configuration it's setting re_max_failures=20,000.

At least on my system, the limit before it segs looks to be about 16,000, but I don't see why we need to cut it that close.

I'm inclined to set it to 4000.  Would any other devs care to comment?

--------------------------------------------------------------------------------
2017-07-13 01:25:39 UTC kevin8t8
* Added comment:
Attaching a patch.  Perhaps 4000 is a bit too conservative, so I decreased the value to 8000.  If no one has an objection, I'll push this up in the next couple days.

--------------------------------------------------------------------------------
2017-07-13 01:26:14 UTC kevin8t8
* Added attachment ticket-3955.patch

--------------------------------------------------------------------------------
2017-07-13 03:10:15 UTC kevin8t8
* Added comment:
Oh, by the way, thank you for the high quality bug report.  A ticket with a stack trace, core file, and detailed instructions on how to reproduce is very, very appreciated!

--------------------------------------------------------------------------------
2017-07-14 22:22:35 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [changeset:"c4e507003aa0a06feff57a6b64205488822d7b5b" 7106:c4e507003aa0]:
{{{
#!CommitTicketReference repository="" revision="c4e507003aa0a06feff57a6b64205488822d7b5b"
Decrease regex failure stack limit. (closes #3955)

When using alloca(), the built-in regexp library limited the failure
stack to 20,000 entries.  This value is too large, and causes alloca()
to segfault in the example provided in the ticket.

Decrease the limit to 8000.

Thanks to Thorsten Wißmann for the excellent bug report, which made
debugging this much easier.
}}}

* resolution changed to fixed
* status changed to closed
