Ticket:  2935
Status:  closed
Summary: Occasional segfault when IMAP inbox updates

Reporter: skunk
Owner:    brendan

Opened:       2007-07-26 01:44:33 UTC
Last Updated: 2009-06-11 17:02:00 UTC

Priority:  major
Component: IMAP
Keywords:  duplicate 2902

--------------------------------------------------------------------------------
Description:
Version: mutt-20070709[[BR]]
Platform: Debian/etch on amd64

I have an IMAP inbox that is updated asynchronously on a remote server. Sometimes, when new messages come in, and Mutt has uncommitted changes to the inbox, it segfaults.

This is what it looks like in GDB:

{{{
Fetching message headers... 1899/1900
Program received signal SIGSEGV, Segmentation fault.
0x000000000044f25e in mx_update_context (ctx=0x7b15d0, new_messages=4)
    at mx.c:1566
1566          h->security = crypt_query (h->content);
(gdb) p h->content
Cannot access memory at address 0x50
(gdb)
}}}

I have two core dumps saved from these crashes, and so can return additional telemetry upon request.

--------------------------------------------------------------------------------
2007-12-07 19:24:29 UTC brendan
* Added comment:
Do you have the mutt version? What patches, if any, are applied?

* milestone changed to 1.6

--------------------------------------------------------------------------------
2008-05-26 22:17:37 UTC berni
* Added comment:
Replying to [comment:1 brendan]:

> Do you have the mutt version? What patches, if any, are applied?

I can confirm this, Ubuntu 8.04 Hardy version on i386

{{{
Mutt 1.5.17+20080114 (2008-01-14)
Copyright (C) 1996-2007 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.6.24-16-generic (i686)
ncurses: ncurses 5.6.20071124 (compiled with 5.6)
libidn: 1.1 (compiled with 1.1)
hcache backend: GDBM version 1.8.3. 10/15/2002 (built Jun 15 2006 21:19:27)
Compile options:
-DOMAIN
+DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  +USE_SMTP  -USE_GSS  -USE_SSL_OPENSSL  +USE_SSL_GNUTLS  +USE_SASL  +HAVE_GETADDRINFO
+HAVE_REGCOMP  -USE_GNU_REGEX
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME
-EXACT_ADDRESS  -SUN_ATTACHMENT
+ENABLE_NLS  -LOCALES_HACK  +COMPRESSED  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_LIBIDN  +HAVE_GETSID  +USE_HCACHE
-ISPELL
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
MIXMASTER="mixmaster"
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please visit http://bugs.mutt.org/.

patch-1.5.13.cd.ifdef.2
patch-1.5.13.cd.purge_message.3.4
patch-1.5.13.nt+ab.xtitles.4
patch-1.5.4.vk.pgp_verbose_mime
patch-1.5.6.dw.maildir-mtime.1
patch-1.5.8.hr.sensible_browser_position.3
}}}

gdb backtrace looks like this

{{{
#0  mx_update_context (ctx=0x817fd28, new_messages=3) at ../mx.c:1664
        h = (HEADER *) 0x0
        msgno = 308
#1  0x080e2f71 in imap_read_headers (idata=0x81a2580, msgbegin=306, msgend=307) at ../../imap/message.c:346
        ctx = (CONTEXT *) 0x817fd28
        buf = "FETCH 307:308 (UID FLAGS INTERNALDATE RFC822.SIZE BODY.PEEK[HEADER.FIELDS (DATE FROM SUBJECT TO CC MESSAGE-ID REFERENCES C
ONTENT-TYPE CONTENT-DESCRIPTION IN-REPLY-TO REPLY-TO LINES LIST-POST X-LABEL)]"...
        hdrreq = "BODY.PEEK[HEADER.FIELDS (DATE FROM SUBJECT TO CC MESSAGE-ID REFERENCES CONTENT-TYPE CONTENT-DESCRIPTION IN-REPLY-TO REP
LY-TO LINES LIST-POST X-LABEL)]", '\0' <repeats 14 times>, "�qη�qη�k��\034%/\b���\020)/\bPqη�k���mٷ\020l��"...
        fp = (FILE *) 0x82ee1c8
        tempfile = "/tmp/mutt-lxbsc01-1000-14756-18\000\000\000\000�8\000\000\000diɷ\000\000\000\0008\b)\bhj���L�;�\020\b ��\000\000\000\
000\t\000\000\000���\000\000\000\000p}\020\b\210j��\020\002�\000\000\000\000�\200Է1m�tj��\000\000\000\000\030k���Z�hk��<3;H\024\v\n\000\0
20\002�\001\000\000\000�j��hk��\006o�\001\000\000\000�j���k��\000\000\000\000@k��#�ηLk���k��\200\000\000\000�\n�\001Tk��"...
        msgno = 308
        h = {sid = 308, data = 0x82ee358, received = 1211839279, content_length = 646}
        status = <value optimized out>
        rc = -1209998992
        mfhrc = 307
        oldmsgcount = 306
        fetchlast = 308
        maxuid = 69032
        progress = {inc = 10, flags = 2, msg = 0xb7ac2004 <Address 0xb7ac2004 out of bounds>, pos = 307, size = 308, timestamp = 0,
  sizestr = "308", '\0' <repeats 124 times>}
        uid_validity = (unsigned int *) 0x0
        uidnext = (unsigned int *) 0x0
        evalhc = 0
#2  0x080da7be in imap_cmd_finish (idata=0x81a2580) at ../../imap/command.c:308
No locals.
#3  0x080dcab8 in imap_check_mailbox (ctx=0x817fd28, index_hint=0xbfb66d8c, force=0) at ../../imap/imap.c:1393
        idata = (IMAP_DATA *) 0x81a2580
        result = 0
}}}

Mailbox is open with Thunderbird in parallel.

Looks like a duplicate to #2985 and #2570

--------------------------------------------------------------------------------
2008-05-26 23:36:21 UTC berni
* Added comment:
okay, did some more research, I'm not a C coder by any chance but I found some weird things

first of all, imap_read_headers is called

{{{ imap_read_headers (idata=0x81a2580, msgbegin=306, msgend=307) at ../../imap/message.c:346 }}}

for msg 306-307 (so two messages). However, in the end msgcount is 308 and mx_update_context is called for three new messages

{{{ #0  mx_update_context (ctx=0x817fd28, new_messages=3) at ../mx.c:1664 }}}

the pointer to the headers for messages 306 and 307 are fine, but for 308 the address is 0x0 and a segfault occurs when accessing h->security

{{{
(gdb) p ctx->hdrs[306]
$7 = (HEADER *) 0x82ee590
(gdb) p ctx->hdrs[307]
$8 = (HEADER *) 0x8308d90
(gdb) p ctx->hdrs[308]
$9 = (HEADER *) 0x0
}}}

Could this be related to IMAP IDLE? I'm running dovecot on the server side (happens with both Dovecot 1.0 and 1.1), which is mentioned in the manpage for causing connection stalls.

I'll try with imap_idle=no now, unfortunately this bug is not easy to reproduce.

--------------------------------------------------------------------------------
2008-05-26 23:45:22 UTC berni
* cc changed to berni@birkenwald.de

--------------------------------------------------------------------------------
2008-08-28 16:50:35 UTC brendan
* status changed to assigned

--------------------------------------------------------------------------------
2008-12-10 10:01:25 UTC pdmef
* Added comment:
See traced attached to #3004.

--------------------------------------------------------------------------------
2008-12-11 09:53:39 UTC vegard
* cc changed to berni@birkenwald.de, vegard@svanberg.no
* Added comment:
*bump* :)

(ref. #3004): Let me know if you need more info.

* version changed to 1.5.18

--------------------------------------------------------------------------------
2008-12-11 10:07:48 UTC vegard
* Added comment:

{{{
Program terminated with signal 11, Segmentation fault.
#0  mutt_sort_threads (ctx=0x812ddb8, init=1) at thread.c:787
787         if (!cur->thread)
(gdb) p cur->thread
Cannot access memory at address 0x48
(gdb) p ctx->msgcount
$1 = 72852
(gdb) p ctx->hdrs[72852]
$2 = (HEADER *) 0x0
(gdb) p ctx->hdrs[72851]
$3 = (HEADER *) 0x0
(gdb) p ctx->hdrs[72850]
$4 = (HEADER *) 0x0
(gdb) p ctx->hdrs[72849]
$5 = (HEADER *) 0x0
(gdb) p ctx->hdrs[72848]
$6 = (HEADER *) 0x0
(gdb) p ctx->hdrs[72847]
$7 = (HEADER *) 0x0
(gdb) p ctx->hdrs[72846]
$8 = (HEADER *) 0xbf02d20

}}}

--------------------------------------------------------------------------------
2008-12-11 10:27:16 UTC vegard
* Added comment:
Added the following on line 787 and 882 in thread.c. No more segfaults.

{{{
if (!cur)
     continue;
}}}

--------------------------------------------------------------------------------
2008-12-11 10:56:48 UTC vegard
* Added comment:
Well, until now :) But they occur everywhere, in various files. The root of the problem seems to be that msgcount is too big. There is a mismatch between msgcount and hdrs. I don't know Mutt's code well enough to say if it's msgcount increasing or hdrs not being updated properly, or if it's a race. As a temporary fix I've inserted the above code almost everywhere I've found the cur = ctx->hdrs[i] assignment, but I think it's better to fix the source of the problem than to add these in everywhere.

Again - I'm not familiar with the code, so I'm not sure where to begin. Any takers?

--------------------------------------------------------------------------------
2009-01-04 00:44:59 UTC brendan
* keywords changed to duplicate 2902

--------------------------------------------------------------------------------
2009-01-04 01:44:21 UTC brendan
* Added comment:
hdrs[msgcount - 1] should always be valid, and hdrs[msgcount] should never be -- hdrs is indexed from 0, and msgcount is the total number of headers. For example, if you have one message in your mailbox, hdrs[0] should be set, hdrs[1] should not be, and msgcount should be 1. This particular clue is, I'm afraid, a red herring.

* status changed to started

--------------------------------------------------------------------------------
2009-01-04 04:38:07 UTC Brendan Cully <brendan@kublai.com>
* Added comment:
(In [2c7cf8d68d6d]) Ignore unexpected FETCH responses during imap_read_headers.
Thanks to Chris Li for the excellent bug report in #3041.
See #2902, #2935, #2985, #3028, #3041, #3143.
With luck, this may close them. In truth, imap_read_headers
is still a horrible kludge of duct tape and chicken wire.

--------------------------------------------------------------------------------
2009-01-04 05:09:42 UTC brendan
* Added comment:
Can you retest against tip? Thanks.

--------------------------------------------------------------------------------
2009-01-07 08:41:39 UTC vegard
* Added comment:
Working much better, but still crashing from time to time:


{{{
Program terminated with signal 11, Segmentation fault.
#0  mx_update_context (ctx=0x812d510, new_messages=2) at mx.c:1561
1561          h->security = crypt_query (h->content);
(gdb) bt
#0  mx_update_context (ctx=0x812d510, new_messages=2) at mx.c:1561
#1  0x080ce572 in imap_read_headers (idata=0x8102098, msgbegin=77677, msgend=77677) at message.c:372
#2  0x080c7631 in imap_cmd_finish (idata=0x8102098) at command.c:265
#3  0x080c9288 in imap_check_mailbox (ctx=0x812d510, index_hint=0xffdb71c0, force=0) at imap.c:1417
#4  0x08062168 in mutt_index_menu () at curs_main.c:482
#5  0x0807e73e in main (argc=Cannot access memory at address 0x4bdb8
) at main.c:1016
(gdb) p h->content
Cannot access memory at address 0x3c
(gdb) p h
$1 = (HEADER *) 0x0
(gdb) p ctx->msgcount
$2 = 77679
(gdb) p ctx->hdrs[ctx->msgcount]
$3 = (HEADER *) 0x0
(gdb) p ctx->hdrs[ctx->msgcount - 1]
$4 = (HEADER *) 0x0
(gdb) p ctx->hdrs[ctx->msgcount - 2]
$5 = (HEADER *) 0xc8fb4a8
(gdb) 

}}}

--------------------------------------------------------------------------------
2009-01-08 00:24:12 UTC brendan
* Added comment:
Rats. Can you run mutt with -d2 and send me a ~/.muttdebug0 along with the back trace for a session that crashes?

* status changed to infoneeded

--------------------------------------------------------------------------------
2009-06-02 09:07:43 UTC vegard
* Added comment:
*bump* :) 

Sent the muttdebug to you a couple of months ago (sent it directly due to privacy) -- any news? Thanks.

* status changed to assigned

--------------------------------------------------------------------------------
2009-06-02 17:19:58 UTC Brendan Cully
* Added comment:
{{{
On Tuesday, 02 June 2009 at 09:07, Mutt wrote:

Sorry, I got very busy. But I have mutt time now, and will look at it
this week.
}}}

--------------------------------------------------------------------------------
2009-06-03 14:28:39 UTC uspoerlein
* cc changed to berni@birkenwald.de, vegard@svanberg.no, uspoerlein@gmail.com
* Added comment:
FWIW, I encounter this every other day with mutt v1.5.19 and the sidebar patch. I'm connecting via IMAP and use the header cache. This never happened using Maildir directly.

It crashed just again, mutt is sitting there idle, while offlineimap is syncing state to that IMAP server. So messages were moved/renamed/etc without mutt knowing. 

Since the moved mails caused a screen refresh and mutt jumping to another mail to display, it panic'ed due to NULL pointers. Please let me know if you think this could've been fixed between 1.5.19 and tip, so I'll try a fresh checkout.


{{{
---Mutt: =INBOX [Msgs:22 New:2 Flag:3 Post:1 Inc:14 48K]---(threads/date)-------------------------------------------------------------------------------(all)---
Fetching message headers... 0/23 (0%)
Program received signal SIGSEGV, Segmentation fault.
[Switching to Thread 0x28701040 (LWP 100191)]
mx_update_context (ctx=0x28744400, new_messages=2) at mx.c:1617
1617          h->security = crypt_query (h->content);
(gdb) bt
#0  mx_update_context (ctx=0x28744400, new_messages=2) at mx.c:1617
#1  0x080d14d2 in imap_read_headers (idata=0x287fa480, msgbegin=22, msgend=22) at message.c:372
#2  0x080ca786 in imap_cmd_finish (idata=0x287fa480) at command.c:294
#3  0x080cc2f8 in imap_check_mailbox (ctx=0x28744400, index_hint=0xbfbfdde4, force=0) at imap.c:1420
#4  0x08062e3a in mutt_index_menu () at curs_main.c:485
#5  0x0807e186 in main (argc=Error accessing memory address 0x5c: Bad address.
) at main.c:1022
(gdb) f 0
#0  mx_update_context (ctx=0x28744400, new_messages=2) at mx.c:1617
1617          h->security = crypt_query (h->content);
(gdb) p *ctx
$1 = {path = 0x28788060 "imap://XXXX/INBOX", fp = 0x0, atime = 0, mtime = 0, size = 54511, vsize = 59292, pattern = 0x0, limit_pattern = 0x0,
  hdrs = 0x2890a480, last_tag = 0x0, tree = 0x28788080, id_hash = 0x0, subj_hash = 0x0, thread_hash = 0x287067d0, v2r = 0x2890a600, hdrmax = 96,
  msgcount = 24, vcount = 23, tagged = 0, new = 1, unread = 1, deleted = 0, flagged = 3, msgnotreadyet = -1, magic = 5, rights = "\177\001",
  compressinfo = 0x0, realpath = 0x0, locked = 0, changed = 1, readonly = 0, dontwrite = 0, append = 0, quiet = 0, collapsed = 0, closing = 0, peekonly = 0,
  data = 0x287fa480, mx_close = 0x80cc3d0 <imap_close_mailbox>}
(gdb) p ctx->hdrs[msgno]
$5 = (HEADER *) 0x0
(gdb) p msgno
$6 = 23

}}}

* version changed to 1.5.19

--------------------------------------------------------------------------------
2009-06-07 04:37:21 UTC brendan
* status changed to started

--------------------------------------------------------------------------------
2009-06-07 05:10:32 UTC brendan
* cc changed to berni@birkenwald.de, vegard@svanberg.no, uspoerlein@gmail.com
* Added comment:
Ok, from Vegard's muttdebug, I think I've found the problem:
{{{
4> a2520 FETCH 98488:98488 (UID FLAGS INTERNALDATE RFC822.SIZE BODY.PEEK[HEADER.FIELDS (DATE FR
OM SUBJECT TO CC MESSAGE-ID REFERENCES CONTENT-TYPE CONTENT-DESCRIPTION IN-REPLY-TO REPLY-TO LI
NES LIST-POST X-LABEL)])
4< * 98488 FETCH (UID 190458 RFC822.SIZE 11310 FLAGS (NonJunk) INTERNALDATE "01-Apr-2009 12:11:
29 +0000" BODY[HEADER.FIELDS (Date From Subject To Cc Message-Id References Content-Type Conten
t-Description In-Reply-To Reply-To Lines List-Post X-Label)] {570}
imap_read_literal: reading 570 bytes
4< )
parse_parameters: `charset=iso-8859-1'
parse_parameter: `charset' = `iso-8859-1'
4< * 98488 FETCH (UID 190458 FLAGS (NonJunk))
Message UID 190458 updated
Only handle FLAGS updates
parse_parameters: `charset=iso-8859-1'
parse_parameter: `charset' = `iso-8859-1'
4< a2520 OK done
}}}

We're receiving two separate updates for message 98488, and it's confusing imap_read_headers. That's why new_messages in mx_update_context is 2 when only one new message has arrived. I think this should be fairly straightforward to fix.

* Updated description:
Version: mutt-20070709[[BR]]
Platform: Debian/etch on amd64

I have an IMAP inbox that is updated asynchronously on a remote server. Sometimes, when new messages come in, and Mutt has uncommitted changes to the inbox, it segfaults.

This is what it looks like in GDB:

{{{
Fetching message headers... 1899/1900
Program received signal SIGSEGV, Segmentation fault.
0x000000000044f25e in mx_update_context (ctx=0x7b15d0, new_messages=4)
    at mx.c:1566
1566          h->security = crypt_query (h->content);
(gdb) p h->content
Cannot access memory at address 0x50
(gdb)
}}}

I have two core dumps saved from these crashes, and so can return additional telemetry upon request.

--------------------------------------------------------------------------------
2009-06-07 05:31:24 UTC Brendan Cully <brendan@kublai.com>
* Added comment:
(In [e5c2befbf0f5]) Do not treat already-seen headers as if they are new.
See #2935.

--------------------------------------------------------------------------------
2009-06-07 05:32:31 UTC brendan
* cc changed to berni@birkenwald.de, vegard@svanberg.no, uspoerlein@gmail.com
* Added comment:
I think I've fixed the bug. Please give the latest version from hg (revision 5861) a try!

--------------------------------------------------------------------------------
2009-06-07 17:23:12 UTC brendan
* Updated description:
Version: mutt-20070709[[BR]]
Platform: Debian/etch on amd64

I have an IMAP inbox that is updated asynchronously on a remote server. Sometimes, when new messages come in, and Mutt has uncommitted changes to the inbox, it segfaults.

This is what it looks like in GDB:

{{{
Fetching message headers... 1899/1900
Program received signal SIGSEGV, Segmentation fault.
0x000000000044f25e in mx_update_context (ctx=0x7b15d0, new_messages=4)
    at mx.c:1566
1566          h->security = crypt_query (h->content);
(gdb) p h->content
Cannot access memory at address 0x50
(gdb)
}}}

I have two core dumps saved from these crashes, and so can return additional telemetry upon request.
* status changed to infoneeded

--------------------------------------------------------------------------------
2009-06-07 17:27:06 UTC Vegard Svanberg
* Added comment:
{{{
* Mutt <fleas@mutt.org> [2009-06-07 07:32]:


I'll test and report back in a few days. First thing I notice is that
Mutt now seems to update the flags correctly. Earlier all unread
messages were all marked as new. Now they're marked as old (which is
correct because Thunderbird has already opened/seen the inbox). 
}}}

--------------------------------------------------------------------------------
2009-06-11 06:54:39 UTC Vegard Svanberg
* Added comment:
{{{
* Mutt <fleas@mutt.org> [2009-06-07 19:27]:


No segfaults since the upgrade! :)

However I do now experience a strange thing (though rarely) where the
mutt index shows an e-mail with a certain sender and subject. When I
open the mail, another mail is displayed. There is no way to read the
message mutt actually displays in the index without restarting mutt. 
}}}

--------------------------------------------------------------------------------
2009-06-11 17:01:52 UTC brendan
* Added comment:
Great, thanks for testing! The header mismatch sounds like it ought to be a different bug (possibly related to the header cache). Can you try deleting your header cache files and reproducing the new bug? If you can make it reproduce, please report it as a new bug. I'm closing this one.

* status changed to assigned

--------------------------------------------------------------------------------
2009-06-11 17:02:00 UTC brendan
* resolution changed to fixed
* status changed to closed
