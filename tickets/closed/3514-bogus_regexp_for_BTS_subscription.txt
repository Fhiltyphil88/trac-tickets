Ticket:  3514
Status:  closed
Summary: bogus regexp for BTS subscription

Reporter: antonio@dyne.org
Owner:    mutt-dev

Opened:       2011-05-02 21:39:31 UTC
Last Updated: 2011-06-20 23:07:46 UTC

Priority:  trivial
Component: mutt
Keywords:  patch

--------------------------------------------------------------------------------
Description:
Forwarding from http://bugs.debian.org/611412

{{{
The manual reports a bad regexp for Debian bugs in BTS:

subscribe [0-9]*.*@bugs.debian.org

But this regex matches, amongst others, submit@bugs.debian.org and 
control@bugs.debian.org.

The new proposed regexp is:
subscribe [0-9]+.*@bugs.debian.org

The attached patch fixes the problem
}}}

--------------------------------------------------------------------------------
2011-05-02 21:40:10 UTC antonio@dyne.org
* Added attachment 611412-bts-regexp.patch

--------------------------------------------------------------------------------
2011-06-20 23:07:46 UTC me
* resolution changed to fixed
* status changed to closed
