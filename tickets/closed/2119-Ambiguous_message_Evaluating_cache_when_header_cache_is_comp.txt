Ticket:  2119
Status:  closed
Summary: Ambiguous message "Evaluating cache" when header cache is compiled in but switched off

Reporter: raorn@altlinux.ru
Owner:    mutt-dev

Opened:       2005-10-26 09:20:42 UTC
Last Updated: 2005-10-31 11:57:26 UTC

Priority:  trivial
Component: IMAP
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Mutt (IMAP) tries to evaluate empty cache and informs user about it with "Evaluating cache" message.  This confuses some users.

This also generates series of useless commands to IMAP server.
>How-To-Repeat:
>Fix:
imap/message.c, imap_read_headers(), #if USE_HCACHE - check
ressult of hc = mutt_hcache_open (HeaderCache, ctx->path); and skip entrie #if/#endif block if hc is NULL.  (NOT TESTED).
}}}

--------------------------------------------------------------------------------
2005-11-01 05:57:26 UTC roessler
* Added comment:
{{{
fixed in CVS
}}}

* resolution changed to fixed
* status changed to closed
