Ticket:  3104
Status:  closed
Summary: mutt-1.5.18: Body search for non-ASCII symbols can fail

Reporter: yar@comp.chem.msu.su
Owner:    mutt-dev

Opened:       2008-08-14 13:47:32 UTC
Last Updated: 2008-08-21 09:02:05 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:


{{{
Package: mutt
Version: 1.5.18
Severity: normal

-- Please type your report below this line

When I do a body search (~b) for a string containing non-ASCII symbols
(Cyrillic ones in my case), the search never finds it although the string
is known to be there.

The problem seems specific to ~b and UTF-8 environment because it won't
show up if:
- I search in headers, e.g., with ~s, or
- I switch to 8-bit Cyrillic environment, e.g., KOI8-R.

By "environment" I mean my current locale (LANG) and Mutt's charset
settings:

UTF-8:
export LANG=ru_RU.UTF-8 # in the shell
set charset=utf-8 # in Mutt

KOI8-R:
export LANG=ru_RU.KOI8-R # in the shell
set charset=koi8-r # in Mutt

Just tested if body search works for Latin-1 characters.  No luck,
it equally fails.  OTOH, it works if the search string is plain ASCII
(or if headers, not body, are searched.)  What is really weird is that
Latin-1 body search fails even in 8-bit environment:
export LANG=en_US.ISO8859-1
set charset=iso8859-1

My local configuration doesn't seem to matter: when I disable the
system-wide muttrc and cut my personal muttrc down to "set charset=...",
the issue still manifests itself.

My platform (FreeBSD) also doesn't seem relevant because I hit the same
issue under MacOS X.

Thanks!

-- System Information
System Version: FreeBSD dg.local 8.0-CURRENT FreeBSD 8.0-CURRENT #1: Tue Aug 12 14:23:28 MSD 2008     root@dg.local:/usr/obj/usr/src/sys/GENERIC  i386

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
cc -I/usr/local/include
Using built-in specs.
Target: i386-undermydesk-freebsd
Configured with: FreeBSD/i386 system compiler
Thread model: posix
gcc version 4.2.1 20070719  [FreeBSD]

- CFLAGS
-O2 -pipe -fno-strict-aliasing

-- Mutt Version Information

Mutt 1.5.18 (2008-05-17)
Copyright (C) 1996-2008 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: FreeBSD 8.0-CURRENT (i386)
ncurses: ncurses 5.6.20080503 (compiled with 5.6)
libiconv: 1.11
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
-USE_FCNTL  +USE_FLOCK   
+USE_POP  +USE_IMAP  +USE_SMTP  +USE_GSS  +USE_SSL_OPENSSL  -USE_SSL_GNUTLS  +USE_SASL  +HAVE_GETADDRINFO  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  
-EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  -HAVE_LIBIDN  +HAVE_GETSID  -USE_HCACHE  
-ISPELL
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/local/share/mutt"
SYSCONFDIR="/usr/local/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please visit http://bugs.mutt.org/.

AssumingUnrIgnoringAssumeIgnoreApply anyway? [n] Hunk #%d ignored at %ld.
patch-1.5.0.ats.date_conditional.1
}}}

--------------------------------------------------------------------------------
2008-08-21 09:02:05 UTC Rocco Rutte <pdmef@gmx.net>
* Added comment:
(In [4139ac196ca8]) Better document the effect of $thorough_search being unset.
Closes #3104.

* resolution changed to fixed
* status changed to closed
