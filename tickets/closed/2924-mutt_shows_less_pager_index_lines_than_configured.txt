Ticket:  2924
Status:  closed
Summary: mutt shows less pager index lines than configured

Reporter: LeSpocky
Owner:    mutt-dev

Opened:       2007-06-26 09:01:59 UTC
Last Updated: 2007-09-03 13:24:38 UTC

Priority:  minor
Component: display
Keywords:  

--------------------------------------------------------------------------------
Description:
I have the following line in my .muttrc
{{{
set pager_index_lines = 5
}}}
Mutt only shows 4 index entries above the pager. See the attached screenshot of my mutt mailing list folder. The messages 186--189 are shown in index. That are 4 instead of 5 I would expect.

--------------------------------------------------------------------------------
2007-06-26 09:02:34 UTC LeSpocky
* Added attachment 24bit.png
* Added comment:
screenshot of mutt showing less index lines than configured

--------------------------------------------------------------------------------
2007-07-16 17:14:13 UTC brendan
* Added comment:
One of the index lines is consumed by the index status bar. Try incrementing pager_index_lines from 0 to 1 and you'll see what I mean. It could be argued that pager_index_lines=1 isn't very useful, so it should jump from 0 lines consumed to 2 when pager_index_lines is 1 (1 for the index entry and 1 more for the status), but I suspect others might find that confusing. I'll attach the patch that would do this, but I'm not sure it's a good idea to apply it.

* component changed to display

--------------------------------------------------------------------------------
2007-07-16 17:14:49 UTC brendan
* Added attachment 2924.diff
* Added comment:
Make pager_index_lines control the number of index entries available.

--------------------------------------------------------------------------------
2007-07-31 08:05:08 UTC LeSpocky
* Added comment:
I see the effect and this explains the behaviour. Perhaps it would be enough to update the documentation to make this point clear. 

--------------------------------------------------------------------------------
2007-09-03 13:24:38 UTC pdmef
* Added comment:
The manual contains already this for pager_index_lines:

''One of the lines is reserved for the status bar from the index, so a pager_index_lines of 6 will only show 5 lines of the actual index. A value of 0 results in no index being shown.''

Please re-open with suggestions on how to make it clearer.

* resolution changed to invalid
* status changed to closed
