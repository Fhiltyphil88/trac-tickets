Ticket:  2215
Status:  closed
Summary: feature request: progress indicator when fetching multiple mails

Reporter: slrn_robert@yahoo.de
Owner:    mutt-dev

Opened:       2006-04-29 12:13:49 UTC
Last Updated: 2007-03-16 09:47:59 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
When mutt fetches multiple mail, e.g. when search body for some pattern, the status line only shows "fetching message". It would be nice to have a progress indicator showing how far the task is done. This would help to estimate the amount of time, needed to complete those tasks, especially for large mailboxes
>How-To-Repeat:
change to large mailbox, type l (limit messages), enter a pattern as e.g. '~b "\<foo\>"'.
>Fix:
Unknown
}}}

--------------------------------------------------------------------------------
2007-03-17 01:47:59 UTC pdmef
* Added comment:
{{{
Recent mutt have progress indicators for search and limit. 
Please re-open if this doesn't solve your problem.
}}}

* resolution changed to fixed
* status changed to closed
