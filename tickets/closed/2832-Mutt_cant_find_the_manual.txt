Ticket:  2832
Status:  closed
Summary: Mutt can't find the manual

Reporter: code@pizzashack.org
Owner:    mutt-dev

Opened:       2007-03-08 00:30:22 UTC
Last Updated: 2007-03-22 14:42:53 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
On a freshly installed system, with no previous installations of mutt, I downloaded mutt 1.5.14 and configured with the following command:

./configure --enable-imap --enable-pop --with-ssl --enable-hcache

Then I built mutt and installed it.  When I hit F1 to look at the manual, I only get the following error message:

/doc/mutt/manual.txt: No such file or directory
Press any key to continue...

...i.e. mutt is looking for the manual in /doc, for some reason.  It's probably a problem with the autoconf/automake stuff, but I haven't had a chance to look into it.

FWIW, I think this bug is actually quite old.  I've run into it before, the last time I downloaded an actual release of Mutt (i.e. not a CVS release -- I can't build the manual with the CVS release because I'm missing the tools).  It might have been somewhere around 1.5.11 or .12 that I first noticed this.  I've lost track of which development versions I was running before then...

The manual does in fact exist, and you can view it just fine  using the command:

  less /usr/local/doc/mutt/manual.txt

The build environment seems to not be including the prefix in whatever part of mutt binds the manual to F1.

Indeed, I just checked /usr/local/etc/Muttrc, and it contans the line:

macro generic,pager <F1> "<shell-escape> less ${prefix}/doc/mutt/manual.txt<Ente
r>" "show Mutt documentation"

Maybe someone used ${prefix} when they meant to use $(prefix)?

I can obviously fix this manually (and just did), but it really ought to build correctly...
>How-To-Repeat:
>Fix:
Unknown
}}}

--------------------------------------------------------------------------------
2007-03-08 21:26:34 UTC Vincent Lefevre <vincent@vinc17.org>
* Added comment:
{{{
On 2007-03-08 01:30:23 +0100, code@pizzashack.org wrote:
> The manual does in fact exist, and you can view it just fine using
> the command:
>=20
>   less /usr/local/doc/mutt/manual.txt

IMHO, this is a bug. The manual should be in /usr/local/share/doc/mutt.
Indeed http://www.pathname.com/fhs/pub/fhs-2.3.html says:

  Requirements

  The following directories, or symbolic links to directories, must be
  in /usr/local

  Directory       Description
  bin             Local binaries
  etc             Host-specific system configuration for local binaries
  games           Local game binaries
  include         Local C header files
  lib             Local libraries
  man             Local online manuals
  sbin            Local system binaries
  share           Local architecture-independent hierarchy
  src             Local source code

  No other directories, except those listed below, may be in
  /usr/local after first installing a FHS-compliant system.

--=20
Vincent Lef=E8vre <vincent@vinc17.org> - Web: <http://www.vinc17.org/>
100% accessible validated (X)HTML - Blog: <http://www.vinc17.org/blog/>
Work: CR INRIA - computer arithmetic / Arenaire project (LIP, ENS-Lyon)
}}}

--------------------------------------------------------------------------------
2007-03-14 06:43:29 UTC Brendan Cully <brendan@kublai.com>
* Added comment:
{{{
On Thursday, 08 March 2007 at 01:30, code@pizzashack.org wrote:
> >Number:         2832
> >Notify-List:    
> >Category:       mutt
> >Synopsis:       Mutt can't find the manual
> >Confidential:   no
> >Severity:       normal
> >Priority:       medium
> >Responsible:    mutt-dev
> >State:          open
> >Keywords:       
> >Class:          sw-bug
> >Submitter-Id:   net
> >Arrival-Date:   Thu Mar 08 01:30:22 +0100 2007
> >Originator:     code@pizzashack.org
> >Release:        
> >Organization:
> >Environment:
> >Description:
> On a freshly installed system, with no previous installations of mutt, I downloaded mutt 1.5.14 and configured with the following command:
> 
> ./configure --enable-imap --enable-pop --with-ssl --enable-hcache
> 
> Then I built mutt and installed it.  When I hit F1 to look at the manual, I only get the following error message:
> 
> /doc/mutt/manual.txt: No such file or directory
> Press any key to continue...
> 
> ...i.e. mutt is looking for the manual in /doc, for some reason.  It's probably a problem with the autoconf/automake stuff, but I haven't had a chance to look into it.
> 
> FWIW, I think this bug is actually quite old.  I've run into it before, the last time I downloaded an actual release of Mutt (i.e. not a CVS release -- I can't build the manual with the CVS release because I'm missing the tools).  It might have been somewhere around 1.5.11 or .12 that I first noticed this.  I've lost track of which development versions I was running before then...
> 
> The manual does in fact exist, and you can view it just fine  using the command:
> 
>   less /usr/local/doc/mutt/manual.txt
> 
> The build environment seems to not be including the prefix in whatever part of mutt binds the manual to F1.
> 
> Indeed, I just checked /usr/local/etc/Muttrc, and it contans the line:
> 
> macro generic,pager <F1> "<shell-escape> less ${prefix}/doc/mutt/manual.txt<Ente
> r>" "show Mutt documentation"
> 
> Maybe someone used ${prefix} when they meant to use $(prefix)?
> 
> I can obviously fix this manually (and just did), but it really ought to build correctly...

Thanks. Should be fixed in 61b2845af4da.
}}}

--------------------------------------------------------------------------------
2007-03-17 03:04:42 UTC pdmef
* Added comment:
{{{
On 2007-03-08 01:30:23 +0100, code@pizzashack.org wrote:
 On Thursday, 08 March 2007 at 01:30, code@pizzashack.org wrote:
}}}

--------------------------------------------------------------------------------
2007-03-23 06:42:53 UTC cb
* Added comment:
{{{
fixed in http://dev.mutt.org/hg/mutt/rev/61b2845af4da
}}}

* resolution changed to fixed
* status changed to closed
