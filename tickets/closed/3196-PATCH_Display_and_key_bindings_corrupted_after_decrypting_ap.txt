Ticket:  3196
Status:  closed
Summary: [PATCH] Display and key bindings corrupted after decrypting application/pgp message with gpg-agent

Reporter: tmz
Owner:    mutt-dev

Opened:       2009-03-01 21:08:58 UTC
Last Updated: 2009-03-17 15:45:00 UTC

Priority:  minor
Component: mutt
Keywords:  pgp

--------------------------------------------------------------------------------
Description:
When using gpg-agent, pinentry-curses, and the classic pgp backend, the display becomes corrupted after decrypting (or attempting to decrypt) a message sent in traditional pgp (application/pgp) format.

The fix is to call mutt_need_hard_redraw(), which the attached patch does.  Hopefully, there aren't many more areas where this still needs fixed.  I apparently don't get many application/pgp encrypted messages, as I didn't notice this one for quite a long time.


--------------------------------------------------------------------------------
2009-03-01 21:11:14 UTC tmz
* Added attachment mutt-pgp-classic-curses-redraw.patch
* Added comment:
patch to call mutt_need_hard_redraw() as needed

--------------------------------------------------------------------------------
2009-03-17 15:45:00 UTC Todd Zullinger <tmz@pobox.com>
* Added comment:
(In [bb9b845800f6]) Ensure display is redrawn for application/pgp

When using gpg-agent, a hard redraw is needed after the pinentry program
is called.  Otherwise, the screen is garbled and key bindings are
screwed up.

Closes #3196.

* resolution changed to fixed
* status changed to closed
