Ticket:  2941
Status:  closed
Summary: muttrc man page formatting improvement (with patch!)

Reporter: memoryhole
Owner:    mutt-dev

Opened:       2007-08-07 14:38:04 UTC
Last Updated: 2007-08-08 17:49:43 UTC

Priority:  minor
Component: doc
Keywords:  

--------------------------------------------------------------------------------
Description:
In places in the documentation (from init.h) where a list of printf-style options is made, it's currently formatted in the man page like this:

.RS
.IP %C
charset

.IP %c
requires charset conversion (n or c)

That results in a man page with a lot of extra whitespace (two blank lines between each printf option), making it hard to read. That would probably be better formatted like this:

.RS
.TP
%C
charset
.TP
%c
requires charset conversion (n or c)

The attached patch to makedoc.c should greatly improve readability of the man page.

--------------------------------------------------------------------------------
2007-08-07 14:53:46 UTC memoryhole
* Added attachment mutt-makedoc.patch
* Added comment:
The mentioned patch

--------------------------------------------------------------------------------
2007-08-08 17:49:43 UTC Kyle Wheeler
* Added comment:
(In [6d3e90261321]) Trim whitespace in definition lists for man pages (closes #2941).

* resolution changed to fixed
* status changed to closed
