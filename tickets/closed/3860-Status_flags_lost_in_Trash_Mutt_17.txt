Ticket:  3860
Status:  closed
Summary: Status flags lost in Trash (Mutt 1.7)

Reporter: kirchwitz
Owner:    mutt-dev

Opened:       2016-08-19 01:48:28 UTC
Last Updated: 2017-09-13 22:57:11 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
Just installed the brandnew Mutt 1.7 (Linux) and enabled the new "trash" variable. Great feature!

I'm accessing my mail via IMAP (Dovecot). I read new messages, delete some of then (they get the 'D'eleted flag in memory) and then I quit Mutt. Mutt moves those deleted messages to the folder specified in "trash". So far, so good.

However, those deleted mails now pop up as new (!) messages in all mail clients because Mutt didn't save the modified flags properly (messages should no longer be flagged as 'N'ew but read).

It works fine if I sync the mailbox (key-command $) before I start deleting messages so that the flags are no longer just in memory but permanently stored with the messages. Read mail stays read in Trash.

Funny thing is that simply saving a message (key-command s) to a different folder does not show this problem. No need to sync the mailbox before saving it, flags are correctly preserved. Even saving it directly to my Trash folder works.

Besides that report, it might be worth thinking of generally removing the 'N'ew flag from deleted messages that are going to be moved to Trash. Even if they haven't been actually read. On Mutt's index screen, I usually mark all obvious spam messages for deletion without reading those messages. Nobody wants them to show up as new/unread messages in Trash. (That is an important part of the folder-hook magic that most people currently use to emulate the Trash feature.)

If you need any further details please let me know.

It's cool that Mutt now supports the Trash feature internally. Hopefully it can replace all the complicated folder-hook magic soon.


--------------------------------------------------------------------------------
2016-09-02 14:19:48 UTC kevin8t8
* Added comment:
Thanks for the bug report.  I'm guessing this is because of an "improvement" I made to the  imap_fast_trash() function, that probably turned out to be a bad idea.

--------------------------------------------------------------------------------
2016-10-05 02:43:30 UTC kevin8t8
* Added comment:
Actually, the easiest solution appears to be what you suggested: mark all trashed email as read before performing the UID COPY for trash. 

The copy/save function for imap syncs all the flags (including deleted) before performing the UID COPY.  I am not sure it's desirable to include the deleted flag for that case, but as-is it makes the code not reusable for the fast-trash function.

Adding a imap_exec_msgset() to set \\Seen for the same message set is pretty easy, and I think is pretty reasonable behavior.

--------------------------------------------------------------------------------
2016-10-05 03:11:47 UTC kevin8t8
* Added attachment ticket-3860.patch

--------------------------------------------------------------------------------
2016-10-05 03:12:02 UTC kevin8t8
* Added comment:
Attached a proposed patch.

--------------------------------------------------------------------------------
2016-10-06 19:39:20 UTC kevin8t8
* _comment0 changed to 1475783027241918
* Added comment:
After chatting a bit with Brendan, it isn't clear that what copy/save is doing (preserving the deleted flag in the target mailbox) is the right thing to do.  If this is changed, then this ticket may be able to use the same revised imap_sync_message() that copy/save uses just before performing the server side copy.

I will commit this fix for now, but will keep this ticket open as a reminder to revisit this.

--------------------------------------------------------------------------------
2016-10-06 19:41:35 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [changeset:"323e3d6e5e4cf5874b6edf40f25abe1fe107bbc8" 6808:323e3d6e5e4c]:
{{{
#!CommitTicketReference repository="" revision="323e3d6e5e4cf5874b6edf40f25abe1fe107bbc8"
Mark IMAP fast-trash'ed messages as read before copying. (see #3860)

Regular copying/saving messages in mutt via a UID COPY first calls
imap_sync_message().  However that function is designed to sync all
flags (including deleted), and so isn't useful for the fast-trash
code.

As an easier solution, instead add a UID STORE to set \\Seen for the
same msgset as the trashed messages.
}}}

--------------------------------------------------------------------------------
2017-09-13 22:57:08 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [changeset:"19597bb7baa6e1e4ce4006a994df9a0e56dee8a7" 7152:19597bb7baa6]:
{{{
#!CommitTicketReference repository="" revision="19597bb7baa6e1e4ce4006a994df9a0e56dee8a7"
Remove \Seen flag setting for imap trash.  (see #3966) (see #3860)

Commit 323e3d6e5e4c has a side effect where spurious FETCH flag
updates after setting the \Seen flag can cause a sync to abort.

Remove manually setting \Seen for all trashed message before copying.

The next commit will change the imap trash function to use the same
code as the imap copy/save function for syncing the message before
server-side copying.
}}}

--------------------------------------------------------------------------------
2017-09-13 22:57:11 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [changeset:"f90712538cd9198a1fecbca362eecefe5541e4a5" 7153:f90712538cd9]:
{{{
#!CommitTicketReference repository="" revision="f90712538cd9198a1fecbca362eecefe5541e4a5"
Change imap copy/save and trash to sync flags, excluding deleted. (closes #3966) (closes #3860)

imap_copy_messages() uses a helper to sync the flags before performing
a server-side copy.  However, it had a bug that the "deleted" flag on
a local message, if set, will be propagated to the copy too.

Change the copy sync helper to ignore the deleted flag.  Then, change
the imap trash function to use the same helper.

Thanks to Anton Lindqvist for his excellent bug report, suggested
fixes, and help testing.
}}}

* resolution changed to fixed
* status changed to closed
