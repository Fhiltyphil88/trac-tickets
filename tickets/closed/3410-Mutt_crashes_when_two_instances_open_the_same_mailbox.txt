Ticket:  3410
Status:  closed
Summary: Mutt crashes when two instances open the same mailbox

Reporter: vext01
Owner:    me

Opened:       2010-05-17 12:32:58 UTC
Last Updated: 2010-08-14 15:45:32 UTC

Priority:  major
Component: mutt
Keywords:  imap segfault

--------------------------------------------------------------------------------
Description:
My university mail is stored on an IMAP server (UoW). If I run two copies of mutt on the same mailbox on my OpenBSD workstation (running hg head mutt), the older of the two will seg fault.

I am using a strict malloc.conf on OpenBSD. In the attached backtrace, notice the following:

{{{
#1  0x1c082ca6 in mutt_strcmp (
    a=0x8133c000 "imaps://url.censored.sorry:993/Sent Items",
    b=0xdfdfdfdf <Address 0xdfdfdfdf out of bounds>) at lib.c:870
}}}

The 0xdfdfdfdf indicates a use after free and is provided by the J flag for malloc.conf:

{{{
     J       ``Junk''.  Fill some junk into the area allocated.  Currently
             junk is bytes of 0xd0 when allocating; this is pronounced
             ``Duh''.  :-) Freed chunks are filled with 0xdf.
}}}

For more information see the malloc.conf manual page:
http://www.openbsd.org/cgi-bin/man.cgi?query=malloc.conf&apropos=0&sektion=0&manpath=OpenBSD+Current&arch=i386&format=html

--------------------------------------------------------------------------------
2010-05-17 12:34:16 UTC vext01
* Added attachment backtrace.log
* Added comment:
backtrace

--------------------------------------------------------------------------------
2010-05-18 01:30:11 UTC me
* Added comment:
Does Mutt print any sort of error or message right before it crashes?

Can you run with debugging enabled to help track this down?

* status changed to infoneeded_new

--------------------------------------------------------------------------------
2010-05-18 10:42:02 UTC vext01
* Added comment:
> Does Mutt print any sort of error or message right before it crashes? 

Yes, "mailbox closed" and then after about a minute or two is followed by the seg fault.

> Can you run with debugging enabled to help track this down?

The debug log (at level 99), end like so:

{{{
...
[2010-05-18 11:39:49] mutt_addr_is_user: no, all failed.
[2010-05-18 11:39:49] mutt_addr_is_user: no, all failed.                        
[2010-05-18 11:39:49] mutt_addr_is_user: no, all failed.                        
[2010-05-18 11:39:49] mutt_addr_is_user: no, all failed.                        
[2010-05-18 11:39:49] mutt_addr_is_user: no, all failed.                        
[2010-05-18 11:39:49] mutt_addr_is_user: no, all failed.                        
[2010-05-18 11:39:49] mutt_addr_is_user: no, all failed.                        
[2010-05-18 11:39:49] mutt_addr_is_user: no, all failed.                        
[2010-05-18 11:39:49] mutt_addr_is_user: no, all failed.                        
[2010-05-18 11:39:49] mutt_addr_is_user: yes, my@email.org = my@email.org                                                                               
[2010-05-18 11:39:49] mutt_addr_is_user: no, all failed.                        
[2010-05-18 11:39:49] mutt_addr_is_user: no, all failed.                        
[2010-05-18 11:39:49] mutt_addr_is_user: yes, my@email.org = my@email.org                                                                               
[2010-05-18 11:39:49] mutt_addr_is_user: no, all failed.                        
[2010-05-18 11:39:50] mutt_index_menu[605]: Got op 115                          
[2010-05-18 11:39:50] You are on the last message.                              
[2010-05-18 11:39:50] mutt_index_menu[605]: Got op 120                          
[2010-05-18 11:39:50] mutt_addr_is_user: no, all failed.                        
[2010-05-18 11:39:50] mutt_addr_is_user: no, all failed.                        
[2010-05-18 11:39:50] mutt_index_menu[605]: Got op 115                          
[2010-05-18 11:39:50] mutt_addr_is_user: no, all failed.                        
[2010-05-18 11:39:50] mutt_addr_is_user: no, all failed.                        
[2010-05-18 11:41:30] 5> a0010 NOOP^M                                           
[2010-05-18 11:41:30] 5< * BYE Lost mailbox lock                                
[2010-05-18 11:41:30] Handling BYE                                              
[2010-05-18 11:41:30] Lost mailbox lock                                         
[2010-05-18 11:41:32] Mailbox closed                                            
[2010-05-18 11:41:33] imap_exec: command failed: * BYE Lost mailbox lock        
[2010-05-18 11:44:53] mutt_index_menu[605]: Got op -1                           
[2010-05-18 11:44:53] mx_check_mailbox: null or invalid context.                
}}}

(I have been asked by our systems group to censor all user names and hostnames)

* status changed to new

--------------------------------------------------------------------------------
2010-05-18 19:24:26 UTC me
* owner changed to me
* status changed to assigned

--------------------------------------------------------------------------------
2010-05-18 19:42:02 UTC me
* Added comment:
The problem resides in imap/command.c in cmd_handle_fatal().  mx_fastclose_mailbox() is called on idata->ctx which in this case is an alias for the global Context.

I'm guessing that this code should not close the context because I don't see anywhere that the imap code internally opens the mailbox.

--------------------------------------------------------------------------------
2010-05-20 04:58:42 UTC brendan
* Added comment:
I don't think there's anything wrong in principle with closing Context here -- mutt_index_menu and mutt_buffy_check are both careful to check whether Context or Context->path are NULL. The context alias comes from the call to imap_get_mailbox, called from imap_buffy_check, called from mutt_buffy_check. It is a bit unfortunate that idata->ctx is an alias for Context, but mutt's heavy use of globals makes it a bit hard to fix.

The trigger of this bug is actually openbsd's malloc. Mutt is setting idata->ctx->path to NULL in mx_fastclose_mailbox, then freeing idata->ctx. openbsd's malloc is presumably overwriting *idata->ctx with its junk value instead of NULL, which causes the !Context->path test on 417 of mutt_buffy_check to fail. Now, this is certainly a mutt bug -- dereferencing Context is not kosher here -- but it is normally harmless.

One possible fix would be to have mx_fastclose_mailbox compare Context to ctx, and null out the global if it points to the same place.

--------------------------------------------------------------------------------
2010-05-21 06:36:52 UTC brendan
* Added comment:
I've misdiagnosed the bug slightly in comment 5. mx_fastclose_mailbox doesn't free ctx, it just memsets it to 0. The rest of the comment should still stand, and I'll put together a test patch that compares ctx to Context in mx_fastclose_mailbox.

--------------------------------------------------------------------------------
2010-05-21 06:47:38 UTC brendan
* Added comment:
Never mind, it must be too late for me to think straight. idata->ctx and Context ought to be pointing to the same piece of memory (idata->ctx is assigned in imap_open_mailbox, using the pointer created in mx_open_mailbox), so the memset ought to null out ctx->path and consequently Context->path, causing mutt_buffy_check:417 to short-circuit harmlessly. So I'm confused again.

--------------------------------------------------------------------------------
2010-06-14 15:54:52 UTC vext01
* Added comment:
any news on this?

--------------------------------------------------------------------------------
2010-06-23 08:34:27 UTC vext01
* cc changed to vext01@gmail.com
* Added comment:
I am also able to get this on my gmail account, when only one client is connected, so perhaps the title of this ticket is incorrect:

Program received signal SIGBUS, Bus error.
strcmp (s1=0x2045c4e00 "imaps://imap.gmail.com:993/INBOX", 
    s2=0xdfdfdfdfdfdfdfdf <Address 0xdfdfdfdfdfdfdfdf out of bounds>)
    at /usr/src/lib/libc/string/strcmp.c:47
47              while (*s1 == *s2++)
(gdb) bt
#0  strcmp (s1=0x2045c4e00 "imaps://imap.gmail.com:993/INBOX", 
    s2=0xdfdfdfdfdfdfdfdf <Address 0xdfdfdfdfdfdfdfdf out of bounds>)
    at /usr/src/lib/libc/string/strcmp.c:47
#1  0x000000000048fdb8 in mutt_strcmp (
    a=0x2045c4e00 "imaps://imap.gmail.com:993/INBOX", 
    b=0xdfdfdfdfdfdfdfdf <Address 0xdfdfdfdfdfdfdfdf out of bounds>)
    at lib.c:870
#2  0x00000000004116ed in mutt_buffy_check (force=0) at buffy.c:417
#3  0x0000000000424258 in mutt_index_menu () at curs_main.c:522
#4  0x0000000000449279 in main (argc=1, argv=0x7f7ffffcd648) at main.c:1020


Full backtrace is attached.

--------------------------------------------------------------------------------
2010-06-23 08:35:34 UTC vext01
* Added attachment gdb-gmail.txt
* Added comment:
Crash against GMail's IMAP server with only one client connected.

--------------------------------------------------------------------------------
2010-06-27 14:03:57 UTC vext01
* Added comment:
I am getting this crash about twice a day on my GMail account. 

--------------------------------------------------------------------------------
2010-06-28 23:49:07 UTC derekmartin
* Added comment:
Replying to [comment:10 vext01]:
> I am getting this crash about twice a day on my GMail account. 

So, I can't find anywhere in this bug where you mentioned what version of Mutt you're using... it occurs to me that one reason Brendan *might* be confused is because he's not looking at the same code your version of Mutt was compiled from.

Also note that Brendan recently became a dad... I think he has other things on his mind besides your bug report. :)

--------------------------------------------------------------------------------
2010-06-29 00:05:57 UTC vext01
* Added comment:
Replying to [comment:11 derekmartin]:
> So, I can't find anywhere in this bug where you mentioned what version of Mutt you're using... it occurs to me that one reason Brendan *might* be confused is because he's not looking at the same code your version of Mutt was compiled from.

It's hg HEAD.

> Also note that Brendan recently became a dad... I think he has other things on his mind besides your bug report. :)

Ah well, congrats are in order! :)

--------------------------------------------------------------------------------
2010-07-07 16:10:05 UTC vext01
* Added comment:
Perhaps I can bribe another developer with beer?

--------------------------------------------------------------------------------
2010-08-03 11:41:41 UTC vinc17
* Added comment:
I think you should say how you compiled Mutt (e.g. the configure options). You should also try to see where the invalid address 0xdfdfdfdfdfdfdfdf appeared for the first time. If your platform has a utility like valgrind, this can also be useful.

Note that it can also be a bug on your platform (C compiler or C library).

--------------------------------------------------------------------------------
2010-08-03 12:51:44 UTC vext01
* Added comment:
I have an alias for configuring mutt, which looks like this:
configure_mutt='CPPFLAGS=-I/usr/local/include LDFLAGS=-L/usr/local/lib ./configure --prefix=/opt/mutt --with-sasl --enable-smtp --enable-imap --enable-hcache --with-ssl'

Although this could be a compiler bug, IIRC this happened before OpenBSD switch to GCC4 (which was only a few months back).

What is the best way to track the origin of that 0xdfdf...?

--------------------------------------------------------------------------------
2010-08-04 12:24:22 UTC vinc17
* cc changed to vext01@gmail.com, vincent@vinc17.org
* Added comment:
Replying to [comment:15 vext01]:
> What is the best way to track the origin of that 0xdfdf...?
You can use gdb, by putting breakpoints at the right places (you need to guess and try) and stepping the program. As some compiler optimizations are incompatible with gdb, you need to compile without optimizations (-O0); -O1 may work too, without guarantee. First try if you can reproduce the bug without optimizations; if you can't, this is probably a compiler bug.

If for some reason you can't use gdb, you can modify Mutt's code to add some debugging messages outputting values of variables at the right places. This may also be more convenient that gdb (in case of multiple calls of functions that would make breakpoints tedious).

--------------------------------------------------------------------------------
2010-08-06 07:37:49 UTC Matthias Andree
* Added comment:
{{{
Am 03.08.2010, 14:51 Uhr, schrieb Mutt:


(Only skimming through this, so I may have missed an attachment:)

Can you post config.log so that developers can see what libraries exactly  
are getting picked up? There's still some magic underneath, particularly  
with the hcache stuff, although I believe it's unrelated.


To figure if it's a GCC3 bug, make a backup copy of your /opt/mutt and  
recompile with GCC4 :)


It's likely any of the free() calls given that malloc.conf is configured  
to spam freed memory regions with 0xdf (probably shorthand for dead flash,  
alonside the 0xd0 for doh...)

I'm not sure if it's practical to look at all free() calls though. Skilled  
debugger users might set conditional breakpoints, or perhaps figure where  
the relevant memory is allocated, set a breakpoint there, and then set  
watch- or tracepoints.

purify, valgrind, and similar real-time memory debuggers would be a bit  
more promising IMO. In need, try dmalloc or efence, or export  
MALLOC_CHECK_=2 on GNU libc systems - and be sure you can dump core.
}}}

--------------------------------------------------------------------------------
2010-08-06 11:00:56 UTC Patrick Welche
* Added comment:
{{{
On Fri, Aug 06, 2010 at 09:37:40AM +0200, Matthias Andree wrote:


Me too - but I just saw "--enable-hcache" above and then you wrote


I had to get rid of a load of FREE(&h) calls which try to free memory
allocated and accounted by the db storing the hcache. AFAICT mutt should
not be freeing it - it is up to the db library.

I kept meaning to clean up a patch, but as I have ENOTIME, here is what
I am currently using. (It also enables boring but working Berkeley db 3.)
(Part of the clean up was to go through each different db backend and
check that mutt shouldn't free() - it was true for all those I did check.)

Hope this helps,

Patrick
}}}

--------------------------------------------------------------------------------
2010-08-07 09:50:18 UTC vext01
* Added comment:
OK, so I went and found myself a Linux box and ran it on mutt HEAD. I used the free filling option to fill freed memory with 0xdf, so as to emulate OpenBSD malloc.conf. Sure enough it crashed.

Valgrind reports an alarming number of bad memory accesses, including many use after frees (if i read the output correctly); such as:

==6567== Invalid read of size 4
==6567==    at 0x8068C6F: mutt_index_menu (curs_main.c:480)
==6567==    by 0x808C138: main (main.c:1019)
==6567==  Address 0x43be270 is 72 bytes inside a block of size 116 free'd
==6567==    at 0x4024B3A: free (vg_replace_malloc.c:366)
==6567==    by 0x80CE072: safe_free (lib.c:198)
==6567==    by 0x80FE4F8: imap_keepalive (util.c:766)
==6567==    by 0x8089095: km_dokey (keymap.c:407)
==6567==    by 0x806925F: mutt_index_menu (curs_main.c:603)
==6567==    by 0x808C138: main (main.c:1019)

Attached is a gzipped log.

--------------------------------------------------------------------------------
2010-08-07 09:50:54 UTC vext01
* Added attachment VALGRIND.out.gz
* Added comment:
Valgrind output

--------------------------------------------------------------------------------
2010-08-07 17:40:37 UTC me
* Added attachment imap-buffy-segfault.diff
* Added comment:
proposed fix

--------------------------------------------------------------------------------
2010-08-07 17:42:31 UTC me
* Added comment:
I believe I have tracked down the problem.  Please try the attached patch.

* keywords changed to imap segfault

--------------------------------------------------------------------------------
2010-08-08 22:05:58 UTC vext01
* Added comment:
I left mutt running for quite some time without it crashing with this bug, so I think you may have nailed it. However, mutt crashed in a different way when I marked a message read: see #3440. Probably a separate issue.

I recommend a few more days of testing before commit/close.

--------------------------------------------------------------------------------
2010-08-10 08:00:52 UTC vext01
* Added comment:
This works well. I reckon it is OK to commit.

We have also back ported this into 1.5.20 for OpenBSD ports.

Thanks guys!

--------------------------------------------------------------------------------
2010-08-14 15:45:32 UTC Michael Elkins <me@mutt.org>
* Added comment:
(In [ced5d3dca974]) Fix bug in imap_keepalive() which erroneously free Context when the IMAP connection is shut down by the server.

Closes #3410

* resolution changed to fixed
* status changed to closed
