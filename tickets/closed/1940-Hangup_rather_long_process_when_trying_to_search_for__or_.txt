Ticket:  1940
Status:  closed
Summary: Hangup (rather long process) when trying to search for '^' or '$'

Reporter: s_fokin@gorodok.net
Owner:    mutt-dev

Opened:       2004-11-10 11:23:46 UTC
Last Updated: 2005-09-21 05:55:53 UTC

Priority:  minor
Component: mutt
Keywords:  #1921

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.5.6-20040907
Severity: normal

-- Please type your report below this line

I do following things:
run mutt (it opens ~/mail/INBOX as spoolfile), then click '?'(open help screen), '/'(search), enter $ in prompt and press enter
after this mutt stucks for about a minute (and top(1) shows that it eats my processor time

same things occure when i put in prompt ^


-- System Information
System Version: Linux land5 2.4.23-ow2 #2 ÷ÓË ñÎ× 18 05:25:18 NOVT 2004 i686 GNU/Linux

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
cc
Reading specs from /usr/lib/gcc-lib/i486-linux/3.3.5/specs
Configured with: ../src/configure -v --enable-languages=c,c++,java,f77,pascal,objc,ada,treelang --prefix=/usr --mandir=/usr/share/man --infodir=/usr/share/info --with-gxx-include-dir=/usr/include/c++/3.3 --enable-shared --with-system-zlib --enable-nls --without-included-gettext --enable-__cxa_atexit --enable-clocale=gnu --enable-debug --enable-java-gc=boehm --enable-java-awt=xlib --enable-objc-gc i486-linux
Thread model: posix
gcc version 3.3.5 (Debian 1:3.3.5-1)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.5.6+20040907i (CVS)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.23-ow2 (i686) [using ncurses 5.4] [using libidn 0.5.2 (compiled with 0.5.2)]
Compile options:
-DOMAIN
+DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  +IMAP_EDIT_THREADS  -USE_GSS  -USE_SSL  +USE_GNUTLS  +USE_SASL  +USE_SASL2  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +COMPRESSED  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_LIBIDN  +HAVE_GETSID  +HAVE_GETADDRINFO  +USE_HCACHE  
ISPELL="ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
MIXMASTER="mixmaster"
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.

Debian.Md.etc_mailname_gethostbyname
Debian.Md.gpg_status_fd
Debian.Md.muttbug
Debian.Md.use_debian_editor
mutt-cvs-header-cache.21
patch-1.3.23.1.ametzler.pgp_good_sign
patch-1.3.23.bj.current_shortcut.1
patch-1.4.admcd.gnutls.59d
patch-1.5.4.fw.maildir_inode_sort
patch-1.5.4.vk.pgp_verbose_mime
patch-1.5.5.1.cd.edit_threads.9.5
patch-1.5.5.1.nt.xtitles.3.ab.1
patch-1.5.6.dw.maildir-mtime.1
patch-1.5.6.rr.compressed.1
patch-1.5.6.tt.compat.1
patch.helmersson.incomplete-mbyte.2


>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2005-08-01 20:51:57 UTC brendan
* Added comment:
{{{
I can't reproduce this. Is it a libc bug?
}}}

* status changed to assigned

--------------------------------------------------------------------------------
2005-08-28 09:08:40 UTC rado
* Added comment:
{{{
Moved reporters from #1587 to this notify-list.

Reporters, can you please retry this on a different system?
Other OS, other curses/slang?
Other regexp libs?

+HAVE_REGCOMP  -USE_GNU_REGEX on solaris 9 doesn't do this, immediately returns.
}}}

--------------------------------------------------------------------------------
2005-09-07 09:21:53 UTC brendan
* Added comment:
{{{
Cross-reference with #1921.
}}}

--------------------------------------------------------------------------------
2005-09-21 23:55:53 UTC brendan
* Added comment:
{{{
Unreproducible, no feedback for 6 weeks.
}}}

* resolution changed to fixed
* status changed to closed
