Ticket:  2122
Status:  closed
Summary: Should encrypt messages to the Sender: key as well as to recipients

Reporter: jfs@computer.org
Owner:    mutt-dev

Opened:       2005-10-28 18:13:00 UTC
Last Updated: 2017-06-14 22:42:21 UTC

Priority:  trivial
Component: crypto
Keywords:  patch

--------------------------------------------------------------------------------
Description:
{{{
Currently, GPG only encrypts messages to the To:, CC: or Bcc: but *not* to the From: of an email message. This means that any user that sends an encrypted mail to any other user will not be able to read the e-mail in the future in a default configuration (see workarounds for possible ways to prevent this)
I don't see any reason for outgoing mail to not be encrypted to the sender's key so he is able to read it in the future and other software (i.e. PGP Desktop) does this by default.
I suggest the attached (tested) patch be introduced to mutt to introduce this new behavior.
>How-To-Repeat:
Send a PGP/GPG encrypted mail to someone (without having 'encrypt-to' with your own GPG key in the gnupg.options file) and try to open the mail saved on the folder
>Fix:
The current work around to this issue is either adding 'encrypt-to YourKeyID' in gnupg.options or e-mailing to yourself (in Bcc: or To:) so that either a) gnupg encrypts to you by default or b) mutt tells gnupg to encrypt to you (adds you to the keylist)
}}}

--------------------------------------------------------------------------------
2005-10-29 10:33:31 UTC Paul Walker <paul@black-sun.demon.co.uk>
* Added comment:
{{{
On Fri, Oct 28, 2005 at 07:13:00PM +0200, New Mutt PR wrote:

> I don't see any reason for outgoing mail to not be encrypted to the
> sender's key so he is able to read it in the future and other software
> (i.e. PGP Desktop) does this by default.

Which is fine, but from a quick glance at your patch, I couldn't see how you
disable this. The user needs to be able to turn this off, if it's there.

-- 
Paul

"A lady came up to me on the street and pointed at my suede jacket. You
know a cow was murdered for that jacket?' she sneered.
I replied in a psychotic tone, 'I didn't know there were any witnesses.
Now I'll have to kill you too." --Jake Johansen
}}}

--------------------------------------------------------------------------------
2005-10-29 10:47:33 UTC Paul Walker <paul@black-sun.demon.co.uk>
* Added comment:
{{{
On Fri, Oct 28, 2005 at 07:40:13PM +0200, Javier Fern?ndez-Sanguino Pe?a wrote:

> > Which is fine, but from a quick glance at your patch, I couldn't see how you
> > disable this. The user needs to be able to turn this off, if it's there.
> There's no way. Just like there is no current way to disable encrypting to
> Bcc or CC: :-)

The difference is that Cc and Bcc don't intrinsically leave a copy of the
message in more or less the same place as the key. ;-)

In addition, if your patch is applied, the user has no way to make sure that
they cannot produce a record of the email's contents.

-- 
Paul

Now I have visions of engines mounted at 45 degree angles to the (normal)
direction of travel of the car.  Because, you know, 90 degree angles cause
bad ch'i and all that. -- Logan Shaw

--y0ulUmNC+osPPQO6
Content-Type: application/pgp-signature; name="signature.asc"
Content-Description: Digital signature
Content-Disposition: inline

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.2 (GNU/Linux)

iD8DBQFDYmQ1P9fOqdxRstoRApB4AJ4ua6VmN9KzL3upYMpP+l2TI+3kcwCffZsJ
ZrfdmnfNrjM1gSwfXtXg204=yK6V
-----END PGP SIGNATURE-----

--y0ulUmNC+osPPQO6--
}}}

--------------------------------------------------------------------------------
2005-10-29 12:40:13 UTC Javier Fernández-Sanguino Peña <jfs@computer.org>
* Added comment:
{{{
On Fri, Oct 28, 2005 at 06:33:31PM +0100, Paul Walker wrote:
> On Fri, Oct 28, 2005 at 07:13:00PM +0200, New Mutt PR wrote:
> 
> > I don't see any reason for outgoing mail to not be encrypted to the
> > sender's key so he is able to read it in the future and other software
> > (i.e. PGP Desktop) does this by default.
> 
> Which is fine, but from a quick glance at your patch, I couldn't see how you
> disable this. The user needs to be able to turn this off, if it's there.

There's no way. Just like there is no current way to disable encrypting to
Bcc or CC: :-)

Regards

Javier

--J/dobhs11T7y2rNN
Content-Type: application/pgp-signature; name="signature.asc"
Content-Description: Digital signature
Content-Disposition: inline

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.2 (GNU/Linux)

iD8DBQFDYmJ9sandgtyBSwkRAhtlAJ9m4DUH2o4xA1gCGXsi95QRUEW0IgCfe5sq
8DKIo0yQexWfKA9J1/sSNZg=jKxn
-----END PGP SIGNATURE-----

--J/dobhs11T7y2rNN--
}}}

--------------------------------------------------------------------------------
2005-11-26 15:51:53 UTC ab
* Added comment:
{{{
On Fri, Oct 28, 2005 at 07:13:00PM +0200, New Mutt PR wrote:
 
 
 
    Thanks Javier. It seems to me that all 3 of encrypting
to sender, to a special storage key, or to no self, are
valid expectations: User should have the choice. He should
also have the choice to hide his keyid. All this can be done
in options to $pgp_encrypt(_sign)_command, but I agree it's
not very practical, especially in dynamic configs, even more
in PGP/GPG heterogenous installations.

    So a way to set self-encryption easely would be usefull.
But it has to be configurable, and I agree with Michael that
for security reason the default should be no self, as today.

    Refiled as wishlist, tagged patch.
}}}

--------------------------------------------------------------------------------
2005-11-27 13:58:10 UTC Javier Fernández-Sanguino Peña <jfs@computer.org>
* Added comment:
{{{
On Fri, Nov 25, 2005 at 10:51:53PM +0100, Alain Bench wrote:
> Synopsis: Should encrypt messages to the Sender: key as well as to recipients
> 
> **** Comment added by ab on Fri, 25 Nov 2005 22:51:53 +0100 ****
>  
>     Thanks Javier. It seems to me that all 3 of encrypting
> to sender, to a special storage key, or to no self, are
> valid expectations: User should have the choice. He should
> also have the choice to hide his keyid. All this can be done
> in options to $pgp_encrypt(_sign)_command, but I agree it's
> not very practical, especially in dynamic configs, even more
> in PGP/GPG heterogenous installations.

Ok.

>     So a way to set self-encryption easely would be usefull.
> But it has to be configurable, and I agree with Michael that

I don't understand why it needs to be configurable and signing for Bcc is
not.

> for security reason the default should be no self, as today.

For what "security reasons"? If you want to mention privacy (or protection
from legal action), fine, but for "availability" (which is a security
concept) users would expect to have their saved mail encrypted against their
private key so that they are able to read it in the future. Not doing it
makes mutt fail the "principle of least astonishment", most people
will think that the mail will sent will be readable after for them, however,
it it will not. I've personally seen this mistake a few times on several
people that use mutt (me included) and I bet there are lots others.

As I said in my report, the default behaviour for other mail encryption
software (such as PGP) is to encrypt with your private key per default. I
suggest that be the default too for mutt.

Regards

Javier

--5vNYLRcllDrimb99
Content-Type: application/pgp-signature; name="signature.asc"
Content-Description: Digital signature
Content-Disposition: inline

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.2 (GNU/Linux)

iD8DBQFDiL5RsandgtyBSwkRAtaaAJ44JE5cvQicjXuorr4/IbRAavjnMQCfbMlP
635oPZm0iPDLhfmxTxZyTk0=9uQa
-----END PGP SIGNATURE-----

--5vNYLRcllDrimb99--
}}}

--------------------------------------------------------------------------------
2005-11-28 14:42:25 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
On Saturday, November 26, 2005 at 21:05:02 +0100,
 Javier Fern=E1ndez-Sanguino Pe=F1a wrote:

> On Fri, Nov 25, 2005 at 10:51:53PM +0100, Alain Bench wrote:
>> a way to set self-encryption easely would be usefull. But it has t=
o
>> be configurable
> I don't understand why it needs to be configurable and signing for =
Bcc
> is not.

    These are two different problems: The one can't justify the other=
.
Are you talking about bug/1090 (bcc:s given away through encryption)?


>> for security reason the default should be no self, as today.
> For what "security reasons"? If you want to mention privacy (or
> protection from legal action), fine

    Yes, and also making the sender an additional attackable target.


> but for "availability" (which is a security concept) users would
> expect to have their saved mail encrypted against their private key=
 so
> that they are able to read it in the future.

    Yes, some users expect and prefer self-encryption. Probably the
majority (?), as it's usefull and handy to have readable archives. Bu=
t
when user-friendliness and security fight for an option's default val=
ue,
security should win, I believe. That principle has been leading Mutt'=
s
and GnuPG's design in the past.


> mutt fail the "principle of least astonishment", most people will
> think that the mail will sent will be readable after for them

    Yes, you're probably right, and that's unfortunate. Also remember=
 by
default $record is empty, so no local copy of sent mail at all. Good
documentation could perhaps moderate the astonishment.


> I've personally seen this mistake a few times on several people tha=
t
> use mutt (me included) and I bet there are lots others.

    V'yy qral univat pbasrffrq vg, ohg lbh pna pbhag zr, ybat ntb. ;-=
)


Bye!=09Alain.
--=20
Followups to bug reports are public, and should go to bug-any@bugs.mu=
tt.org,
and the reporter (unless he is a member of mutt-dev). Do not CC mutt-=
dev mailing
list, the BTS does it already. Do not send to mutt-dev only, your wri=
tings would
not be tracked. Do not remove the "mutt/nnnn:" tag from subject.
}}}

--------------------------------------------------------------------------------
2007-03-27 18:07:25 UTC 
* Added attachment mutt_encrypt_from.diff
* Added comment:
mutt_encrypt_from.diff

--------------------------------------------------------------------------------
2009-06-30 14:55:45 UTC pdmef
* component changed to crypto
* Updated description:
{{{
Currently, GPG only encrypts messages to the To:, CC: or Bcc: but *not* to the From: of an email message. This means that any user that sends an encrypted mail to any other user will not be able to read the e-mail in the future in a default configuration (see workarounds for possible ways to prevent this)

I don't see any reason for outgoing mail to not be encrypted to the sender's key so he is able to read it in the future and other software (i.e. PGP Desktop) does this by default.

I suggest the attached (tested) patch be introduced to mutt to introduce this new behavior.
>How-To-Repeat:
Send a PGP/GPG encrypted mail to someone (without having 'encrypt-to' with your own GPG key in the gnupg.options file) and try to open the mail saved on the folder
>Fix:
The current work around to this issue is either adding 'encrypt-to YourKeyID' in gnupg.options or e-mailing to yourself (in Bcc: or To:) so that either a) gnupg encrypts to you by default or b) mutt tells gnupg to encrypt to you (adds you to the keylist)
}}}

--------------------------------------------------------------------------------
2016-08-25 06:39:52 UTC antonio@dyne.org
* Added comment:
Any chance you could consider this patch for inclusion?

--------------------------------------------------------------------------------
2017-06-14 22:42:21 UTC kevin8t8
* Added comment:
Changeset ca95f3e38355 added $pgp_self_encrypt, $pgp_self_encrypt_as, $smime_self_encrypt, $smime_self_encrypt_as configuration variables.  This should be roughly equivalent to the patch, except that it requires manual enabling and setting of the key desired.

* Updated description:
{{{
Currently, GPG only encrypts messages to the To:, CC: or Bcc: but *not* to the From: of an email message. This means that any user that sends an encrypted mail to any other user will not be able to read the e-mail in the future in a default configuration (see workarounds for possible ways to prevent this)
I don't see any reason for outgoing mail to not be encrypted to the sender's key so he is able to read it in the future and other software (i.e. PGP Desktop) does this by default.
I suggest the attached (tested) patch be introduced to mutt to introduce this new behavior.
>How-To-Repeat:
Send a PGP/GPG encrypted mail to someone (without having 'encrypt-to' with your own GPG key in the gnupg.options file) and try to open the mail saved on the folder
>Fix:
The current work around to this issue is either adding 'encrypt-to YourKeyID' in gnupg.options or e-mailing to yourself (in Bcc: or To:) so that either a) gnupg encrypts to you by default or b) mutt tells gnupg to encrypt to you (adds you to the keylist)
}}}
* resolution changed to fixed
* status changed to closed
