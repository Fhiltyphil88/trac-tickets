Ticket:  371
Status:  closed
Summary: Tagging with ~h over IMAP reloads mailbox headers *and* body

Reporter: tabor wells <twells@atg.com>
Owner:    mutt-dev

Opened:       2000-11-28 09:31:38 UTC
Last Updated: 2005-09-06 04:35:07 UTC

Priority:  trivial
Component: mutt
Keywords:  IMAP

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.3.11i
Severity: normal

-- Please type your report below this line

I'm using mutt to read mail via IMAP mailboxes. When I tag via "T" doing
something like '~h "search pattern"' mutt behaves, IMO the wrong way. With
~h it shouldn't need to fetch anything since the headers have already been
downloaded. But it does fetch from the server (although subsequent ~h searches
don't make any requests from the server). Also a ~h search fetches all 
headers and bodies of every message in the mailbox. If I'd used ~b I could 
see that this behavior would make sense. 

On a large mailbox, this is a significant performance hit that doesn't
seem necessary.

Thanks,

Tabor
twells@atg.com

-- Mutt Version Information

Mutt 1.3.11i (2000-11-01)
Copyright (C) 1996-2000 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: SunOS 5.6 [using slang 10402]
Compile options:
-DOMAIN
+DEBUG
-HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_GSS  -USE_SSL  -USE_SASL  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  -HAVE_START_COLOR  -HAVE_TYPEAHEAD  -HAVE_BKGDSET  
-HAVE_CURS_SET  -HAVE_META  -HAVE_RESIZETERM  
+HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  -HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  +ICONV_NONTRANS  +HAVE_GETSID  -HAVE_GETADDRINFO  
ISPELL="/usr/local/bin/ispell"
SENDMAIL="/usr/lib/sendmail"
MAILPATH="/var/mail"
SHAREDIR="/usr/local/share/mutt"
SYSCONFDIR="/usr/local/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.


--- Begin /users/twells/.muttrc
set allow_8bit		# never do Q-P encoding on legal 8-bit chars
unset arrow_cursor	# use -> instead of hiliting the whole line
set autoedit		# go to the editor right away when composing
set noconfirmappend	# don't ask me if i want to append to mailboxes
set confirmcreate	# ask me if i want to create new mailboxes
set edit_hdrs		# let me edit the message header when composing
set fast_reply		# skip initial prompts when replying
unset force_name  # save outgoing by recipient, create if mailbox doesn't exist
unset header		# include message header when replying
set help		# show the help lines
unset markers		# I don't want wrap characters
set nomime_fwd		# use message/rfc822 type to forward messages
set noprompt_after	# ask me for a command after the external pager exits
set resolve		# move to the next message when an action is performed
set reverse_alias	# attempt to look up my names for people
unset reverse_name	# set the from address as the one that came in
set nosave_empty	# remove files when no messages are left
set status_on_top	# some people prefer the status bar on top
unset tilde		# use virtual lines to pad blank lines in the pager
set abort_nosubject=no	# Sometimes I wanna send w/o a subject
set realname="Tabor J. Wells"	# Thats me ...
set attribution="On %d,\n%n <%a> is thought to have said:\n"
set charset="iso-8859-1"	# character set for your terminal
set hdr_format="%Z %3C %{%b %d} %-19.19L (%5c) %s"
set hostname="atg.com"	# my DNS domain
set indent_str="> "		# how to quote replied text
set quote_regexp="^ *[a-zA-Z]*[>:#}]"	# how to catch quoted text
set reply_regexp="^re:[ \t]*"   # how to identify replies in the subject:
set folder={pop.atg.com}		# where i keep my mailboxes
set spoolfile={pop.atg.com}INBOX
set imap_user=twells
set imap_checkinterval=30
set alias_file=~/.mutt-aliases	# where I keep my aliases
set editor="pico -t -z"		# editor to use when composing messages
set mailcap_path="${HOME}/.mailcap:/usr/local/share/mutt/mailcap:/etc/mailcap:/etc/mailcap:/usr/etc/mailcap:/usr/local/etc/mailcap"
set postponed=+postponed-msgs	# mailbox to store postponed messages in
set print_cmd=/bin/false	# how to print things (I like to save trees)
set record=+sent-mail		# save copies of outgoing messages in +outbox
set signature="~/.signature"	# file which contains my signature
set shell="/bin/tcsh"		# program to use for shell escapes
set tmpdir=~/tmp		# where to store temp files
set visual=vi			# editor invoked by ~v in the builtin editor
set include=ask-yes		# do I include the message in replies ?
set abort_unmodified=yes	# automatically abort replies if I don't
set copy=yes			# always save a copy of outgoing messages
set delete=yes			# purge deleted messages without asking
set include=ask-yes                # always include the message in replies
set move=no			# don't ask about moving messages, just do it
set recall=ask-yes	# when there are postponed messages, ask me to resume
set reply_to=ask-yes			# always use reply-to if present
set print=ask-yes		# ask me if I really want to print messages
set read_inc=50		# show progress when reading a mailbox
set timeout=400		# how long to wait before autochecking for new mail
set write_inc=50	# show progress while writing mailboxes
set pager=builtin		# some people prefer an external pager
set pager_context=1		# no. of lines of context to give when scrolling
set pager_format="%S %C/%m %-35.35f %s"	# format of the pager status bar
set sort_browser=alpha			# how to sort files in the dir browser
ignore   Return-Path: Received: Message-Id: X-Mailer: MIME-Version: \
         Content- Status: In-Reply-To: From Envelope-to: Delivery-date: \
         References: precedence: originator: errors-to: lines: \
         Resent- Approved Delivered
unignore from:
auto_view text/html application/x-X-sun-attachment
color hdrdefault brightcyan black
color quoted cyan black
color signature brightred black
color indicator brightyellow red
color indicator color9 black
color error brightred black
color status yellow blue
color status color11 color12
color tree brightmagenta black	# the thread tree in the index menu
color tilde brightmagenta black
color message brightcyan black
color message color9 black
color markers brightmagenta black
color normal brightwhite black
color attachment brightmagenta black
color search black green	# how to hilite search patterns in the pager
color header brightyellow black ^(From|Subject):
color body brightyellow black "(ftp|http)://[^ ]+"	# point out URLs
color body brightcyan black [-a-z_0-9.]+@[-a-z_0-9.]+	# e-mail addresses
color underline brightgreen black
mono header bold ^(From|Subject):
mono body bold "(ftp|http)://[^ ]+"	# point out URLs
mono body bold [-a-z_0-9.]+@[-a-z_0-9.]+	# e-mail addresses
bind index v display-message
bind index p previous-undeleted
bind index n next-undeleted
bind index ' ' next-page
bind index B bounce-message
bind index c mail
bind index - previous-page
bind index g change-folder
bind index w search
bind index / search
bind index x sync-mailbox
bind index a tag-prefix
bind index ; tag-entry
bind index <up> previous-entry
bind index <down> next-entry
bind index r group-reply
bind index R reply
bind index { top-page
bind index } bottom-page
bind index <insert> first-entry
bind index <delete> last-entry
bind index \177 last-entry
bind index \t next-unread
bind index Q quit
bind index d delete-message
bind pager p previous-undeleted
bind pager b previous-page
bind pager B bounce-message
bind pager n next-undeleted
bind pager ' ' next-page
bind pager - previous-page
bind pager w search
bind pager / search
bind pager <up> previous-line
bind pager <down> next-line
bind pager c mail
bind pager g change-folder
bind pager r group-reply
bind pager R reply
bind pager d delete-message
bind compose \cx send-message
bind compose \co postpone-message
bind pager G bottom	# just like vi and less
bind pager Q quit
bind editor <delete> backspace
my_hdr X-PGP-Fingerprint: 68 AF BA 9C 65 9E DF AD  FA 96 61 D5 F8 48 96 EA
my_hdr X-BOFH: Do you feel lucky?
my_hdr X-Jihad: I will hunt down all who spam my account. Try me.
my_hdr X-Spammer-Kill-Ratio: 75%
source ~/.muttrc-local	# config commands local
--- End /users/twells/.muttrc


--- Begin /usr/local/etc/Muttrc
ignore "from " received content- mime-version status x-status message-id
ignore sender references return-path lines
macro index \eb '/~b ' 'search in message bodies'
macro index \cb |urlview\n 'call urlview to extract URLs out of a message'
macro pager \cb |urlview\n 'call urlview to extract URLs out of a message'
macro generic <f1> "!less /usr/local/doc/mutt/manual.txt\n" "Show Mutt documentation"
macro index   <f1> "!less /usr/local/doc/mutt/manual.txt\n" "Show Mutt documentation"
macro pager   <f1> "!less /usr/local/doc/mutt/manual.txt\n" "Show Mutt documentation"
--- End /usr/local/etc/Muttrc


>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2000-11-29 01:43:43 UTC Brendan Cully <brendan@kublai.com>
* Added comment:
{{{
On Monday, 27 November 2000 at 22:31, tabor wells wrote:
> I'm using mutt to read mail via IMAP mailboxes. When I tag via "T" doing
> something like '~h "search pattern"' mutt behaves, IMO the wrong way. With
> ~~h it shouldn't need to fetch anything since the headers have already been
> downloaded. But it does fetch from the server (although subsequent ~h searches
> don't make any requests from the server). Also a ~h search fetches all 
> headers and bodies of every message in the mailbox. If I'd used ~b I could 
> see that this behavior would make sense. 
> 
> On a large mailbox, this is a significant performance hit that doesn't
> seem necessary.

Things could certainly be done more efficiently, but you seem to have
some misunderstanding about the fetch process. When opening the index,
mutt _does NOT_ fetch the entire headers of each message, only just
enough to display the index. So when you do a ~h search, mutt has to go
and get the rest of the headers (like X-Mailer, Sender, etc). If you
only want to search on things like from, to, and subject (which are
fetched immediately), please use ~f, ~t, ~s etc instead, and you'll get
much better performance.

I'd like to use the IMAP SEARCH command eventually, but this is the
semester from hell for me, so I haven't done any work on mutt since
about 1.3.7...

-Brendan
}}}

--------------------------------------------------------------------------------
2000-11-29 06:47:13 UTC "Tabor J. Wells" <twells@atg.com>
* Added comment:
{{{
On Tue, Nov 28, 2000 at 02:43:43PM -0500,
Brendan Cully <brendan@kublai.com> is thought to have said:

> Things could certainly be done more efficiently, but you seem to have
> some misunderstanding about the fetch process. When opening the index,
> mutt _does NOT_ fetch the entire headers of each message, only just
> enough to display the index. 

Ah. Thanks for the info. I see what you mean in my imapd debug logs.

> So when you do a ~h search, mutt has to go
> and get the rest of the headers (like X-Mailer, Sender, etc). If you
> only want to search on things like from, to, and subject (which are
> fetched immediately), please use ~f, ~t, ~s etc instead, and you'll get
> much better performance.

Makes sense, however ~h still should fetch only the headers not headers and
bodies.

> I'd like to use the IMAP SEARCH command eventually, but this is the
> semester from hell for me, so I haven't done any work on mutt since
> about 1.3.7...

Understandable. Thanks for the update.

Tabor

-- 
------------------------------------------------------------------------
Tabor J. Wells                                            twells@atg.com
Systems Administrator                                 
Art Technology Group                                  http://www.atg.com
}}}

--------------------------------------------------------------------------------
2005-09-06 22:35:07 UTC brendan
* Added comment:
{{{
IMAP server-side search added to CVS.
}}}

* resolution changed to fixed
* status changed to closed
