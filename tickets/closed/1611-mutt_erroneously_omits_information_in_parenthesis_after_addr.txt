Ticket:  1611
Status:  closed
Summary: mutt erroneously omits information in parenthesis after addresses in Cc headers

Reporter: Marco d'Itri <md@linux.it>
Owner:    mutt-dev

Opened:       2003-08-21 06:27:27 UTC
Last Updated: 2011-02-08 11:57:50 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.5.4-1
Severity: normal

[NOTE: this bug report has been submitted to the debian BTS as Bug#206364.
Please Cc all your replies to 206364@bugs.debian.org .]

From: Josip Rodin <joy@srce.hr>
Subject: mutt erroneously omits information in parenthesis after addresses in Cc headers
Date: Wed, 20 Aug 2003 13:06:30 +0200

Hi,

This set of headers:

From: owner@bugs.debian.org (Debian Bug Tracking System)
To: Marc Haber <mh+debian-bugs@zugschlus.de>
Cc: Bdale Garbee <bdale@gag.com> (sudo #89743),
    Anthony Towns <ajt@debian.org> (ifupdown #88945 #88946 #88947 #88948,...),
    Robert Luberda <robert@debian.org> (solid-pop3d #77710 #77711),
    Torsten Landschoff <torsten@debian.org> (tcpdump #75788),
    Daniel Jacobowitz <dan@debian.org> (bitchx #58192),
    Luigi Gangitano <luigi@debian.org> (squid #103130 #81667),
    Antonin Kral <A.Kral@sh.cvut.cz> (fmirror #70989 #75965 #78359),
    LaMont Jones <lamont@debian.org> (util-linux #63073),
    Mark Baker <mbaker@iee.org> (exim #59545 #95325),
    APT Development Team <deity@lists.debian.org> (apt #57795 #59723),
    Robert Woodcock <rcw@debian.org> (mtr #58610),
    Josip Rodin <joy-packages@debian.org> (gman #82376, lintian #111948),
    Jason Thomas <jason@debian.org> (grub #93537),
    Sean 'Shaleh' Perry <shaleh@debian.org> (anacron #63543),
    Matthew Vernon <matthew@debian.org> (ssh #84008 #84010),
    Steve Kemp <skx@debian.org> (checksecurity #59809)

Gets displayed in my mutt like this:

From: Debian Bug Tracking System <owner@bugs.debian.org>
To: Marc Haber <mh+debian-bugs@zugschlus.de>
Cc: Bdale Garbee <bdale@gag.com>, Anthony Towns <ajt@debian.org>,
        Robert Luberda <robert@debian.org>,
        Torsten Landschoff <torsten@debian.org>,
        Daniel Jacobowitz <dan@debian.org>,
        Luigi Gangitano <luigi@debian.org>,
        Antonin Kral <A.Kral@sh.cvut.cz>, LaMont Jones <lamont@debian.org>,
        Mark Baker <mbaker@iee.org>,
        APT Development Team <deity@lists.debian.org>,
        Robert Woodcock <rcw@debian.org>,
        Josip Rodin <joy-packages@debian.org>,
        Jason Thomas <jason@debian.org>,
        Sean 'Shaleh' Perry <shaleh@debian.org>,
        Matthew Vernon <matthew@debian.org>, Steve Kemp <skx@debian.org>

Valuable information in parenthesis is lost. Please don't do that. TIA.

-- 
     2. That which causes joy or happiness.


>How-To-Repeat:
	
>Fix:
}}}

--------------------------------------------------------------------------------
2003-08-21 06:27:27 UTC Marco d'Itri <md@linux.it>
* Added comment:
{{{
Package: mutt
Version: 1.5.4-1
Severity: normal

[NOTE: this bug report has been submitted to the debian BTS as Bug#206364.
Please Cc all your replies to 206364@bugs.debian.org .]

From: Josip Rodin <joy@srce.hr>
Subject: mutt erroneously omits information in parenthesis after addresses in Cc headers
Date: Wed, 20 Aug 2003 13:06:30 +0200

Hi,

This set of headers:

From: owner@bugs.debian.org (Debian Bug Tracking System)
To: Marc Haber <mh+debian-bugs@zugschlus.de>
Cc: Bdale Garbee <bdale@gag.com> (sudo #89743),
   Anthony Towns <ajt@debian.org> (ifupdown #88945 #88946 #88947 #88948,...),
   Robert Luberda <robert@debian.org> (solid-pop3d #77710 #77711),
   Torsten Landschoff <torsten@debian.org> (tcpdump #75788),
   Daniel Jacobowitz <dan@debian.org> (bitchx #58192),
   Luigi Gangitano <luigi@debian.org> (squid #103130 #81667),
   Antonin Kral <A.Kral@sh.cvut.cz> (fmirror #70989 #75965 #78359),
   LaMont Jones <lamont@debian.org> (util-linux #63073),
   Mark Baker <mbaker@iee.org> (exim #59545 #95325),
   APT Development Team <deity@lists.debian.org> (apt #57795 #59723),
   Robert Woodcock <rcw@debian.org> (mtr #58610),
   Josip Rodin <joy-packages@debian.org> (gman #82376, lintian #111948),
   Jason Thomas <jason@debian.org> (grub #93537),
   Sean 'Shaleh' Perry <shaleh@debian.org> (anacron #63543),
   Matthew Vernon <matthew@debian.org> (ssh #84008 #84010),
   Steve Kemp <skx@debian.org> (checksecurity #59809)

Gets displayed in my mutt like this:

From: Debian Bug Tracking System <owner@bugs.debian.org>
To: Marc Haber <mh+debian-bugs@zugschlus.de>
Cc: Bdale Garbee <bdale@gag.com>, Anthony Towns <ajt@debian.org>,
       Robert Luberda <robert@debian.org>,
       Torsten Landschoff <torsten@debian.org>,
       Daniel Jacobowitz <dan@debian.org>,
       Luigi Gangitano <luigi@debian.org>,
       Antonin Kral <A.Kral@sh.cvut.cz>, LaMont Jones <lamont@debian.org>,
       Mark Baker <mbaker@iee.org>,
       APT Development Team <deity@lists.debian.org>,
       Robert Woodcock <rcw@debian.org>,
       Josip Rodin <joy-packages@debian.org>,
       Jason Thomas <jason@debian.org>,
       Sean 'Shaleh' Perry <shaleh@debian.org>,
       Matthew Vernon <matthew@debian.org>, Steve Kemp <skx@debian.org>

Valuable information in parenthesis is lost. Please don't do that. TIA.

-- 
    2. That which causes joy or happiness.
}}}

--------------------------------------------------------------------------------
2009-06-19 21:30:32 UTC antonio@dyne.org
* Added comment:
this bug is still there

* version changed to 1.5.20

--------------------------------------------------------------------------------
2009-06-19 23:43:02 UTC agriffis
* Added comment:
Those don't look like legal address specs to me.  See http://tools.ietf.org/html/rfc5322#section-3.4

--------------------------------------------------------------------------------
2009-06-19 23:45:51 UTC agriffis
* Added comment:
Even as far back as RFC 822 it wasn't legal to put stuff after the <address>.  See http://tools.ietf.org/html/rfc822#section-6

--------------------------------------------------------------------------------
2009-06-20 00:18:41 UTC agriffis
* cc changed to 206364@bugs.debian.org, agriffis@n01se.net

--------------------------------------------------------------------------------
2009-06-20 03:15:37 UTC Derek Martin
* Added attachment part0001.pgp
* Added comment:
Added by email2trac

* Added comment:
{{{
On Fri, Jun 19, 2009 at 11:43:02PM -0000, Mutt wrote:

Keep reading.  See section 4.

Note in particular, in section 4, first paragraph:

    4. Obsolete Syntax

   Earlier versions of this specification allowed for different (usually
   more liberal) syntax than is allowed in this version.  Also, there
   have been syntactic elements used in messages on the Internet whose
   interpretations have never been documented.  Though these syntactic
   forms MUST NOT be generated according to the grammar in section 3,
   they MUST be accepted and parsed by a conformant receiver.  This
   section documents many of these syntactic elements.  Taking the
   grammar in section 3 and adding the definitions presented in this
   section will result in the grammar to use for the interpretation of
   messages.

See also section 4.4, and RFC 822.

Sigh.
}}}

--------------------------------------------------------------------------------
2009-06-20 03:37:07 UTC Derek Martin
* Added attachment part0001.2.pgp
* Added comment:
Added by email2trac

* Added comment:
{{{
On Fri, Jun 19, 2009 at 10:15:25PM -0500, Derek Martin wrote:

Actually I read the spec much too quickly, and got it totally wrong.
It's explicitly allowed in RFC 5322, in section 3.4 that you point to.

  3.4. Address Specification


     Addresses occur in several message header fields to indicate
     senders and recipients of messages.  An address may either be an
     individual mailbox, or a group of mailboxes.

     address         =   mailbox / group

     mailbox         =   name-addr / addr-spec

     name-addr       =   [display-name] angle-addr

     angle-addr      =   [CFWS] "<" addr-spec ">" [CFWS] /
                         obs-angle-addr

See section 3.2.2 for an explanation of [CFWS].  

It is likewise explicitly allowed in RFCs 2822, and 822, and 733.

Note that 822 and 733 are standards; the more recent ones are not (not
that it matters in this case).
}}}

--------------------------------------------------------------------------------
2009-06-20 10:09:53 UTC vinc17
* Added comment:
Replying to [comment:7 Derek Martin]:
> It is likewise explicitly allowed in RFCs 2822, and 822, and 733.
> 
> Note that 822 and 733 are standards; the more recent ones are not (not
> that it matters in this case).

No, RFC 2822 is a ''proposed standard'' therefore '''is a standard'''. From RFC 4677:
{{{
There are six kinds of RFCs:

   o  Proposed standards

   o  Draft standards

   o  Internet standards (sometimes called "full standards")

   o  Informational documents

   o  Experimental protocols

   o  Historic documents

   Only the first three (proposed, draft, and full) are standards within
   the IETF.
}}}

--------------------------------------------------------------------------------
2010-08-05 22:44:15 UTC me
* Added comment:
This is the purpose of the --enable-exact-address option to configure.  It is not enabled by default because it uses a lot of extra memory and most people don't really care since the commented form of the RFC2822 address is rarely used.

* Updated description:
{{{
Package: mutt
Version: 1.5.4-1
Severity: normal

[NOTE: this bug report has been submitted to the debian BTS as Bug#206364.
Please Cc all your replies to 206364@bugs.debian.org .]

From: Josip Rodin <joy@srce.hr>
Subject: mutt erroneously omits information in parenthesis after addresses in Cc headers
Date: Wed, 20 Aug 2003 13:06:30 +0200

Hi,

This set of headers:

From: owner@bugs.debian.org (Debian Bug Tracking System)
To: Marc Haber <mh+debian-bugs@zugschlus.de>
Cc: Bdale Garbee <bdale@gag.com> (sudo #89743),
    Anthony Towns <ajt@debian.org> (ifupdown #88945 #88946 #88947 #88948,...),
    Robert Luberda <robert@debian.org> (solid-pop3d #77710 #77711),
    Torsten Landschoff <torsten@debian.org> (tcpdump #75788),
    Daniel Jacobowitz <dan@debian.org> (bitchx #58192),
    Luigi Gangitano <luigi@debian.org> (squid #103130 #81667),
    Antonin Kral <A.Kral@sh.cvut.cz> (fmirror #70989 #75965 #78359),
    LaMont Jones <lamont@debian.org> (util-linux #63073),
    Mark Baker <mbaker@iee.org> (exim #59545 #95325),
    APT Development Team <deity@lists.debian.org> (apt #57795 #59723),
    Robert Woodcock <rcw@debian.org> (mtr #58610),
    Josip Rodin <joy-packages@debian.org> (gman #82376, lintian #111948),
    Jason Thomas <jason@debian.org> (grub #93537),
    Sean 'Shaleh' Perry <shaleh@debian.org> (anacron #63543),
    Matthew Vernon <matthew@debian.org> (ssh #84008 #84010),
    Steve Kemp <skx@debian.org> (checksecurity #59809)

Gets displayed in my mutt like this:

From: Debian Bug Tracking System <owner@bugs.debian.org>
To: Marc Haber <mh+debian-bugs@zugschlus.de>
Cc: Bdale Garbee <bdale@gag.com>, Anthony Towns <ajt@debian.org>,
        Robert Luberda <robert@debian.org>,
        Torsten Landschoff <torsten@debian.org>,
        Daniel Jacobowitz <dan@debian.org>,
        Luigi Gangitano <luigi@debian.org>,
        Antonin Kral <A.Kral@sh.cvut.cz>, LaMont Jones <lamont@debian.org>,
        Mark Baker <mbaker@iee.org>,
        APT Development Team <deity@lists.debian.org>,
        Robert Woodcock <rcw@debian.org>,
        Josip Rodin <joy-packages@debian.org>,
        Jason Thomas <jason@debian.org>,
        Sean 'Shaleh' Perry <shaleh@debian.org>,
        Matthew Vernon <matthew@debian.org>, Steve Kemp <skx@debian.org>

Valuable information in parenthesis is lost. Please don't do that. TIA.

-- 
     2. That which causes joy or happiness.


>How-To-Repeat:
	
>Fix:
}}}
* milestone changed to 
* resolution changed to invalid
* status changed to closed

--------------------------------------------------------------------------------
2010-08-05 23:45:19 UTC Gregor Zattler
* Added comment:
{{{
Hi mutt developers and package maintainer,,
* Mutt <fleas@mutt.org> [05. Aug. 2010]:
[... for actual bug report see thread ..]

Then this option should be enabled by default, at least for Debian?

Ciao, Gregor

 
}}}

--------------------------------------------------------------------------------
2011-02-08 11:57:50 UTC KiBi
* Added comment:
Hi,

I'm wondering whether you would welcome a patch which would make it a runtime configuration option? This way, users could turn it on at their discretion (instead of having to keep a locally-built mutt).

!KiBi.
