Ticket:  3213
Status:  closed
Summary: realname option does not accept apostrophe

Reporter: mogorman
Owner:    mutt-dev

Opened:       2009-04-02 22:24:12 UTC
Last Updated: 2009-04-03 02:44:34 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
my name is Matthew O'Gorman but mutt cant seem to understand that escaping it with \ doesnt work. it just says invalid option Gorman. bug has existed for some time im currently running debian
Mutt 1.5.18 (2008-05-17)


--------------------------------------------------------------------------------
2009-04-03 00:58:46 UTC agriffis
* Added comment:
This works fine for me:

{{{
set realname="Matthew O'Gorman"
}}}

--------------------------------------------------------------------------------
2009-04-03 01:58:44 UTC mogorman
* Added comment:
hmm you are correct i guess i stated problem wrong
folder-hook Lists set realname="Matt O'Gorman"

i use folder hooks as in some accounts i just put mog and in some i put full name, and didn't realize that.

--------------------------------------------------------------------------------
2009-04-03 02:39:58 UTC agriffis
* Added comment:
The quotes are being parsed when the folder-hook is read.  You need this:

{{{
folder-hook Lists "set realname=\"Matt O'Gorman\""
}}}

* resolution changed to invalid
* status changed to closed

--------------------------------------------------------------------------------
2009-04-03 02:44:34 UTC mogorman
* Added comment:
thanks good to know
