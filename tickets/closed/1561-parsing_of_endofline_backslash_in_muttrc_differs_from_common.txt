Ticket:  1561
Status:  closed
Summary: parsing of end-of-line backslash in muttrc differs from common shells' behaviour

Reporter: manuel@samper.dyndns.org
Owner:    mutt-dev

Opened:       2003-05-07 12:49:28 UTC
Last Updated: 2005-10-07 16:28:12 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.5.4-1
Severity: normal

-- Please type your report below this line

While composing some complex macros, I splitted it in several lines,
e.g.:

  macro x key '\
  <comand>\
  more_comands'

And in the process, I commented out some things... and this arise. Test
the following in a muttrc:

  # whatever \
  macro index \Cw '<limit>~F<enter>'
  macro index \Cy '<limit>~F<enter>'

And pressing ^W in the index gives a "Key is not bound.  Press '?' for help."
error:

  ^U           undelete-thread             undelete all messages in thread
  ^Y           macro                       <limit>~F<enter>
  <Esc><Tab>   previous-new-then-unread    jump to the previous new or ...

Yes, there is no ^W macro at all, the backslash at the end of the
comment is "eating" the first macro... or whatever is here until the
first newline:

  # hello \
  this don't raise any error!


-- System Information
{ doesn't matter here }
>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2005-08-25 14:14:20 UTC rado
* Added comment:
{{{
Questionable whether this is a bug or a feature
(although definitely not sufficiently documented!).
}}}

* status changed to new

--------------------------------------------------------------------------------
2005-08-25 14:14:21 UTC rado
* Added comment:
{{{
It stands to be discussed whether this is a bug or a feature.

I don't know about the history of/ reasoning behind this, but being able to un-/comment a complex
multi-liner with a single # can be handy if you know that it works that way.

On the other hand, SHELLs' behaviour is known to many people and they might expect "conformity",
which could be avoided if difference were well documented.

As long as this isn't changed it should at least be prominently mentionend in the manual!
}}}

--------------------------------------------------------------------------------
2005-09-23 09:09:08 UTC rado
* Added comment:
{{{
raised prio
}}}

--------------------------------------------------------------------------------
2005-10-08 10:28:12 UTC rado
* Added comment:
{{{
no input from mutt-dev, will stay as it is.
}}}

* resolution changed to wontfix
* status changed to closed
