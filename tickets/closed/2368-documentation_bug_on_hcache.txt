Ticket:  2368
Status:  closed
Summary: documentation bug on hcache

Reporter: ckillian@cs.ucsd.edu
Owner:    mutt-dev

Opened:       2006-07-20 06:02:43 UTC
Last Updated: 2006-08-04 00:47:38 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
document says to use --enable--hcache to enable header caching.  Should be --enable-hcache
>How-To-Repeat:
>Fix:
Index: manual.xml.head
===================================================================
RCS file: /cvsroots/mutt/doc/manual.xml.head,v
retrieving revision 3.19
diff -r3.19 manual.xml.head
4508c4508
< <emphasis>--enable--hcache</emphasis> option. It's not turned on
---
> <emphasis>--enable-hcache</emphasis> option. It's not turned on
}}}

--------------------------------------------------------------------------------
2006-08-04 18:47:38 UTC brendan
* Added comment:
{{{
Patch applied, thanks.
}}}

* resolution changed to fixed
* status changed to closed
