Ticket:  1945
Status:  closed
Summary: change mtime of folder for deleleted msgs, too, not only for new/ changed msgs.

Reporter: Dan Jacobson <jidanni@jidanni.org>
Owner:    mutt-dev

Opened:       2004-12-29 00:34:14 UTC
Last Updated: 2005-10-07 15:00:44 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.5.6-20040907
Severity: wishlist

It would be best if
$ mutt -f somebox
changed the mtime of somebox.
Even though we delete messages, after we quit, ls -l still shows
the previous modification time, even though the size shrunk.


>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2004-12-29 00:34:14 UTC Dan Jacobson <jidanni@jidanni.org>
* Added comment:
{{{
Package: mutt
Version: 1.5.6-20040907
Severity: wishlist

It would be best if
$ mutt -f somebox
changed the mtime of somebox.
Even though we delete messages, after we quit, ls -l still shows
the previous modification time, even though the size shrunk.
}}}

--------------------------------------------------------------------------------
2005-08-28 10:44:52 UTC rado
* Added comment:
{{{
clearer subject, raised importance.

This behaviour doesn't apply only to cmd-line -f, but within mutt, too.
}}}

--------------------------------------------------------------------------------
2005-09-23 09:43:32 UTC rado
* Added comment:
{{{
Rado was sloppy when working on the first batch of reports,
sorry.
}}}

* status changed to new

--------------------------------------------------------------------------------
2005-09-23 09:43:33 UTC rado
* Added comment:
{{{
Now that I spent more time thinking about this,
changing mtime would collide with mutt's detection of new mail:
you'd get false positive "new mail" for deleting mails.
As much as I personally would like to have such a thing
work next to "new mail" detection, I fear it can't work.
}}}

--------------------------------------------------------------------------------
2005-10-08 09:00:44 UTC rado
* Added comment:
{{{
conflict with "new mail" detection; couldn't discern correct
flagging.
}}}

* resolution changed to fixed
* status changed to closed
