Ticket:  3848
Status:  closed
Summary: The soft hyphen character confuses various terminals

Reporter: vinc17
Owner:    mutt-dev

Opened:       2016-07-01 22:37:07 UTC
Last Updated: 2016-07-07 16:22:39 UTC

Priority:  major
Component: display
Keywords:  

--------------------------------------------------------------------------------
Description:
I've received a mail with a U+00AD SOFT HYPHEN character in Subject, and it confused the terminal. I've done tests with 5 different terminals, and there are display issues in 4 of them (major issues with GNU screen).

I think that the right solution would be to ignore this character since Mutt/ncurses (correctly) expects that it is not displayed.

Test case: attached mbox file, where the muttrc file should be:
{{{
set arrow_cursor
color normal white black
color index brightwhite black .
color header brightwhite blue ^subject:
}}}

Details about the different behaviors:

* In xterm, everything seems to be OK in the index menu. But the soft hyphen is visible. An additional character is output at column 81 (with a 80-column terminal), leaving a glitch, which seems to correspond to the following more general bug: https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=738794

  So, it seems that Mutt/ncurses expects that this character is not visible. When viewing the message and reducing the terminal width to 69 columns, the "s" at the end of "vidéos" is replaced by a space.

* In rxvt, everything seems to be OK. The soft hyphen is not visible, which is the expected behavior.

* In GNOME Terminal, everything seems to be OK in the index menu. But the soft hyphen is displayed as a space. This breaks word-wrapping when viewing the message with a 68-column terminal, where "vidéos" is split between the "o" and the "s". And when going back to the index menu, the "s" remains visible.

* In mlterm, the soft hyphen is visible, but this yields some display issues with the blue background (from muttrc settings), and mlterm has the same word-wrapping issue as GNOME Terminal.

* In GNU screen in xterm, there are major display issues. In particular, the first message in the initial index menu is not visible, and the second line is not correctly aligned: the spaces for the arrow cursor are missing.

--------------------------------------------------------------------------------
2016-07-01 22:38:53 UTC vinc17
* Added attachment disp-screen.png
* Added comment:
initial index menu with GNU screen in xterm

--------------------------------------------------------------------------------
2016-07-01 22:44:55 UTC vinc17
* Added comment:
Replying to [ticket:3848 vinc17]:
> I think that the right solution would be to ignore this character since Mutt/ncurses (correctly) expects that it is not displayed.

It should be ignored, not just in the headers, but also in the body (I haven't any test there, but it's obvious that it would have similar issues, in particular with word-wrapping).

--------------------------------------------------------------------------------
2016-07-05 23:14:28 UTC kevin8t8
* Added comment:
The attached patch seems to fix the problem for me.  If it looks okay with you I'll push it up.

--------------------------------------------------------------------------------
2016-07-05 23:14:54 UTC kevin8t8
* Added attachment ticket-3848.patch

--------------------------------------------------------------------------------
2016-07-07 00:14:55 UTC vinc17
* Added attachment mbox
* Added comment:
test case (mbox file)

--------------------------------------------------------------------------------
2016-07-07 00:16:13 UTC vinc17
* Added comment:
Replying to [comment:2 kevin8t8]:
> The attached patch seems to fix the problem for me.  If it looks okay with you I'll push it up.

Yes, it is OK. Thanks. I've also updated the mbox file to test the body.

--------------------------------------------------------------------------------
2016-07-07 16:22:39 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [changeset:"5229c7fbc37e4dd6b2715443dde39114cb7b1715" 6716:5229c7fbc37e]:
{{{
#!CommitTicketReference repository="" revision="5229c7fbc37e4dd6b2715443dde39114cb7b1715"
Filter soft hypen from pager and headers.  (closes #3848)

Add U+00AD SOFT HYPHEN to the filtered characters in headers and the
pager.  In some terminals and situations it causes major display problems.
}}}

* resolution changed to fixed
* status changed to closed
