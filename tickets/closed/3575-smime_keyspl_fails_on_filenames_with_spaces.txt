Ticket:  3575
Status:  closed
Summary: smime_keys.pl fails on filenames with spaces

Reporter: Y_Plentyn
Owner:    mutt-dev

Opened:       2012-04-22 14:23:25 UTC
Last Updated: 2015-05-18 20:29:02 UTC

Priority:  minor
Component: crypto
Keywords:  

--------------------------------------------------------------------------------
Description:
Forwarding from  http://bugs.debian.org/658324 :

{{{
Dear Maintainer,

I recently needed to update by smime-keyring with a new version of my own
certificate to a new one, and saved it to a file named
"cacert kandre@ak-online.be renewed.p12".

Unfortunately 'smime_keys add_p12 "cacert kandre@ak-online.be renewed.p12"'
failed with the return code 256 from openssl and a dump of it's commandline
options.

I looked into smime_keys and found that the perl-script builds the command
in line 156 directly from $ARGV[1]:

>>>> snip <<<<
156:    my $cmd = "$opensslbin pkcs12 -in $ARGV[1] -out $pem_file";
157:        system $cmd and die "'$cmd' returned $?";
>>>> snip <<<<

The error-message also contained the exact commandline it tried to execute
which was:
>>>> snip <<<<
'/usr/bin/openssl pkcs12 -in cacert kandre@ak-online.be renewed.p12 -out
cacert kandre@ak-online.be renewed.p12.pem' returned 256 at
/usr/bin/smime_keys line 157.
>>>> snip <<<<

As there were no quotation marks around the filename I tried it with a
symlink to the same file, but without any space in the filename and it
worked like a charm.

I therefore think that there is some kind of escaping of $ARGV[1] neccesary
to stop smime_keys from passing the spaces directly to the shell that
executes openssl.

Kind regards, Andre
}}}

--------------------------------------------------------------------------------
2013-01-01 05:03:58 UTC me
* Added comment:
I've attached a patch which I believe will solve this issue.

* Updated description:
Forwarding from  http://bugs.debian.org/658324 :

{{{
Dear Maintainer,

I recently needed to update by smime-keyring with a new version of my own
certificate to a new one, and saved it to a file named
"cacert kandre@ak-online.be renewed.p12".

Unfortunately 'smime_keys add_p12 "cacert kandre@ak-online.be renewed.p12"'
failed with the return code 256 from openssl and a dump of it's commandline
options.

I looked into smime_keys and found that the perl-script builds the command
in line 156 directly from $ARGV[1]:

>>>> snip <<<<
156:    my $cmd = "$opensslbin pkcs12 -in $ARGV[1] -out $pem_file";
157:        system $cmd and die "'$cmd' returned $?";
>>>> snip <<<<

The error-message also contained the exact commandline it tried to execute
which was:
>>>> snip <<<<
'/usr/bin/openssl pkcs12 -in cacert kandre@ak-online.be renewed.p12 -out
cacert kandre@ak-online.be renewed.p12.pem' returned 256 at
/usr/bin/smime_keys line 157.
>>>> snip <<<<

As there were no quotation marks around the filename I tried it with a
symlink to the same file, but without any space in the filename and it
worked like a charm.

I therefore think that there is some kind of escaping of $ARGV[1] neccesary
to stop smime_keys from passing the spaces directly to the shell that
executes openssl.

Kind regards, Andre
}}}
* status changed to infoneeded_new

--------------------------------------------------------------------------------
2013-01-01 05:05:58 UTC me
* Added attachment smime_keys_quote_filename
* Added comment:
proposed fix

--------------------------------------------------------------------------------
2015-05-15 21:13:12 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [c66a6bd5d0d57eca9fba0a90b34c9adab0c0ce27]:
{{{
#!CommitTicketReference repository="" revision="c66a6bd5d0d57eca9fba0a90b34c9adab0c0ce27"
smime_keys: quote filenames. (closes #3575) (see #2456)

Wrap the various filename parameters in single quotes in case there are
spaces in the filename.
}}}

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2015-05-15 22:06:20 UTC danf
* Added comment:
What if the filename contains single quotes instead of spaces?

--------------------------------------------------------------------------------
2015-05-15 22:24:35 UTC kevin8t8
* Added comment:
If it contains quotes, then obviously it will blow up, as it did before.  The best way around that is to use system() instead.  I'm not done cleaning up the script, and perhaps will do that too.

--------------------------------------------------------------------------------
2015-05-15 22:45:43 UTC kevin8t8
* Added comment:
Err... or was it open with a pipe (since we have to capture the output).  I have to look again. :-)

--------------------------------------------------------------------------------
2015-05-16 08:28:22 UTC vinc17
* Added comment:
Yes, if you want to capture the output, it is the form:
{{{
open FILE, "-|", $cmd, @args
}}}

--------------------------------------------------------------------------------
2015-05-18 20:29:02 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [577987ca2d02d898880a19f7fd0ee3e7baa41798]:
{{{
#!CommitTicketReference repository="" revision="577987ca2d02d898880a19f7fd0ee3e7baa41798"
smime_keys: Convert openssl execution to use open("-|",...). (see #3575) (see #2456)

This does a fork/exec, bypassing the shell, and so handles
spaces, quotes, and other shell-characters problems better than the
simple fix in changeset:c66a6bd5d0d5

This also fixes the "verify with crl" bug in #2456: the grep is now done
in perl.

Thank you Vincent Lefevre for your review and feedback!
}}}
