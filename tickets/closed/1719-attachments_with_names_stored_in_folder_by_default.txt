Ticket:  1719
Status:  closed
Summary: attachments with =names stored in $folder by default

Reporter: Björn Jacke <bjacke@lisa.goe.net>
Owner:    mutt-dev

Opened:       2003-11-30 17:23:04 UTC
Last Updated: 2008-01-15 16:39:53 UTC

Priority:  minor
Component: mutt
Keywords:  #1766 #351890

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.4.1i
Severity: normal

-- Please type your report below this line

when I have not set rfc2047_parameters and want to save an attachment named
like =?iso-8859-1?Q?file=5F=E4=5F991116=2Ezip?= this is saved in $folder
instead of the current working directory. When rfc2047_parameters is set it is
written to cwd.


-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/lib/gcc-lib/i586-suse-linux/3.3.1/specs
Konfiguriert mit: ../configure --enable-threads=posix --prefix=/usr --with-local-prefix=/usr/local --infodir=/usr/share/info --mandir=/usr/share/man --libdir=/usr/lib --enable-languages=c,c++,f77,objc,java,ada --disable-checking --enable-libgcj --with-gxx-include-dir=/usr/include/g++ --with-slibdir=/lib --with-system-zlib --enable-shared --enable-__cxa_atexit i586-suse-linux
Thread model: posix
gcc-Version 3.3.1 (SuSE Linux)

- CFLAGS
-Wall -pedantic -Wall -O2 -march=i586 -mcpu=i686 -fmessage-length=0 -I. -D_GNU_SOURCE

-- Mutt Version Information

Mutt 1.4.1i (2003-03-19)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.21-144-default (i586) [using ncurses 5.3]
Einstellungen bei der Compilierung:
-DOMAIN
-DEBUG
-HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_GSS  +USE_SSL  +USE_SASL  +USE_SASL2  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+HAVE_PGP  +BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +COMPRESSED  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  +HAVE_GETADDRINFO  
-ISPELL
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
Um die Entwickler zu kontaktieren, schicken Sie bitte
eine Nachricht (in englisch) an <mutt-dev@mutt.org>.
Um einen Bug zu melden, verwenden Sie bitte das Programm flea(1).

patch-1.4.1.rr.compressed.1


>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2003-11-30 18:17:16 UTC Bjoern Jacke <bjacke@lisa.goe.net>
* Added comment:
{{{
On 2003-11-30 at 00:23 +0100 Bjoern Jacke sent off:
>when I have not set rfc2047_parameters and want to save an attachment named
>like =?iso-8859-1?Q?file=5F=E4=5F991116=2Ezip?= this is saved in $folder
>instead of the current working directory. When rfc2047_parameters is set it is
>written to cwd.

eg, yes, it's written into $folder because mutt interprets the leading 
= as abbeviation for $folder. Shouldn't mutt per default escape all 
such special characters when they are at the beginning of a 
to-be-saved filename?

For example, when I have an attachment named 
=?iso-8859-1?Q?file=5F=E4=5F991116=2Ezip?=

mutt should suggest to write it to

\=?iso-8859-1?Q?file=5F=E4=5F991116=2Ezip?=

and not in

=?iso-8859-1?Q?file=5F=E4=5F991116=2Ezip?=

comments?
}}}

--------------------------------------------------------------------------------
2005-11-02 11:27:04 UTC ab
* Added comment:
{{{
retitle s/broken/=/
crossref closed dupe /1766
add /1766 reporter to notifieds
deduppe unform
}}}

--------------------------------------------------------------------------------
2006-02-24 00:43:01 UTC ab
* Added comment:
{{{
add to notifieds Tobias, the reporter of Debian Bug#351890
"dangerous handling of attachment filenames". Reference
#351890.
}}}

--------------------------------------------------------------------------------
2007-04-10 04:43:34 UTC brendan
* Updated description:
{{{
Package: mutt
Version: 1.4.1i
Severity: normal

-- Please type your report below this line

when I have not set rfc2047_parameters and want to save an attachment named
like =?iso-8859-1?Q?file=5F=E4=5F991116=2Ezip?= this is saved in $folder
instead of the current working directory. When rfc2047_parameters is set it is
written to cwd.


-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/lib/gcc-lib/i586-suse-linux/3.3.1/specs
Konfiguriert mit: ../configure --enable-threads=posix --prefix=/usr --with-local-prefix=/usr/local --infodir=/usr/share/info --mandir=/usr/share/man --libdir=/usr/lib --enable-languages=c,c++,f77,objc,java,ada --disable-checking --enable-libgcj --with-gxx-include-dir=/usr/include/g++ --with-slibdir=/lib --with-system-zlib --enable-shared --enable-__cxa_atexit i586-suse-linux
Thread model: posix
gcc-Version 3.3.1 (SuSE Linux)

- CFLAGS
-Wall -pedantic -Wall -O2 -march=i586 -mcpu=i686 -fmessage-length=0 -I. -D_GNU_SOURCE

-- Mutt Version Information

Mutt 1.4.1i (2003-03-19)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.21-144-default (i586) [using ncurses 5.3]
Einstellungen bei der Compilierung:
-DOMAIN
-DEBUG
-HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_GSS  +USE_SSL  +USE_SASL  +USE_SASL2  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+HAVE_PGP  +BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +COMPRESSED  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  +HAVE_GETADDRINFO  
-ISPELL
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
Um die Entwickler zu kontaktieren, schicken Sie bitte
eine Nachricht (in englisch) an <mutt-dev@mutt.org>.
Um einen Bug zu melden, verwenden Sie bitte das Programm flea(1).

patch-1.4.1.rr.compressed.1


>How-To-Repeat:
>Fix:
}}}
* milestone changed to 1.6

--------------------------------------------------------------------------------
2008-01-15 16:39:53 UTC pdmef
* Added comment:
(In [b3cde588fe9a]) Prepend './' to (some) suggested filenames when saving attachments.
Attachment filenames may start with characters that are special to
mutt_expand_path() may cause undesired expansion. Closes #1719.

* resolution changed to fixed
* status changed to closed
