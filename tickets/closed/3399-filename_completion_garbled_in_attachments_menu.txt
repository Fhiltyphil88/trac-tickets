Ticket:  3399
Status:  closed
Summary: filename completion garbled in attachments menu

Reporter: balderdash
Owner:    mutt-dev

Opened:       2010-03-31 20:39:13 UTC
Last Updated: 2010-03-31 22:00:50 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
I just build from todays tip ("avoid bufferflow" was most recent change).  Instantly, strange behavior.

Compose a new message.  Exit editor.  Hit 'a' to be prompted for a file to attach.  Hit the up arrow to bring up the last one I typed in.  Result: the string "OA" appears.   If I hit the down arrow, "OB" appears.  Seems like some kind of encoding problem, but everything worked fine until the latest tip, and I haven't touched my terminal settings.

This bug happens on both Terminal.app and iTerm.app



--------------------------------------------------------------------------------
2010-03-31 20:42:52 UTC brendan
* Added comment:
Sounds like you're on a Mac. Did you also update to OS X 10.6.3 recently? I've heard it broke ncurses. Try building against something like fink's ncurses, if possible.

--------------------------------------------------------------------------------
2010-03-31 21:22:51 UTC balderdash
* Added comment:
Replying to [comment:1 brendan]:
> Sounds like you're on a Mac. Did you also update to OS X 10.6.3 recently? I've heard it broke ncurses. Try building against something like fink's ncurses, if possible.

Yes, I am on a Mac, and did upgrade to 10.6.3.  Will try to find  a fix.

--------------------------------------------------------------------------------
2010-03-31 21:36:26 UTC balderdash
* Added comment:
Replying to [comment:2 balderdash]:
> Replying to [comment:1 brendan]:
> > Sounds like you're on a Mac. Did you also update to OS X 10.6.3 recently? I've heard it broke ncurses. Try building against something like fink's ncurses, if possible.
> 
> Yes, I am on a Mac, and did upgrade to 10.6.3.  Will try to find  a fix.

I saw a tip on some webpage to "revert to 10.6.2's version of /usr/lib/libncurses.5.4.dylib".  I did this (or at least, I think I did) by diving into /usr/lib on my store-bought Snow Leopard DVD and copying the file from there into my real /usr/lib.  I then changed owner and group to match the other things in that directory, and now filename completion in mutt seems to work again.

--------------------------------------------------------------------------------
2010-03-31 21:48:28 UTC brendan
* Added comment:
Closing as not a mutt bug.

* resolution changed to invalid
* status changed to closed

--------------------------------------------------------------------------------
2010-03-31 22:00:50 UTC Thomas Dickey
* Added comment:
{{{
On Wed, 31 Mar 2010, Mutt wrote:


http://www.opensource-archive.org/showthread.php?p=199981

(looks like a packaging problem rather than ncurses-specific)
}}}
