Ticket:  3043
Status:  closed
Summary: pressing 'y' after starting 'mutt -y' exits

Reporter: faxm0dem
Owner:    mutt-dev

Opened:       2008-04-02 11:24:38 UTC
Last Updated: 2008-04-03 18:49:03 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
Run mutt using 'mutt -y'
press 'y' while in mutt

mutt quits immediately returning 0

tested using trivial config file:

$ cat .mutt/muttrc 
mailboxes ~/Mail
$ 

--------------------------------------------------------------------------------
2008-04-03 00:00:52 UTC Michael Tatge
* Added comment:
{{{
* On Wed, Apr 02, 2008 Mutt wrote:
> #3043: pressing 'y' after starting 'mutt -y' exits
> 
>  Run mutt using 'mutt -y'
>  press 'y' while in mutt
> 
>  mutt quits immediately returning 0

I only get "Key is not bound as expected.

Michael
}}}

--------------------------------------------------------------------------------
2008-04-03 06:56:24 UTC 
* Added comment:
{{{
On Thu, Apr 03, 2008 at 12:01:17AM -0000, Mutt wrote:
> #3043: pressing 'y' after starting 'mutt -y' exits
> 
> Comment (by Michael Tatge):
> 
>  {{{
>  * On Wed, Apr 02, 2008 Mutt wrote:
>  > #3043: pressing 'y' after starting 'mutt -y' exits
>  >
>  >  Run mutt using 'mutt -y'
>  >  press 'y' while in mutt
>  >
>  >  mutt quits immediately returning 0
> 
>  I only get "Key is not bound as expected.
> 
>  Michael
>  }}}

hmmm strange.
I made sure to use a blank configuration screen.
Maybe it's debian's default key bindings?

||/ Name           Version        Description
+++-==============-==============-============================================
ii  mutt           1.5.17+2008011 text-based mailreader supporting MIME, GPG, 
}}}

--------------------------------------------------------------------------------
2008-04-03 07:15:40 UTC Michael Tatge
* Added comment:
{{{
* On Thu, Apr 03, 2008 Mutt wrote:
> #3043: pressing 'y' after starting 'mutt -y' exits
> 
> Comment (by ):
>  {{{
>  On Thu, Apr 03, 2008 at 12:01:17AM -0000, Mutt wrote:
>  > Comment (by Michael Tatge):
>  >  {{{
>  >  * On Wed, Apr 02, 2008 Mutt wrote:
>  >  I only get "Key is not bound as expected.
> 
>  hmmm strange.
>  I made sure to use a blank configuration screen.
>  Maybe it's debian's default key bindings?
> 
>  ||/ Name           Version        Description
>  +++-==============-==============-============================================
>  ii  mutt           1.5.17+2008011 text-based mailreader supporting MIME,
>  }}}

So you have 1.5.17, i still use 1.5.16. Maybe the bug was introduced in
.17.
Just to make sure run »mutt -n -F /dev/null -e 'mailboxes
[/path/to/existing/mailbox]' -y«

Michael
}}}

--------------------------------------------------------------------------------
2008-04-03 07:51:57 UTC 
* Added comment:
{{{
On Thu, Apr 03, 2008 at 07:15:40AM -0000, Mutt wrote:
>  So you have 1.5.17, i still use 1.5.16. Maybe the bug was introduced in
>  .17.
>  Just to make sure run »mutt -n -F /dev/null -e 'mailboxes
>  [/path/to/existing/mailbox]' -y«
> 
>  Michael

the '-n' flag solves the problem.
After looking at debian's /etc/Muttrc I came across:

[...]
bind browser y exit
[...]

sorry for not investigating this further in the first place!
You can close the bug

Thanks!
}}}

--------------------------------------------------------------------------------
2008-04-03 18:49:03 UTC brendan
* resolution changed to invalid
* status changed to closed
