Ticket:  1281
Status:  closed
Summary: Can't save altered mailboxes, then things get worse.

Reporter: mike@velgarian.sytes.net
Owner:    mutt-dev

Opened:       2002-07-21 21:40:29 UTC
Last Updated: 2005-09-06 06:43:04 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.3.27i
Severity: normal

-- Please type your report below this line

When running mutt, occasionaly it complains about =inbox being locked too many
times.  Then after that, when trying to read mail, if I simply go to the
different mboxes without reading or opening any of the messages, I can change
fine.  If I so much as read one, delete, or anything else, when I go to
change to another mbox it complains about Write failed! saving temporary
etc...

Trying the cvs version  (with the below gcc environment) gives the same
results.

Compiled with debugging information, here's the resulting file:

Mutt 1.5.1i started at Sun Jul 21 08:37:58 2002


>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2005-08-02 20:03:10 UTC brendan
* Added comment:
{{{
Can you reproduce this with a current version?
}}}

* status changed to assigned

--------------------------------------------------------------------------------
2005-09-07 00:43:04 UTC brendan
* Added comment:
{{{
Unreproducible, no response from submitter.
}}}

* resolution changed to fixed
* status changed to closed
