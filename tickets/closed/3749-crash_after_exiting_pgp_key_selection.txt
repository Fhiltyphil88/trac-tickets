Ticket:  3749
Status:  closed
Summary: crash after exiting pgp key selection

Reporter: izlemek
Owner:    mutt-dev

Opened:       2015-04-10 18:34:10 UTC
Last Updated: 2015-04-11 11:25:20 UTC

Priority:  major
Component: crypto
Keywords:  gpg

--------------------------------------------------------------------------------
Description:
Hello!

I'm using mutt (version 1.5.23; FreeBSD 10.1) and added to the addresses with an available GPG key a send-hook to automatically encrypt the sended messages.

Currently I wrote an email to several recipients. From one person I have a GPG key. So mutt wanted to encrypt the message and when sending the message I got an field to enter the GPG key id for the person I have no key. I pressed enter and got to the selection list with all available keys.

Then -- as I had no key -- I pressed "q" to exit the selection list. After pressing "q" mutt crashed with following message:
sh: bus error (core) dumped mutt

Debugging with gdb I got the following backtrace:

(gdb) bt

#0  0x0000000801ecff8b in ?? ()

#1  0x00007fffffffb100 in ?? ()

#2  0x0000000800913054 in ?? ()

#3  0x8ecd5f55e3fb9844 in ?? ()

#4  0x0000000000000000 in ?? ()

I have no debug symbols installed but I think the bug is easily reproducable.

Thanks for your help!

--------------------------------------------------------------------------------
2015-04-10 20:19:09 UTC kevin8t8
* Added comment:
Thank you for the bug report.  This was fixed in changeset:5a86319adad0.  Hopefully we'll get a release out with this fix in it soon.

Until then, a workaround is to not leave the prompt for a key id blank when you hit enter.

* resolution changed to duplicate
* status changed to closed

--------------------------------------------------------------------------------
2015-04-11 11:25:20 UTC izlemek
* Added comment:
Thank you for your fast help!
