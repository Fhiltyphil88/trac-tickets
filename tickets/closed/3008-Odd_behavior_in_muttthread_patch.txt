Ticket:  3008
Status:  closed
Summary: Odd behavior in mutt-thread patch?

Reporter: VPhantom
Owner:    mutt-dev

Opened:       2008-01-06 23:37:32 UTC
Last Updated: 2009-01-27 17:34:33 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
This is my first bug report here, so please be kind if I'm out of line, or something. :)

I run Debian testing's Mutt 1.5.17. I encountered unexpected behavior with the ~(pattern) syntax which I suspect is a bug, though nothing obvious jumped at me when I looked at what the patch added to pattern.c. Incidently my C is a bit rusty...

I view collapsed threads by default:

{{{
set sort='threads'
set sort_aux='last-date-received'
folder-hook . 'push <collapse-all>'
}}}

I wanted to highlight flagged messages, not only for stand-alone messages, but for entire threads containing a flagged message, so I could always see where there were any. First try:

{{{
color index brightred default "~(~F)"
color index brightred default "~F"
}}}

This works as expected: all messages of threads containing at least one flagged message get highlighted, and stand-alones too of course. I wanted, however, that expanded threads only highlight flagged messages, not the whole thread:

{{{
color index brightred default "~(~F ~v)"
color index brightred default "~F"
}}}

Unexpectedly, this does not highlight anything. I tried removing the space, adding extra parenthesis, changing the order to ~v ~F, nothing worked. Either of the two patterns works as expected alone, but together like this, match nothing.

I worked around the issue with a wild guess:

{{{
color index brightred default "~(~F) ~(~v)"
color index brightred default "~F"
}}}

This does work as I initially intended: highlight collapsed threads containing flagged messages.

PS: I am unfamiliar with TRAC's way of sending e-mail updates. I am wary of the "Cc" field in case it makes my address public (i.e. for spam-bots.) Please "subscribe" me to this ticket so I don't have to monitor its web page to notice you replied. Thanks! :)

--------------------------------------------------------------------------------
2008-01-11 08:04:08 UTC pdmef
* Added comment:
Confirmed. The cause is either a) a bug in the ~v implementation or b) wrong documentation for ~v: ~v seems to only match if the message is the root of a collapsed (sub)thread, not an arbitrary message within one, see changeset #833.

That's also why
{{{
color index brightred default "~(~F) ~(~v)"
}}}
works: for a given thread it matches if a) this thread has flagged messages and b) if it's collapsed. Note that this is not the same thing as
{{{
color index brightred default "~(~F ~v)"
}}}
which would match if the thread contains a message that a) is flagged and b) is the root of a collapsed (sub)thread. As b) will likely never occur, this one will never match.

--------------------------------------------------------------------------------
2008-01-11 14:34:38 UTC VPhantom
* Added comment:
You are right on. I have simplified my working pattern and it still works as expected:

{{{
color index brightred default "~(~F) ~v"
color index brightred default "~F"
}}}

--------------------------------------------------------------------------------
2009-01-27 17:34:33 UTC pdmef
* resolution changed to worksforme
* status changed to closed
