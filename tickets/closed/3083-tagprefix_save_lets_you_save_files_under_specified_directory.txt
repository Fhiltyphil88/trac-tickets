Ticket:  3083
Status:  closed
Summary: tag-prefix save lets you save files under specified directory and a new name - but not first file

Reporter: Ulrich
Owner:    kevin8t8

Opened:       2008-06-23 13:57:55 UTC
Last Updated: 2013-10-23 22:22:20 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
tag-prefix save (with a directory as argument) always saves the first file under the name specified in the email.  The names of all subsequent files can be changed.

--------------------------------------------------------------------------------
2013-07-15 02:25:46 UTC kevin8t8
* owner changed to kevin8t8

--------------------------------------------------------------------------------
2013-07-15 02:28:47 UTC kevin8t8
* Added comment:
Attached is one relatively non-intrusive solution for this ticket.  The first attachment now confirms the desired filename before continuing on to the overwrite check and save.

--------------------------------------------------------------------------------
2013-07-15 02:33:27 UTC kevin8t8
* status changed to assigned

--------------------------------------------------------------------------------
2013-07-15 02:51:13 UTC kevin8t8
* Added attachment prompt-for-first-attach-name.patch

--------------------------------------------------------------------------------
2013-10-23 22:22:20 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [b5c894182719829ad43154b92e2e47ba59981ec4]:
{{{
#!CommitTicketReference repository="" revision="b5c894182719829ad43154b92e2e47ba59981ec4"
Allow filename prompt when saving multiple attachments to a directory. (closes #3083)

Currently, if you specify a directory when saving multiple attachments
and choose (a)ll, the first attachment is saved without confirming the
filename.  Subsequent attachments prompt for the filename.
}}}

* resolution changed to fixed
* status changed to closed
