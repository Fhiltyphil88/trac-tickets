Ticket:  3097
Status:  closed
Summary: mutt's multi-file -a handling fails for POSIX-compliant getopt()

Reporter: jhawk
Owner:    mutt-dev

Opened:       2008-07-31 06:10:10 UTC
Last Updated: 2008-08-23 22:28:09 UTC

Priority:  minor
Component: mutt
Keywords:  attachment -a posix getopt patch

--------------------------------------------------------------------------------
Description:
Under Solaris (or under Linux with env POSIXLY_COMPLAINT=1), mutt's getopt handling fails to work correctly when trying to attach multiple files:

{{{
mutt -a f1 f2 -- jhawk
--: unable to attach file.
}}}

It turns out that mutt is relying on non-POSIX -compliant behavior of GNU getopt, and assuming that GNU getopt will reorder the -- in argv to the end of the valid option list and set optind after it. E.g.:
{{{
argv[] = { ./mutt, -a, f1, --, f2, jhawk }
optind=4
argv[optind] = f2
}}}
however this behavior is nonportable. What Solaris' posix-compliant getopt does is this:
{{{
argv[] = { ./mutt, -a, f1, f2, --, jhawk }
optind=3
argv[optind] = f2
}}}
This means that under Solaris, mutt tries to attach {{{argv[4]}}}, which is --, it fails.

The attached patch prepends a "+" to the getopt arg string, which instructs GNU getopt to behave in a POSIX-compliant fashion, and then changes the post-getopt atttachment arg -parsing loop so it does not attempt to attach the --, but increments optind past it.

Tested under Linux and Solaris 10.

--------------------------------------------------------------------------------
2008-07-31 06:10:22 UTC jhawk
* Added attachment p

--------------------------------------------------------------------------------
2008-08-15 00:31:29 UTC vinc17
* Added comment:
Ditto under Mac OS X.

And if a file -- exists, Mutt doesn't fail, but -- is attached, which is quite annoying if the user doesn't notice it.

--------------------------------------------------------------------------------
2008-08-15 20:45:09 UTC agriffis
* Added comment:
I'm not sure we should switch to POSIXLY_CORRECT parsing by default.  It prevents this common usage from working:

{{{
mutt addr1 addr2 -s "subject"
}}}

The current behavior is actually broken even with GNU getopt.  For example:

{{{
mutt addr1 -a file           # works
mutt addr1 -a file -- addr2  # fails with "addr1: unable to attach file."
}}}

I suppose this is a silly example but it shows that right now it's not robust.  Maybe the best answer would be to force GNU getopt (include it with mutt for compatibility with non-GNU systems) and use "-" at the start of options rather than "+".  That would give mutt the flexibility to handle options correctly.

Patch attached, though it doesn't augment the configure checks nor does it embed GNU getopt, this is just proof-of-concept.

--------------------------------------------------------------------------------
2008-08-15 20:45:31 UTC agriffis
* Added attachment 3097-getopt-dash.patch

--------------------------------------------------------------------------------
2008-08-17 13:52:31 UTC pdmef
* Added comment:
According to synopsis, both

{{{
mutt addr1 addr2 -s "subject"
}}}

and

{{{
mutt addr1 -a file -- addr2
}}}

are unsupported (I've never even seen or used that form). If argument reordering makes any of these work, this is a "nice to have" feature/side effect IMHO, but we shouldn't start supporting it officially. When using extensions such as leading - or +, we need to either provide solutions for non-GNU-getopt or ship GNU getopt with mutt.

I don't know what the best practice to handle this type of problem is, but how about looking for "--" first, set it to NULL, parse the option string before it using getopt() and take the address list behind it?

--------------------------------------------------------------------------------
2008-08-17 17:15:40 UTC jhawk
* Added comment:
Just to be perfectly clear, using "+" at the start is an instruction to GNU getopt to behave like POSIX getopt, and POSIX getopt ignores the "+". So using + to force GNU getopt to behave in POSIX form seems the right answer for everyone -- it ensures consistent POSIX behavior on GNU and non-GNU systems.

The forms cited by agriffis are not valid in the synopsis, nor do they work on my system. I also don't see the need for anything beyond the document forms...

--------------------------------------------------------------------------------
2008-08-17 18:23:01 UTC agriffis
* Added comment:
Replying to [comment:3 pdmef]:
> [...] are unsupported (I've never even seen or used that form). If argument reordering makes any of these work, this is a "nice to have" feature/side effect IMHO, but we shouldn't start supporting it officially. When using extensions such as leading - or +, we need to either provide solutions for non-GNU-getopt or ship GNU getopt with mutt.

Regarding unsupported syntax, I agree the second one was contrived and unnecessary.  I think the first form is relatively common, though that doesn't mean it must be supported.  Regarding my patch, I don't think it's necessarily the best solution, but if possible I'd like to avoid the POSIX requirement of options before addresses.

> I don't know what the best practice to handle this type of problem is, but how about looking for "--" first, set it to NULL, parse the option string before it using getopt() and take the address list behind it?

I think that makes assumptions about how getopt will consume argv (though I was also making similar assumptions in my patch).  I think you'd have to pass in a recalculated argc to getopt.  It also assumes that -- can never be an optarg.

Replying to [comment:4 jhawk]:
> Just to be perfectly clear, using "+" at the start is an instruction to GNU getopt to behave like POSIX getopt, and POSIX getopt ignores the "+". So using + to force GNU getopt to behave in POSIX form seems the right answer for everyone -- it ensures consistent POSIX behavior on GNU and non-GNU systems.
> 
> The forms cited by agriffis are not valid in the synopsis, nor do they work on my system. I also don't see the need for anything beyond the document forms...

Sorry, I didn't mean to muddy the water with an alternative patch.  Maybe your original patch is best... it's certainly the simplest solution.  I'm just not fond of the way it prevents syntax that presently works on Linux.

--------------------------------------------------------------------------------
2008-08-18 07:39:26 UTC pdmef
* Added comment:
Replying to [comment:5 agriffis]:
> Replying to [comment:3 pdmef]:

> > I don't know what the best practice to handle this type of problem is, but how about looking for "--" first, set it to NULL, parse the option string before it using getopt() and take the address list behind it?

> I think that makes assumptions about how getopt will consume argv (though I was also making similar assumptions in my patch).  I think you'd have to pass in a recalculated argc to getopt.  It also assumes that -- can never be an optarg.

I think the latter assumption (made by the current code already) has to be made when using getopt. If not, I think we need to roll our own (partial) option parsing as -- actually only has a special meaning following an -a option. Given something like

{{{
mutt ... -s -- addr1 addr2
}}}

means a subject of "--" and addr1+addr2 (i.e. -- not special) as recipients while in

{{{
mutt -s foo -a file1 file2 -- addr1 addr2
}}}

it is special.

Regardless of what patch makes it into the tree, I think we need another one fixing the code to only accept -- after an -a argument.

Attached is a patch that reverse-looks for --, adjusts argc and passes the shortened argv to getopt. It can then simply parse all non-option arguments as files and takes all arguments in argv tail after -- as recipients.

--------------------------------------------------------------------------------
2008-08-18 07:39:56 UTC pdmef
* Added attachment argv.diff

--------------------------------------------------------------------------------
2008-08-18 23:53:29 UTC agriffis
* Added comment:
Replying to [comment:6 pdmef]:
> I think the latter assumption (made by the current code already) has to be made when using getopt. If not, I think we need to roll our own (partial) option parsing as -- actually only has a special meaning following an -a option. 

Yeah, I see the current code does that.  Ideally, though, -- should always
signal end-of-options regardless of -a, except when -- appears as an optarg.

> Given something like
> 
> {{{
> mutt ... -s -- addr1 addr2
> }}}
> 
> means a subject of "--" and addr1+addr2 (i.e. -- not special) as recipients while in
> 
> {{{
> mutt -s foo -a file1 file2 -- addr1 addr2
> }}}
> 
> it is special.

I agree, but it should be special even when -a is missing.  For example, mutt
presently fails on this:

{{{
mutt -s foo -- -aron-@domain
}}}

I think I have a patch that solves all the problems entirely.  It's inspired by
jhawk's patch by using "+" to put GNU getopt in POSIX mode.  (As jhawk
mentioned, it won't harm non-GNU getopt.)  My patch puts an outer loop around
getopt() to handle the non-POSIX behavior.  A bonus of this patch is that it
will make all UNIXes behave the same way.

I tried a series of pathelogical tests on Linux, all passed:

{{{
mutt addr1 addr2
mutt addr1 addr2 --
mutt -a attach1 attach2
mutt -a attach1 attach2 --
mutt -a attach1 attach2 -- addr1 addr2
mutt addr1 addr2 -a attach1 attach2 -- addr3 addr4
mutt addr1 addr2 -a attach1 attach2 -s foo attach3 -- addr3 addr4
mutt -s -- -a attach1 attach2 -- addr1 --  # last -- is an address
mutt -s -- -a attach1 -- addr1 --          # last -- is an address
mutt -s -- -a -- -- addr1 --               # dash insanity
}}}

I believe this will work equally on Solaris and other POSIX getopts.

--------------------------------------------------------------------------------
2008-08-19 00:23:08 UTC agriffis
* Added comment:
That patch uses 1-space indentation for a few lines, just to avoid needlessly re-indenting the entire switch block.

--------------------------------------------------------------------------------
2008-08-19 20:45:54 UTC agriffis
* Added attachment posix-with-loop.patch

--------------------------------------------------------------------------------
2008-08-22 08:49:14 UTC pdmef
* keywords changed to attachment -a posix getopt patch

--------------------------------------------------------------------------------
2008-08-23 22:26:20 UTC Aron Griffis <agriffis@n01se.net>
* Added comment:
(In [31c9e9727d42]) Handle -- correctly with or without -a, closes #3097

Prefix optstring with "+" to force POSIX behavior on GNU getopt, and thereby
prevent reordering argv.  This allows us to correctly handle mixed addresses,
attachments, options and the double-dash to signal end-of-options.

Signed-off-by: Aron Griffis <agriffis@n01se.net>

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2008-08-23 22:28:09 UTC brendan
* Added comment:
Thanks, Aron! I've pushed the patch along with a fix for the bare '-' case.
