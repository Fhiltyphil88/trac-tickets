Ticket:  3056
Status:  closed
Summary: umask facility

Reporter: keld
Owner:    mutt-dev

Opened:       2008-05-18 19:45:05 UTC
Last Updated: 2009-06-15 18:09:28 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
mutt always use 077 as umask. This may not be desirable. I would like a facility to either set another default, or use the default umask of the environment (I prefer the latter). 

There are already patches for mutt for this, but I would like it to be included in the source tree. 

Kyle Wheeler wrote:

Well, unfortunately, there is currently no such setting in standard
mutt. The Debian folks have put together a patch to provide that
functionality, and you can apply that to your mutt. Here's the
relevant Debian bug report:
http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=426317

Here's a similar patch:
http://bereshit.synflood.at/svn/mutt-patches/trunk/patches/ak.umask.diff




--------------------------------------------------------------------------------
2009-06-04 23:52:54 UTC antonio@dyne.org
* Added comment:
Just to note from http://bugs.debian.org/179119

You already implemented this by closing #2101, the configurable $umask was implemented in http://dev.mutt.org/hg/mutt/rev/537672d8fefb, can you please clarify why this was removed and if it can be reintroduced again?

Cheers
Antonio

--------------------------------------------------------------------------------
2009-06-13 22:16:40 UTC brendan
* Added comment:
Please see this thread:
http://www.mail-archive.com/mutt-dev@mutt.org/msg00382.html

I'm closing this bug and reopening 2101, so the discussion's in one place.

* resolution changed to duplicate
* status changed to closed

--------------------------------------------------------------------------------
2009-06-15 18:09:28 UTC Keld Jørn Simonsen
* Added comment:
{{{
On Sat, Jun 13, 2009 at 10:16:40PM -0000, Mutt wrote:

Hmm, what is the status then right now?

It is still something that annoys me a lot, and which has lead to a
number of embarrassing situations for me. Mostly saving attachments,
which I then erroneously upload to a server, with 600 permissions, which people then
cannot access. If I could have something that could give 644
permissions, I would save quite some work, and the security issue is not
relevant for me on my workstation, which I am the only one using.

best regards
keld
}}}
