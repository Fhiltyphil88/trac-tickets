Ticket:  953
Status:  closed
Summary: wishlist: add capability to pipe draft text to mutt and edit before sending

Reporter: Henry House <hajhouse@houseag.com>
Owner:    mutt-dev

Opened:       2002-01-07 16:30:09 UTC
Last Updated: 2005-08-01 18:46:44 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.3.24-2
Severity: wishlist

-- Please type your report below this line

It would be terribly nice to be able to pipe some text to mutt and have this
inserted into a new message which could be edited before sending. As it is,
piped text becomes the entire message, which may neither be PGP-signed nor
edited.

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
cc
Reading specs from /usr/lib/gcc-lib/i386-linux/2.95.4/specs
gcc version 2.95.4 20011006 (Debian prerelease)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.3.24i (2001-11-29)
Copyright (C) 1996-2001 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.17-grsecurity-1.9.2grsec1 (i686) [using ncurses 5.2]
Compile options:
-DOMAIN
+DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_GSS  -USE_SSL  +USE_GNUTLS  +USE_SASL  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +COMPRESSED  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
MIXMASTER="mixmaster"
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.

patch-1.3.24.cd.edit_threads.7
patch-1.3.15.sw.pgp-outlook.1
patch-1.3.24.admcd.gnutls.1
Md.use-editor
Md.paths-mutt.man
Md.use-etc-mailname
Md.Muttrc
Md.muttbug-warning
patch-1.3.23.2.rr.compressed.1
patch-1.2.xtitles.1
patch-1.3.23.1.ametzler.pgp_good_sign

--- Begin /home/hajhouse/.muttrc
set spoolfile=~/Procmail/Mail/INBOX
mailboxes \
	~/Procmail/Mail/INBOX \
	~/Procmail/Mail/lists/wikipedia \
	~/Procmail/Mail/lists/abiword-dev \
	~/Procmail/Mail/lists/debian-user \
	~/Procmail/Mail/lists/debian-alpha \
	~/Procmail/Mail/lists/ruby-talk \
	~/Procmail/Mail/lists/pgsql \
	~/Procmail/Mail/lists/dillo-dev \
	~/Procmail/Mail/lists/gnumeric \
	~/Procmail/Mail/lists/gnome-doc \
	~/Procmail/Mail/Junk/default
set mbox = ~/Mail/read
set postponed = ~/Mail/postponed
set save_name
set record = ~/Mail/record
set abort_nosubject = no
set fast_reply = yes
set abort_unmodified = no
set include = yes
set mime_forward = no
set alternates = "(sys|librarian|lert)@lugod.org|(hajhouse|henry)@(romana|wotan|davros|mail)?(hajhouse.org|houseag.com|cocoranch.com)"
bind pager [ previous-entry
bind pager ] next-entry
bind pager k    half-up
bind pager j  half-down
set pager_stop # Don't move to next msg when arrow-down is used at end of msg
set ascii_chars
ignore *
unignore from: date: subject: to: cc:
unignore organization: organisation: x-mailer: x-newsreader:
unignore posted-to:
set sort=threads
set editor='vi "+set tw=77"' # -c startinsert'
set copy
set delete=yes # Auto-purge deleted
set pgp_autosign
set pgp_timeout=600
set wait_key
set tilde
set print_command="a2ps -=mail -1 -o - | tmpf gv"
set print=yes
unset mark_old
source ~/.addresses
set alias_file="~/.addresses"
--- End /home/hajhouse/.muttrc


--- Begin /etc/Muttrc
ignore "from " received content- mime-version status x-status message-id
ignore sender references return-path lines
ignore date delivered-to precedence errors-to in-reply-to user-agent
ignore x-loop x-sender x-mailer x-msmail-priority x-mimeole x-priority
ignore x-accept-language x-authentication-warning
bind editor    "\e<delete>"    kill-word
bind editor    "\e<backspace>" kill-word
bind editor     <delete>  delete-char
unset use_domain
unset use_from
set sort=threads
unset write_bcc
unset bounce_delivered
macro index \eb '/~b ' 'search in message bodies'
macro index \cb |urlview\n 'call urlview to extract URLs out of a message'
macro pager \cb |urlview\n 'call urlview to extract URLs out of a message'
macro generic <f1> "!zless /usr/share/doc/mutt/manual.txt.gz\n" "Show Mutt documentation"
macro index   <f1> "!zless /usr/share/doc/mutt/manual.txt.gz\n" "Show Mutt documentation"
macro pager   <f1> "!zless /usr/share/doc/mutt/manual.txt.gz\n" "Show Mutt documentation"
open-hook \\.gz$ "gzip -cd %f > %t"
close-hook \\.gz$ "gzip -c %t > %f"
append-hook \\.gz$ "gzip -c %t >> %f"
color normal	white black
color attachment brightyellow black
color hdrdefault cyan black
color indicator black cyan
color markers	brightred black
color quoted	green black
color signature cyan black
color status	brightgreen blue
color tilde	blue black
color tree	red black
charset-hook windows-1250 CP1250
charset-hook windows-1251 CP1251
charset-hook windows-1252 CP1252
charset-hook windows-1253 CP1253
charset-hook windows-1254 CP1254
charset-hook windows-1255 CP1255
charset-hook windows-1256 CP1256
charset-hook windows-1257 CP1257
charset-hook windows-1258 CP1258
set ispell=ispell
set pgp_decode_command="/usr/bin/gpg   %?p?--passphrase-fd 0? --no-verbose --quiet  --batch  --output - %f"
set pgp_verify_command="/usr/bin/gpg   --no-verbose --quiet  --batch  --output - --verify %s %f"
set pgp_decrypt_command="/usr/bin/gpg   --passphrase-fd 0 --no-verbose --quiet  --batch  --output - %f"
set pgp_sign_command="/usr/bin/gpg    --no-verbose --batch --quiet   --output - --passphrase-fd 0 --armor --detach-sign --textmode %?a?-u %a? %f"
set pgp_clearsign_command="/usr/bin/gpg   --no-verbose --batch --quiet   --output - --passphrase-fd 0 --armor --textmode --clearsign %?a?-u %a? %f"
set pgp_encrypt_only_command="/usr/lib/mutt/pgpewrap /usr/bin/gpg    --batch  --quiet  --no-verbose --output - --encrypt --textmode --armor --always-trust -- -r %r -- %f"
set pgp_encrypt_sign_command="/usr/lib/mutt/pgpewrap /usr/bin/gpg  --passphrase-fd 0  --batch --quiet  --no-verbose  --textmode --output - --encrypt --sign %?a?-u %a? --armor --always-trust -- -r %r -- %f"
set pgp_import_command="/usr/bin/gpg  --no-verbose --import -v %f"
set pgp_export_command="/usr/bin/gpg   --no-verbose --export --armor %r"
set pgp_verify_key_command="/usr/bin/gpg   --verbose --batch  --fingerprint --check-sigs %r"
set pgp_list_pubring_command="/usr/bin/gpg   --no-verbose --batch --quiet   --with-colons --list-keys %r" 
set pgp_list_secring_command="/usr/bin/gpg   --no-verbose --batch --quiet   --with-colons --list-secret-keys %r" 
--- End /etc/Muttrc



>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2005-08-02 12:46:44 UTC brendan
* Added comment:
{{{
You want mutt -i.
}}}

* resolution changed to fixed
* status changed to closed
