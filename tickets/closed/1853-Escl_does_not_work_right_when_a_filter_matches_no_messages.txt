Ticket:  1853
Status:  closed
Summary: Esc+l does not work right when a filter matches no messages

Reporter: Enrico Zini <zinie@cs.unibo.it>
Owner:    mutt-dev

Opened:       2004-04-14 11:25:45 UTC
Last Updated: 2005-09-04 06:58:49 UTC

Priority:  minor
Component: mutt
Keywords:  patch, #1906, #242398

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.5.5.1-200401
Severity: normal

-- Please type your report below this line

Hello,

thanks for mutt.  Small bug here.

If I type 'l' ("limit") and then something impossible like "safgalskjhe", then
no messages are displayed.  However, if I type Esc+l ("show-limit"), it tells
me "No limit pattern is in effect.", which is wrong.

Keep up the good work!

Bye,

Enrico



-- System Information
System Version: Linux mitac 2.6.5-1-686 #1 Wed Apr 7 00:24:54 EST 2004 i686 GNU/Linux

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
cc
Reading specs from /usr/lib/gcc-lib/i486-linux/3.3.3/specs
Configured with: ../src/configure -v --enable-languages=c,c++,java,f77,pascal,objc,ada,treelang --prefix=/usr --mandir=/usr/share/man --infodir=/usr/share/info --with-gxx-include-dir=/usr/include/c++/3.3 --enable-shared --with-system-zlib --enable-nls --without-included-gettext --enable-__cxa_atexit --enable-clocale=gnu --enable-debug --enable-java-gc=boehm --enable-java-awt=xlib --enable-objc-gc i486-linux
Thread model: posix
gcc version 3.3.3 (Debian 20040401)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.5.5.1+cvs20040105i (2003-11-05)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.6.5-1-686 (i686) [using ncurses 5.3] [using libidn 0.4.1 (compiled with 0.3.4)]
Opzioni di compilazione:
-DOMAIN
-DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  +IMAP_EDIT_THREADS  -USE_GSS  -USE_SSL  +USE_GNUTLS  +USE_SASL  +USE_SASL2  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +COMPRESSED  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_LIBIDN  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
MIXMASTER="mixmaster"
Per contattare gli sviluppatori scrivi a <mutt-dev@mutt.org>.
Per segnalare un bug usa il programma flea(1).

patch-1.5.5.1.tt.compat.1-cl
patch-1.5.4.vk.pgp_verbose_mime
patch-1.5.3.rr.compressed.1
patch-1.5.4.helmersson.incomplete_multibyte
patch-1.5.4.fw.maildir_inode_sort
patch-1.3.23.1.ametzler.pgp_good_sign
patch-1.5.3.Md.gpg_status_fd
patch-1.4.Md.gpg-agent
patch-1.5.3.Md.etc_mailname_gethostbyname
patch-1.5.1.cd.edit_threads.9.2
patch-1.3.27.bse.xtitles.1
Md.use_debian_editor
Md.muttbug
patch-1.4.admcd.gnutlsdlopen.53d
patch-1.4.admcd.gnutlsbuild.53d
patch-1.4.admcd.gnutls.56d


>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2005-08-27 04:03:04 UTC ab
* Added comment:
{{{
See mutt/1906 for a patch.
}}}

* status changed to assigned

--------------------------------------------------------------------------------
2005-09-03 03:13:29 UTC ab
* Added comment:
{{{
crossrefed
}}}

--------------------------------------------------------------------------------
2005-09-05 00:58:49 UTC brendan
* Added comment:
{{{
Fixed in CVS (Alain's patch applied).
}}}

* resolution changed to fixed
* status changed to closed
