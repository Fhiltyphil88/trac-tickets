Ticket:  2366
Status:  closed
Summary: Does not support username with @

Reporter: hadmut@danisch.de
Owner:    mutt-dev

Opened:       2006-07-20 00:07:38 UTC
Last Updated: 2006-08-09 17:45:03 UTC

Priority:  minor
Component: IMAP
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
If I connect to an IMAP server where the username is not just a username, but something in the form user@domain , then 
mutt gets confused within the TLS protocol. If I turn off TLS at the server side, mutt immediately terminates when logging in.
>How-To-Repeat:
Use an IMAP server which supports login names with domains, such as dovecot.
>Fix:
Unknown
}}}

--------------------------------------------------------------------------------
2006-07-20 00:25:51 UTC Brendan Cully <brendan@kublai.com>
* Added comment:
{{{
On Thursday, 20 July 2006 at 01:07, hadmut@danisch.de wrote:
> >Number:         2366
> >Notify-List:    
> >Category:       imap
> >Synopsis:       Does not support username with @
> >Confidential:   no
> >Severity:       normal
> >Priority:       medium
> >Responsible:    mutt-dev
> >State:          open
> >Keywords:       
> >Class:          sw-bug
> >Submitter-Id:   net
> >Arrival-Date:   Thu Jul 20 01:07:38 +0200 2006
> >Originator:     Hadmut
> >Release:        1.5.11+cvs20060126
> >Organization:
> >Environment:
> >Description:
> If I connect to an IMAP server where the username is not just a username, but something in the form user@domain , then 
> mutt gets confused within the TLS protocol. If I turn off TLS at the server side, mutt immediately terminates when logging in.
> >How-To-Repeat:
> Use an IMAP server which supports login names with domains, such as dovecot.
> >Fix:
> Unknown
> >Add-To-Audit-Trail:
> 
> >Unformatted:

Mutt handles usernames with @ just fine. Something else is
wrong. Please run a debug-enabled mutt with -d2 and send the trace of
a failed login.
}}}

--------------------------------------------------------------------------------
2006-08-09 16:43:51 UTC Brendan Cully <brendan@kublai.com>
* Added comment:
{{{
On Wednesday, 09 August 2006 at 08:12, Hadmut Danisch wrote:
> On Tue, Aug 08, 2006 at 11:58:18PM +0200, Brendan Cully wrote:
> > Synopsis: Does not support username with @
> > 
> > State-Changed-From-To: open->closed
> > State-Changed-By: brendan
> > State-Changed-When: Tue, 08 Aug 2006 23:58:18 +0200
> > State-Changed-Why:
> > Not a bug.
> 
> 
> What does that mean: "not a bug"? 
> 
> Usernames with a @ are more and more common...

It means mutt handles usernames with @ just fine. If there is a bug,
that's not it. I asked for more information but never got it, so I
assumed this was a hit and run report.
}}}

--------------------------------------------------------------------------------
2006-08-09 16:58:18 UTC brendan
* Added comment:
{{{
On Thursday, 20 July 2006 at 01:07, hadmut@danisch.de wrote:
Not a bug.
}}}

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2006-08-10 01:12:27 UTC hadmut@danisch.de (Hadmut Danisch)
* Added comment:
{{{
On Tue, Aug 08, 2006 at 11:58:18PM +0200, Brendan Cully wrote:
> Synopsis: Does not support username with @
> 
> State-Changed-From-To: open->closed
> State-Changed-By: brendan
> State-Changed-When: Tue, 08 Aug 2006 23:58:18 +0200
> State-Changed-Why:
> Not a bug.


What does that mean: "not a bug"? 

Usernames with a @ are more and more common...




regards
Hadmut
}}}

--------------------------------------------------------------------------------
2006-08-10 05:05:42 UTC Michael Tatge <Michael.Tatge@web.de>
* Added comment:
{{{
* On Wed, Aug 09, 2006 Hadmut Danisch (hadmut@danisch.de) muttered:
> The following reply was made to PR imap/2366; it has been noted by GNATS.
> 
> From: hadmut@danisch.de (Hadmut Danisch)
> To: bug-any@bugs.mutt.org
> Cc: brendan@kublai.com, mutt-dev@mutt.org
> Subject: Re: imap/2366: Does not support username with @
> Date: Wed, 9 Aug 2006 08:12:27 +0200
> 
>  On Tue, Aug 08, 2006 at 11:58:18PM +0200, Brendan Cully wrote:
>  > Synopsis: Does not support username with @
>  > State-Changed-From-To: open->closed
>  > Not a bug.
>  
>  
>  What does that mean: "not a bug"? 
>  
>  Usernames with a @ are more and more common...

It's not a bug because mutt handles usernames like user@foo just fine
and since you didn't follow up with a debugtrace brendan closed the PR.

Michael
-- 
"Problem solving under linux has never been the circus that it is under
AIX."
(By Pete Ehlke in comp.unix.aix)

PGP-Key-ID: 0xDC1A44DD
Jabber:     init[0]@amessage.de
}}}
