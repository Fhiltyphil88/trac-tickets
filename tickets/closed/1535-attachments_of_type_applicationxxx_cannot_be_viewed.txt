Ticket:  1535
Status:  closed
Summary: attachments of type "application/xxx" cannot be viewed

Reporter: <5wklxvdgo5001@sneakemail.com>
Owner:    mutt-dev

Opened:       2003-04-12 07:37:10 UTC
Last Updated: 2008-05-22 10:29:01 UTC

Priority:  minor
Component: MIME
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.5.3i
Severity: normal

-- Please type your report below this line
When mutt sees an attachments of type "application/whatever",
it reports incorrectly that this attachment cannot be viewd, without
even checking the appropriate mailcap file along mailcap_path.

Changing "application/whatever" to "image/whatever" makes the problem
disappear, so it's clearly a problem with the use of TYPEAPPLICATION
in the source code.

How to reproduce:


$ tm1=/tmp/temp.mailbox.$$.1
$ tm2=/tmp/temp.mailbox.$$.2
$ cat <<EOF >$tm1
From user@host Fri Apr 11 12:00:00 2003
From: user@host
To: user@host
Subject: test
Date: Fri, 11 Apr 2003 12:00:00 +0000
Message-ID: <123@host>
Mime-Version: 1.0
Content-Type: multipart/mixed; boundary="azLHFNyN32YCQGCU"
Content-Disposition: inline


--azLHFNyN32YCQGCU
Content-Type: application/xxx
Content-Disposition: attachment; filename="a.xxx"

ABCD

--azLHFNyN32YCQGCU--
EOF
$ strace -f mail -f $tm1 2>log  # start mutt, attempt to view attachment
$ grep -i mailcap log           # no attempt to open mailcap file
$
$ # changing application/xxx to image/xxx makes the problem go away
$ sed 's/application/image/' <$tm1 >$tm2
$ strace -f mail -f $tm1 2>log    # start mutt, attempt to view attachment
$                                 # it won't find an appopriate entry
$                                 # in mailcap, but that's ok
$ grep -i 'open.*mailcap' log     # mailcap file was indeed open()-ed
open("/home/user/.mailcap", O_RDONLY)      = 5
open("/usr/share/mutt/mailcap", O_RDONLY) = -1 ENOENT (No such file or
directory)
open("/usr/share/mutt/mailcap", O_RDONLY) = -1 ENOENT (No such file or
directory)
open("/etc/mailcap", O_RDONLY)          = -1 ENOENT (No such file or
directory)
open("/usr/etc/mailcap", O_RDONLY)      = -1 ENOENT (No such file or
directory)
open("/usr/local/etc/mailcap", O_RDONLY) = -1 ENOENT (No such file or
directory)







-- System Information
System Version: Linux server 2.4.18 #1 Sat Jan  4 12:00:00 UTC 2003 i686
unknown

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/lib/gcc-lib/i486-suse-linux/2.95.2/specs
gcc version 2.95.2 19991024 (release)

- CFLAGS
-Wall -pedantic -O2 -fomit-frame-pointer -I /usr/include/slang

-- Mutt Version Information

Mutt 1.5.3i (2002-12-17)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.18 (i686) [using slang 10401]
Compile options:
-DOMAIN
-DEBUG
+HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE
+USE_FCNTL  -USE_FLOCK
-USE_POP  -USE_IMAP  -USE_GSS  -USE_SSL  -USE_SASL  -USE_SASL2
+HAVE_REGCOMP  -USE_GNU_REGEX
+HAVE_COLOR  -HAVE_START_COLOR  -HAVE_TYPEAHEAD  -HAVE_BKGDSET
-HAVE_CURS_SET  -HAVE_META  -HAVE_RESIZETERM
+HAVE_PGP  -HAVE_SMIME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT
-ENABLE_NLS  +LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET
+HAVE_LANGINFO_YESEXPR
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  -HAVE_GETADDRINFO



>How-To-Repeat:
	
>Fix:
}}}

--------------------------------------------------------------------------------
2007-04-02 02:50:14 UTC brendan
* component changed to MIME
* Updated description:
{{{
Package: mutt
Version: 1.5.3i
Severity: normal

-- Please type your report below this line
When mutt sees an attachments of type "application/whatever",
it reports incorrectly that this attachment cannot be viewd, without
even checking the appropriate mailcap file along mailcap_path.

Changing "application/whatever" to "image/whatever" makes the problem
disappear, so it's clearly a problem with the use of TYPEAPPLICATION
in the source code.

How to reproduce:


$ tm1=/tmp/temp.mailbox.$$.1
$ tm2=/tmp/temp.mailbox.$$.2
$ cat <<EOF >$tm1
From user@host Fri Apr 11 12:00:00 2003
From: user@host
To: user@host
Subject: test
Date: Fri, 11 Apr 2003 12:00:00 +0000
Message-ID: <123@host>
Mime-Version: 1.0
Content-Type: multipart/mixed; boundary="azLHFNyN32YCQGCU"
Content-Disposition: inline


--azLHFNyN32YCQGCU
Content-Type: application/xxx
Content-Disposition: attachment; filename="a.xxx"

ABCD

--azLHFNyN32YCQGCU--
EOF
$ strace -f mail -f $tm1 2>log  # start mutt, attempt to view attachment
$ grep -i mailcap log           # no attempt to open mailcap file
$
$ # changing application/xxx to image/xxx makes the problem go away
$ sed 's/application/image/' <$tm1 >$tm2
$ strace -f mail -f $tm1 2>log    # start mutt, attempt to view attachment
$                                 # it won't find an appopriate entry
$                                 # in mailcap, but that's ok
$ grep -i 'open.*mailcap' log     # mailcap file was indeed open()-ed
open("/home/user/.mailcap", O_RDONLY)      = 5
open("/usr/share/mutt/mailcap", O_RDONLY) = -1 ENOENT (No such file or
directory)
open("/usr/share/mutt/mailcap", O_RDONLY) = -1 ENOENT (No such file or
directory)
open("/etc/mailcap", O_RDONLY)          = -1 ENOENT (No such file or
directory)
open("/usr/etc/mailcap", O_RDONLY)      = -1 ENOENT (No such file or
directory)
open("/usr/local/etc/mailcap", O_RDONLY) = -1 ENOENT (No such file or
directory)







-- System Information
System Version: Linux server 2.4.18 #1 Sat Jan  4 12:00:00 UTC 2003 i686
unknown

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/lib/gcc-lib/i486-suse-linux/2.95.2/specs
gcc version 2.95.2 19991024 (release)

- CFLAGS
-Wall -pedantic -O2 -fomit-frame-pointer -I /usr/include/slang

-- Mutt Version Information

Mutt 1.5.3i (2002-12-17)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.18 (i686) [using slang 10401]
Compile options:
-DOMAIN
-DEBUG
+HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE
+USE_FCNTL  -USE_FLOCK
-USE_POP  -USE_IMAP  -USE_GSS  -USE_SSL  -USE_SASL  -USE_SASL2
+HAVE_REGCOMP  -USE_GNU_REGEX
+HAVE_COLOR  -HAVE_START_COLOR  -HAVE_TYPEAHEAD  -HAVE_BKGDSET
-HAVE_CURS_SET  -HAVE_META  -HAVE_RESIZETERM
+HAVE_PGP  -HAVE_SMIME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT
-ENABLE_NLS  +LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET
+HAVE_LANGINFO_YESEXPR
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  -HAVE_GETADDRINFO



>How-To-Repeat:
	
>Fix:
}}}

--------------------------------------------------------------------------------
2008-05-22 10:29:01 UTC pdmef
* Added comment:
Please see the auto_view command.

* resolution changed to invalid
* status changed to closed
