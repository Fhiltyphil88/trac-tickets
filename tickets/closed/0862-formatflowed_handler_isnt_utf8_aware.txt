Ticket:  862
Status:  closed
Summary: format=flowed handler isn't utf-8 aware

Reporter: roessler@does-not-exist.org (Thomas Roessler)
Owner:    mutt-dev

Opened:       2001-11-08 16:28:46 UTC
Last Updated: 2007-11-19 14:36:10 UTC

Priority:  minor
Component: display
Keywords:  

--------------------------------------------------------------------------------
Description:
Just for the record: The format=flowed handler is not yet utf-8
aware.  This will have to be fixed at a later point of view.

--------------------------------------------------------------------------------
2007-04-02 02:40:13 UTC brendan
* component changed to display
* Updated description:
Just for the record: The format=flowed handler is not yet utf-8
aware.  This will have to be fixed at a later point of view.
* milestone changed to 1.6

--------------------------------------------------------------------------------
2007-04-02 02:41:06 UTC brendan
* reporter changed to roessler@does-not-exist.org (Thomas Roessler)

--------------------------------------------------------------------------------
2007-04-05 20:02:05 UTC brendan
* Added comment:
Does anyone know if this bug still applies to the new f=f handler?

--------------------------------------------------------------------------------
2007-04-11 12:55:21 UTC Rocco Rutte
* Added comment:
{{{
Hi,

* Mutt [07-04-05 20:02:06 -0000] wrote:

> Does anyone know if this bug still applies to the new f=f handler?

The description is rather short so I need to guess what issues could 
arise with the new f=f handler.

1. It searches for break points on ASCII space only, so there 
shouldn't be any issue.

2. It uses byte offsets within the flowed line (i.e. the full paragraph 
concatenated) to detect whether a line is too short/long so that flowed 
lines with many multibyte chars may be shorter on screen than expected, 
but never longer (i.e. it respects $wrap).

3. For languages that don't use spaces (for DelSp), there's still the 
trailing space to break on so that there's no need to search for 
character boundaries in multibyte strings. Only 2) could apply.

So I'd say doesn't need to be utf-8 aware to be bug-free except for 
point 2. I will look into these for 1.6.

For now I think it's flowed aren't perfectly flowed up to the maximum 
length but little less so I think it's more wishlist thing than a real 
bug (given there are no utf-8 issues I didn't think of).

   bye, Rocco
}}}

--------------------------------------------------------------------------------
2007-04-11 15:25:15 UTC Brendan Cully
* Added comment:
{{{
On Wednesday, 11 April 2007 at 12:55, Rocco Rutte wrote:
> > Does anyone know if this bug still applies to the new f=f handler?
>
> 1. It searches for break points on ASCII space only, so there
> shouldn't be any issue.

Is it possible that in some charsets ASCII space is part of another
character? So that we'd have to scan the line forward instead of just
jumping to the wrap point?

>  2. It uses byte offsets within the flowed line (i.e. the full paragraph
>  concatenated) to detect whether a line is too short/long so that flowed
>  lines with many multibyte chars may be shorter on screen than expected,
>  but never longer (i.e. it respects $wrap).

if we find a space correctly, I think this is true.
}}}

--------------------------------------------------------------------------------
2007-11-19 14:36:10 UTC pdmef
* Added comment:
(In [033eba2d6834]) Make f=f handler multibyte-aware by not buffering flowed content until it's finished.
We now write out content as we read it (which simplifies the code quite
a bit) and properly flow multibyte paragraphs. This should improve
DelSp=yes handling where a flowed paragraph may not have spaces at all
except trailing ones for flowable paragraphs (after concatenating lines
we didn't have access to the original break points any longer). We still
split content by ASCII space (this should be safe as f=f itself uses
ASCII space to mark flowable lines (even for languages that aren't
expected to use spaces, see DelSp). Closes #862.

* resolution changed to fixed
* status changed to closed
