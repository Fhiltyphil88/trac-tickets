Ticket:  3168
Status:  closed
Summary: $sendmail option unconditionally and wrongly uses --

Reporter: antonio@dyne.org
Owner:    kevin8t8

Opened:       2009-01-26 21:40:39 UTC
Last Updated: 2016-10-13 01:14:49 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
Forwarded from http://bugs.debian.org

---

The $sendmail option is described as follows:

  # Name: sendmail
  
  # Type: path
  
  # Default: "/usr/sbin/sendmail -oem -oi"
  
  # Specifies the program and arguments used to deliver mail sent by Mutt.
  
  # Mutt expects that the specified program interprets additional
  
  # arguments as recipient addresses.

I need to add an "always-bcc" address, so I tried

  set sendmail="/usr/sbin/sendmail -oem -oi madduck+spool@madduck.net"

and also

  set sendmail="/usr/sbin/sendmail -oem -oi -- madduck+spool@madduck.net"

It turns out, however, that mutt appends not only additional
arguments (recipients), but also unconditionally a '--'. In the
above cases, this yields:

  /usr/sbin/sendmail -oem -oi madduck+spool@madduck.net -- foo@bar.com
  
 /usr/sbin/sendmail -oem -oi -- madduck+spool@madduck.net -- foo@bar.com

Both are wrong. mutt should probably not append -- if it's already
contained in $sendmail.

PS: is there a better way to make mutt always BCC another address,
other than actually using my_hdr, which is simply too brittle?



--------------------------------------------------------------------------------
2009-01-26 21:41:40 UTC antonio@dyne.org
* Added comment:
We also have the following patch from Steve Kemp:

skx@gold:~/git/mutt/mutt-1.5.19$ diffs
--- sendlib.c-orig  2009-01-20 22:57:28.000000000 +0000
+++ sendlib.c   2009-01-20 22:57:57.000000000 +0000
@@ -2206,7 +2206,11 @@
     args = add_option (args, &argslen, &argsmax, "-R");
     args = add_option (args, &argslen, &argsmax, DsnReturn);
   }
+  if ( strstr( args, "--" ) == NULL )
+  {
+    /* Only append "--" if not already present. */
   args = add_option (args, &argslen, &argsmax, "--");
+  }
   args = add_args (args, &argslen, &argsmax, to);
   args = add_args (args, &argslen, &argsmax, cc);
   args = add_args (args, &argslen, &argsmax, bcc);

--------------------------------------------------------------------------------
2009-01-26 21:42:46 UTC antonio@dyne.org
* Added comment:
This is better:

{{{
skx@gold:~/git/mutt/mutt-1.5.19$ diffs
--- sendlib.c-orig  2009-01-20 22:57:28.000000000 +0000
+++ sendlib.c   2009-01-20 22:57:57.000000000 +0000
@@ -2206,7 +2206,11 @@
     args = add_option (args, &argslen, &argsmax, "-R");
     args = add_option (args, &argslen, &argsmax, DsnReturn);
   }
+  if ( strstr( args, "--" ) == NULL )
+  {
+    /* Only append "--" if not already present. */
   args = add_option (args, &argslen, &argsmax, "--");
+  }
   args = add_args (args, &argslen, &argsmax, to);
   args = add_args (args, &argslen, &argsmax, cc);
   args = add_args (args, &argslen, &argsmax, bcc);

}}}

--------------------------------------------------------------------------------
2009-04-02 11:12:30 UTC Steve Kemp <skx@debian.org>
* Added comment:
(In [23c7b469ff20]) Only append -- to $sendmail if not present. Closes #3168.

This allows users to add custom recipients via $sendmail.

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2009-04-05 11:49:08 UTC Rocco Rutte <pdmef@gmx.net>
* Added comment:
(In [5d393e716c51]) Backed out changeset 23c7b469ff20

This requires more thought: if the user has -- in $sendmail,
we need to add our arguments for 8BITMIME/DSN/etc. before it
and not after. See #3168.

--------------------------------------------------------------------------------
2009-04-06 16:02:07 UTC pdmef
* resolution changed to 
* status changed to new

--------------------------------------------------------------------------------
2016-09-04 19:54:22 UTC antonio@dyne.org
* Added comment:
Any chance you could have another try at this bug?

--------------------------------------------------------------------------------
2016-10-13 00:27:05 UTC kevin8t8
* Updated description:
Forwarded from http://bugs.debian.org

---

The $sendmail option is described as follows:

  # Name: sendmail
  
  # Type: path
  
  # Default: "/usr/sbin/sendmail -oem -oi"
  
  # Specifies the program and arguments used to deliver mail sent by Mutt.
  
  # Mutt expects that the specified program interprets additional
  
  # arguments as recipient addresses.

I need to add an "always-bcc" address, so I tried

  set sendmail="/usr/sbin/sendmail -oem -oi madduck+spool@madduck.net"

and also

  set sendmail="/usr/sbin/sendmail -oem -oi -- madduck+spool@madduck.net"

It turns out, however, that mutt appends not only additional
arguments (recipients), but also unconditionally a '--'. In the
above cases, this yields:

  /usr/sbin/sendmail -oem -oi madduck+spool@madduck.net -- foo@bar.com
  
 /usr/sbin/sendmail -oem -oi -- madduck+spool@madduck.net -- foo@bar.com

Both are wrong. mutt should probably not append -- if it's already
contained in $sendmail.

PS: is there a better way to make mutt always BCC another address,
other than actually using my_hdr, which is simply too brittle?

* owner changed to kevin8t8
* status changed to assigned

--------------------------------------------------------------------------------
2016-10-13 01:14:49 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [changeset:"a3e35631b503196385bb4fd1b4880992a4f059e8" 6816:a3e35631b503]:
{{{
#!CommitTicketReference repository="" revision="a3e35631b503196385bb4fd1b4880992a4f059e8"
Handle presence of '--' delimiter in $sendmail. (closes #3168)

If the delimiter exists, additional sendmail flags will be inserted
before the delimiter.  Any arguments after the delimiter will be
preserved as recipients.
}}}

* resolution changed to fixed
* status changed to closed
