Ticket:  2921
Status:  closed
Summary: using ~(pattern) fails when not sort=thread without notice

Reporter: rado
Owner:    mutt-dev

Opened:       2007-06-24 14:16:30 UTC
Last Updated: 2007-09-04 18:00:17 UTC

Priority:  minor
Component: mutt
Keywords:  sort thread ~() pattern

--------------------------------------------------------------------------------
Description:
The {{{~(pattern)}}} pattern fails when {{{sort!=thread}}}:
rather than giving the desired list, it just beeps in error for not matching anything (muttdebug).
Unfortunately there is no pointer in the status-line giving a clue why it fails.
Also it isn't documented.

Ideally {{{~()}}} could work in other sort-modes, too, but until this works,
a note in the docs and an error message in the status line would help analyzing mismatches.

--------------------------------------------------------------------------------
2007-09-04 13:02:50 UTC pdmef
* Added comment:
Can you please provide an example? I tried to reproduce as follows:

 * Open mutt-dev folder :)
 * sort folder by date
 * limit to ~(~sPATCH)
 * sort folder by thread

As a result, in date-sort order it displays all messages from threads containing messages with PATCH in the subject. When selecting thread-sorting, the mails are displayed properly threaded. Without the limit it also appears to be working for searching.

--------------------------------------------------------------------------------
2007-09-04 18:00:17 UTC rado
* Added comment:
Can't reproduce, might have been bad usage.

* resolution changed to invalid
* status changed to closed
