Ticket:  3407
Status:  closed
Summary: Gmail + IMAP crashes mutt regularly with recent snapshot

Reporter: vext01
Owner:    brendan

Opened:       2010-04-24 09:24:48 UTC
Last Updated: 2010-04-24 23:12:27 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
Hi,

For too long now I have been using mutt and seeing it crash on my gmail account. So last week I fetched a snapshot and built wit hdebug symbols to see if the bug still exists and to get a backtrace if it does.

I reproduced the bug within a few hours. All I need to do is login and do nothing for a while.

Here is the backtrace (I can't see any sensitive information, passwords etc here, but let me know if you do):
{{{
#0  0x037da635 in write () from /usr/lib/libc.so.53.1
No symbol table info available.
#1  0x05902016 in sock_write (b=0x7fb7fec0, in=0x82553000 "\025\003\001", 
    inl=23) at /usr/src/lib/libssl/src/crypto/bio/bss_sock.c:158
        ret = 2142764736
#2  0x059664d5 in BIO_write (b=0x7fb7fec0, in=0x82553000, inl=23)
    at /usr/src/lib/libssl/src/crypto/bio/bio_lib.c:247
        i = 32
        cb = (long int (*)(BIO *, int, const char *, int, long int, 
    long int)) 0
#3  0x0e9a10ea in ssl3_write_pending (s=0x8a87d800, type=32, 
    buf=0x20 <Address 0x20 out of bounds>, len=2)
    at /usr/src/lib/libssl/src/ssl/s3_pkt.c:741
        i = -2078685184
#4  0x0e9a0f41 in do_ssl3_write (s=0x8a87d800, type=21, buf=0x8419c99c "\001", 
    len=2, create_empty_fragment=0) at /usr/src/lib/libssl/src/ssl/s3_pkt.c:714
        p = (unsigned char *) 0x82553005
"bï¿½#ï¿½nQï¿½@ï¿½ï¿½ï¿½ï¿½\237ï¿½\006Eï¿½
        plen = (unsigned char *) 0x82553003 ""
        i = 32
        mac_size = 16
        clear = -2108346363
        prefix_len = 0
        wr = (SSL3_RECORD *) 0x8419c924
        wb = (SSL3_BUFFER *) 0x8419c8f0
#5  0x0e9a1cb2 in ssl3_dispatch_alert (s=0x8a87d800)
    at /usr/src/lib/libssl/src/ssl/s3_pkt.c:1299
        i = -1970808832
        cb = (void (*)(const SSL *, int, 
    int)) 0x237ad67c <_CurrentNumericLocale+4264>
#6  0x0e9a1c46 in ssl3_send_alert (s=0x8a87d800, level=1, desc=0)
    at /usr/src/lib/libssl/src/ssl/s3_pkt.c:1288
No locals.
#7  0x0e998269 in ssl3_shutdown (s=0x8a87d800)
    at /usr/src/lib/libssl/src/ssl/s3_lib.c:2474
No locals.
#8  0x0e9999b6 in SSL_shutdown (s=0x8a87d800)
    at /usr/src/lib/libssl/src/ssl/ssl_lib.c:949
No locals.
#9  0x1c072da9 in ssl_socket_close (conn=0x83e43000) at mutt_ssl.c:369
        data = (sslsockdata *) 0x7c35ef90
#10 0x1c071ac5 in mutt_socket_close (conn=0x83e43000) at mutt_socket.c:81
        rc = 32
#11 0x1c071c8c in mutt_socket_readchar (conn=0x83e43000, 
    c=0x20 <Address 0x20 out of bounds>) at mutt_socket.c:186
No locals.
#12 0x1c071ceb in mutt_socket_readln_d (buf=0x7e1f0600 "a0954 OK Success", 
    buflen=512, conn=0x83e43000, dbg=2) at mutt_socket.c:202
        ch = 0 '\0'
        i = 0
#13 0x1c077eb2 in imap_cmd_step (idata=0x8042d100) at command.c:113
        len = 0
        c = -2143104768
        rc = -1
        stillrunning = 0
        cmd = (IMAP_COMMAND *) 0x0
#14 0x1c078111 in imap_exec (idata=0x8042d100, cmdstr=0x0, flags=1)
    at command.c:245
        rc = 32
#15 0x1c0782ee in cmd_queue (idata=0x8042d100, 
    cmdstr=0xcfbd8150 "STATUS \"Drafts\" (UIDNEXT UIDVALIDITY UNSEEN RECENT)")
    at command.c:378
        cmd = (IMAP_COMMAND *) 0x8419c800
        rc = 32
#16 0x1c078352 in cmd_start (idata=0x8042d100, 
    cmdstr=0x20 <Address 0x20 out of bounds>, flags=4) at command.c:403
        rc = 595252860
#17 0x1c0780ee in imap_exec (idata=0x8042d100, 
    cmdstr=0xcfbd8150 "STATUS \"Drafts\" (UIDNEXT UIDVALIDITY UNSEEN RECENT)", 
    flags=4) at command.c:235
        rc = 32
#18 0x1c07b6a0 in imap_buffy_check (force=0) at imap.c:1526
        idata = (IMAP_DATA *) 0x8042d100
        lastdata = (IMAP_DATA *) 0x8042d100
        mailbox = (BUFFY *) 0x7e405a00
        name = "Drafts\000\000\000
focus\000\000anges\000ï¿½210\205ï¿½ï¿½a\000\000\000\000\000\000\000ï¿½|\212@\000\204\200|ï¿½#\000ï¿½~\001\000\000\000ï¿½\205ï¿½ï¿½ï¿½177\003@\000\204\200ï¿½|\212\000ï¿½~\000ï¿½~ï¿½\206ï¿½ï¿½\205ï¿½ï¿½000ï¿½~|ï¿½#ï¿½\001<\000ï¿½~ï¿½205ï¿½ï¿½ï¿½177\003\000ï¿½~(ï¿½|\003ï¿½205ï¿½ï¿½ï¿½177\003%ï¿½~\005@\037~\000\000\000\000pï¿½213%\000ï¿½~ï¿½\206ï¿½ï¿½b\206ï¿½ï¿½\226\226\005\000ï¿½~\017\216M\022ï¿½o\037\207ï¿½\226\226\005pï¿½213%ï¿½\206ï¿½ï¿½\206ï¿½ï¿½ï¿½224\005\000ï¿½~`\000\000\000"...
        command = "STATUS \"Drafts\" (UIDNEXT UIDVALIDITY UNSEEN
RECENT)\000\000\000ECENT)\000\000CENT)\000T)\000ï¿½002\000\000\000|ï¿½#@\000\000\000\202ï¿½8\212x\203ï¿½ï¿½ï¿½201\003ï¿½201ï¿½ï¿½200ï¿½~\214\203ï¿½ï¿½036\000\000\000@\203ï¿½ï¿½ï¿½ï¿½ï¿½\220ï¿½001<\230ï¿½201\003\204ï¿½8\212@\203ï¿½ï¿½\000\000\000\b\002ï¿½ï¿½\202ï¿½8\212?\000\000\000x\204ï¿½Ï¹ï¿½\201\003\234\202ï¿½ï¿½202ï¿½~\001\000\000\000ï¿½203ï¿½ï¿½\202ï¿½ï¿½\203ï¿½ï¿½\000\000\000\000\203ï¿½ï¿½234\202ï¿½ï¿½\000\000\000"...
        munged = "\"Drafts\"\000\000\000focus\"\000\000nges\"\000m\"", '\0'
<repeats 277 times>,
"ï¿½ï¿½ï¿½ï¿½ï¿½\201ï¿½Ï¸~ï¿½ï¿½ï¿½\005\034#A5\031\200\000\000\000\0349\001<;ï¿½\000<\237c\000<-a\000<\000\000\000\000\000\000\000\000\200\177ï¿½ï¿½002\000\000\0008\200ï¿½Ï¬t\202\003\202ï¿½8\212ï¿½200ï¿½ï¿½002\000\000\000\b\002ï¿½ï¿½200\177ï¿½ï¿½200\000\000\000\024\000\000<ï¿½\205ï¿½ï¿½000\000\000\000ï¿½200ï¿½ï¿½000\000\000\000|ï¿½#p\200ï¿½ï¿½ï¿½ï¿½ï¿½\030\177ï¿½ï¿½230ï¿½201\003ï¿½201ï¿½ï¿½\200ï¿½ï¿½a\000\000\000pï¿½201\003|ï¿½#\002\000\000\000ï¿½\2---Type
<return> to 01ï¿½Ï¹ï¿½\201\003ï¿½177ï¿½"...
        buffies = 0
#19 0x1c00bb3f in mutt_buffy_check (force=0) at buffy.c:380
        tmp = (BUFFY *) 0x0
        sb = {st_dev = 0, st_ino = 741500136, st_mode = 1271597635, 
  st_nlink = 2212769792, st_uid = 2151862528, st_gid = 0, st_rdev = 0, 
  st_lspare0 = 58757814, st_atim = {tv_sec = -809661896, tv_nsec = 0}, 
  st_mtim = {tv_sec = 1, tv_nsec = 100}, st_ctim = {tv_sec = -2, tv_nsec = 0}, 
  st_size = 0, st_blocks = 2151862528, st_blksize = 3485305464, 
  st_flags = 470266782, st_gen = 2151862528, st_lspare1 = 0, __st_birthtim = {
    tv_sec = -809661848, tv_nsec = 204651666}, st_qspare = {0, 259796147008}}
        contex_sb = {st_dev = 0, st_ino = 0, st_mode = 3485305336, 
  st_nlink = 58884739, st_uid = 0, st_gid = 3485305264, st_rdev = -809662008, 
  st_lspare0 = 204653691, st_atim = {tv_sec = 10754, tv_nsec = 1}, st_mtim = {
    tv_sec = 18944, tv_nsec = 1223}, st_ctim = {tv_sec = 2130771716, 
    tv_nsec = -15592169}, st_size = 1087077324213066755, 
  st_blocks = 164931039068161, st_blksize = 38400, st_flags = 0, 
  st_gen = 422920483, st_lspare1 = 0, __st_birthtim = {tv_sec = 2, 
    tv_nsec = 0}, st_qspare = {-4294967297, 2018006463573494328}}
        t = 595252860
#20 0x1c01dfca in mutt_index_menu () at curs_main.c:522
        check = 0
        buf = "---Mutt: =INBOX [Msgs:60 Post:52 Inc:16
856K]---(threads/last-date-received)-(end)---\000\000\000
\001\000\000`\217ï¿½ï¿½000\000\000\000\001\000\000\000x\223ï¿½ï¿½l\001\034\000ï¿½8\212@ï¿½001<ï¿½ï¿½ï¿½ï¿½`\217ï¿½ï¿½orting
mailbox...\000\000\200ï¿½8\212ï¿½\223ï¿½ï¿½l\001\034\000ï¿½8\212@ï¿½001<ï¿½ï¿½ï¿½ï¿½\220\217ï¿½ï¿½eading
imaps://\"INBOX\"\000"...
        helpstr = "q:Quit  d:Del  u:Undel  s:Save  m:Mail  r:Reply  g:Group
?:Help", '\0' <repeats 272 times>,
"ï¿½ï¿½ï¿½ï¿½`\232\016}X\214ï¿½ï¿½\215ï¿½ï¿½A5\031a\232\016}", '\0' <repeats 12
times>,
"`\232\016}\000\000\000\001$x2,\000e2,`\232\016}H\215ï¿½ï¿½24\f\203ï¿½8\212?\000\000\000`\232\016}\a\000\000\000m\000\000\000$x2,ï¿½214ï¿½ï¿½ï¿½\fm\000\000\000\000e2,ï¿½214ï¿½ï¿½211ï¿½\f\000ï¿½\200ï¿½ï¿½\000\000ï¿½214ï¿½ï¿½
4\f"...
        op = -1
        done = 0
        i = 0
        j = 0
        tag = 0
        newcount = 16
        oldcount = 16
        rc = 32
        menu = (MUTTMENU *) 0x82561f80
        cp = 0x10 <Address 0x10 out of bounds>
        index_hint = 59
        do_buffy_notify = 1
        close = 0
        attach_msg = 0
        do_buffy_notify = 1
        close = 0
        attach_msg = 0
#21 0x1c03147a in main (argc=3, argv=0xcfbd9c68) at main.c:1020
        folder = "imaps://imap.gmail.com:993/INBOX", '\0' <repeats 223 times>
        subject = 0x0
        includeFile = 0x0
        draftFile = 0x0
        newMagic = 0x0
        msg = (HEADER *) 0x0
        attach = (LIST *) 0x0
        commands = (LIST *) 0x0
        queries = (LIST *) 0x0
        alias_queries = (LIST *) 0x0
        sendflags = 0
        flags = 0
        version = 0
        i = 1006757064
        explicit_folder = 0
        dump_variables = 0
        double_dash = 3
        nargc = 1
}}}

I am using a snapshot from april the 19th on OpenBSD-4.7-current. The build was configure with hcache, ssl, sasl, imap and smtp. This bug has always existed in the OpenBSD packages as far as I remember.

Any more info needed, mail me.

Thanks!

--------------------------------------------------------------------------------
2010-04-24 09:26:47 UTC vext01
* Added comment:
Sorry about the formatting, I can't see a way to correct it.

--------------------------------------------------------------------------------
2010-04-24 19:27:50 UTC brendan
* Updated description:
Hi,

For too long now I have been using mutt and seeing it crash on my gmail account. So last week I fetched a snapshot and built wit hdebug symbols to see if the bug still exists and to get a backtrace if it does.

I reproduced the bug within a few hours. All I need to do is login and do nothing for a while.

Here is the backtrace (I can't see any sensitive information, passwords etc here, but let me know if you do):
{{{
#0  0x037da635 in write () from /usr/lib/libc.so.53.1
No symbol table info available.
#1  0x05902016 in sock_write (b=0x7fb7fec0, in=0x82553000 "\025\003\001", 
    inl=23) at /usr/src/lib/libssl/src/crypto/bio/bss_sock.c:158
        ret = 2142764736
#2  0x059664d5 in BIO_write (b=0x7fb7fec0, in=0x82553000, inl=23)
    at /usr/src/lib/libssl/src/crypto/bio/bio_lib.c:247
        i = 32
        cb = (long int (*)(BIO *, int, const char *, int, long int, 
    long int)) 0
#3  0x0e9a10ea in ssl3_write_pending (s=0x8a87d800, type=32, 
    buf=0x20 <Address 0x20 out of bounds>, len=2)
    at /usr/src/lib/libssl/src/ssl/s3_pkt.c:741
        i = -2078685184
#4  0x0e9a0f41 in do_ssl3_write (s=0x8a87d800, type=21, buf=0x8419c99c "\001", 
    len=2, create_empty_fragment=0) at /usr/src/lib/libssl/src/ssl/s3_pkt.c:714
        p = (unsigned char *) 0x82553005
"bï¿½#ï¿½nQï¿½@ï¿½ï¿½ï¿½ï¿½\237ï¿½\006Eï¿½
        plen = (unsigned char *) 0x82553003 ""
        i = 32
        mac_size = 16
        clear = -2108346363
        prefix_len = 0
        wr = (SSL3_RECORD *) 0x8419c924
        wb = (SSL3_BUFFER *) 0x8419c8f0
#5  0x0e9a1cb2 in ssl3_dispatch_alert (s=0x8a87d800)
    at /usr/src/lib/libssl/src/ssl/s3_pkt.c:1299
        i = -1970808832
        cb = (void (*)(const SSL *, int, 
    int)) 0x237ad67c <_CurrentNumericLocale+4264>
#6  0x0e9a1c46 in ssl3_send_alert (s=0x8a87d800, level=1, desc=0)
    at /usr/src/lib/libssl/src/ssl/s3_pkt.c:1288
No locals.
#7  0x0e998269 in ssl3_shutdown (s=0x8a87d800)
    at /usr/src/lib/libssl/src/ssl/s3_lib.c:2474
No locals.
#8  0x0e9999b6 in SSL_shutdown (s=0x8a87d800)
    at /usr/src/lib/libssl/src/ssl/ssl_lib.c:949
No locals.
#9  0x1c072da9 in ssl_socket_close (conn=0x83e43000) at mutt_ssl.c:369
        data = (sslsockdata *) 0x7c35ef90
#10 0x1c071ac5 in mutt_socket_close (conn=0x83e43000) at mutt_socket.c:81
        rc = 32
#11 0x1c071c8c in mutt_socket_readchar (conn=0x83e43000, 
    c=0x20 <Address 0x20 out of bounds>) at mutt_socket.c:186
No locals.
#12 0x1c071ceb in mutt_socket_readln_d (buf=0x7e1f0600 "a0954 OK Success", 
    buflen=512, conn=0x83e43000, dbg=2) at mutt_socket.c:202
        ch = 0 '\0'
        i = 0
#13 0x1c077eb2 in imap_cmd_step (idata=0x8042d100) at command.c:113
        len = 0
        c = -2143104768
        rc = -1
        stillrunning = 0
        cmd = (IMAP_COMMAND *) 0x0
#14 0x1c078111 in imap_exec (idata=0x8042d100, cmdstr=0x0, flags=1)
    at command.c:245
        rc = 32
#15 0x1c0782ee in cmd_queue (idata=0x8042d100, 
    cmdstr=0xcfbd8150 "STATUS \"Drafts\" (UIDNEXT UIDVALIDITY UNSEEN RECENT)")
    at command.c:378
        cmd = (IMAP_COMMAND *) 0x8419c800
        rc = 32
#16 0x1c078352 in cmd_start (idata=0x8042d100, 
    cmdstr=0x20 <Address 0x20 out of bounds>, flags=4) at command.c:403
        rc = 595252860
#17 0x1c0780ee in imap_exec (idata=0x8042d100, 
    cmdstr=0xcfbd8150 "STATUS \"Drafts\" (UIDNEXT UIDVALIDITY UNSEEN RECENT)", 
    flags=4) at command.c:235
        rc = 32
#18 0x1c07b6a0 in imap_buffy_check (force=0) at imap.c:1526
        idata = (IMAP_DATA *) 0x8042d100
        lastdata = (IMAP_DATA *) 0x8042d100
        mailbox = (BUFFY *) 0x7e405a00
        name = "Drafts\000\000\000
focus\000\000anges\000ï¿½210\205ï¿½ï¿½a\000\000\000\000\000\000\000ï¿½|\212@\000\204\200|ï¿½#\000ï¿½~\001\000\000\000ï¿½\205ï¿½ï¿½ï¿½177\003@\000\204\200ï¿½|\212\000ï¿½~\000ï¿½~ï¿½\206ï¿½ï¿½\205ï¿½ï¿½000ï¿½~|ï¿½#ï¿½\001<\000ï¿½~ï¿½205ï¿½ï¿½ï¿½177\003\000ï¿½~(ï¿½|\003ï¿½205ï¿½ï¿½ï¿½177\003%ï¿½~\005@\037~\000\000\000\000pï¿½213%\000ï¿½~ï¿½\206ï¿½ï¿½b\206ï¿½ï¿½\226\226\005\000ï¿½~\017\216M\022ï¿½o\037\207ï¿½\226\226\005pï¿½213%ï¿½\206ï¿½ï¿½\206ï¿½ï¿½ï¿½224\005\000ï¿½~`\000\000\000"...
        command = "STATUS \"Drafts\" (UIDNEXT UIDVALIDITY UNSEEN
RECENT)\000\000\000ECENT)\000\000CENT)\000T)\000ï¿½002\000\000\000|ï¿½#@\000\000\000\202ï¿½8\212x\203ï¿½ï¿½ï¿½201\003ï¿½201ï¿½ï¿½200ï¿½~\214\203ï¿½ï¿½036\000\000\000@\203ï¿½ï¿½ï¿½ï¿½ï¿½\220ï¿½001<\230ï¿½201\003\204ï¿½8\212@\203ï¿½ï¿½\000\000\000\b\002ï¿½ï¿½\202ï¿½8\212?\000\000\000x\204ï¿½Ï¹ï¿½\201\003\234\202ï¿½ï¿½202ï¿½~\001\000\000\000ï¿½203ï¿½ï¿½\202ï¿½ï¿½\203ï¿½ï¿½\000\000\000\000\203ï¿½ï¿½234\202ï¿½ï¿½\000\000\000"...
        munged = "\"Drafts\"\000\000\000focus\"\000\000nges\"\000m\"", '\0'
<repeats 277 times>,
"ï¿½ï¿½ï¿½ï¿½ï¿½\201ï¿½Ï¸~ï¿½ï¿½ï¿½\005\034#A5\031\200\000\000\000\0349\001<;ï¿½\000<\237c\000<-a\000<\000\000\000\000\000\000\000\000\200\177ï¿½ï¿½002\000\000\0008\200ï¿½Ï¬t\202\003\202ï¿½8\212ï¿½200ï¿½ï¿½002\000\000\000\b\002ï¿½ï¿½200\177ï¿½ï¿½200\000\000\000\024\000\000<ï¿½\205ï¿½ï¿½000\000\000\000ï¿½200ï¿½ï¿½000\000\000\000|ï¿½#p\200ï¿½ï¿½ï¿½ï¿½ï¿½\030\177ï¿½ï¿½230ï¿½201\003ï¿½201ï¿½ï¿½\200ï¿½ï¿½a\000\000\000pï¿½201\003|ï¿½#\002\000\000\000ï¿½\2---Type
<return> to 01ï¿½Ï¹ï¿½\201\003ï¿½177ï¿½"...
        buffies = 0
#19 0x1c00bb3f in mutt_buffy_check (force=0) at buffy.c:380
        tmp = (BUFFY *) 0x0
        sb = {st_dev = 0, st_ino = 741500136, st_mode = 1271597635, 
  st_nlink = 2212769792, st_uid = 2151862528, st_gid = 0, st_rdev = 0, 
  st_lspare0 = 58757814, st_atim = {tv_sec = -809661896, tv_nsec = 0}, 
  st_mtim = {tv_sec = 1, tv_nsec = 100}, st_ctim = {tv_sec = -2, tv_nsec = 0}, 
  st_size = 0, st_blocks = 2151862528, st_blksize = 3485305464, 
  st_flags = 470266782, st_gen = 2151862528, st_lspare1 = 0, __st_birthtim = {
    tv_sec = -809661848, tv_nsec = 204651666}, st_qspare = {0, 259796147008}}
        contex_sb = {st_dev = 0, st_ino = 0, st_mode = 3485305336, 
  st_nlink = 58884739, st_uid = 0, st_gid = 3485305264, st_rdev = -809662008, 
  st_lspare0 = 204653691, st_atim = {tv_sec = 10754, tv_nsec = 1}, st_mtim = {
    tv_sec = 18944, tv_nsec = 1223}, st_ctim = {tv_sec = 2130771716, 
    tv_nsec = -15592169}, st_size = 1087077324213066755, 
  st_blocks = 164931039068161, st_blksize = 38400, st_flags = 0, 
  st_gen = 422920483, st_lspare1 = 0, __st_birthtim = {tv_sec = 2, 
    tv_nsec = 0}, st_qspare = {-4294967297, 2018006463573494328}}
        t = 595252860
#20 0x1c01dfca in mutt_index_menu () at curs_main.c:522
        check = 0
        buf = "---Mutt: =INBOX [Msgs:60 Post:52 Inc:16
856K]---(threads/last-date-received)-(end)---\000\000\000
\001\000\000`\217ï¿½ï¿½000\000\000\000\001\000\000\000x\223ï¿½ï¿½l\001\034\000ï¿½8\212@ï¿½001<ï¿½ï¿½ï¿½ï¿½`\217ï¿½ï¿½orting
mailbox...\000\000\200ï¿½8\212ï¿½\223ï¿½ï¿½l\001\034\000ï¿½8\212@ï¿½001<ï¿½ï¿½ï¿½ï¿½\220\217ï¿½ï¿½eading
imaps://\"INBOX\"\000"...
        helpstr = "q:Quit  d:Del  u:Undel  s:Save  m:Mail  r:Reply  g:Group
?:Help", '\0' <repeats 272 times>,
"ï¿½ï¿½ï¿½ï¿½`\232\016}X\214ï¿½ï¿½\215ï¿½ï¿½A5\031a\232\016}", '\0' <repeats 12
times>,
"`\232\016}\000\000\000\001$x2,\000e2,`\232\016}H\215ï¿½ï¿½24\f\203ï¿½8\212?\000\000\000`\232\016}\a\000\000\000m\000\000\000$x2,ï¿½214ï¿½ï¿½ï¿½\fm\000\000\000\000e2,ï¿½214ï¿½ï¿½211ï¿½\f\000ï¿½\200ï¿½ï¿½\000\000ï¿½214ï¿½ï¿½
4\f"...
        op = -1
        done = 0
        i = 0
        j = 0
        tag = 0
        newcount = 16
        oldcount = 16
        rc = 32
        menu = (MUTTMENU *) 0x82561f80
        cp = 0x10 <Address 0x10 out of bounds>
        index_hint = 59
        do_buffy_notify = 1
        close = 0
        attach_msg = 0
        do_buffy_notify = 1
        close = 0
        attach_msg = 0
#21 0x1c03147a in main (argc=3, argv=0xcfbd9c68) at main.c:1020
        folder = "imaps://imap.gmail.com:993/INBOX", '\0' <repeats 223 times>
        subject = 0x0
        includeFile = 0x0
        draftFile = 0x0
        newMagic = 0x0
        msg = (HEADER *) 0x0
        attach = (LIST *) 0x0
        commands = (LIST *) 0x0
        queries = (LIST *) 0x0
        alias_queries = (LIST *) 0x0
        sendflags = 0
        flags = 0
        version = 0
        i = 1006757064
        explicit_folder = 0
        dump_variables = 0
        double_dash = 3
        nargc = 1
}}}

I am using a snapshot from april the 19th on OpenBSD-4.7-current. The build was configure with hcache, ssl, sasl, imap and smtp. This bug has always existed in the OpenBSD packages as far as I remember.

Any more info needed, mail me.

Thanks!

--------------------------------------------------------------------------------
2010-04-24 19:33:10 UTC brendan
* owner changed to brendan
* status changed to accepted

--------------------------------------------------------------------------------
2010-04-24 23:12:27 UTC Brendan Cully <brendan@kublai.com>
* Added comment:
(In [4cd2daafd03b]) openssl: only call SSL_shutdown during clean shutdown (closes #3407)

* resolution changed to fixed
* status changed to closed
