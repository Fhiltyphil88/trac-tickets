Ticket:  1938
Status:  closed
Summary: When viewing html attachments, mutt removes the temp file too quickly

Reporter: justinpryzby@users.sourceforge.net
Owner:    mutt-dev

Opened:       2004-11-06 01:14:31 UTC
Last Updated: 2005-08-01 02:55:51 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.5.6-20040722
Severity: normal

-- Please type your report below this line


If I get an HTML mail, I press 'v' to view the components, the press
'enter' when the text/html component is selected.  It opens a firefox
browser, and most of the time firefox responds with "'/tmp/mutt.html'
not found".  Based on my strace, it *appears* that mutt is launching
firefox directly, based on the contents of /etc/mailcap:

	text/html; /usr/bin/mozilla-firefox '%s'; description=HTML Text; test=test -n "$DISPLAY";  nametemplate=%s.html

If that's wrong, then let me know so I can file this on url-handler or
whatever.

I donno if there's a way to check if mozilla has opened a page, but
what mutt should probably do is to *background* the viewer, and remove
the tempfile ("/tmp/mutt.html") when I switch messages or something.

This also resolves a problem I have when mozilla appears to hang
because I have suspended (^Z) the mutt which spawned it.


-- System Information
System Version: Linux andromeda 2.6.9-rc1 #24 Sat Aug 28 23:41:47 EDT 2004 i686 GNU/Linux

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
cc
Reading specs from /usr/lib/gcc-lib/i486-linux/3.3.4/specs
Configured with: ../src/configure -v --enable-languages=c,c++,java,f77,pascal,objc,ada,treelang --prefix=/usr --mandir=/usr/share/man --infodir=/usr/share/info --with-gxx-include-dir=/usr/include/c++/3.3 --enable-shared --with-system-zlib --enable-nls --without-included-gettext --enable-__cxa_atexit --enable-clocale=gnu --enable-debug --enable-java-gc=boehm --enable-java-awt=xlib --enable-objc-gc i486-linux
Thread model: posix
gcc version 3.3.4 (Debian 1:3.3.4-13)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.5.6+20040722i (CVS)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.6.9-rc1 (i686) [using ncurses 5.4] [using libidn 0.5.2 (compiled with 0.4.1)]
Compile options:
-DOMAIN
+DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  +IMAP_EDIT_THREADS  -USE_GSS  -USE_SSL  +USE_GNUTLS  +USE_SASL  +USE_SASL2  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +COMPRESSED  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_LIBIDN  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
MIXMASTER="mixmaster"
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.

patch-1.5.6.tt.compat.1
patch-1.5.3.rr.compressed.1
patch-1.5.6.helmersson.incomplete_multibyte.2
patch-1.5.5.1.nt.xtitles.3.ab.1
patch-1.5.4.vk.pgp_verbose_mime
patch-1.5.4.fw.maildir_inode_sort
patch-1.3.23.1.ametzler.pgp_good_sign
patch-1.5.1.cd.edit_threads.9.2
patch-1.5.3.Md.gpg_status_fd
patch-1.5.3.Md.etc_mailname_gethostbyname
Md.use_debian_editor
Md.muttbug
patch-1.4.admcd.gnutlsbuild.53d
patch-1.4.admcd.gnutls.59d


>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2005-08-01 20:55:51 UTC brendan
* Added comment:
{{{
This is a mailcap bug, not a mutt bug. The better solution is to use a wrapper 
script that holds onto the temp file as long as necessary before returning.
}}}

* resolution changed to fixed
* status changed to closed
