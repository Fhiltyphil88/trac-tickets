Ticket:  3435
Status:  closed
Summary: mutt-1.5.20: Error opening mailbox when too many imap_headers are requested

Reporter: Sylvain.Soliman@m4x.org
Owner:    mutt-dev

Opened:       2010-07-29 16:59:32 UTC
Last Updated: 2010-08-08 17:41:56 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:


{{{
Package: mutt
Version: 1.5.20
Severity: normal

-- Please type your report below this line

imap_headers can be used to ask mutt to request more headers from the IMAP
server. As noted in ticket #3354 one cannot request less...

However standard headers plus a few custom ones result in a request that fails
to fit in the 256 characters reserved for it (see imap/message.c) resulting in
a quite puzzling "Error opening mailbox" for the user.

To reproduce the problem try:
=====
set imap_headers="X-SPAM X-SPAM-STATUS X-SPAM-FLAG X-AMAVIS-SPAM-STATUS
X-SPAM-LEVEL X-PROOFPOINT-SPAM-DETAILS X-MAILER X-BULKMAIL"
=====

I suppose it is not really a severe bug but:
- the error message is not clear for the user;
- at worst less headers (truncate?) could be asked for (and a warning
  issued at config-reading time)
- at best the request could be split...


-- System Information
System Version: Linux vaqueyras 2.6.17-16mdv #1 SMP Wed Sep 26 16:10:48 EDT 2007 i686 Intel(R) Core(TM)2 CPU          6300  @ 1.86GHz GNU/Linux
RedHat Release: Mandriva Linux release 2007.1 (Official) for i586

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Using built-in specs.
Target: i586-mandriva-linux-gnu
Configured with: ../configure --prefix=/usr --libexecdir=/usr/lib --with-slibdir=/lib --mandir=/usr/share/man --infodir=/usr/share/info --enable-checking=release --enable-languages=c,c++,ada,fortran,objc,obj-c++,java --host=i586-mandriva-linux-gnu --with-cpu=generic --with-system-zlib --enable-threads=posix --enable-shared --enable-long-long --enable-__cxa_atexit --disable-libunwind-exceptions --enable-clocale=gnu --enable-java-awt=gtk --with-java-home=/usr/lib/jvm/java-1.4.2-gcj-1.4.2.0/jre --enable-gtk-cairo --disable-libjava-multilib --enable-ssp --disable-libssp
Thread model: posix
gcc version 4.1.2 20070302 (prerelease) (4.1.2-1mdv2007.1)

- CFLAGS
-Wall -pedantic -Wno-long-long -g -O2

-- Mutt Version Information

Mutt 1.5.20 (2009-06-14)
Copyright (C) 1996-2009 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.6.17-16mdv (i686)
slang: 20104
hcache backend: Sleepycat Software: Berkeley DB 4.2.52: (October 31, 2006)
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  +USE_FCNTL  -USE_FLOCK   
+USE_POP  +USE_NNTP  +USE_IMAP  +USE_SMTP  
+USE_SSL_OPENSSL  -USE_SSL_GNUTLS  -USE_SASL  -USE_GSS  +HAVE_GETADDRINFO  
+HAVE_REGCOMP  -USE_GNU_REGEX  +COMPRESSED  
+HAVE_COLOR  -HAVE_START_COLOR  -HAVE_TYPEAHEAD  -HAVE_BKGDSET  
-HAVE_CURS_SET  -HAVE_META  -HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  
-EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  -HAVE_LIBIDN  +HAVE_GETSID  +USE_HCACHE  
ISPELL="/usr/local/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/local/share/mutt"
SYSCONFDIR="/usr/local/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please visit http://bugs.mutt.org/.

vvv.nntp
rr.compressed
}}}

--------------------------------------------------------------------------------
2010-07-29 16:59:32 UTC 
* Added comment:

This message has 0 attachment(s)


* id changed to 3435

--------------------------------------------------------------------------------
2010-08-05 16:35:04 UTC me
* Added comment:
Please try the attached patches.

* status changed to infoneeded_new

--------------------------------------------------------------------------------
2010-08-05 17:33:11 UTC Sylvain Soliman
* Added comment:
{{{
* Mutt <fleas@mutt.org>:

I'll try them tonight (currently on vacation without my source version of
mutt ;).

        Sylvain
}}}

--------------------------------------------------------------------------------
2010-08-05 18:23:47 UTC me
* Added attachment mutt-sprintf.diff
* Added comment:
aux function required for fix

--------------------------------------------------------------------------------
2010-08-05 18:23:56 UTC me
* Added attachment imap-many-headers.diff
* Added comment:
patch allowing arbitrary number of user headers via imap

--------------------------------------------------------------------------------
2010-08-05 18:28:55 UTC me
* Added comment:
Just fixed a problem in the patches.  Please download them again if you already did so when I first added them to this bug report.

--------------------------------------------------------------------------------
2010-08-05 20:32:05 UTC Sylvain Soliman
* Added comment:
{{{
* Mutt <fleas@mutt.org>:

Ok, I must admit I did download them first, and it seemed ok,
re-downloaded, and it also seems fine (not realy intensive testing yet,
but...).

Headers above the 256chars requested and obtained without problem.

Thanks,

        Sylvain
}}}

--------------------------------------------------------------------------------
2010-08-08 17:41:56 UTC Michael Elkins <me@mutt.org>
* Added comment:
(In [53b677c8898b]) avoid error when the user has requested many extra headers via IMAP

closes #3435

* status changed to closed
