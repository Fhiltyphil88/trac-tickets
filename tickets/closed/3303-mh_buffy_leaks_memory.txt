Ticket:  3303
Status:  closed
Summary: mh_buffy leaks memory

Reporter: kees
Owner:    mutt-dev

Opened:       2009-07-21 22:20:13 UTC
Last Updated: 2009-07-22 12:44:46 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:

{{{
==27761== 762,246 bytes in 135 blocks are definitely lost in loss record 69 of 69
==27761==    at 0x4C279E1: realloc (vg_replace_malloc.c:429)
==27761==    by 0x474BB4: safe_realloc (lib.c:176)
==27761==    by 0x442FF4: mhs_set (mh.c:102)
==27761==    by 0x4431B2: mh_read_sequences (mh.c:201)
==27761==    by 0x443F7F: mh_buffy (mh.c:236)
==27761==    by 0x40FDDD: mutt_buffy_check (buffy.c:427)
==27761==    by 0x420034: mutt_index_menu (curs_main.c:535)
==27761==    by 0x43D6C6: main (main.c:1026)
}}}

Looks like the mhs flags aren't free; attached patch solves this.

--------------------------------------------------------------------------------
2009-07-21 22:20:41 UTC kees
* Added attachment mh-leak-fix.patch

--------------------------------------------------------------------------------
2009-07-21 22:32:19 UTC kees
* Added comment:
Additionally, mh_read_sequences does not return "rc", but always "0":

{{{
    rc = -1;
    goto out;
      }
      for (; first <= last; first++)
    mhs_set (mhs, first, f);
    }
  }

  rc = 0;

out:
  FREE (&buff);
  safe_fclose (&fp);
  return 0;
}}}

should be

{{{
  return rc;
}}}

--------------------------------------------------------------------------------
2009-07-22 12:44:46 UTC kees@outflux.net
* Added comment:
(In [9f3053f75f27]) Don't leak mhs flags in mh_buffy(). Closes #3303.

* resolution changed to fixed
* status changed to closed
