Ticket:  1322
Status:  closed
Summary: sh: gettext: not found when starting mutt

Reporter: Tony Leneis <tony@cvr.ds.adp.com>
Owner:    mutt-dev

Opened:       2002-08-22 05:37:44 UTC
Last Updated: 2005-08-02 02:04:41 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.4i
Severity: normal

-- Please type your report below this line

The last line of doc/mutt/samples/gpg.rc fails on systems lacking a             
gettext(1) command:                                                             
                                                                                
set pgp_good_sign="`gettext -d gnupg -s 'Good signature from "' | tr -d '"'`"   
                                                                                
If you source it on such a system, mutt displays the following on startup:      
                                                                                
sh: gettext: not found                                                          
                                                                                
Don't current gpg's indicate a good signature via their return code?  Maybe     
configure could check if gpg works correctly, and if so leave the above         
pgp_good_sign configuration variable commented out?                             
                                                                                
-Tony

p.s. I submitted this last week, but never received a confirmation so I
     don't think it appeared on bugs.guug.de.  I apologize if this is a
     duplicate bug report.

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
cc


cc: Error: no source, object or ucode file specified

- CFLAGS
-std1 -fast -O4 -tune host -arch host -D_XOPEN_SOURCE_EXTENDED

-- Mutt Version Information

Mutt 1.4i (2002-05-29)
Copyright (C) 1996-2001 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: OSF1 V4.0 (alpha)
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_GSS  +USE_SSL  -USE_SASL  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  -HAVE_RESIZETERM  
+HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  -HAVE_GETADDRINFO  
ISPELL="/usr/local/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/spool/mail"
PKGDATADIR="/usr/local/share/mutt"
SYSCONFDIR="/usr/local/etc"
EXECSHELL="/bin/sh"
MIXMASTER="mixmaster"
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.



>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2005-08-02 20:04:41 UTC brendan
* Added comment:
{{{
pgp_good_sign is unset by default.
}}}

* resolution changed to fixed
* status changed to closed
