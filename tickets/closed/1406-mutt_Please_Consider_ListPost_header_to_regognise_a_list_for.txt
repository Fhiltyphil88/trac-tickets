Ticket:  1406
Status:  closed
Summary: mutt: Please Consider "List-Post:" header to regognise a list, for 'L'

Reporter: Marco d'Itri <md@linux.it>
Owner:    mutt-dev

Opened:       2002-12-05 19:40:19 UTC
Last Updated: 2007-09-05 09:34:48 UTC

Priority:  trivial
Component: mutt
Keywords:  patch

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.4.0-5
Severity: normal

[NOTE: this bug report has been submitted to the debian BTS as Bug#171801.
Please Cc all your replies to 171801@bugs.debian.org .]

From: Benoît Rouits <brouits@free.fr>
Subject: mutt: Please Consider "List-Post:" header to regognise a list, for 'L'
Date: 

many mailman uses the (standard?) List-Post: List-etc... headers to
apsecify that the mail is comming from a list. Currently, mutt
seems to only recognise the "Reply-to:list@foo.bar" to identify
a list .. ? it should now consider the "List-*" also. Please.

my .muttrc: ignore_list_reply_to = no 
does not any effect, then.

thx for mutt,
Ben

-- System Information:
Debian Release: testing/unstable
Architecture: i386
Kernel: Linux guinness 2.2.20 #9 Sat Nov 9 18:18:53 CET 2002 i686
Locale: LANG=fr_FR@euro, LC_CTYPE=fr_FR@euro

Versions of packages mutt depends on:
ii  exim [mail-transport-agen 3.36-3         An MTA (Mail Transport Agent)
ii  libc6                     2.3.1-5        GNU C Library: Shared libraries an
ii  libncurses5               5.3.20021109-1 Shared libraries for terminal hand
ii  libsasl7                  1.5.27-3.3     Authentication abstraction library

-- no debconf information


Received: (at submit) by bugs.guug.de; 22 Mar 2003 06:28:02 +0000
From david@eelf.ddts.net Sat Mar 22 07:28:02 2003
Received: from blackhole.oftc.net ([213.70.168.208])
	by trithemius.gnupg.org with esmtp (Exim 3.35 #1 (Debian))
	id 18wcTv-0006kl-00
	for <submit@bugs.guug.de>; Sat, 22 Mar 2003 07:27:51 +0100
Received: from cpe0040f6b429d8-cm400049226055.cpe.net.cable.rogers.com
	([24.101.53.149] helo=willow.eelf.ddts.net ident=mail)
	by blackhole.oftc.net with esmtp (Exim 3.35 #1 (Debian))
	id 18wcVg-0002Y4-00
	for <submit@bugs.guug.de>; Sat, 22 Mar 2003 06:29:40 +0000
Received: from david by willow.eelf.ddts.net with local (Exim 4.12 #1 (Debian) [+prerelease])
	id 18wcVd-00058x-00; Sat, 22 Mar 2003 01:29:37 -0500
From: david@eelf.ddts.net
Subject: mutt-1.5.4i: Lack of support for List-Post
To: submit@bugs.guug.de
Message-Id: <E18wcVd-00058x-00@willow.eelf.ddts.net>
Date: Sat, 22 Mar 2003 01:29:37 -0500

Package: mutt
Version: 1.5.4-1
Severity: wishlist

Many mailing lists these days support List-* headers. I get a fair volume of
mail to lists, but which don't have the list address in To:, Cc:, and friends.
It would be *great* if Mutt also checked List-Post: :)

I will try to implement this myself (and I've done some minor poking at the
source, and been fruitful in those endeavours), but I'm largely unfamiliar
with the Mutt codebase, so I don't know if I'll be able to manage it.

Thanks for the work thus far :)

-- System Information
System Version: Linux willow.eelf.ddts.net 2.4.20-xfs-a10 #1 Sun Feb 16 03:24:36 EST 2003 i686 unknown unknown GNU/Linux

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
cc
Reading specs from /usr/lib/gcc-lib/i386-linux/3.2.3/specs
Configured with: ../src/configure -v --enable-languages=c,c++,java,f77,proto,pascal,objc,ada --prefix=/usr --mandir=/usr/share/man --infodir=/usr/share/info --with-gxx-include-dir=/usr/include/c++/3.2 --enable-shared --with-system-zlib --enable-nls --without-included-gettext --enable-__cxa_atexit --enable-clocale=gnu --enable-java-gc=boehm --enable-objc-gc i386-linux
Thread model: posix
gcc version 3.2.3 20030316 (Debian prerelease)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.5.4i (2003-03-19)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.20-xfs-a10 (i686) [using ncurses 5.3] [using libidn 0.1.11 (compiled with 0.1.11)]
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  +IMAP_EDIT_THREADS  -USE_GSS  -USE_SSL  +USE_GNUTLS  +USE_SASL  -USE_SASL2  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +COMPRESSED  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_LIBIDN  +HAVE_GETSID  +HAVE_GETADDRINFO  +USE_CACHE  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
MIXMASTER="mixmaster"
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.

patch-1.5.3.Md.gpg_status_fd
patch-1.3.23.1.ametzler.pgp_good_sign
patch-1.4.Md.gpg-agent
patch-1.5.1.cd.edit_threads.9.2
patch-1.5.3.Md.etc_mailname_gethostbyname
patch-1.3.27.bse.xtitles.1
Md.muttbug
Md.use_debian_editor
patch-1.4.admcd.gnutlsdlopen.53d
patch-1.5.4.Z.hcache.8
patch-1.4.admcd.gnutlsbuild.53d
patch-1.4.admcd.gnutls.55d
patch-1.5.3.rr.compressed.1


>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2002-12-05 19:40:19 UTC Marco d'Itri <md@linux.it>
* Added comment:
{{{
Package: mutt
Version: 1.4.0-5
Severity: normal

[NOTE: this bug report has been submitted to the debian BTS as Bug#171801.
Please Cc all your replies to 171801@bugs.debian.org .]

From: Benoît Rouits <brouits@free.fr>
Subject: mutt: Please Consider "List-Post:" header to regognise a list, for 'L'
Date: 

many mailman uses the (standard?) List-Post: List-etc... headers to
apsecify that the mail is comming from a list. Currently, mutt
seems to only recognise the "Reply-to:list@foo.bar" to identify
a list .. ? it should now consider the "List-*" also. Please.

my .muttrc: ignore_list_reply_to = no 
does not any effect, then.

thx for mutt,
Ben

-- System Information:
Debian Release: testing/unstable
Architecture: i386
Kernel: Linux guinness 2.2.20 #9 Sat Nov 9 18:18:53 CET 2002 i686
Locale: LANG=fr_FR@euro, LC_CTYPE=fr_FR@euro

Versions of packages mutt depends on:
ii  exim [mail-transport-agen 3.36-3         An MTA (Mail Transport Agent)
ii  libc6                     2.3.1-5        GNU C Library: Shared libraries an
ii  libncurses5               5.3.20021109-1 Shared libraries for terminal hand
ii  libsasl7                  1.5.27-3.3     Authentication abstraction library

-- no debconf information
}}}

--------------------------------------------------------------------------------
2003-03-22 12:29:37 UTC david@eelf.ddts.net
* Added comment:
{{{
Package: mutt
Version: 1.5.4-1
Severity: wishlist

Many mailing lists these days support List-* headers. I get a fair volume of
mail to lists, but which don't have the list address in To:, Cc:, and friends.
It would be *great* if Mutt also checked List-Post: :)

I will try to implement this myself (and I've done some minor poking at the
source, and been fruitful in those endeavours), but I'm largely unfamiliar
with the Mutt codebase, so I don't know if I'll be able to manage it.

Thanks for the work thus far :)

-- System Information
System Version: Linux willow.eelf.ddts.net 2.4.20-xfs-a10 #1 Sun Feb 16 03:24:36 EST 2003 i686 unknown unknown GNU/Linux

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
cc
Reading specs from /usr/lib/gcc-lib/i386-linux/3.2.3/specs
Configured with: ../src/configure -v --enable-languages=c,c++,java,f77,proto,pascal,objc,ada --prefix=/usr --mandir=/usr/share/man --infodir=/usr/share/info --with-gxx-include-dir=/usr/include/c++/3.2 --enable-shared --with-system-zlib --enable-nls --without-included-gettext --enable-__cxa_atexit --enable-clocale=gnu --enable-java-gc=boehm --enable-objc-gc i386-linux
Thread model: posix
gcc version 3.2.3 20030316 (Debian prerelease)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.5.4i (2003-03-19)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.20-xfs-a10 (i686) [using ncurses 5.3] [using libidn 0.1.11 (compiled with 0.1.11)]
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  +IMAP_EDIT_THREADS  -USE_GSS  -USE_SSL  +USE_GNUTLS  +USE_SASL  -USE_SASL2  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +COMPRESSED  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_LIBIDN  +HAVE_GETSID  +HAVE_GETADDRINFO  +USE_CACHE  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
MIXMASTER="mixmaster"
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.

patch-1.5.3.Md.gpg_status_fd
patch-1.3.23.1.ametzler.pgp_good_sign
patch-1.4.Md.gpg-agent
patch-1.5.1.cd.edit_threads.9.2
patch-1.5.3.Md.etc_mailname_gethostbyname
patch-1.3.27.bse.xtitles.1
Md.muttbug
Md.use_debian_editor
patch-1.4.admcd.gnutlsdlopen.53d
patch-1.5.4.Z.hcache.8
patch-1.4.admcd.gnutlsbuild.53d
patch-1.4.admcd.gnutls.55d
patch-1.5.3.rr.compressed.1
}}}

--------------------------------------------------------------------------------
2003-09-15 16:21:55 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
# one problem one solution
merge 965 1178 1228
merge 1406 1505
# no usable reports, no followups to inquieries
close 1057
close 1408
# not a wishlist but a bug
severity 1495 normal
# contains a selfsolving patch
tags 1062 patch
}}}

--------------------------------------------------------------------------------
2003-09-15 16:21:56 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
# one problem one solution
merge 965 1178 1228
merge 1406 1505
# no usable reports, no followups to inquieries
close 1057
close 1408
# not a wishlist but a bug
severity 1495 normal
# contains a selfsolving patch
tags 1062 patch
}}}

--------------------------------------------------------------------------------
2003-09-17 16:17:22 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
# fixed long ago by Thomas
close 910
# not a bug
close 923
# weddings
merge 751 1463
merge 1407 1427
# could lead to data loss by misdirected replies
severity 1488 grave
# submitter agrees lowering
severity 1106 wishlist
tags 1231 patch
tags 1362 patch
tags 1406 patch
retitle 1623 dummy bug for testing retitle -- ignore
-- I wonder why
my inline #comments disapeared.
}}}

--------------------------------------------------------------------------------
2003-09-17 16:17:23 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
# fixed long ago by Thomas
close 910
# not a bug
close 923
# weddings
merge 751 1463
merge 1407 1427
# could lead to data loss by misdirected replies
severity 1488 grave
# submitter agrees lowering
severity 1106 wishlist
tags 1231 patch
tags 1362 patch
tags 1406 patch
retitle 1623 dummy bug for testing retitle -- ignore
-- I wonder why
my inline #comments disapeared.
}}}

--------------------------------------------------------------------------------
2003-09-19 10:49:25 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
Bonjour Benoît, et merci pour la suggestion.

On Thursday, December 5, 2002 at 2:40:19 AM +0100, Benoît Rouits wrote:

> many mailman uses the (standard?) List-Post: List-etc... headers to
> apsecify that the mail is comming from a list.

   For the record: There is a patch by David B. Harris (submitter of
Mutt bug#1505) to recognise the List-Post: header. You can find it on
mutt-dev archives msgid <20030325023345.GD4930@eelf.ddts.net>. Much
thanks David!


> Currently, mutt seems to only recognise the "Reply-to:list@foo.bar" to
> identify a list .. ?

   Actually only To: and Cc:.


Bye!	Alain.
-- 
OpenSSH 3.7 is released.
}}}

--------------------------------------------------------------------------------
2005-09-05 12:49:55 UTC brendan
* Added comment:
{{{
Refile as change-request.
}}}

--------------------------------------------------------------------------------
2007-09-05 09:34:48 UTC pdmef
* Added comment:
hg tip supports mailto-URLs in List-Post headers.

* resolution changed to fixed
* status changed to closed
