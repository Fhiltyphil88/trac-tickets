Ticket:  3512
Status:  closed
Summary: incorrect encoding of Resent-* headers when

Reporter: Giel
Owner:    mutt-dev

Opened:       2011-04-26 10:03:46 UTC
Last Updated: 2011-06-20 23:38:33 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
When bouncing a message to an address like `René xxxxxxxxxxxx <xxxx@xxxxxx.xxx>` (with emphasis on the ''é''

{{{
Resent-From: Giel xxxxxxxxxxxx <xxxx@xxxxxx.xxx>
Resent-Date: Tue, 26 Apr 2011 11:53:27 +0200
Resent-Message-ID: <20110426095327.GI9175@xxxxxxx.xx.xxxxxx.xxx>
Resent-To: RenÃ© xxxxxxxxxxxx <xxxx@xxxxxx.xxx>
}}}

And yes, that ^^ is a Latin1 decoded view of the specific headers, as it illustrates that the ''é'' has been UTF-8 encoded.  This while I believe, like all words containing non-ASCII characters, it ''should'' be encoded as an `encoded-word` as specified in [http://tools.ietf.org/html/rfc2047 RFC 2047].

--------------------------------------------------------------------------------
2011-06-20 23:38:33 UTC Michael Elkins <me@mutt.org>
* Added comment:
(In [b574d6f9f532]) rfc2047 encode recipient list in resent-to header field.  closes #3512

* resolution changed to fixed
* status changed to closed
