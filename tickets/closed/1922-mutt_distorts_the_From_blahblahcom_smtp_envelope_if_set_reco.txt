Ticket:  1922
Status:  closed
Summary: mutt distorts the 'From blah@blah.com' smtp envelope if set record="imaps://foo.com/sent

Reporter: kiran@in.ibm.com
Owner:    roessler

Opened:       2004-10-15 11:38:22 UTC
Last Updated: 2005-08-01 17:45:15 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.4.1i
Severity: normal

-- Please type your report below this line
If ~/.muttrc contains 
set record="imaps://kiran@foo.in.ibm.com/mutt/sent"
Mutt changes the smtp From envelope as:
From kiran@foo.in.ibm.com Thu Oct 14 15:10:30 2004 +0530
If the set record points to a local folder, then the From contains 
kiran@in.ibm.com as it should.


-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/lib/gcc-lib/i386-redhat-linux/3.3.3/specs
Configured with: ../configure --prefix=/usr --mandir=/usr/share/man --infodir=/usr/share/info --enable-shared --enable-threads=posix --disable-checking --disable-libunwind-exceptions --with-system-zlib --enable-__cxa_atexit --host=i386-redhat-linux
Thread model: posix
gcc version 3.3.3 20040412 (Red Hat Linux 3.3.3-7)

- CFLAGS
-O2 -g -pipe -march=i386 -mcpu=i686

-- Mutt Version Information

Mutt 1.4.1i (2003-03-19)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.6.8-1.541_10.rhfc2.at (i686) [using ncurses 5.4]
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  -USE_SETGID  -USE_DOTLOCK  -DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  +USE_GSS  +USE_SSL  +USE_SASL  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.


--- Begin /home/kiran/.muttrc
ignore *
unignore "From" "To" "Cc" "Subject" "Date" "X-Spam-Level:" "from"
source ~/.mail_aliases
set alias_file="~/.mail_aliases"
set beep_new=yes
set folder="imaps://kiran@foo.in.ibm.com/mutt"
set forward_quote=yes
set from="kiran@in.ibm.com"
set mark_old=no
set mbox="~/Mail/mbox"
set postponed="~/Mail/postponed"
set realname="Ravikiran G Thirumalai"
set record="imaps://kiran@foo.in.ibm.com/mutt/sent"
set sort=threads
color hdrdefault cyan black
color indicator brightred black
color status black brightgreen
--- End /home/kiran/.muttrc


I can't reproduce this problem. How are you extracting the SMTP from header? If you're reading the fcc folder directly, your IMAP server is probably responsible for generating the header.
>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2005-07-24 20:55:57 UTC brendan
* Added comment:
{{{
Can't reproduce. How is the submitter extracting the SMTP FROM header?
}}}

* status changed to assigned

--------------------------------------------------------------------------------
2005-08-02 05:51:00 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
Hello Ravikiran, and thanks for the report.

 On Thursday, October 14, 2004 at 3:08:22 PM +0530,
 Ravikiran G. Thirumalai wrote:

> If ~/.muttrc contains
>| set record="imaps://kiran@foo.in.ibm.com/mutt/sent"
> Mutt changes the smtp From envelope as:
>| From kiran@foo.in.ibm.com Thu Oct 14 15:10:30 2004 +0530
> If the set record points to a local folder, then the From contains
> kiran@in.ibm.com as it should.

    Thomas or Brendan wrote:

| I can't reproduce this problem. How are you extracting the SMTP from
| header? If you're reading the fcc folder directly, your IMAP server is
| probably responsible for generating the header.

    I could reproduce with Mutt 1.5.9 on an old uw-imapd 2001a with mbox
storage, and can confirm the scenario: Mutt has *not* recorded "From_"
to imap folder, the server generated it as "server_username@server" for
the needs of its own local mbox storage, and this "From_" line is only
visible when reading directly the mbox storage file on the server.

    IIRC Mutt generates a "From_" line only when needed, when writing to
an mbox. Not when writing to a Maildir nor an imap folder. The "From_"
line is purely an mbox thing: A separator, in some way not part of a
message.

    Do you validate the scenario? Can we close mutt/1922?


Bye!	Alain.
-- 
New Mutt BTS Gnats at <URL:http://bugs.mutt.org/>.
<URL:http://bugs.mutt.org/cgi-bin/gnatsweb.pl?cmd=view%20audit-trail&database=mutt&pr=1922>
}}}

--------------------------------------------------------------------------------
2005-08-02 11:45:15 UTC ab
* Added comment:
{{{
Hello Ravikiran, and thanks for the report.
    Not a bug, and submitter unreachable (User kiran 
(kiran@in.ibm.com) not listed in Domino Directory).
}}}

* resolution changed to fixed
* status changed to closed
