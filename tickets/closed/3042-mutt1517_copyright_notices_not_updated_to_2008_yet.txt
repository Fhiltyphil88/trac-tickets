Ticket:  3042
Status:  closed
Summary: mutt-1.5.17: copyright notices not updated to 2008 yet?

Reporter: m-a
Owner:    mutt-dev

Opened:       2008-03-26 13:05:33 UTC
Last Updated: 2008-05-11 07:51:37 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:


{{{
Package: mutt
Version: 1.5.17
Severity: wishlist

-- Please type your report below this line
mutt -vv (from Mercurial), whilst stating 2008-03-.. as date, still
has copyright notices only up to and including 2007. mutt -v (as shown
in this report) seems to be up to date.

Remember to update relevant entries to 2008 before release :-)


-- System Information
System Version: Linux merlin 2.6.18.8-0.9-default #1 SMP Sun Feb 10 22:48:05 UTC 2008 i686 athlon i386 GNU/Linux
SuSE Release: openSUSE 10.2 (i586)

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Using built-in specs.
Target: i586-suse-linux
Configured with: ../configure --enable-threads=posix --prefix=/usr --with-local-prefix=/usr/local --infodir=/usr/share/info --mandir=/usr/share/man --libdir=/usr/lib --libexecdir=/usr/lib --enable-languages=c,c++,objc,fortran,obj-c++,java,ada --enable-checking=release --with-gxx-include-dir=/usr/include/c++/4.1.2 --enable-ssp --disable-libssp --disable-libgcj --with-slibdir=/lib --with-system-zlib --enable-shared --enable-__cxa_atexit --enable-libstdcxx-allocator=new --program-suffix=-4.1 --enable-version-specific-runtime-libs --without-system-libunwind --with-cpu=generic --host=i586-suse-linux
Thread model: posix
gcc version 4.1.2 20061115 (prerelease) (SUSE Linux)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.5.17 (2008-03-09)
Copyright (C) 1996-2008 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.6.18.8-0.9-default (i686)
ncurses: ncurses 5.5.20060513 (compiled with 5.5)
libidn: 0.6.8 (compiled with 0.1.14)
hcache backend: qdbm 1.8.75
Compile options:
-DOMAIN
+DEBUG
-HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK   
+USE_POP  +USE_IMAP  -USE_SMTP  +USE_GSS  +USE_SSL_OPENSSL  -USE_SSL_GNUTLS  +USE_SASL  +HAVE_GETADDRINFO  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  +CRYPT_BACKEND_GPGME  
-EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_LIBIDN  +HAVE_GETSID  +USE_HCACHE  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/local/share/mutt"
SYSCONFDIR="/usr/local/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please visit http://bugs.mutt.org/.

patch-1.5.14.tamo.pgp_charsethack.1
}}}

--------------------------------------------------------------------------------
2008-05-11 07:51:37 UTC brendan
* Added comment:
(In [98c39c5187a9]) Update copyright in -vv output. Closes #3042.

* resolution changed to fixed
* status changed to closed
