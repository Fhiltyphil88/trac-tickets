Ticket:  3767
Status:  closed
Summary: Compiler warnings re copy.c on older OSX

Reporter: balderdash
Owner:    mutt-dev

Opened:       2015-07-24 21:39:45 UTC
Last Updated: 2015-07-29 09:08:38 UTC

Priority:  major
Component: build
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
On MacOSX 10.4.11, I see the following warning during the make phase:

gcc -DPKGDATADIR=\"/usr/local/share/mutt\" -DSYSCONFDIR=\"/usr/local/etc\" -DBINDIR=\"/usr/local/bin\" -DMUTTLOCALEDIR=\"/usr/local/share/locale\" -DHAVE_CONFIG_H=1 -I.  -I. -I. -I./imap  -Iintl -I/usr/local/ssl/include -I/usr/local/opt/cyrus-sasl/include -I./intl  -Wall -pedantic -Wno-long-long -g -O2 -MT copy.o -MD -MP -MF .deps/copy.Tpo -c -o copy.o copy.c
copy.c: In function ‘mutt_copy_header’:
copy.c:411: warning: ISO C does not support the ‘q’ printf length modifier
copy.c: In function ‘_mutt_copy_message’:
copy.c:519: warning: ISO C does not support the ‘q’ printf length modifier
copy.c: In function ‘copy_delete_attach’:
copy.c:772: warning: ISO C does not support the ‘q’ printf length modifier


I don't know whether that's important or not.  Mutt builds just fine.
}}}

--------------------------------------------------------------------------------
2015-07-25 16:56:53 UTC kevin8t8
* Added comment:
It looks like Mutt is using the PRId64 modifier.  OS X is translating this to "qd".  The compiler is warning it's not "ISO", but that seems okay in this case, since it's OS X's own declarations that are doing this.  Thanks for reporting the warning, but in this case I don't think we should (or even can) do anything about it.

* resolution changed to wontfix
* status changed to closed

--------------------------------------------------------------------------------
2015-07-26 09:43:00 UTC ossi
* Added comment:
nahhhh ... you can't say that unless the problem persists when you remove the -ansi command line switch.

--------------------------------------------------------------------------------
2015-07-26 14:46:44 UTC kevin8t8
* Added comment:
Hi Oswald,

Sorry, would you explain more?  Which switch are you referring to (I don't see -ansi in the invocation he pasted above), and why is this warning a concern?

Thanks,

-Kevin

--------------------------------------------------------------------------------
2015-07-26 14:51:42 UTC ossi
* _comment0 changed to 1437922321045303
* Added comment:
sorry, i meant -pedantic. -ansi is somewhat related, but not used here.

--------------------------------------------------------------------------------
2015-07-26 17:24:12 UTC kevin8t8
* Added comment:
balderdash, would you mind running:
{{{
gcc -DPKGDATADIR=\"/usr/local/share/mutt\" -DSYSCONFDIR=\"/usr/local/etc\" -DBINDIR=\"/usr/local/bin\" -DMUTTLOCALEDIR=\"/usr/local/share/locale\" -DHAVE_CONFIG_H=1 -I.  -I. -I. -I./imap  -Iintl -I/usr/local/ssl/include -I/usr/local/opt/cyrus-sasl/include -I./intl  -Wall -Wno-long-long -g -O2 -MT copy.o -MD -MP -MF .deps/copy.Tpo -c -o copy.o copy.c
}}}
and see if it still gives the warning?

--------------------------------------------------------------------------------
2015-07-27 17:40:29 UTC balderdash
* Added comment:
I ran that line immediately after running the configure script with my usual flags.  I didn't do 'make' beforehand.  It emitted no warning.

I appreciate you taking the time to investigate a non-essential aspect of building mutt on a very old version of Mac OSX.  Don't fix this if it runs a risk of goofing up the build process on other systems.

--------------------------------------------------------------------------------
2015-07-28 14:04:30 UTC kevin8t8
* Added comment:
Thank you for running the command, balderdash.  I appreciate you reporting the warnings, even if we can't fix all of them.

Since this warning disappears without the "pedantic" flag, I am inclined to leave this bug closed.  Is that alright with you, Oswald?

--------------------------------------------------------------------------------
2015-07-29 09:08:38 UTC ossi
* Added comment:
huh? that entirely depends on where that flag came from. if it's requested by mutt's configure, this positively would be a flea. if it's from the CFLAGS env variable or some other external source, we're done here.
