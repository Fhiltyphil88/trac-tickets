Ticket:  2676
Status:  closed
Summary: mutt + imap header results in a NULL pointer dereference crash

Reporter: mail@rink.nu
Owner:    mutt-dev

Opened:       2007-01-10 05:47:49 UTC
Last Updated: 2007-04-12 16:25:18 UTC

Priority:  major
Component: IMAP
Keywords:  

--------------------------------------------------------------------------------
Description:
Whenever I try to check my employer's mail (via IMAP+TLS),
I use IMAP header-caching. However, mutt crashes on this particular mailbox. gdb(1) gives:
{{{
Fetching message headers... [402/402]
Program received signal SIGSEGV, Segmentation fault.
0x080927df in mx_update_context (ctx=0x8121900, new_messages=402) at
mx.c:1618
1618          h->security = crypt_query (h->content);
(gdb) inspect h
$1 = (HEADER *) 0x0
(gdb) inspect ctx->hdrs[399]
$3 = (HEADER *) 0x81c4d80
(gdb) inspect ctx->hdrs[400]
$4 = (HEADER *) 0x0
(gdb) inspect ctx->hdrs[401]
$5 = (HEADER *) 0x8121980
(gdb) bt
#0  0x080927df in mx_update_context (ctx=0x8121900, new_messages=402)
    at mx.c:1618
#1  0x080e2069 in imap_read_headers (idata=0x8117a00, msgbegin=401,
msgend=401)
    at message.c:344
#2  0x080de706 in imap_open_mailbox (ctx=0x8121900) at imap.c:737
#3  0x080908e2 in mx_open_mailbox (path=0xbfbfdc30
"imap://imap.qsp.nl/",
    flags=0, pctx=0x0) at mx.c:719
#4  0x08067827 in mutt_index_menu () at curs_main.c:1129
#5  0x08085b2a in main (argc=1, argv=0xbfbfe884) at main.c:969
}}}
For some reason, message 400's headers point to a NULL pointer. However,
I can read the message using mutt without headercache (which is enabled
using --with-hcache in configure) just fine.

I have left the gdb(1) running; let me know if anything else needs to be inspected
>How-To-Repeat:
Try to open my employer's IMAP mailbox, while IMAP header caching is enabled.
>Fix:
Disable headercaching (configuring with --with-hcache will do) solves the problem.

--------------------------------------------------------------------------------
2007-03-31 06:14:15 UTC brendan
* Updated description:
Whenever I try to check my employer's mail (via IMAP+TLS),
I use IMAP header-caching. However, mutt crashes on this particular mailbox. gdb(1) gives:
{{{
Fetching message headers... [402/402]
Program received signal SIGSEGV, Segmentation fault.
0x080927df in mx_update_context (ctx=0x8121900, new_messages=402) at
mx.c:1618
1618          h->security = crypt_query (h->content);
(gdb) inspect h
$1 = (HEADER *) 0x0
(gdb) inspect ctx->hdrs[399]
$3 = (HEADER *) 0x81c4d80
(gdb) inspect ctx->hdrs[400]
$4 = (HEADER *) 0x0
(gdb) inspect ctx->hdrs[401]
$5 = (HEADER *) 0x8121980
(gdb) bt
#0  0x080927df in mx_update_context (ctx=0x8121900, new_messages=402)
    at mx.c:1618
#1  0x080e2069 in imap_read_headers (idata=0x8117a00, msgbegin=401,
msgend=401)
    at message.c:344
#2  0x080de706 in imap_open_mailbox (ctx=0x8121900) at imap.c:737
#3  0x080908e2 in mx_open_mailbox (path=0xbfbfdc30
"imap://imap.qsp.nl/",
    flags=0, pctx=0x0) at mx.c:719
#4  0x08067827 in mutt_index_menu () at curs_main.c:1129
#5  0x08085b2a in main (argc=1, argv=0xbfbfe884) at main.c:969
}}}
For some reason, message 400's headers point to a NULL pointer. However,
I can read the message using mutt without headercache (which is enabled
using --with-hcache in configure) just fine.

I have left the gdb(1) running; let me know if anything else needs to be inspected
>How-To-Repeat:
Try to open my employer's IMAP mailbox, while IMAP header caching is enabled.
>Fix:
Disable headercaching (configuring with --with-hcache will do) solves the problem.

--------------------------------------------------------------------------------
2007-04-02 01:47:14 UTC brendan
* milestone changed to 1.6

--------------------------------------------------------------------------------
2007-04-07 20:45:30 UTC brendan
* Added comment:
Can you retry with 1.5.15? There have been some header cache fixes recently, but I've never been able to reproduce this bug.

--------------------------------------------------------------------------------
2007-04-12 16:25:18 UTC Brendan Cully <brendan@kublai.com>
* Added comment:
(In [eb5db1cd5251]) Handle a missing or corrupted header cache entry (closes #2676)
If imap_hcache_get fails, stop parsing the header cache and simply
refetch from that point on.

* resolution changed to fixed
* status changed to closed
