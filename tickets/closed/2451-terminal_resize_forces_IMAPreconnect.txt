Ticket:  2451
Status:  closed
Summary: terminal resize forces IMAP-reconnect

Reporter: p+lists@2006.smokva.net
Owner:    mutt-dev

Opened:       2006-08-26 22:35:47 UTC
Last Updated: 2006-09-19 19:54:38 UTC

Priority:  minor
Component: IMAP
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
I prefer to work with a tiling window manager so it happens that mutt often gets resized. The resize works.. sometimes. Sometimes mutt just hangs or the current IMAP-Mailbox gets closed and a re-login is required.

I noticed this behaviour while using putty on Windows resp. xterm / rxvt / urxvt / gnome-terminal on X.
>How-To-Repeat:
-> Spawn an `IMAP-configured' mutt.
-> Wait for the `Password..' prompt.
-> Resize mutt.
-> Watch how mutt reconnects..
-> ..or Watch how mutt hangs.
>Fix:
}}}

--------------------------------------------------------------------------------
2006-09-20 13:54:38 UTC rado
* Added comment:
{{{
requested by OP
}}}

* resolution changed to fixed
* status changed to closed
