Ticket:  3179
Status:  closed
Summary: Attach: pseudoheader doesn't work with files with spaces

Reporter: edgewood
Owner:    mutt-dev

Opened:       2009-01-29 22:26:58 UTC
Last Updated: 2009-02-15 15:10:15 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
I'm running into problems with using the Attach: pseudoheader with files with spaces in their names.

If I try attach a file named 'space test.txt' using "Attach: space test.txt", mutt replies "space: unable to attach file".

If I use "Attach: space\ test.txt", mutt replies "space\: unable to attach file".  I've also tried "Attach: 'space test.txt'" and the same with double quotes, with similar error messages.

Mutt's manual indicates that the Attach: pseudoheader allows a file name optionally followed by a space and a description, and the error message leads me to believe that mutt assumes that any space after the start of the filename is the description delimiter.

--------------------------------------------------------------------------------
2009-02-04 15:22:37 UTC chrisbra
* Added comment:
I have created an experimental patch for that issue. My C-skills are rusty, but it seems to work for me. This patch is against 1.5.18.

--------------------------------------------------------------------------------
2009-02-04 15:22:47 UTC chrisbra
* Added attachment fix_attach_patch.diff

--------------------------------------------------------------------------------
2009-02-09 17:59:43 UTC edgewood
* Added comment:
There seems to be a missing semicolon between the lines "*q='\0'" and
"q-=leng;" (listed as lines 184 and 185 at
<http://dev.mutt.org/trac/attachment/ticket/3179/fix_attach_patch.diff>)

Adding the semicolon manually at the end of line 184 allows the patch to
compile, and once compiled, the patch works as expected.

--------------------------------------------------------------------------------
2009-02-15 15:10:15 UTC Rocco Rutte <pdmef@gmx.net>
* Added comment:
(In [ee5e696a9d08]) Support spaces in Attach: pseudoheader. Closes #3179.

* resolution changed to fixed
* status changed to closed
