Ticket:  1550
Status:  closed
Summary: [patch] wishlist: bind on multiple maps at the same time

Reporter: Aaron Lehmann <aaronl@vitelus.com>
Owner:    mutt-dev

Opened:       2003-04-24 02:47:52 UTC
Last Updated: 2005-09-04 19:10:10 UTC

Priority:  trivial
Component: mutt
Keywords:  patch

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.5.4i
Severity: wishlist
Tags: patch

Very often you want a keybinding to work in more than just a single
context. For example, if you want to bind q to "mail" such that it works
both when you're in the pager and in the index, you'd need a bind line
for each of these maps. With this patch, macros and bindings can be
attached to multiple maps if their names are seperated by commas.
Examples:

	bind index,pager q mail
	bind generic,browser,index,attach <home> first-entry
	macro index,pager o "c?\t"

I was able to make my muttrc 34 lines shorter by combining bind and
macro lines like this (I have a lot). It makes editing and reading the
muttrc a lot easier.

The following patch implements the feature and adds documentation for
it.


Index: keymap.c
===================================================================
RCS file: /home/roessler/cvs/mutt/keymap.c,v
retrieving revision 3.7
diff -u -u -r3.7 keymap.c
--- keymap.c	21 Jan 2003 12:33:41 -0000	3.7
+++ keymap.c	24 Apr 2003 00:51:06 -0000
@@ -685,38 +685,53 @@
   return (r);
 }
 
-/* expects to see: <menu-string> <key-string> */
-char *parse_keymap (int *menu, BUFFER *s, BUFFER *err)
+/* expects to see: <menu-string>,<menu-string>,... <key-string> */
+static char *parse_keymap (int *menu, BUFFER *s, int *nummenus, BUFFER *err)
 {
   BUFFER buf;
+  int i=0;
+  char *p, *q;
 
   memset (&buf, 0, sizeof (buf));
 
   /* menu name */
   mutt_extract_token (&buf, s, 0);
+  p = buf.data;
   if (MoreArgs (s))
   {
-    if ((*menu = mutt_check_menu (buf.data)) == -1)
+    while (i != sizeof(Menus)/sizeof(struct mapping_t)-1)
     {
-      snprintf (err->data, err->dsize, _("%s: no such menu"), buf.data);
-    }
-    else
-    {
-      /* key sequence */
-      mutt_extract_token (&buf, s, 0);
+      q = strchr(p,',');
+      if (q)
+        *q = '\0';
 
-      if (!*buf.data)
+      if ((menu[i] = mutt_check_menu (p)) == -1)
       {
-	strfcpy (err->data, _("null key sequence"), err->dsize);
+         snprintf (err->data, err->dsize, _("%s: no such menu"), p);
+         goto error;
       }
-      else if (MoreArgs (s))
-	return (buf.data);
+      ++i;
+	  if (q)
+        p = q+1;
+      else
+        break;
+    }
+    *nummenus=i;
+    /* key sequence */
+    mutt_extract_token (&buf, s, 0);
+
+    if (!*buf.data)
+    {
+      strfcpy (err->data, _("null key sequence"), err->dsize);
     }
+    else if (MoreArgs (s))
+      return (buf.data);
   }
   else
   {
     strfcpy (err->data, _("too few arguments"), err->dsize);
   }
+error:
   FREE (&buf.data);
   return (NULL);
 }
@@ -777,9 +792,9 @@
 {
   struct binding_t *bindings = NULL;
   char *key;
-  int menu, r = 0;
+  int menu[sizeof(Menus)/sizeof(struct mapping_t)-1], r = 0, nummenus, i;
 
-  if ((key = parse_keymap (&menu, s, err)) == NULL)
+  if ((key = parse_keymap (menu, s, &nummenus, err)) == NULL)
     return (-1);
 
   /* function to execute */
@@ -790,19 +805,26 @@
     r = -1;
   }
   else if (ascii_strcasecmp ("noop", buf->data) == 0)
-    km_bindkey (key, menu, OP_NULL); /* the `unbind' command */
+    for (i=0; i != nummenus; ++i)
+    {
+      km_bindkey (key, menu[i], OP_NULL); /* the `unbind' command */
+    }
   else
   {
-    /* First check the "generic" list of commands */
-    if (menu == MENU_PAGER || menu == MENU_EDITOR || menu == MENU_GENERIC ||
-	try_bind (key, menu, buf->data, OpGeneric) != 0)
-    {
-      /* Now check the menu-specific list of commands (if they exist) */
-      bindings = km_get_table (menu);
-      if (bindings && try_bind (key, menu, buf->data, bindings) != 0)
+    for (i=0; i != nummenus; ++i)
+    {
+      /* First check the "generic" list of commands */
+      if (menu[i] == MENU_PAGER || menu[i] == MENU_EDITOR ||
+      menu[i] == MENU_GENERIC ||
+	  try_bind (key, menu[i], buf->data, OpGeneric) != 0)
       {
-	snprintf (err->data, err->dsize, _("%s: no such function in map"), buf->data);
-	r = -1;
+        /* Now check the menu-specific list of commands (if they exist) */
+        bindings = km_get_table (menu[i]);
+        if (bindings && try_bind (key, menu[i], buf->data, bindings) != 0)
+        {
+          snprintf (err->data, err->dsize, _("%s: no such function in map"), buf->data);
+          r = -1;
+        }
       }
     }
   }
@@ -813,11 +835,11 @@
 /* macro <menu> <key> <macro> <description> */
 int mutt_parse_macro (BUFFER *buf, BUFFER *s, unsigned long data, BUFFER *err)
 {
-  int menu, r = -1;
+  int menu[sizeof(Menus)/sizeof(struct mapping_t)-1], r = -1, nummenus, i;
   char *seq = NULL;
   char *key;
 
-  if ((key = parse_keymap (&menu, s, err)) == NULL)
+  if ((key = parse_keymap (menu, s, &nummenus, err)) == NULL)
     return (-1);
 
   mutt_extract_token (buf, s, M_TOKEN_CONDENSE);
@@ -839,16 +861,22 @@
       }
       else
       {
-	km_bind (key, menu, OP_MACRO, seq, buf->data);
-	r = 0;
+        for (i=0; i != nummenus; ++i)
+        {
+          km_bind (key, menu[i], OP_MACRO, seq, buf->data);
+          r = 0;
+        }
       }
 
       FREE (&seq);
     }
     else
     {
-      km_bind (key, menu, OP_MACRO, buf->data, NULL);
-      r = 0;
+      for (i=0; i != nummenus; ++i)
+      {
+        km_bind (key, menu[i], OP_MACRO, buf->data, NULL);
+        r = 0;
+      }
     }
   }
   FREE (&key);
Index: doc/manual.sgml.head
===================================================================
RCS file: /home/roessler/cvs/mutt/doc/manual.sgml.head,v
retrieving revision 3.19
diff -u -u -r3.19 manual.sgml.head
--- doc/manual.sgml.head	3 Mar 2003 08:26:21 -0000	3.19
+++ doc/manual.sgml.head	24 Apr 2003 00:51:10 -0000
@@ -882,9 +882,11 @@
 This command allows you to change the default key bindings (operation
 invoked when pressing a key).
 
-<em/map/ specifies in which menu the binding belongs.  The currently
-defined maps are:
+<em/map/ specifies in which menu the binding belongs.  Multiple maps may
+be specified by separating them with commas (no additional whitespace is
+allowed). The currently defined maps are:
 
+<label id="maps">
 <descrip>
 <tag/generic
 This is not a real menu, but is used as a fallback for all of the other
@@ -1019,6 +1021,11 @@
 you had typed <em/sequence/.  So if you have a common sequence of commands
 you type, you can create a macro to execute those commands with a single
 key.
+
+<em/menu/ is the <ref id="maps" name="map"> which the macro will be bound.
+Multiple maps may be specified by separating multiple menu arguments by
+commas. Whitespace may not be used in between the menu arguments and the
+commas separating them.
 
 <em/key/ and <em/sequence/ are expanded by the same rules as the <ref
 id="bind" name="key bindings">.  There are some additions however.  The
Index: doc/muttrc.man.head
===================================================================
RCS file: /home/roessler/cvs/mutt/doc/muttrc.man.head,v
retrieving revision 3.6
diff -u -u -r3.6 muttrc.man.head
--- doc/muttrc.man.head	19 Dec 2002 11:48:58 -0000	3.6
+++ doc/muttrc.man.head	24 Apr 2003 00:51:10 -0000
@@ -113,9 +113,10 @@
 entry given for the original MIME type.  For instance, you may add
 the \fBapplication/octet-stream\fP MIME type to this list.
 .TP
-\fBbind\fP \fImap\fP \fIkey\fP \fIfunction\fP
-This command binds the given \fIkey\fP for the given \fImap\fP to
-the given \fIfunction\fP.
+\fBbind\fP \fImap1,map2,...\fP \fIkey\fP \fIfunction\fP
+This command binds the given \fIkey\fP for the given \fImap\fP or maps
+to the given \fIfunction\fP. Multiple maps may be specified by
+separating them with commas (no whitespace is allowed).
 .IP
 Valid maps are:
 .BR generic ", " alias ", " attach ", " 
@@ -167,7 +168,8 @@
 .TP
 \fBmacro\fP \fImap\fP \fIkey\fP \fIsequence\fP [ \fIdescription\fP ]
 This command binds the given \fIsequence\fP of keys to the given
-\fIkey\fP in the given \fImap\fP.  For valid maps, see \fBbind\fP.
+\fIkey\fP in the given \fImap\fP or maps.  For valid maps, see \fBbind\fP. To
+specify multipe maps, put only a comma between the maps.
 .PP
 .nf
 \fBcolor\fP \fIobject\fP \fIforeground\fP \fIbackground\fP [ \fI regexp\fP ]


>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2003-04-24 02:47:52 UTC Aaron Lehmann <aaronl@vitelus.com>
* Added comment:
{{{
Package: mutt
Version: 1.5.4i
Severity: wishlist
Tags: patch

Very often you want a keybinding to work in more than just a single
context. For example, if you want to bind q to "mail" such that it works
both when you're in the pager and in the index, you'd need a bind line
for each of these maps. With this patch, macros and bindings can be
attached to multiple maps if their names are seperated by commas.
Examples:

	bind index,pager q mail
	bind generic,browser,index,attach <home> first-entry
	macro index,pager o "c?\t"

I was able to make my muttrc 34 lines shorter by combining bind and
macro lines like this (I have a lot). It makes editing and reading the
muttrc a lot easier.

The following patch implements the feature and adds documentation for
it.


Index: keymap.c
===================================================================
RCS file: /home/roessler/cvs/mutt/keymap.c,v
retrieving revision 3.7
diff -u -u -r3.7 keymap.c
--- keymap.c	21 Jan 2003 12:33:41 -0000	3.7
+++ keymap.c	24 Apr 2003 00:51:06 -0000
@@ -685,38 +685,53 @@
  return (r);
}

-/* expects to see: <menu-string> <key-string> */
-char *parse_keymap (int *menu, BUFFER *s, BUFFER *err)
+/* expects to see: <menu-string>,<menu-string>,... <key-string> */
+static char *parse_keymap (int *menu, BUFFER *s, int *nummenus, BUFFER *err)
{
  BUFFER buf;
+  int i=0;
+  char *p, *q;

  memset (&buf, 0, sizeof (buf));

  /* menu name */
  mutt_extract_token (&buf, s, 0);
+  p = buf.data;
  if (MoreArgs (s))
  {
-    if ((*menu = mutt_check_menu (buf.data)) == -1)
+    while (i != sizeof(Menus)/sizeof(struct mapping_t)-1)
    {
-      snprintf (err->data, err->dsize, _("%s: no such menu"), buf.data);
-    }
-    else
-    {
-      /* key sequence */
-      mutt_extract_token (&buf, s, 0);
+      q = strchr(p,',');
+      if (q)
+        *q = '\0';

-      if (!*buf.data)
+      if ((menu[i] = mutt_check_menu (p)) == -1)
      {
-	strfcpy (err->data, _("null key sequence"), err->dsize);
+         snprintf (err->data, err->dsize, _("%s: no such menu"), p);
+         goto error;
      }
-      else if (MoreArgs (s))
-	return (buf.data);
+      ++i;
+	  if (q)
+        p = q+1;
+      else
+        break;
+    }
+    *nummenus=i;
+    /* key sequence */
+    mutt_extract_token (&buf, s, 0);
+
+    if (!*buf.data)
+    {
+      strfcpy (err->data, _("null key sequence"), err->dsize);
    }
+    else if (MoreArgs (s))
+      return (buf.data);
  }
  else
  {
    strfcpy (err->data, _("too few arguments"), err->dsize);
  }
+error:
  FREE (&buf.data);
  return (NULL);
}
@@ -777,9 +792,9 @@
{
  struct binding_t *bindings = NULL;
  char *key;
-  int menu, r = 0;
+  int menu[sizeof(Menus)/sizeof(struct mapping_t)-1], r = 0, nummenus, i;

-  if ((key = parse_keymap (&menu, s, err)) == NULL)
+  if ((key = parse_keymap (menu, s, &nummenus, err)) == NULL)
    return (-1);

  /* function to execute */
@@ -790,19 +805,26 @@
    r = -1;
  }
  else if (ascii_strcasecmp ("noop", buf->data) == 0)
-    km_bindkey (key, menu, OP_NULL); /* the `unbind' command */
+    for (i=0; i != nummenus; ++i)
+    {
+      km_bindkey (key, menu[i], OP_NULL); /* the `unbind' command */
+    }
  else
  {
-    /* First check the "generic" list of commands */
-    if (menu == MENU_PAGER || menu == MENU_EDITOR || menu == MENU_GENERIC ||
-	try_bind (key, menu, buf->data, OpGeneric) != 0)
-    {
-      /* Now check the menu-specific list of commands (if they exist) */
-      bindings = km_get_table (menu);
-      if (bindings && try_bind (key, menu, buf->data, bindings) != 0)
+    for (i=0; i != nummenus; ++i)
+    {
+      /* First check the "generic" list of commands */
+      if (menu[i] == MENU_PAGER || menu[i] == MENU_EDITOR ||
+      menu[i] == MENU_GENERIC ||
+	  try_bind (key, menu[i], buf->data, OpGeneric) != 0)
      {
-	snprintf (err->data, err->dsize, _("%s: no such function in map"), buf->data);
-	r = -1;
+        /* Now check the menu-specific list of commands (if they exist) */
+        bindings = km_get_table (menu[i]);
+        if (bindings && try_bind (key, menu[i], buf->data, bindings) != 0)
+        {
+          snprintf (err->data, err->dsize, _("%s: no such function in map"), buf->data);
+          r = -1;
+        }
      }
    }
  }
@@ -813,11 +835,11 @@
/* macro <menu> <key> <macro> <description> */
int mutt_parse_macro (BUFFER *buf, BUFFER *s, unsigned long data, BUFFER *err)
{
-  int menu, r = -1;
+  int menu[sizeof(Menus)/sizeof(struct mapping_t)-1], r = -1, nummenus, i;
  char *seq = NULL;
  char *key;

-  if ((key = parse_keymap (&menu, s, err)) == NULL)
+  if ((key = parse_keymap (menu, s, &nummenus, err)) == NULL)
    return (-1);

  mutt_extract_token (buf, s, M_TOKEN_CONDENSE);
@@ -839,16 +861,22 @@
      }
      else
      {
-	km_bind (key, menu, OP_MACRO, seq, buf->data);
-	r = 0;
+        for (i=0; i != nummenus; ++i)
+        {
+          km_bind (key, menu[i], OP_MACRO, seq, buf->data);
+          r = 0;
+        }
      }

      FREE (&seq);
    }
    else
    {
-      km_bind (key, menu, OP_MACRO, buf->data, NULL);
-      r = 0;
+      for (i=0; i != nummenus; ++i)
+      {
+        km_bind (key, menu[i], OP_MACRO, buf->data, NULL);
+        r = 0;
+      }
    }
  }
  FREE (&key);
Index: doc/manual.sgml.head
===================================================================
RCS file: /home/roessler/cvs/mutt/doc/manual.sgml.head,v
retrieving revision 3.19
diff -u -u -r3.19 manual.sgml.head
--- doc/manual.sgml.head	3 Mar 2003 08:26:21 -0000	3.19
+++ doc/manual.sgml.head	24 Apr 2003 00:51:10 -0000
@@ -882,9 +882,11 @@
This command allows you to change the default key bindings (operation
invoked when pressing a key).

-<em/map/ specifies in which menu the binding belongs.  The currently
-defined maps are:
+<em/map/ specifies in which menu the binding belongs.  Multiple maps may
+be specified by separating them with commas (no additional whitespace is
+allowed). The currently defined maps are:

+<label id="maps">
<descrip>
<tag/generic
This is not a real menu, but is used as a fallback for all of the other
@@ -1019,6 +1021,11 @@
you had typed <em/sequence/.  So if you have a common sequence of commands
you type, you can create a macro to execute those commands with a single
key.
+
+<em/menu/ is the <ref id="maps" name="map"> which the macro will be bound.
+Multiple maps may be specified by separating multiple menu arguments by
+commas. Whitespace may not be used in between the menu arguments and the
+commas separating them.

<em/key/ and <em/sequence/ are expanded by the same rules as the <ref
id="bind" name="key bindings">.  There are some additions however.  The
Index: doc/muttrc.man.head
===================================================================
RCS file: /home/roessler/cvs/mutt/doc/muttrc.man.head,v
retrieving revision 3.6
diff -u -u -r3.6 muttrc.man.head
--- doc/muttrc.man.head	19 Dec 2002 11:48:58 -0000	3.6
+++ doc/muttrc.man.head	24 Apr 2003 00:51:10 -0000
@@ -113,9 +113,10 @@
entry given for the original MIME type.  For instance, you may add
the \fBapplication/octet-stream\fP MIME type to this list.
.TP
-\fBbind\fP \fImap\fP \fIkey\fP \fIfunction\fP
-This command binds the given \fIkey\fP for the given \fImap\fP to
-the given \fIfunction\fP.
+\fBbind\fP \fImap1,map2,...\fP \fIkey\fP \fIfunction\fP
+This command binds the given \fIkey\fP for the given \fImap\fP or maps
+to the given \fIfunction\fP. Multiple maps may be specified by
+separating them with commas (no whitespace is allowed).
.IP
Valid maps are:
.BR generic ", " alias ", " attach ", " 
@@ -167,7 +168,8 @@
.TP
\fBmacro\fP \fImap\fP \fIkey\fP \fIsequence\fP [ \fIdescription\fP ]
This command binds the given \fIsequence\fP of keys to the given
-\fIkey\fP in the given \fImap\fP.  For valid maps, see \fBbind\fP.
+\fIkey\fP in the given \fImap\fP or maps.  For valid maps, see \fBbind\fP. To
+specify multipe maps, put only a comma between the maps.
.PP
.nf
\fBcolor\fP \fIobject\fP \fIforeground\fP \fIbackground\fP [ \fI regexp\fP ]
}}}

--------------------------------------------------------------------------------
2003-08-20 18:49:38 UTC Aaron Lehmann <aaronl@vitelus.com>
* Added comment:
{{{
I'm a little disappointed that nobody's even commented on this idea or
patch. I've been using it for months and haven't experienced problems.
I'd love it if someone could take a look. Even a rejection would be
appreciated.

Here is an updated version of the patch.


Index: keymap.c
===================================================================
RCS file: /home/roessler/cvs/mutt/keymap.c,v
retrieving revision 3.10
diff -u -r3.10 keymap.c
--- keymap.c	24 Jul 2003 18:40:50 -0000	3.10
+++ keymap.c	20 Aug 2003 17:42:18 -0000
@@ -688,38 +688,53 @@
  return (r);
}

-/* expects to see: <menu-string> <key-string> */
-char *parse_keymap (int *menu, BUFFER *s, BUFFER *err)
+/* expects to see: <menu-string>,<menu-string>,... <key-string> */
+static char *parse_keymap (int *menu, BUFFER *s, int *nummenus, BUFFER *err)
{
  BUFFER buf;
+  int i=0;
+  char *p, *q;

  memset (&buf, 0, sizeof (buf));

  /* menu name */
  mutt_extract_token (&buf, s, 0);
+  p = buf.data;
  if (MoreArgs (s))
  {
-    if ((*menu = mutt_check_menu (buf.data)) == -1)
+    while (i != sizeof(Menus)/sizeof(struct mapping_t)-1)
    {
-      snprintf (err->data, err->dsize, _("%s: no such menu"), buf.data);
-    }
-    else
-    {
-      /* key sequence */
-      mutt_extract_token (&buf, s, 0);
+      q = strchr(p,',');
+      if (q)
+        *q = '\0';

-      if (!*buf.data)
+      if ((menu[i] = mutt_check_menu (p)) == -1)
      {
-	strfcpy (err->data, _("null key sequence"), err->dsize);
+         snprintf (err->data, err->dsize, _("%s: no such menu"), p);
+         goto error;
      }
-      else if (MoreArgs (s))
-	return (buf.data);
+      ++i;
+	  if (q)
+        p = q+1;
+      else
+        break;
+    }
+    *nummenus=i;
+    /* key sequence */
+    mutt_extract_token (&buf, s, 0);
+
+    if (!*buf.data)
+    {
+      strfcpy (err->data, _("null key sequence"), err->dsize);
    }
+    else if (MoreArgs (s))
+      return (buf.data);
  }
  else
  {
    strfcpy (err->data, _("too few arguments"), err->dsize);
  }
+error:
  FREE (&buf.data);
  return (NULL);
}
@@ -780,9 +795,9 @@
{
  struct binding_t *bindings = NULL;
  char *key;
-  int menu, r = 0;
+  int menu[sizeof(Menus)/sizeof(struct mapping_t)-1], r = 0, nummenus, i;

-  if ((key = parse_keymap (&menu, s, err)) == NULL)
+  if ((key = parse_keymap (menu, s, &nummenus, err)) == NULL)
    return (-1);

  /* function to execute */
@@ -793,19 +808,26 @@
    r = -1;
  }
  else if (ascii_strcasecmp ("noop", buf->data) == 0)
-    km_bindkey (key, menu, OP_NULL); /* the `unbind' command */
+    for (i=0; i != nummenus; ++i)
+    {
+      km_bindkey (key, menu[i], OP_NULL); /* the `unbind' command */
+    }
  else
  {
-    /* First check the "generic" list of commands */
-    if (menu == MENU_PAGER || menu == MENU_EDITOR || menu == MENU_GENERIC ||
-	try_bind (key, menu, buf->data, OpGeneric) != 0)
-    {
-      /* Now check the menu-specific list of commands (if they exist) */
-      bindings = km_get_table (menu);
-      if (bindings && try_bind (key, menu, buf->data, bindings) != 0)
+    for (i=0; i != nummenus; ++i)
+    {
+      /* First check the "generic" list of commands */
+      if (menu[i] == MENU_PAGER || menu[i] == MENU_EDITOR ||
+      menu[i] == MENU_GENERIC ||
+	  try_bind (key, menu[i], buf->data, OpGeneric) != 0)
      {
-	snprintf (err->data, err->dsize, _("%s: no such function in map"), buf->data);
-	r = -1;
+        /* Now check the menu-specific list of commands (if they exist) */
+        bindings = km_get_table (menu[i]);
+        if (bindings && try_bind (key, menu[i], buf->data, bindings) != 0)
+        {
+          snprintf (err->data, err->dsize, _("%s: no such function in map"), buf->data);
+          r = -1;
+        }
      }
    }
  }
@@ -816,11 +838,11 @@
/* macro <menu> <key> <macro> <description> */
int mutt_parse_macro (BUFFER *buf, BUFFER *s, unsigned long data, BUFFER *err)
{
-  int menu, r = -1;
+  int menu[sizeof(Menus)/sizeof(struct mapping_t)-1], r = -1, nummenus, i;
  char *seq = NULL;
  char *key;

-  if ((key = parse_keymap (&menu, s, err)) == NULL)
+  if ((key = parse_keymap (menu, s, &nummenus, err)) == NULL)
    return (-1);

  mutt_extract_token (buf, s, M_TOKEN_CONDENSE);
@@ -842,16 +864,22 @@
      }
      else
      {
-	km_bind (key, menu, OP_MACRO, seq, buf->data);
-	r = 0;
+        for (i=0; i != nummenus; ++i)
+        {
+          km_bind (key, menu[i], OP_MACRO, seq, buf->data);
+          r = 0;
+        }
      }

      FREE (&seq);
    }
    else
    {
-      km_bind (key, menu, OP_MACRO, buf->data, NULL);
-      r = 0;
+      for (i=0; i != nummenus; ++i)
+      {
+        km_bind (key, menu[i], OP_MACRO, buf->data, NULL);
+        r = 0;
+      }
    }
  }
  FREE (&key);
Index: doc/manual.sgml.head
===================================================================
RCS file: /home/roessler/cvs/mutt/doc/manual.sgml.head,v
retrieving revision 3.21
diff -u -r3.21 manual.sgml.head
--- doc/manual.sgml.head	13 May 2003 12:53:20 -0000	3.21
+++ doc/manual.sgml.head	20 Aug 2003 17:42:22 -0000
@@ -881,9 +881,11 @@
This command allows you to change the default key bindings (operation
invoked when pressing a key).

-<em/map/ specifies in which menu the binding belongs.  The currently
-defined maps are:
+<em/map/ specifies in which menu the binding belongs.  Multiple maps may
+be specified by separating them with commas (no additional whitespace is
+allowed). The currently defined maps are:

+<label id="maps">
<descrip>
<tag/generic
This is not a real menu, but is used as a fallback for all of the other
@@ -1018,6 +1020,11 @@
you had typed <em/sequence/.  So if you have a common sequence of commands
you type, you can create a macro to execute those commands with a single
key.
+
+<em/menu/ is the <ref id="maps" name="map"> which the macro will be bound.
+Multiple maps may be specified by separating multiple menu arguments by
+commas. Whitespace may not be used in between the menu arguments and the
+commas separating them.

<em/key/ and <em/sequence/ are expanded by the same rules as the <ref
id="bind" name="key bindings">.  There are some additions however.  The
Index: doc/muttrc.man.head
===================================================================
RCS file: /home/roessler/cvs/mutt/doc/muttrc.man.head,v
retrieving revision 3.7
diff -u -r3.7 muttrc.man.head
--- doc/muttrc.man.head	11 Aug 2003 14:05:13 -0000	3.7
+++ doc/muttrc.man.head	20 Aug 2003 17:42:23 -0000
@@ -115,9 +115,10 @@
entry given for the original MIME type.  For instance, you may add
the \fBapplication/octet-stream\fP MIME type to this list.
.TP
-\fBbind\fP \fImap\fP \fIkey\fP \fIfunction\fP
-This command binds the given \fIkey\fP for the given \fImap\fP to
-the given \fIfunction\fP.
+\fBbind\fP \fImap1,map2,...\fP \fIkey\fP \fIfunction\fP
+This command binds the given \fIkey\fP for the given \fImap\fP or maps
+to the given \fIfunction\fP. Multiple maps may be specified by
+separating them with commas (no whitespace is allowed).
.IP
Valid maps are:
.BR generic ", " alias ", " attach ", " 
@@ -169,7 +170,8 @@
.TP
\fBmacro\fP \fImap\fP \fIkey\fP \fIsequence\fP [ \fIdescription\fP ]
This command binds the given \fIsequence\fP of keys to the given
-\fIkey\fP in the given \fImap\fP.  For valid maps, see \fBbind\fP.
+\fIkey\fP in the given \fImap\fP or maps.  For valid maps, see \fBbind\fP. To
+specify multipe maps, put only a comma between the maps.
.PP
.nf
\fBcolor\fP \fIobject\fP \fIforeground\fP \fIbackground\fP [ \fI regexp\fP ]
}}}

--------------------------------------------------------------------------------
2003-09-26 05:13:42 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
# no infos, no followups
close 1032
close 1394
merge 1200 1407
severity 1022 wishlist
severity 1608 wishlist
merge 1022 1579 1608
tags 1022 patch
merge 1199 1517
tags 1481 patch
tags 1550 patch
}}}

--------------------------------------------------------------------------------
2004-02-26 04:30:48 UTC Aaron Lehmann <aaronl@vitelus.com>
* Added comment:
{{{
I've been using the patch I submitted
(http://bugs.guug.de/db/15/1550.html) since April and it works very
well for me. I inquired about getting it merged into CVS a few times
but never got a response. I think it's a feature that is sorely
needed, because it makes it much easier to customize mutt's keymaps
consistently. I'd really appreciate some criticism of the idea or
patch, or even marking the bug wontfix.

For convenience, here's a copy of my original submission, with a patch
against HEAD substituted:



Package: mutt
Version: 1.5.4i
Severity: wishlist
Tags: patch

Very often you want a keybinding to work in more than just a single
context. For example, if you want to bind q to "mail" such that it
works both when you're in the pager and in the index, you'd need a
bind line for each of these maps. With this patch, macros and bindings
can be attached to multiple maps if their names are seperated by
commas.  Examples:

	bind index,pager q mail
	bind generic,browser,index,attach <home> first-entry
	macro index,pager o "c?\t"

I was able to make my muttrc 34 lines shorter by combining bind and
macro lines like this (I have a lot). It makes editing and reading the
muttrc a lot easier.

The following patch implements the feature and adds documentation for
it.


Index: keymap.c
===================================================================
RCS file: /home/roessler/cvs/mutt/keymap.c,v
retrieving revision 3.10
diff -u -u -r3.10 keymap.c
--- keymap.c	24 Jul 2003 18:40:50 -0000	3.10
+++ keymap.c	26 Feb 2004 04:25:21 -0000
@@ -688,38 +688,53 @@
  return (r);
}

-/* expects to see: <menu-string> <key-string> */
-char *parse_keymap (int *menu, BUFFER *s, BUFFER *err)
+/* expects to see: <menu-string>,<menu-string>,... <key-string> */
+static char *parse_keymap (int *menu, BUFFER *s, int *nummenus, BUFFER *err)
{
  BUFFER buf;
+  int i=0;
+  char *p, *q;

  memset (&buf, 0, sizeof (buf));

  /* menu name */
  mutt_extract_token (&buf, s, 0);
+  p = buf.data;
  if (MoreArgs (s))
  {
-    if ((*menu = mutt_check_menu (buf.data)) == -1)
+    while (i != sizeof(Menus)/sizeof(struct mapping_t)-1)
    {
-      snprintf (err->data, err->dsize, _("%s: no such menu"), buf.data);
-    }
-    else
-    {
-      /* key sequence */
-      mutt_extract_token (&buf, s, 0);
+      q = strchr(p,',');
+      if (q)
+        *q = '\0';

-      if (!*buf.data)
+      if ((menu[i] = mutt_check_menu (p)) == -1)
      {
-	strfcpy (err->data, _("null key sequence"), err->dsize);
+         snprintf (err->data, err->dsize, _("%s: no such menu"), p);
+         goto error;
      }
-      else if (MoreArgs (s))
-	return (buf.data);
+      ++i;
+	  if (q)
+        p = q+1;
+      else
+        break;
+    }
+    *nummenus=i;
+    /* key sequence */
+    mutt_extract_token (&buf, s, 0);
+
+    if (!*buf.data)
+    {
+      strfcpy (err->data, _("null key sequence"), err->dsize);
    }
+    else if (MoreArgs (s))
+      return (buf.data);
  }
  else
  {
    strfcpy (err->data, _("too few arguments"), err->dsize);
  }
+error:
  FREE (&buf.data);
  return (NULL);
}
@@ -780,9 +795,9 @@
{
  struct binding_t *bindings = NULL;
  char *key;
-  int menu, r = 0;
+  int menu[sizeof(Menus)/sizeof(struct mapping_t)-1], r = 0, nummenus, i;

-  if ((key = parse_keymap (&menu, s, err)) == NULL)
+  if ((key = parse_keymap (menu, s, &nummenus, err)) == NULL)
    return (-1);

  /* function to execute */
@@ -793,19 +808,26 @@
    r = -1;
  }
  else if (ascii_strcasecmp ("noop", buf->data) == 0)
-    km_bindkey (key, menu, OP_NULL); /* the `unbind' command */
+    for (i=0; i != nummenus; ++i)
+    {
+      km_bindkey (key, menu[i], OP_NULL); /* the `unbind' command */
+    }
  else
  {
-    /* First check the "generic" list of commands */
-    if (menu == MENU_PAGER || menu == MENU_EDITOR || menu == MENU_GENERIC ||
-	try_bind (key, menu, buf->data, OpGeneric) != 0)
-    {
-      /* Now check the menu-specific list of commands (if they exist) */
-      bindings = km_get_table (menu);
-      if (bindings && try_bind (key, menu, buf->data, bindings) != 0)
+    for (i=0; i != nummenus; ++i)
+    {
+      /* First check the "generic" list of commands */
+      if (menu[i] == MENU_PAGER || menu[i] == MENU_EDITOR ||
+      menu[i] == MENU_GENERIC ||
+	  try_bind (key, menu[i], buf->data, OpGeneric) != 0)
      {
-	snprintf (err->data, err->dsize, _("%s: no such function in map"), buf->data);
-	r = -1;
+        /* Now check the menu-specific list of commands (if they exist) */
+        bindings = km_get_table (menu[i]);
+        if (bindings && try_bind (key, menu[i], buf->data, bindings) != 0)
+        {
+          snprintf (err->data, err->dsize, _("%s: no such function in map"), buf->data);
+          r = -1;
+        }
      }
    }
  }
@@ -816,11 +838,11 @@
/* macro <menu> <key> <macro> <description> */
int mutt_parse_macro (BUFFER *buf, BUFFER *s, unsigned long data, BUFFER *err)
{
-  int menu, r = -1;
+  int menu[sizeof(Menus)/sizeof(struct mapping_t)-1], r = -1, nummenus, i;
  char *seq = NULL;
  char *key;

-  if ((key = parse_keymap (&menu, s, err)) == NULL)
+  if ((key = parse_keymap (menu, s, &nummenus, err)) == NULL)
    return (-1);

  mutt_extract_token (buf, s, M_TOKEN_CONDENSE);
@@ -842,16 +864,22 @@
      }
      else
      {
-	km_bind (key, menu, OP_MACRO, seq, buf->data);
-	r = 0;
+        for (i=0; i != nummenus; ++i)
+        {
+          km_bind (key, menu[i], OP_MACRO, seq, buf->data);
+          r = 0;
+        }
      }

      FREE (&seq);
    }
    else
    {
-      km_bind (key, menu, OP_MACRO, buf->data, NULL);
-      r = 0;
+      for (i=0; i != nummenus; ++i)
+      {
+        km_bind (key, menu[i], OP_MACRO, buf->data, NULL);
+        r = 0;
+      }
    }
  }
  FREE (&key);
Index: doc/manual.sgml.head
===================================================================
RCS file: /home/roessler/cvs/mutt/doc/manual.sgml.head,v
retrieving revision 3.26
diff -u -u -r3.26 manual.sgml.head
--- doc/manual.sgml.head	1 Feb 2004 17:45:33 -0000	3.26
+++ doc/manual.sgml.head	26 Feb 2004 04:25:25 -0000
@@ -881,9 +881,11 @@
This command allows you to change the default key bindings (operation
invoked when pressing a key).

-<em/map/ specifies in which menu the binding belongs.  The currently
-defined maps are:
+<em/map/ specifies in which menu the binding belongs.  Multiple maps may
+be specified by separating them with commas (no additional whitespace is
+allowed). The currently defined maps are:

+<label id="maps">
<descrip>
<tag/generic/
This is not a real menu, but is used as a fallback for all of the other
@@ -1018,6 +1020,11 @@
you had typed <em/sequence/.  So if you have a common sequence of commands
you type, you can create a macro to execute those commands with a single
key.
+
+<em/menu/ is the <ref id="maps" name="map"> which the macro will be bound.
+Multiple maps may be specified by separating multiple menu arguments by
+commas. Whitespace may not be used in between the menu arguments and the
+commas separating them.

<em/key/ and <em/sequence/ are expanded by the same rules as the <ref
id="bind" name="key bindings">.  There are some additions however.  The
Index: doc/muttrc.man.head
===================================================================
RCS file: /home/roessler/cvs/mutt/doc/muttrc.man.head,v
retrieving revision 3.10
diff -u -u -r3.10 muttrc.man.head
--- doc/muttrc.man.head	1 Feb 2004 17:10:43 -0000	3.10
+++ doc/muttrc.man.head	26 Feb 2004 04:25:25 -0000
@@ -125,9 +125,10 @@
entry given for the original MIME type.  For instance, you may add
the \fBapplication/octet-stream\fP MIME type to this list.
.TP
-\fBbind\fP \fImap\fP \fIkey\fP \fIfunction\fP
-This command binds the given \fIkey\fP for the given \fImap\fP to
-the given \fIfunction\fP.
+\fBbind\fP \fImap1,map2,...\fP \fIkey\fP \fIfunction\fP
+This command binds the given \fIkey\fP for the given \fImap\fP or maps
+to the given \fIfunction\fP. Multiple maps may be specified by
+separating them with commas (no whitespace is allowed).
.IP
Valid maps are:
.BR generic ", " alias ", " attach ", " 
@@ -179,7 +180,8 @@
.TP
\fBmacro\fP \fImap\fP \fIkey\fP \fIsequence\fP [ \fIdescription\fP ]
This command binds the given \fIsequence\fP of keys to the given
-\fIkey\fP in the given \fImap\fP.  For valid maps, see \fBbind\fP.
+\fIkey\fP in the given \fImap\fP or maps.  For valid maps, see \fBbind\fP. To
+specify multipe maps, put only a comma between the maps.
.PP
.nf
\fBcolor\fP \fIobject\fP \fIforeground\fP \fIbackground\fP [ \fI regexp\fP ]
}}}

--------------------------------------------------------------------------------
2004-02-29 07:13:58 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
Hello Aaron,

On Wednesday, February 25, 2004 at 8:30:48 PM -0800, Aaron Lehmann wrote:

> I inquired about getting it merged into CVS a few times but never got
> a response.

   Nobody likes to give bad news, destroy well-done work, nor
discourage contributions... OTOS the resulting deep silence may be seen
as a little despicant¹, sorry.


> I'd really appreciate some criticism of the idea or patch, or even
> marking the bug wontfix.

   Generally patches doing in code what can be done in config are not
really well considered.


Sorry again, Alain.
-- 
Note ¹: I don't know this word, meant "méprisant" in French.
}}}

--------------------------------------------------------------------------------
2004-02-29 08:35:44 UTC David Yitzchak Cohen <lists+mutt_bugs@bigfatdave.com>
* Added comment:
{{{
On Sat, Feb 28, 2004 at 08:13:58AM EST, Alain Bench wrote:
>  On Wednesday, February 25, 2004 at 8:30:48 PM -0800, Aaron Lehmann wrote:

> > I inquired about getting it merged into CVS a few times but never got
> > a response.
> 
>     Nobody likes to give bad news, destroy well-done work, nor
> discourage contributions... OTOS the resulting deep silence may be seen
> as a little despicant¹, sorry.
> 
> 
> > I'd really appreciate some criticism of the idea or patch, or even
> > marking the bug wontfix.
> 
>     Generally patches doing in code what can be done in config are not
> really well considered.

FWIW - I've put together a tiny script [1] to do just that (and I've
modified my keymaprc.qwerty [2] (by making it into a keymaprc.qwerty.in
[3] and autogenerating the former with the script) to take advantage
of it).  The only problem is that because the functionality isn't built
into Mutt's parser, resolving stuff like
macro index r '<enter-command>bind pager,index r reply'
isn't trivial (and isn't even attempted by my script), and once
configurations get more complex, resolving ambiguities is actually
impossible to do without duplicating Mutt's internal state (which involves
considerable processing).  The only reasonable solution I can think of
for people using my script instead of the patch is to replace all such
statements by separating them into other RC files and then sourcing the
results of running my script on them (which can become a little messy
if you're used to a single RC file, I guess [4]).

- Dave

[1]
http://www.bigfatdave.com/dave/mutt/muttdir/makekeymaprc

[2]
http://www.bigfatdave.com/dave/mutt/muttdir/keymaprc.qwerty

[3]
http://www.bigfatdave.com/dave/mutt/muttdir/keymaprc.qwerty.in

[4]
Here's an interesting way to do things instead, if you don't like giant
ls(1) outputs:
macro index r '<enter-command>source "echo \"bind pager,index r reply\" | makekeymaprc |"'
This incurs some extra runtime overhead, though, since my Perl script
must be run each time somebody invokes that macro.  You can optimize
the thing by binding the script to a UNIX domain socket and having a
persistent version run in the background, but every optimization only
makes the whole solution even uglier.

-- 
Uncle Cosmo, why do they call this a word processor?
It's simple, Skyler.  You've seen what food processors do to food, right?

Please visit this link:
http://rotter.net/israel
}}}

--------------------------------------------------------------------------------
2005-09-05 13:10:10 UTC brendan
* Added comment:
{{{
This was applied.
}}}

* resolution changed to fixed
* status changed to closed
