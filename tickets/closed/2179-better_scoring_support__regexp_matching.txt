Ticket:  2179
Status:  closed
Summary: better scoring support - regexp matching

Reporter: name@ganten.example
Owner:    mutt-dev

Opened:       2006-02-09 13:58:36 UTC
Last Updated: 2017-11-22 02:26:01 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
muttng devs improved the matching support in scoring:

http://mutt-ng.supersized.org/archives/35-Scoring-in-mutt-ng.html#extended
http://svn.berlios.de/wsvn/mutt-ng/trunk/score.c?op=diff&rev=0&sc=0

It would be great if that was added to mutt too. It makes scoring more useful.
>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2009-06-12 23:33:27 UTC pdmef
* cc changed to 383769@bugs.debian.org
* Added comment:
See #3243

* Updated description:
{{{
muttng devs improved the matching support in scoring:

http://mutt-ng.supersized.org/archives/35-Scoring-in-mutt-ng.html#extended
http://svn.berlios.de/wsvn/mutt-ng/trunk/score.c?op=diff&rev=0&sc=0

It would be great if that was added to mutt too. It makes scoring more useful.
>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2017-11-22 02:26:01 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [changeset:"332af0fa7d4fe2590e44d5d9093fb16512f15e91" 7186:332af0fa7d4f]:
{{{
#!CommitTicketReference repository="" revision="332af0fa7d4fe2590e44d5d9093fb16512f15e91"
Enable full address matching during message scoring. (closes #2179) (see #3243)

The tickets are old, but it seems reasonable to expect the pattern
matching to behave the same in scoring as when applying limits or
searching.

I don't foresee a huge performance hit, and there are no arguments
against the change in the tickets.

Thanks to the muttng devs (noted in ticket 2179) and Kornilios Kourtis
(from the Debian ticket referenced in ticket 3243).
}}}

* resolution changed to fixed
* status changed to closed
