Ticket:  1578
Status:  closed
Summary: "unmono index *" causes segfault in "mutt -Q"

Reporter: Roland Rosenfeld <roland@spinnaker.de>
Owner:    mutt-dev

Opened:       2003-06-24 05:38:50 UTC
Last Updated: 2005-08-02 04:04:40 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.5.4-0.1
Severity: normal

-- Please type your report below this line

I tried to use smime_keys, but this script always failed.  Discovering
the problem showed, that "mutt -Q smime_keys" is the problem, because
it always dies with a segmentation fault in my environment.  A closer
look at my configuration file showed, that the following line of my
muttrc caues the trouble:

unmono index *

Commenting out this line works around the problem.

This problem does not only occur on my self compiled mutt 1.5.4 but
also in the Debian package 1.5.4-1.

-- System Information
Debian Release: testing/unstable
Kernel Version: Linux besan 2.4.20 #1 Son Mär 30 15:32:11 CEST 2003 i686 GNU/Linux

Versions of the packages mutt depends on:
ii  libc6          2.3.1-17       GNU C Library: Shared libraries and Timezone
ii  libncurses5    5.3.20030510-1 Shared libraries for terminal handling
ii  postfix        2.0.9-3        A high-performance mail transport agent
	^^^ (Provides virtual package mail-transport-agent)

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
cc
Reading specs from /usr/lib/gcc-lib/i386-linux/3.3/specs
Configured with: ../src/configure -v --enable-languages=c,c++,java,f77,pascal,objc,ada,treelang --prefix=/usr --mandir=/usr/share/man --infodir=/usr/share/info --with-gxx-include-dir=/usr/include/c++/3.3 --enable-shared --with-system-zlib --enable-nls --without-included-gettext --enable-__cxa_atexit --enable-clocale=gnu --enable-debug --enable-java-gc=boehm --enable-java-awt=xlib --enable-objc-gc i386-linux
Thread model: posix
gcc version 3.3 (Debian)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.5.4i (2003-03-19)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.20 (i686) [using ncurses 5.3]
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_GSS  -USE_SSL  -USE_SASL  -USE_SASL2  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +COMPRESSED  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  -HAVE_LIBIDN  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
MIXMASTER="mixmaster"
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.

patch-1.5.4.rr.compressed.1

-- Core Dump Analysis Output

GNU gdb 5.3-debian
Copyright 2002 Free Software Foundation, Inc.
GDB is free software, covered by the GNU General Public License, and you are
welcome to change it and/or distribute copies of it under certain conditions.
Type "show copying" to see the conditions.
There is absolutely no warranty for GDB.  Type "show warranty" for details.
This GDB was configured as "i386-linux"...
Core was generated by `mutt -Q editor'.
Program terminated with signal 11, Segmentation fault.
Reading symbols from /lib/libncurses.so.5...done.
Loaded symbols for /lib/libncurses.so.5
Reading symbols from /lib/libc.so.6...done.
Loaded symbols for /lib/libc.so.6
Reading symbols from /lib/ld-linux.so.2...done.
Loaded symbols for /lib/ld-linux.so.2
Reading symbols from /lib/libnss_compat.so.2...done.
Loaded symbols for /lib/libnss_compat.so.2
Reading symbols from /lib/libnsl.so.1...done.
Loaded symbols for /lib/libnsl.so.1
Reading symbols from /usr/lib/gconv/ISO8859-1.so...done.
Loaded symbols for /usr/lib/gconv/ISO8859-1.so
#0  0x40036a40 in has_colors () from /lib/libncurses.so.5
#0  0x40036a40 in has_colors () from /lib/libncurses.so.5
#1  0x08051bdd in _mutt_parse_uncolor (buf=0xbfffe9a0, s=0xbfffe950, data=0, 
    err=0xbfffeb10, parse_uncolor=0) at color.c:406
#2  0x08051971 in mutt_parse_unmono (buf=0xbfffe9a0, s=0xbfffe950, data=0, 
    err=0xbfffeb10) at color.c:360
#3  0x08065539 in mutt_parse_rc_line (
    line=0x80f1f78 "unmono index *\t\t\t\t# unset all mono index entries", 
    token=0xbfffe9a0, err=0xbfffeb10) at init.c:1344
#4  0x08065246 in source_rc (rcfile=0x80e01a0 "/home/roland/.mutt/muttrc", 
    err=0xbfffeb10) at init.c:1262
#5  0x0806659e in mutt_init (skip_sys_rc=0, commands=0x0) at init.c:1983
#6  0x0806f8aa in main (argc=3, argv=0xbffff704) at main.c:659
659	  mutt_init (flags & M_NOSYSRC, commands);
660	  mutt_free_list (&commands);
661	
662	  if (queries)
663	    return mutt_query_variables (queries);
664	
665	  if (alias_queries)
666	  {
667	    int rv = 0;
668	    ADDRESS *a;



>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2005-08-02 22:04:40 UTC brendan
* Added comment:
{{{
Appears to have been fixed in CVS, probably with the OPTNOCURSES check.
}}}

* resolution changed to fixed
* status changed to closed
