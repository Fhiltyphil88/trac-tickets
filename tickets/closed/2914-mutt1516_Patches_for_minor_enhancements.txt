Ticket:  2914
Status:  closed
Summary: mutt-1.5.16: Patches for minor enhancements

Reporter: Ernst Boetsch <Ernst.Boetsch@lrz-muenchen.de>
Owner:    mutt-dev

Opened:       2007-06-18 10:39:55 UTC
Last Updated: 2007-07-13 17:15:18 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:


{{{

Package: mutt
Version: mutt-1.5.16
Severity: wishlist

-- Please type your report below this line

Hello,


enclosed you will find patch files for some minor enhancements.
Please feel free to incorporate the patches into the main distribution
of mutt.

The patches are unified diffs for mutt-1.5.16:

 * edit-visual.udiff		--> OPS  compose.c  functions.h

   New function  "edit-visual / OP_COMPOSE_EDIT_VISUAL".

   Therefore it is possible to have two editors at the same time:
    + The fast builtin editor for small mails.
    + The individual visual editor for big mails.
   Default binding:  v


 * help.udiff			--> help.c

   Minor modification of a text string.


 * makefile.dotlock.udiff	--> Makefile.in

    + Update of make variable  "mutt_dotlock_DEPENDENCIES".
    + New make variable  "mutt_dotlock_CFLAGS".
    + New target  "mutt_dotlock.$(OBJEXT):"  for compiling 
      "mutt_dotlock".

   I'm not familiar with GNU automake.  Therefore i didn't modify
   "Makefile.am".


 * makefile.dst.udiff		--> contrib/Makefile.in  doc/Makefile.in

   New make variables:  DST_docdir  DST_htmldir  DST_man1dir DST_man5dir.
   Advantages:
    + In special environments it's easier to change the path names.
    + It's easier to read the makefiles.

   Some more files are installed by  "contrib/Makefile.in":
      language.txt  language50.txt  iconv/README  iconv/make.sh

   I didn't modify  "contrib/Makefile.am"  and  "doc/Makefile.am".


 * restore-full-display.udiff	--> init.h  mutt.h  pattern.c

   New boolean  "restore_full_display / OPTRESTFULLDISP".

   In the plain package no mails are displayed at all if no mails match
   the pattern of a limit command.
   Because of the patch the complete list shall be restored in this case
   if the boolean is set;  a  "limit to 'all'"  should not be necessary.

   Unfortunately my patch is not sufficient.
   I took code of a previous version of mutt.


 * space-is-default.udiff	--> curs_lib.c

   <Space>  additionally triggers the default if a yes/no question
   is asked.


 * visual-editmsg.udiff		--> editmsg.c

   Minor modification of the first argument of  "mutt_edit_file":
   Additionally use  "Visual".


Kind regards,
    Ernst Boetsch
}}}

--------------------------------------------------------------------------------
2007-06-18 10:39:55 UTC Ernst Boetsch
* Added attachment edit-visual.udiff
* Added comment:
Added by email2trac

--------------------------------------------------------------------------------
2007-06-18 10:39:56 UTC Ernst Boetsch
* Added attachment help.udiff
* Added attachment makefile.dotlock.udiff
* Added attachment makefile.dst.udiff
* Added comment:
Added by email2trac

--------------------------------------------------------------------------------
2007-06-18 10:39:57 UTC Ernst Boetsch
* Added attachment restore-full-display.udiff
* Added comment:
Added by email2trac

--------------------------------------------------------------------------------
2007-06-18 10:40:07 UTC Ernst Boetsch
* Added attachment space-is-default.udiff
* Added attachment visual-editmsg.udiff
* Added comment:
Added by email2trac

--------------------------------------------------------------------------------
2007-06-18 10:40:08 UTC Ernst Boetsch
* Added comment:

This message has 7 attachment(s)

* id changed to 2914

--------------------------------------------------------------------------------
2007-06-18 13:25:21 UTC Vincent Lefevre
* Added comment:
{{{
On 2007-06-18 10:40:10 -0000, Mutt wrote:
>   * edit-visual.udiff            --> OPS  compose.c  functions.h
> 
>     New function  "edit-visual / OP_COMPOSE_EDIT_VISUAL".
> 
>     Therefore it is possible to have two editors at the same time:
>      + The fast builtin editor for small mails.
>      + The individual visual editor for big mails.
>     Default binding:  v

AFAIK, Mutt's builtin editor is just a line editor and is not used
for typing mails. I don't see the point of this patch (there's already
edit-message to edit the message).

>   * makefile.dotlock.udiff       --> Makefile.in
> 
>      + Update of make variable  "mutt_dotlock_DEPENDENCIES".
>      + New make variable  "mutt_dotlock_CFLAGS".
>      + New target  "mutt_dotlock.$(OBJEXT):"  for compiling
>        "mutt_dotlock".
> 
>     I'm not familiar with GNU automake.  Therefore i didn't modify
>     "Makefile.am".

Makefile.in is generated from Makefile.am (thus is not in the
repository). So, this patch is invalid.

>   * makefile.dst.udiff           --> contrib/Makefile.in  doc/Makefile.in

Ditto.

>   * space-is-default.udiff       --> curs_lib.c
> 
>     <Space>  additionally triggers the default if a yes/no question
>     is asked.

I'm not really in favor of accepting <Space> too, unless this is
well-accepted (e.g. also used by other software).

>   * visual-editmsg.udiff         --> editmsg.c
> 
>     Minor modification of the first argument of  "mutt_edit_file":
>     Additionally use  "Visual".

Same remark as above.

Now, the manual should be more detailed concerning the difference
between $editor or $visual (since in practice, $editor is used as
a visual editor).
}}}

--------------------------------------------------------------------------------
2007-07-13 17:15:18 UTC brendan
* Added comment:
As far as I can tell, the visual editor trick could be done with a macro. I don't think accepting space is a particularly safe change at this point. For the dotlock build changes, please make them against the automake files and send them as a separate item (in general, multiple changes in a single BTS entry is painful to manage).

* priority changed to trivial
* resolution changed to wontfix
* status changed to closed
