Ticket:  3483
Status:  closed
Summary: segfault on a weird maildir

Reporter: antonio@dyne.org
Owner:    brendan

Opened:       2010-12-30 16:55:03 UTC
Last Updated: 2010-12-30 18:07:48 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
Hi,
mutt segfaults on a weird maildir (attached), the attached patch should fix the problem, it seems that the return value of strchr is not checked in an "else" block (it is checked on the previous "if" block though).

I've put 1.5.20 on the "Version" field but this is reproducible on 1.5.21 as well

--------------------------------------------------------------------------------
2010-12-30 16:55:49 UTC antonio@dyne.org
* Added attachment mutt-segfault-maildir.tar
* Added comment:
the first message on this maildir causes mutt to segfault

--------------------------------------------------------------------------------
2010-12-30 16:56:05 UTC antonio@dyne.org
* Added attachment 578087-strchr.patch
* Added comment:
this patch should fix the problem

--------------------------------------------------------------------------------
2010-12-30 17:50:43 UTC brendan
* version changed to 1.5.21

--------------------------------------------------------------------------------
2010-12-30 18:07:14 UTC brendan
* Added comment:
Thanks, I'm applying an equivalent but shorter fix.

* owner changed to brendan
* status changed to accepted

--------------------------------------------------------------------------------
2010-12-30 18:07:48 UTC Brendan Cully <brendan@kublai.com>
* Added comment:
(In [f01b306ebe0e]) Handle missing : in write_one_header debug statement. Closes #3483

* resolution changed to fixed
* status changed to closed
