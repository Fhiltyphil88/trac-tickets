Ticket:  1610
Status:  closed
Summary: encrypt email to only 1 recipient, but send to many (shared key)

Reporter: guus@sliepen.eu.org
Owner:    mutt-dev

Opened:       2003-08-20 09:41:24 UTC
Last Updated: 2009-07-31 16:35:23 UTC

Priority:  trivial
Component: crypto
Keywords:  #973 + #1897

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.5.4-1
Severity: normal

-- Please type your report below this line

When sending an encrypted email to someone, and other people are listed as
Cc's, after asking for the encryption key of the person in the To: header, it
will also ask for encryption keys of those in the Cc: header. It may not always
be desirable to encrypt it for each and every listed Cc, but there is no way to
tell mutt not to do it.
-- 
>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2003-08-21 02:08:32 UTC Guus Sliepen <guus@sliepen.eu.org>
* Added comment:
{{{
On Tue, Aug 19, 2003 at 11:07:02PM -0400, Derek Martin wrote:

> > When sending an encrypted email to someone, and other people are
> > listed as Cc's, after asking for the encryption key of the person in
> > the To: header, it will also ask for encryption keys of those in the
> > Cc: header. It may not always be desirable to encrypt it for each
> > and every listed Cc, but there is no way to tell mutt not to do it.
> 
> This is not a bug.
> 
> What you are asking for really makes no sense, and may actually have
> detrimental effects.  If a message is important and/or private enough
> to warrant encryption to one party, then to include an unencrypted
> version of the cipher text in the same message is not only pointless,
> but may (or may not) also make it possible for the cryptographically
> savvy to mount cryptographic attacks on future messages encrypted with
> the same key(s).  This is called a known plain-text attack.

I'm sorry if I wasn't clear. What I want is that mutt encrypts it only
once for one receiver, and sends this encrypted message to everyone in
To:, Cc: and Bcc:. It is quite possible to share a private key among a
group of people.

Furthermore, allowing some copies to be sent out in plaintext can
sometimes still be useful. Sure, you must know what you're doing. Maybe
you want unencrypted emails sent to a local account, or maybe someone
only allows emails to be sent to him which are encrypted with his
private key in order to block spam, but otherwise the message doesn't
need to be encrypted at all for the other recipients. If that sounds
far-fetched, have a look at http://www.oberhumer.com/mfx/pgp.php.

--
}}}

--------------------------------------------------------------------------------
2003-08-24 03:21:53 UTC Guus Sliepen <guus@sliepen.eu.org>
* Added comment:
{{{
On Sat, Aug 23, 2003 at 06:07:17PM +1000, Cameron Simpson wrote:

> | I'm sorry if I wasn't clear. What I want is that mutt encrypts it only
> | once for one receiver, and sends this encrypted message to everyone in
> | To:, Cc: and Bcc:. It is quite possible to share a private key among a
> | group of people.
> 
> Then associate the key with an alias/list address and say:
> 
> 	To: them@some.site
> 
> where that's the list or alias. This isn't hard.

I really can't believe you're suggesting that a user has to change the
aliases file or create a mailing list when you happen to want to do
what I said. Yes, if you're regularly mailing that set of people, I can
imagine, but even then setting up a proper mailing list requires root
access on some machine.

It would be even less hard to encrypt the message by hand with the gpg
command line tool, bypassing mutt.

Anyway, consider this bug a wishlist bug then.

-- 
Met vriendelijke groet / with kind regards,
   Guus Sliepen <guus@sliepen.eu.org>
}}}

--------------------------------------------------------------------------------
2003-08-24 19:07:17 UTC Cameron Simpson <cs@zip.com.au>
* Added comment:
{{{
On 09:08 20 Aug 2003, Guus Sliepen <guus@sliepen.eu.org> wrote:
| I'm sorry if I wasn't clear. What I want is that mutt encrypts it only
| once for one receiver, and sends this encrypted message to everyone in
| To:, Cc: and Bcc:. It is quite possible to share a private key among a
| group of people.

Then associate the key with an alias/list address and say:

	To: them@some.site

where that's the list or alias. This isn't hard.
-- 
Cameron Simpson <cs@zip.com.au> DoD#743
http://www.cskk.ezoshosting.com/cs/

No, Sir. I don't think its safe to look at the speedometer when I'm going
that fast.
       - Tom Coradeschi <tcora@pica.army.mil>
}}}

--------------------------------------------------------------------------------
2005-08-26 08:28:12 UTC rado
* Added comment:
{{{
despam

related to 973 + 1897, might be solved together.
}}}

--------------------------------------------------------------------------------
2005-10-05 12:14:47 UTC rado
* Added comment:
{{{
clarified subject,
change request
cleaned unformatted of unnecessary quotes.
}}}

--------------------------------------------------------------------------------
2009-06-30 14:54:43 UTC pdmef
* component changed to crypto
* Updated description:
{{{
Package: mutt
Version: 1.5.4-1
Severity: normal

-- Please type your report below this line

When sending an encrypted email to someone, and other people are listed as
Cc's, after asking for the encryption key of the person in the To: header, it
will also ask for encryption keys of those in the Cc: header. It may not always
be desirable to encrypt it for each and every listed Cc, but there is no way to
tell mutt not to do it.
-- 
>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2009-07-31 16:32:59 UTC pdmef
* resolution changed to duplicate
* status changed to closed

--------------------------------------------------------------------------------
2009-07-31 16:35:23 UTC pdmef
* Added comment:
This is a duplicate of #937.
