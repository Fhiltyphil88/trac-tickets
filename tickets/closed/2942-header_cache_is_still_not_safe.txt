Ticket:  2942
Status:  closed
Summary: header cache is still not safe

Reporter: brendan
Owner:    pdmef

Opened:       2007-08-14 00:56:16 UTC
Last Updated: 2009-04-26 19:10:22 UTC

Priority:  major
Component: header cache
Keywords:  patch

--------------------------------------------------------------------------------
Description:
The header cache does not properly dump or zero several fields in dump_body, leaving several pointers behind that are likely to be invalid. At first glance, the bad fields seem to include charset, content, next, parts, hdr, and aptr, unless these are reset elsewhere in the code after restore_body.

--------------------------------------------------------------------------------
2008-07-10 19:54:13 UTC pdmef
* Added comment:
Add patch updating hcache upon mailbox sync for IMAP, Maildir and MH (can't do it for POP as the ACLs we need to enable for updating to be useful would allow too many operations like editing messages).

--------------------------------------------------------------------------------
2008-07-10 20:09:45 UTC pdmef
* Added comment:
(In [b9ac445b035b]) Prevent some pointers of 'struct body' being saved to hcache

This addresses the hcache safety issue but maybe doesn't fix it
completely, see #2942. This also prevents mutt from crashing when
using a hcache that was synced when syncing the mailbox (updating
changed and fully-parsed messages).

--------------------------------------------------------------------------------
2008-07-11 18:34:27 UTC pdmef
* Added attachment hc-update.diff

--------------------------------------------------------------------------------
2008-08-15 07:08:18 UTC pdmef
* keywords changed to patch

--------------------------------------------------------------------------------
2008-08-30 21:48:35 UTC pdmef
* owner changed to pdmef

--------------------------------------------------------------------------------
2009-02-10 14:02:07 UTC pdmef
* status changed to accepted

--------------------------------------------------------------------------------
2009-02-10 14:02:14 UTC pdmef
* status changed to started

--------------------------------------------------------------------------------
2009-04-26 08:30:02 UTC pdmef
* Added comment:
There's positive feedback about the patch on mutt-users in http://marc.info/?l=mutt-users&m=124022481015277&w=2 ff. So I think a) the fix in [b9ac445b035b] is stable and b) we can apply hc-update.diff as updating hcache makes sense.

--------------------------------------------------------------------------------
2009-04-26 16:11:58 UTC brendan
* Added comment:
Sounds good to me.

--------------------------------------------------------------------------------
2009-04-26 19:10:22 UTC Rocco Rutte <pdmef@gmx.net>
* Added comment:
(In [2fe6f875c7ae]) Sync header to hcache when synching MH/Maildir/IMAP folders. Closes #2942.

* resolution changed to fixed
* status changed to closed
