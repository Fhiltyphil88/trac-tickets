Ticket:  3119
Status:  closed
Summary: problem setting Fcc to save email on Zimbra IMAP server

Reporter: aris
Owner:    brendan

Opened:       2008-09-18 16:24:35 UTC
Last Updated: 2009-01-21 19:08:48 UTC

Priority:  major
Component: IMAP
Keywords:  zimbra fcc literal data

--------------------------------------------------------------------------------
Description:
Hello,
our company recently migrated to Zimbra and I've been storing the
sent-email on a IMAP folder. Since we moved to Zimbra, we (me and
other mutt users) have problems with some messages while saving to
Fcc folder. It's consistent with a message but not for all messages.
There's nothing I could identify that would trigger this.
Here's some info:
version: 1.5.18 (Fedora rawhide package)
I'm trying to find a non internal mail that trigger the problem to
send you the full debug output, but what I checked:
- the size sent to Zimbra on APPEND:
{{{
    a0008 APPEND "INBOX.sent-mail" (\Seen) {1143}^M
    5< + send literal data 
}}}
  matches the message size.
- at the end of the message, a \r\n is sent by mutt, but instead
  of answering with
{{{
    5< a0008 OK [APPENDUID 176542 92201] APPEND completed
}}}
  the server responds with
{{{
    5< + send literal data
}}}
- I wrote a sample patch that solves the problem:

{{{
--- mutt-1.5.18.orig/imap/message.c     2008-01-29 23:26:50.000000000 -0500
+++ mutt-1.5.18/imap/message.c  2008-09-17 16:13:55.000000000 -0400
@@ -651,10 +651,15 @@ int imap_append_message (CONTEXT *ctx, M
   mutt_socket_write (idata->conn, "\r\n");
   fclose (fp);

-  do
+  do {
     rc = imap_cmd_step (idata);
-  while (rc == IMAP_CMD_CONTINUE);

+    if (rc == IMAP_CMD_RESPOND) {
+      /* I hate you zimbra */
+      imap_exec (idata, "NOOP", 0);
+    }
+  } while (rc == IMAP_CMD_CONTINUE);
+
   if (!imap_code (idata->buf))
   { 
     char *pc;
}}}

but that, of course is just a workaround. I have no problems so far
using this patch.

- one clue, that happens consistently with this particular message:
{{{
    (data)
    xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxUpdating progress: 1022
    5> xxxxxxxxxxxxxxxxxxxx ^M
}}}
  I'm not familiar with the mutt code, but one guess would be that
  the "+ send literal data" may come because the message transmit
  was interrupted in the middle for progress display. and it's only
  received after the remaining part is sent, causing confusion.

I know it may be a Zimbra IMAP bug, but I'd like to hear from you
before. This may ring a bell.

Thanks


--------------------------------------------------------------------------------
2008-09-23 06:45:26 UTC dtardon
* Added comment:
I think I've found a trigger for this problem--it's size of the message. After experimenting with meaningless messages I've got following results--a message containing 665 characters is saved right, whilst any one containing 666 (I actually don't believe there is any coincidence :) or more characters is rejected as described above.

--------------------------------------------------------------------------------
2008-09-23 06:46:47 UTC dtardon
* Added attachment msg-good.txt
* Added comment:
a good message

--------------------------------------------------------------------------------
2008-09-23 06:47:00 UTC dtardon
* Added attachment msg-bad.txt
* Added comment:
a bad message

--------------------------------------------------------------------------------
2008-09-23 12:42:59 UTC Michael Tatge
* Added comment:
{{{
* On Tue, Sep 23, 2008 06:45AM -0000 Mutt (fleas@mutt.org) wrote:

Since mutt is working fine with cyrus, I'd tend to think this is a imapd
bug.

HTH,

Michael
}}}

--------------------------------------------------------------------------------
2009-01-04 20:26:03 UTC brendan
* Added comment:
Will look at the RFC again and see what to do about spurious + responses before 1.6

* component changed to IMAP
* milestone changed to 1.6
* owner changed to brendan

--------------------------------------------------------------------------------
2009-01-05 00:22:05 UTC brendan
* status changed to accepted

--------------------------------------------------------------------------------
2009-01-05 00:34:13 UTC brendan
* Added comment:
Probably this is an upstream issue that they've fixed. I just FCC:ed a 2646-byte message to imap://testzimbra.com/Sent (Zimbra 5.0.11_GA_2695.RHEL4_64) with no issue. Can you produce a .muttdebug0 trace with mutt -d2?

* status changed to infoneeded

--------------------------------------------------------------------------------
2009-01-21 16:02:12 UTC aris
* Added comment:
Replying to [comment:5 brendan]:
> Probably this is an upstream issue that they've fixed. I just FCC:ed a 2646-byte message to imap://testzimbra.com/Sent (Zimbra 5.0.11_GA_2695.RHEL4_64) with no issue. Can you produce a .muttdebug0 trace with mutt -d2?

The issue is gone here too, thanks. do you still need the trace?

* status changed to assigned

--------------------------------------------------------------------------------
2009-01-21 19:08:48 UTC brendan
* Added comment:
I don't need it if it doesn't reproduce the bug. Sounds like this one can be closed, thanks for responding.

* resolution changed to worksforme
* status changed to closed
