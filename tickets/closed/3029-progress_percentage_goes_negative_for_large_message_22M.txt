Ticket:  3029
Status:  closed
Summary: progress percentage goes negative for large message (>22M)

Reporter: andersh
Owner:    mutt-dev

Opened:       2008-02-08 16:16:56 UTC
Last Updated: 2008-02-27 13:29:58 UTC

Priority:  minor
Component: display
Keywords:  

--------------------------------------------------------------------------------
Description:
When downloading large messages from an IMAP server, the progress percentage goes negative when the message size exceeds 22Mb (2**31 /100 bytes). This problem does not affect 64-bit versions.

A simle fix in curs_lib.c using float is attached.

--------------------------------------------------------------------------------
2008-02-08 16:18:11 UTC andersh
* Added attachment fixprogress.txt
* Added comment:
patch using float

--------------------------------------------------------------------------------
2008-02-10 00:14:30 UTC vinc17
* Added comment:
Using a double would be a better idea.

--------------------------------------------------------------------------------
2008-02-10 07:06:25 UTC andersh
* Added attachment fixprogress2.txt
* Added comment:
Modified patch using double (instead of double)

--------------------------------------------------------------------------------
2008-02-27 13:29:58 UTC andersh
* Added comment:
(In [d2eb082fd872]) Fix progress updates for large messages. Closes #3029.

* resolution changed to fixed
* status changed to closed
