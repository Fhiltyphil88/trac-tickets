Ticket:  2197
Status:  closed
Summary: segfault on imap-fetch-mail if imap server connect failed

Reporter: mhallock@scs.uiuc.edu
Owner:    mutt-dev

Opened:       2006-03-20 22:15:00 UTC
Last Updated: 2006-03-28 18:04:59 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
The imap-fetch-mail command will cause a segfault if you have not successfully authenticated to an imap server.
>How-To-Repeat:
Attempt to open an imap mailbox, but give wrong credentials.  Then issue an imap-fetch-mail command.
>Fix:
Change curs_main.c:998 to:
if (Context && Context->magic == M_IMAP)
}}}

--------------------------------------------------------------------------------
2006-03-27 20:28:31 UTC paul
* Added comment:
{{{
Bug confirmed; fix looks reasonable.

This is where we start poking Brendan... who's got the stick? ;-)
}}}

--------------------------------------------------------------------------------
2006-03-29 14:04:59 UTC brendan
* Added comment:
{{{
Applied, thanks!
}}}

* resolution changed to fixed
* status changed to closed
