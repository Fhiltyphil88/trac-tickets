Ticket:  3416
Status:  closed
Summary: mutt doesn't respect mailcap editor in Compose view

Reporter: tohPhu2U
Owner:    mutt-dev

Opened:       2010-06-07 21:52:21 UTC
Last Updated: 2010-06-08 17:43:35 UTC

Priority:  major
Component: mutt
Keywords:  mailcap edit

--------------------------------------------------------------------------------
Description:
So, I'm in compose view, just attached a gif image, which is properly identified as 'image/gif'. Now, I need to edit it, default keybinding is !^Xe. For some reason mutt runs $EDITOR for gif.
Here is what my mailcap has:
{{{
image/gif; edit=xpaint %s
}}}

Indeed, after checking source code, mutt unconditionally runs $EDITOR for anything. 
compose.c line ~960:
{{{
 case OP_COMPOSE_EDIT_FILE:
   CHECK_COUNT;
   mutt_edit_file (NONULL(Editor), idx[menu->current]->content->filename);
   mutt_update_encoding (idx[menu->current]->content);
   menu->redraw = REDRAW_CURRENT | REDRAW_STATUS;
   mutt_message_hook (NULL, msg, M_SEND2HOOK);
 break;
}}}
as you can see, mutt_edit_file is always passed Editor, which points to $EDITOR...

--------------------------------------------------------------------------------
2010-06-07 21:59:12 UTC tohPhu2U
* Added comment:
and according to http://www.mutt.org/doc/manual/manual-5.html 
{{{
edit=<command>

    This flag specifies the command to use to edit a specific MIME type. Mutt supports this from the compose menu, and also uses it to compose new attachments. Mutt will default to the defined editor for text attachments.
}}}
which is inconsistent to source code

--------------------------------------------------------------------------------
2010-06-08 02:36:01 UTC tohPhu2U
* Added comment:
Unfortunately, my mailcap entry is incorrect. Changing it to
{{{
image/gif;    ;edit=xpaint %s
}}}
fixes it; but the problem described above is unaffected.

--------------------------------------------------------------------------------
2010-06-08 17:43:35 UTC tohPhu2U
* Added comment:
I'm sorry, ignore this false bug report. `edit-mime' is what i want.

* resolution changed to invalid
* status changed to closed
