Ticket:  1088
Status:  closed
Summary: mailboxes are touched at exit therefore confusing "new mail" report?

Reporter: ossi@mail.kde.org
Owner:    mutt-dev

Opened:       2002-03-07 05:15:19 UTC
Last Updated: 2005-08-20 14:32:27 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.2.5
Severity: normal

folders marked as mailboxes are obviously touched when closed. that way
they always are marked as containing new mail in the folder list.



>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2002-03-07 05:15:19 UTC ossi@mail.kde.org
* Added comment:
{{{
Package: mutt
Version: 1.2.5
Severity: normal

folders marked as mailboxes are obviously touched when closed. that way
they always are marked as containing new mail in the folder list.
}}}

--------------------------------------------------------------------------------
2005-08-21 08:18:42 UTC rado
* Added comment:
{{{
pretty old, asking for update
}}}

* status changed to assigned

--------------------------------------------------------------------------------
2005-08-21 08:18:43 UTC rado
* Added comment:
{{{
Does this still apply to current versions?
(more verbose subject)
}}}

--------------------------------------------------------------------------------
2005-08-21 08:23:58 UTC Oswald Buddenhagen <ossi@kde.org>
* Added comment:
{{{
can't reproduce any more.
}}}

--------------------------------------------------------------------------------
2005-08-21 08:32:27 UTC rado
* Added comment:
{{{
can't reproduce any more.
Not reproducible with current version.
}}}

* resolution changed to fixed
* status changed to closed
