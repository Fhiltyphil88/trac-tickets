Ticket:  2864
Status:  closed
Summary: Bug#329899: mutt: Does not prompt during smtp auth (fwd)

Reporter: Christoph Berg <cb@df7cb.de>
Owner:    mutt-dev

Opened:       2007-03-25 23:15:01 UTC
Last Updated: 2008-06-07 22:37:27 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{

Is this something that should be fixed inside mutt?

----- Forwarded message from Kapil Hari Paranjape <kapil@imsc.res.in> -----

Date: Sat, 24 Sep 2005 15:14:49 +0530
From: Kapil Hari Paranjape <kapil@imsc.res.in>
Reply-To: Kapil Hari Paranjape <kapil@imsc.res.in>, 329899@bugs.debian.org
To: Debian Bug Tracking System <submit@bugs.debian.org>
Subject: Bug#329899: mutt: Does not prompt during smtp auth

Package: mutt
Version: 1.5.9-2
Severity: minor

Hello,

I use mutt with msmtp. I have set sendmail as /usr/bin/msmtp and
sendmail_wait to 0.

The msmtp prompt for password is not passed on to me.

The same happens if I use a passphrase encrypted certificate.

I think this may happen becuase mutt does not "give" a tty/pseudo tty
for the $sendmail program.

Regards,

Kapil.

----- End forwarded message -----

Christoph
-- 
cb@df7cb.de | http://www.df7cb.de/

>Fix:

Unknown
}}}

--------------------------------------------------------------------------------
2007-03-26 23:39:55 UTC Michael Elkins <michael.elkins@gmail.com>
* Added comment:
{{{
On Mon, Mar 26, 2007 at 12:15:02AM +0200, Christoph Berg wrote:
> Is this something that should be fixed inside mutt?

tip now contains ESMTP support with auth directly in Mutt, which should make use
of msmtp unnecessary.  Right now, Mutt always redirects stdin from the
$sendmail subprocess.

me
}}}

--------------------------------------------------------------------------------
2008-06-07 22:37:27 UTC myon
* Added comment:
It doesn't look like this is going to be fixed in mutt. Closing to get rid of fringe bugs.

* Updated description:
{{{

Is this something that should be fixed inside mutt?

----- Forwarded message from Kapil Hari Paranjape <kapil@imsc.res.in> -----

Date: Sat, 24 Sep 2005 15:14:49 +0530
From: Kapil Hari Paranjape <kapil@imsc.res.in>
Reply-To: Kapil Hari Paranjape <kapil@imsc.res.in>, 329899@bugs.debian.org
To: Debian Bug Tracking System <submit@bugs.debian.org>
Subject: Bug#329899: mutt: Does not prompt during smtp auth

Package: mutt
Version: 1.5.9-2
Severity: minor

Hello,

I use mutt with msmtp. I have set sendmail as /usr/bin/msmtp and
sendmail_wait to 0.

The msmtp prompt for password is not passed on to me.

The same happens if I use a passphrase encrypted certificate.

I think this may happen becuase mutt does not "give" a tty/pseudo tty
for the $sendmail program.

Regards,

Kapil.

----- End forwarded message -----

Christoph
-- 
cb@df7cb.de | http://www.df7cb.de/

>Fix:

Unknown
}}}
* resolution changed to wontfix
* status changed to closed
