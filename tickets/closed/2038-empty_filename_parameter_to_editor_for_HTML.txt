Ticket:  2038
Status:  closed
Summary: empty filename parameter to $editor for HTML

Reporter: Alain Bench <veronatif@free.fr>
Owner:    mutt-dev

Opened:       2005-08-13 14:31:58 UTC
Last Updated: 2005-08-15 18:45:02 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
    In $content_type="text/html" mode, and in the absence of a specific edit command for text/html in mailcap, Mutt fallbacks to $editor to compose an HTML mail. Bug: The temporary filename parameter given to $editor is the empty string. More info in July 2005 mutt-users thread "editor and content_type conflict?" beginning at msgid <20050704000403.GA26887@koli.kando.hu>. Reporter Gergely was too lazy to compose a real bugreport.

    Bug2: After composing a first text/html mail, the variable $content_type changes itself to "text".

>How-To-Repeat:
>Fix:
the patch attached.
}}}

--------------------------------------------------------------------------------
2005-08-14 23:14:14 UTC tamo
* Added comment:
{{{
* Sat Aug 13 2005 Alain Bench <veronatif@free.fr>
 My patch is attached.
}}}

--------------------------------------------------------------------------------
2005-08-15 08:38:08 UTC TAKAHASHI Tamotsu <ttakah@lapis.plala.or.jp>
* Added comment:
{{{
* Sat Aug 13 2005 Alain Bench <veronatif@free.fr>
> >Description:
> 
>     In $content_type="text/html" mode, and in the absence of a specific edit command for text/html in mailcap, Mutt fallbacks to $editor to compose an HTML mail. Bug: The temporary filename parameter given to $editor is the empty string. More info in July 2005 mutt-users thread "editor and content_type conflict?" beginning at msgid <20050704000403.GA26887@koli.kando.hu>. Reporter Gergely was too lazy to compose a real bugreport.

The bug is in attach.c here:

: int mutt_edit_attachment (BODY *a)
: {
...
:   char newfile[_POSIX_PATH_MAX] = "";
...
:   if (rfc1524_mailcap_lookup (a, type, entry, M_EDIT))
:   {
:     if (entry->editcommand)
:     {
:       strfcpy (command, entry->editcommand, sizeof (command));
:       if (rfc1524_expand_filename (entry->nametemplate,
: 				      a->filename, newfile, sizeof (newfile)))
...
:     }
:   }
:   else if (a->type == TYPETEXT)
:   {
:     /* On text, default to editor */
:     mutt_edit_file (NONULL (Editor), newfile);
:   }
:   else
...


newfile is still "" when (!rfc1524_mailcap_lookup(...)) and
(a->type==TYPETEXT).
I think we should use a->filename if available, mktemp if not.


>     Bug2: After composing a first text/html mail, the variable $content_type changes itself to "text".

Uggh, mutt_parse_content_type (in parse.c) has to have
(const char *, BODY *) type instead of (char *, BODY *),
and safe_strdup the char*.

Or, we should use safe_strdup'ed ContentType instead of
raw ContentType within ci_send_message (in send.c).

-    mutt_parse_content_type (ContentType, msg->content);
+    char *ctype = safe_strdup (ContentType);
+    mutt_parse_content_type (ctype, msg->content);

-- 
tamo
}}}

--------------------------------------------------------------------------------
2005-08-15 08:58:26 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
Hi Tamo!

 On Sunday, August 14, 2005 at 1:35:01 AM +0200, Tamotsu Takahashi wrote:

> * Sat Aug 13 2005 Alain Bench <veronatif@free.fr>
>> The temporary filename parameter given to $editor is the empty string.
>> the variable $content_type changes itself to "text".
> The bug is [...]

    Well done: Your patch-1.5.10.tamo.texthtml.1 seems to solve both
problems. Thank you very much!


Bye!	Alain.
-- 
How to Report Bugs Effectively
<URL:http://www.chiark.greenend.org.uk/~sgtatham/bugs.html>
}}}

--------------------------------------------------------------------------------
2005-08-15 16:32:41 UTC brendan
* Added comment:
{{{
Hi Tamo!
Applied, thanks.
}}}

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2005-08-16 06:09:40 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
On Saturday, August 13, 2005 at 3:31:58 PM +0200, Alain Bench wrote:

> Reporter Gergely was too lazy to compose a real bugreport.

    ...because Gergely had done a good reporter's job by avoiding
duplicate when he noticed same bug1 was already reported long ago as
mutt/1675. I did not. Apologies.

    Tamo's commited patch also fixes mutt/1675: Closing it.


Bye!	Alain.
-- 
Everything about locales on Sven Mascheck's excellent site at new
location <URL:http://www.in-ulm.de/~mascheck/locale/>. The little tester
utility is at <URL:http://www.in-ulm.de/~mascheck/locale/checklocale.c>.
}}}

--------------------------------------------------------------------------------
2007-04-03 16:43:00 UTC 
* Added attachment patch-1.5.10.tamo.texthtml.1
* Added comment:
patch-1.5.10.tamo.texthtml.1
