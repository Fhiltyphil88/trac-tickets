Ticket:  3870
Status:  closed
Summary: openssl 1.1

Reporter: tamo
Owner:    mutt-dev

Opened:       2016-09-08 01:53:47 UTC
Last Updated: 2016-12-04 23:46:34 UTC

Priority:  trivial
Component: build
Keywords:  ssl

--------------------------------------------------------------------------------
Description:
http://marc.info/?l=mutt-dev&m=147266025212119&w=2
(One week has passed since the last post in this thread.)

Mutt doesn't build with OpenSSL 1.1.

If we dropped support for obsolete versions (<0.9.6) of openssl,
the fix would be easy.

--------------------------------------------------------------------------------
2016-09-08 01:56:09 UTC tamo
* Added attachment mutt_openssl_req_gt_095.diff
* Added comment:
I can build and run mutt (tested with imaps) with openssl 1.0.1 / 1.1.0 after applying this patch

--------------------------------------------------------------------------------
2016-09-08 02:39:14 UTC kevin8t8
* Added comment:
Tamo, thank you very much.  I am going to apply this, and feel no pity for someone running a version of openssl from 2000 or earlier).

--------------------------------------------------------------------------------
2016-09-08 03:03:08 UTC TAKAHASHI Tamotsu <ttakah@lapis.plala.or.jp>
* Added comment:
In [changeset:"821022f6c78ca390a571b87301c8283e4f0b45a9" 6790:821022f6c78c]:
{{{
#!CommitTicketReference repository="" revision="821022f6c78ca390a571b87301c8283e4f0b45a9"
Fix openssl 1.1 compilation issues. (closes #3870)

With these changes, Mutt will no longer compile for versions less than
0.9.6.
}}}

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2016-11-18 14:23:57 UTC vinc17
* Added comment:
With {{{--enable-debug --enable-imap --with-ssl}}}, I still get an error:

{{{
In file included from rfc822.h:23:0,
                 from mutt.h:63,
                 from mutt_ssl.c:33:
mutt_ssl.c: In function ‘check_certificate_by_signer’:
mutt_ssl.c:669:47: error: dereferencing pointer to incomplete type ‘X509 {aka struct x509_st}’
     dprint (2, (debugfile, " [%s]\n", peercert->name));
                                               ^
}}}

* resolution changed to 
* status changed to reopened

--------------------------------------------------------------------------------
2016-11-20 02:48:07 UTC kevin8t8
* Added comment:
X509->name appears to have been a shortcut for X509_NAME_oneline(X509_get_subject_name (cert))

This is only used in debugging.  I've moved printing this out into ssl_check_certificate() and pulled it out of the other places.

--------------------------------------------------------------------------------
2016-11-20 03:38:25 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [changeset:"695243ba637465dc3822df1ff152091bc52b9ec8" 6869:695243ba6374]:
{{{
#!CommitTicketReference repository="" revision="695243ba637465dc3822df1ff152091bc52b9ec8"
More openssl1.1 fixes: remove uses of X509->name in debugging. (closes #3870)

X509->name was a shortcut for the longer
  name = X509_NAME_oneline (X509_get_subject_name (cert),
			    buf, sizeof (buf));
invocation.  Change the debugging to print the cert name and chain
names in the ssl_check_certificate() loop instead.
}}}

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2016-11-21 22:22:11 UTC vinc17
* Added comment:
The {{{X509_NAME_oneline}}} documentation is ambiguous: it does not say that its output is null-terminated if the number of characters reaches the buffer size, and according to https://langui.sh/2016/01/29/x509-name-oneline/ it isn't in such a case. I've fixed the code in r6872:db13010a2e8d.

To avoid the truncation, it is also possible to ask the library to do the allocation by providing a null pointer, but it is not clear how to free the memory ({{{free}}} or {{{OPENSSL_free}}}?), and anyway, a truncation is acceptable in a debugging message. Ditto for the fact that the use of {{{X509_NAME_oneline}}} is "strongly discouraged".

--------------------------------------------------------------------------------
2016-11-21 23:22:12 UTC kevin8t8
* Added comment:
Thanks Vincent.  Just to note, I looked in the openssl source (for versions v1.1, 1.0.2, and 1.0.1) before making this change.  {{{X509_NAME_oneline}}} decrements the buflen parameter by one internally (to leave space for the null-terminator), and does null-terminate the buffer even in the event the length is not long enough.

I also noted the "strongly discouraged" label, but I agree this isn't critical for debugging.  

Unfortunately, we are also using this function in our interactive_check_cert() function, so there might be an issue there.

Also, the 1.0.2 and 1.0.1 versions used {{{OPENSSL_free}}} internally for freeing the return value (inside crypto/asn1/x_x509.c}.


--------------------------------------------------------------------------------
2016-11-22 01:55:25 UTC kevin8t8
* Added comment:
I've reviewed the git history, just to make sure, and it appears {{{X509_NAME_oneline}}} has always had this behavior.  So I'm going to revert db13010a2e8d and just add a comment to the code.

--------------------------------------------------------------------------------
2016-11-22 02:05:11 UTC vinc17
* Added comment:
OK, so I suppose that LibreSSL (which is a fork of OpenSSL and provides a backward-compatible library) has the same behavior too. The only thing I could find is CVE-2015-5334 http://www.securityfocus.com/archive/1/536692 where the null-terminator could be written out-of-bounds in LibreSSL. Concerning the OpenSSL documentation, I've reported a bug there: https://github.com/openssl/openssl/issues/1978

--------------------------------------------------------------------------------
2016-11-22 02:05:49 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [changeset:"65f180f2904f0511268d7d3f1c6e4b7ebe93d300" 6873:65f180f2904f]:
{{{
#!CommitTicketReference repository="" revision="65f180f2904f0511268d7d3f1c6e4b7ebe93d300"
Revert db13010a2e8d but add a comment. (see #3870)

X509_NAME_oneline() always NULL-terminates the string, even when it
has to truncate the data to fit in buf.
}}}

--------------------------------------------------------------------------------
2016-12-04 23:46:32 UTC TAKAHASHI Tamotsu <ttakah@lapis.plala.or.jp>
* Added comment:
In [changeset:"2c1d79d3edd5a19a1d3b41886bc5970826d082e6" 6880:2c1d79d3edd5]:
{{{
#!CommitTicketReference repository="" revision="2c1d79d3edd5a19a1d3b41886bc5970826d082e6"
Fix openssl 1.1 compilation issues. (closes #3870)

With these changes, Mutt will no longer compile for versions less than
0.9.6.
}}}

--------------------------------------------------------------------------------
2016-12-04 23:46:34 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [changeset:"10c4761cea89e09865fed0889285682165645e80" 6881:10c4761cea89]:
{{{
#!CommitTicketReference repository="" revision="10c4761cea89e09865fed0889285682165645e80"
More openssl1.1 fixes: remove uses of X509->name in debugging. (closes #3870)

X509->name was a shortcut for the longer
  name = X509_NAME_oneline (X509_get_subject_name (cert),
			    buf, sizeof (buf));
invocation.  Change the debugging to print the cert name and chain
names in the ssl_check_certificate() loop instead.
}}}
