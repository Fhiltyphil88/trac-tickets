Ticket:  3568
Status:  closed
Summary: Saving attachments to disk

Reporter: aitor
Owner:    mutt-dev

Opened:       2012-03-09 17:37:58 UTC
Last Updated: 2013-01-20 21:35:59 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
Hi,

I sent this mail to mutt-users some days ago. In the past couple of days I've been hit by this behaviour (using mutt 1.5.21 compiled from scratch). Suppose that I receive a mail with several attachments:

I     1 <no description>      [multipa/alternativ, 7bit, 7.2K][[BR]]

I     2 ├─><no description>   [text/plain, 7bit, iso-8859-1, 0.7K][[BR]]

I     3 └─><no description>   [text/html, 7bit, iso-8859-1, 6.3K][[BR]]

A     4 file.txt              [text/plain, 8bit, us-ascii, 2.3K][[BR]]

A     5 file.xml              [text/xml, 8bit, us-ascii, 0.1K][[BR]]

A     6 file.pdf              [text/x-unknown-, base64, us-ascii, 66K][[BR]]



The message has 3 attachments, one text file, one xml document and one pdf document (this is a real example,the sender MUA being Thuderbird 3.1.18). It turns out that the sender MUA got it wrong, and the character encoding is wrongly set (us-ascii), but:

- file.txt is utf-8 encoded

- file.xml is also utf-8 encoded.

- file.pdf is a pdf document.

The problem is that there is no way to save these attachment properly. For instance, when saving file.xml all the non ascii characters are replaced with "?". So if file.xml originally was:

<?xml version="1.0" encoding="utf-8"?>
<a>©Áéñio</a>

After saving to disk it becomes:

<?xml version="1.0" encoding="utf-8"?>
<a>????????io</a>

The same happens with the text file. Even the pdf document is wrongly saved. The only way to actually get the original content of the attchments is to manually set the mime type (using ctrl-e) to
"application/octet-stream", and then saving them to disk.

I locally modified mutt the following below, because I don't want to
perform any character conversions when saving attachments:

diff -r 25a7f8f7d50d attach.c
--- a/attach.c  Wed Sep 15 10:10:39 2010 -0700
+++ b/attach.c  Thu Mar 01 07:12:54 2012 +0100
@@ -802,7 +802,7 @@
       STATE s;

       memset (&s, 0, sizeof (s));
-      s.flags |= M_CHARCONV;
+      s.flags &= ~M_CHARCONV;

       if ((s.fpout = mutt_save_attachment_open (path, flags)) == NULL)
       {

It would be better to parametrize this behaviour with a global option. Even better would be to first try to translate the attachment, and failback to a "raw" save if a conversion error occured (a character could not be translated). But I'm afraid I don't have the capabilities to code this :-(

thanks,
                                         aitor


--------------------------------------------------------------------------------
2013-01-20 21:35:59 UTC me
* Added comment:
This bug has been fixed by backing out [7fcc0049f250].  This was the same bug as #3293.

* Updated description:
Hi,

I sent this mail to mutt-users some days ago. In the past couple of days I've been hit by this behaviour (using mutt 1.5.21 compiled from scratch). Suppose that I receive a mail with several attachments:

I     1 <no description>      [multipa/alternativ, 7bit, 7.2K][[BR]]

I     2 ├─><no description>   [text/plain, 7bit, iso-8859-1, 0.7K][[BR]]

I     3 └─><no description>   [text/html, 7bit, iso-8859-1, 6.3K][[BR]]

A     4 file.txt              [text/plain, 8bit, us-ascii, 2.3K][[BR]]

A     5 file.xml              [text/xml, 8bit, us-ascii, 0.1K][[BR]]

A     6 file.pdf              [text/x-unknown-, base64, us-ascii, 66K][[BR]]



The message has 3 attachments, one text file, one xml document and one pdf document (this is a real example,the sender MUA being Thuderbird 3.1.18). It turns out that the sender MUA got it wrong, and the character encoding is wrongly set (us-ascii), but:

- file.txt is utf-8 encoded

- file.xml is also utf-8 encoded.

- file.pdf is a pdf document.

The problem is that there is no way to save these attachment properly. For instance, when saving file.xml all the non ascii characters are replaced with "?". So if file.xml originally was:

<?xml version="1.0" encoding="utf-8"?>
<a>©Áéñio</a>

After saving to disk it becomes:

<?xml version="1.0" encoding="utf-8"?>
<a>????????io</a>

The same happens with the text file. Even the pdf document is wrongly saved. The only way to actually get the original content of the attchments is to manually set the mime type (using ctrl-e) to
"application/octet-stream", and then saving them to disk.

I locally modified mutt the following below, because I don't want to
perform any character conversions when saving attachments:

diff -r 25a7f8f7d50d attach.c
--- a/attach.c  Wed Sep 15 10:10:39 2010 -0700
+++ b/attach.c  Thu Mar 01 07:12:54 2012 +0100
@@ -802,7 +802,7 @@
       STATE s;

       memset (&s, 0, sizeof (s));
-      s.flags |= M_CHARCONV;
+      s.flags &= ~M_CHARCONV;

       if ((s.fpout = mutt_save_attachment_open (path, flags)) == NULL)
       {

It would be better to parametrize this behaviour with a global option. Even better would be to first try to translate the attachment, and failback to a "raw" save if a conversion error occured (a character could not be translated). But I'm afraid I don't have the capabilities to code this :-(

thanks,
                                         aitor
* resolution changed to fixed
* status changed to closed
