Ticket:  3278
Status:  closed
Summary: info/error message stripped 2 columns before the right of the terminal in command line

Reporter: vinc17
Owner:    mutt-dev

Opened:       2009-06-22 11:56:25 UTC
Last Updated: 2009-06-23 00:08:50 UTC

Priority:  minor
Component: display
Keywords:  

--------------------------------------------------------------------------------
Description:
The informational and error messages in the command line are stripped 2 columns before the right of the terminal. For instance, in a 80-column terminal, only the first 78 characters are displayed.

--------------------------------------------------------------------------------
2009-06-22 16:23:06 UTC Rocco Rutte <pdmef@gmx.net>
* Added comment:
(In [54bc1ef602e7]) Make mutt_curses_(error|message) format message to COLS chars. Closes #3278.

While I'm at it, fold both functions into one.

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2009-06-23 00:08:50 UTC Vincent Lefevre
* Added comment:
{{{
On 2009-06-22 16:23:06 -0000, Mutt wrote:

This is incorrect: the BEEP () must be called only for error messages.
So, you need to replace

    BEEP ();

by

    if (error)
      BEEP ();
}}}
