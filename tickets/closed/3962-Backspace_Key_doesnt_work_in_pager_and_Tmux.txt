Ticket:  3962
Status:  closed
Summary: Backspace Key doesn't work in pager and Tmux

Reporter: ManDay
Owner:    mutt-dev

Opened:       2017-08-21 16:56:55 UTC
Last Updated: 2017-12-28 02:39:08 UTC

Priority:  major
Component: user interface
Keywords:  BackSpace

--------------------------------------------------------------------------------
Description:
BackSpace (bound to previous-line) is not triggered when pressing Backspace in tmux. Tmux (TERM=screen-256color) seems to produce \c? which, when bound, gives the desired behaviour. Perhaps mutt could rely on ncurses/readline at this point to facilitate figuring out the correct sequence of "BackSpace"?

--------------------------------------------------------------------------------
2017-08-21 18:48:45 UTC kevin8t8
* Added comment:
Mutt is relying completely on ncurses for the backspace key translation.

You can peek inside keymap.c if you're comfortable looking at code: the "<backspace>" key name is mapped to the KEY_BACKSPACE keymap as defined by ncurses.  Further below, you can see:
{{{
  km_bindkey ("<backspace>", MENU_PAGER, OP_PREV_LINE);
}}}
which binds this keyname to the "previous line" operation in the pager.

The reason why the line editor is working is because Mutt binds multiple keys to the editor backspace operation:
{{{
  km_bindkey ("<backspace>", MENU_EDITOR, OP_EDITOR_BACKSPACE);
  km_bindkey ("<delete>", MENU_EDITOR, OP_EDITOR_BACKSPACE);
  km_bindkey ("\177", MENU_EDITOR, OP_EDITOR_BACKSPACE);
}}}

I'd guess that the terminal is actually sending the "delete" key when you hit backspace.  You may want to try running
{{{
  :bind pager <delete> previous-line
}}}
and see if that "fixes" the problem.  If you aren't binding delete to anything in the pager, you could just add that to your .muttrc.

Alternatively, this may be something in either tmux or your terminfo that isn't quite right.  I'm not really a terminfo expert, so that's a bit beyond what I can help with.



--------------------------------------------------------------------------------
2017-08-22 08:27:37 UTC ManDay
* Added comment:
Maybe there is another, more reliable way with ncurses to bind to backspace and delete? When I'm trying in ncmpcpp, for example the / "find" line entry, backspace and delete do have their correct, respective effect (as opposed to mutt's line-entry where backspace and delete both act as backspace-style deletion). And they do so in tmux and outside. I haven't sought out the place in it's source where bindings to line entries would be made, but I suppose it's worth considering.

--------------------------------------------------------------------------------
2017-08-22 08:29:54 UTC ManDay
* Added comment:
In fact, from a simple grep -i backspace I don't see any place where the keybindings for a line entry would be explicitly established (indeed, I'm surprised that you'd have to). I suppose ncurses has a simpler method to provide line entry? And if with this methods backspace and delete can be properly distinguished, I suppose the same should be possible with generic bindings.

--------------------------------------------------------------------------------
2017-08-22 20:48:10 UTC kevin8t8
* Added comment:
> Maybe there is another, more reliable way with ncurses to bind to
> backspace and delete?

Perhaps, but I don't know of any.  We're using the most straightfoward
ncurses function: getch().  We count on ncurses to look at the
appropriate terminfo entries and return the correct code to mutt.

You didn't mention whether the "bind" command above fixed the problem,
but I believe getch() is likely returing the delete keypad code when you
press Backspace.

Out of curiosity, what is the output if you run:
{{{
infocmp screen-256color
stty -a
}}}

and what is output if you enter:
{{{
stty raw; sleep 10s; stty -raw
}}}
and you hit backspace while it's sleeping.

> In fact, from a simple grep -i backspace I don't see any place where
> the keybindings for a line entry would be explicitly established
> (indeed, I'm surprised that you'd have to).

ncmpcpp is using readline.  Looking at the code, it does appear to be
delegating to readline for the search function prompt.  It's likely
readline has some sort of hack working around the issue.

I am not convinced Mutt should have code to work around broken configs.
We're likely to break something else and end up with another ticket with
the same request to "rely on ncurses to figure out the correct
sequence".

So far I've not been able to duplicate the issue, so I can't test
myself.  The "jump to parent directory" and "replay song" actions appear
to be bound to backspace.  Are those working for you?

--------------------------------------------------------------------------------
2017-08-23 13:32:22 UTC kevin8t8
* Added comment:
Another useful function to try in mutt is <what-key>:
{{{
  :exec what-key
}}}
What does that output when you hit backspace and delete?

--------------------------------------------------------------------------------
2017-08-24 08:27:19 UTC ManDay
* Added comment:
Replying to [comment:4 kevin8t8]:
> You didn't mention whether the "bind" command above fixed the problem,
> but I believe getch() is likely returing the delete keypad code when you
> press Backspace.

No, binding to <delete> does not work.

> Out of curiosity, what is the output if you run:
> {{{
> infocmp screen-256color
> stty -a
> }}}

{{{
#       Reconstructed via infocmp from file: /usr/share/terminfo/s/screen-256color
screen-256color|GNU Screen with 256 colors,
        am, km, mir, msgr, xenl,
        colors#256, cols#80, it#8, lines#24, pairs#32767,
        acsc=++\,\,--..00``aaffgghhiijjkkllmmnnooppqqrrssttuuvvwwxxyyzz{{||}}~~,
        bel=^G, blink=\E[5m, bold=\E[1m, cbt=\E[Z, civis=\E[?25l,
        clear=\E[H\E[J, cnorm=\E[34h\E[?25h, cr=^M,
        csr=\E[%i%p1%d;%p2%dr, cub=\E[%p1%dD, cub1=^H,
        cud=\E[%p1%dB, cud1=^J, cuf=\E[%p1%dC, cuf1=\E[C,
        cup=\E[%i%p1%d;%p2%dH, cuu=\E[%p1%dA, cuu1=\EM,
        cvvis=\E[34l, dch=\E[%p1%dP, dch1=\E[P, dim=\E[2m,
        dl=\E[%p1%dM, dl1=\E[M, ed=\E[J, el=\E[K, el1=\E[1K,
        enacs=\E(B\E)0, flash=\Eg, home=\E[H, ht=^I, hts=\EH,
        ich=\E[%p1%d@, il=\E[%p1%dL, il1=\E[L, ind=^J, is2=\E)0,
        kbs=^H, kcbt=\E[Z, kcub1=\EOD, kcud1=\EOB, kcuf1=\EOC,
        kcuu1=\EOA, kdch1=\E[3~, kend=\E[4~, kf1=\EOP, kf10=\E[21~,
        kf11=\E[23~, kf12=\E[24~, kf2=\EOQ, kf3=\EOR, kf4=\EOS,
        kf5=\E[15~, kf6=\E[17~, kf7=\E[18~, kf8=\E[19~, kf9=\E[20~,
        khome=\E[1~, kich1=\E[2~, kmous=\E[M, knp=\E[6~, kpp=\E[5~,
        nel=\EE, op=\E[39;49m, rc=\E8, rev=\E[7m, ri=\EM, rmacs=^O,
        rmcup=\E[?1049l, rmir=\E[4l, rmkx=\E[?1l\E>, rmso=\E[23m,
        rmul=\E[24m, rs2=\Ec\E[?1000l\E[?25h, sc=\E7,
        setab=\E[%?%p1%{8}%<%t4%p1%d%e%p1%{16}%<%t10%p1%{8}%-%d%e48;5;%p1%d%;m,
        setaf=\E[%?%p1%{8}%<%t3%p1%d%e%p1%{16}%<%t9%p1%{8}%-%d%e38;5;%p1%d%;m,
        sgr=\E[0%?%p6%t;1%;%?%p1%t;3%;%?%p2%t;4%;%?%p3%t;7%;%?%p4%t;5%;%?%p5%t;2%;m%?%p9%t\016%e\017%;,
        sgr0=\E[m\017, smacs=^N, smcup=\E[?1049h, smir=\E[4h,
        smkx=\E[?1h\E=, smso=\E[3m, smul=\E[4m, tbc=\E[3g,
}}}

{{{
speed 38400 baud; rows 42; columns 174; line = 0;
intr = ^C; quit = ^\; erase = ^?; kill = ^U; eof = ^D; eol = <undef>; eol2 = <undef>; swtch = <undef>; start = ^Q; stop = ^S; susp = ^Z; rprnt = ^R; werase = ^W; lnext = ^V; discard = ^O; min = 1; time = 0;
-parenb -parodd -cmspar cs8 -hupcl -cstopb cread -clocal -crtscts
-ignbrk -brkint -ignpar -parmrk -inpck -istrip -inlcr -igncr icrnl ixon -ixoff -iuclc -ixany -imaxbel iutf8
opost -olcuc -ocrnl onlcr -onocr -onlret -ofill -ofdel nl0 cr0 tab0 bs0 vt0 ff0
isig icanon iexten echo echoe echok -echonl -noflsh -xcase -tostop -echoprt echoctl echoke -flusho -extproc
}}}

> and what is output if you enter:
> {{{
> stty raw; sleep 10s; stty -raw
> }}}
> and you hit backspace while it's sleeping.

For backspace, `^?` is returned. For delete, `^[[3~` is returned.

> So far I've not been able to duplicate the issue, so I can't test
> myself.  The "jump to parent directory" and "replay song" actions appear
> to be bound to backspace.  Are those working for you?

Yes, perhaps I shouldn't have used the find prompt as an example, as I could have guessed it's using readline. Backspace and delete to "replay song" and "delete track", respectively. And they do indeed have their respective effect.

`what-key` returns the following for backspace
{{{
Char = ^?, Octal = 177, Decimal = 127
}}}
and for delete
{{{
Char = <Delete>, Octal = 512, Decimal = 330
}}}

--------------------------------------------------------------------------------
2017-08-24 20:12:19 UTC kevin8t8
* Added comment:
Thank you for the output to all those commands; that helps a lot.  So the "short answer" is the terminfo entry for screen-256color has a problem.  

kbs contains the string the terminal expects to be sent by Backspace.  This would normally be `kbs=\177` (which is `^?` or DEL).  Indeed \177 is what is being sent when you type Backspace.  But if you look in the output of infocmp, you'll see `kbs=^H`.

So ncurses, which reads the terminfo entry, is not returning the {{{KEY_BACKSPACE}}} keycode to mutt when you press backspace, because it thinks backspace is `^H`.  The entry on my system is `kbs=\177` which explains why I couldn't reproduce the issue.

I took another look at the ncmpcpp source.  I'm not a C++ programmer, but it looks like it is not relying on ncurses to do keycode translation, as I originally thought.  It seems to be doing its own translation at a pretty low level, handling some of the common terminal sequences for xterm, eterm, rxvt, etc.

It also hardcodes the Backspace keycode to 127 (\177 or `^?`).  So that's why it is working for you.

So what does this mean for you.  Well, first it may be worth filing a bug with your distro.  If you are comfortable compiling terminfo entries, you could even try fixing and reinstalling the terminfo entry yourself.

A more practical short-term solution would be what you originally did:
{{{
 bind pager \177 previous-line
}}}

From my side, I think it may be worth automatically translating \177 to KEY_BACKSPACE.  If getch() returns the raw value it means it's not bound to anything in the terminfo file, so this is probably safe.  We're frozen for the 1.9 release right now, but I will poll the other developers about the idea and unless I get pushback will commit something when develop opens up again after the release.

--------------------------------------------------------------------------------
2017-08-25 11:08:43 UTC vinc17
* Added comment:
Replying to [comment:7 kevin8t8]:
> So what does this mean for you.  Well, first it may be worth filing a bug with your distro.

FYI, in Debian, this was fixed in 2010:

  https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=602300

> If you are comfortable compiling terminfo entries, you could even try fixing and reinstalling the terminfo entry yourself.

This is quite easy with the {{{infocmp}}} and {{{tic}}} commands.

> From my side, I think it may be worth automatically translating \177 to KEY_BACKSPACE.

I disagree. The use of \177 for KEY_BACKSPACE is specific to Linux (and other OS that copied on Linux). AFAIK, this is why upstream does not change kbs to follow the Linux convention. Other platforms may choose KEY_DELETE.

--------------------------------------------------------------------------------
2017-08-25 16:23:26 UTC kevin8t8
* Added comment:
Thanks Vincent.  That was my initial concern in comment:4, and it sounds like caution is appropriate.

ManDay, I saw your pings on IRC now, but looks like I missed you.  Feel free to email me directly if you have questions or comments you don't want to put in the ticket.

--------------------------------------------------------------------------------
2017-11-06 20:56:26 UTC kevin8t8
* Added comment:
ManDay, sorry for the delay.  Did you have any success in getting this patched in your distro?  I think, given Vincent's (appropriate) disagreement with workarounds in the Mutt source, there isn't much to do on our side.  Would you mind if I closed this ticket out?  Thank you!

--------------------------------------------------------------------------------
2017-12-28 02:39:08 UTC kevin8t8
* Added comment:
Closing this ticket, as it wasn't a Mutt issue.

* resolution changed to wontfix
* status changed to closed
