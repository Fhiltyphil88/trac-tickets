Ticket:  814
Status:  closed
Summary: Translation: in Czech confirmation should be by pressing a not y.

Reporter: Matìj Cepl <ceplma00@yahoo.com>
Owner:    mutt-dev

Opened:       2001-10-11 04:59:25 UTC
Last Updated: 2015-12-07 21:35:39 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.3.22.1i
Severity: normal

-- Please type your report below this line

Hi,

when Czech locale are set, mutt offers in confirmation dialog options [Ano/Ne]
(which is Yes/No in Czech). However, for confirmation it expects Y on stdin
not A.

Thanks

Matej Cepl


-- Mutt Version Information

Mutt 1.3.22.1i (2001-08-30)
Copyright (C) 1996-2001 Michael R. Elkins a dal¹í.
Mutt je roz¹iøován BEZ JAKÉKOLI ZÁRUKY; dal¹í informace získáte pøíkazem
`mutt -vv'.
Mutt je volné programové vybavení. Roz¹iøování tohoto programu je vítáno,
musíte ov¹em dodr¾et urèitá pravidla; dal¹í informace získáte pøíkazem
`mutt -vv'.

System: Linux 2.2.16-22 [using slang 10401]
Pøelo¾eno s volbami:
-DOMAIN
+DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_NNTP  +USE_IMAP  -USE_GSS  +USE_SSL  -USE_SASL  
+HAVE_REGCOMP  -USE_GNU_REGEX  +COMPRESSED  
+HAVE_COLOR  -HAVE_START_COLOR  -HAVE_TYPEAHEAD  -HAVE_BKGDSET  
-HAVE_CURS_SET  -HAVE_META  -HAVE_RESIZETERM  
+HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  -HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/local/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
INEWS="/usr/bin/inews -hS"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
Vývojáøe programu mù¾ete kontaktovat na adrese <mutt-dev@mutt.org> (anglicky).
Chyby v programu oznamujte pomocí utility flea(1).
Pøipomínky k pøekladu zasílejte na adresu <cs@li.org> (èesky).



>How-To-Repeat:
	
>Fix:
}}}

--------------------------------------------------------------------------------
2001-10-11 04:59:25 UTC Matìj Cepl <ceplma00@yahoo.com>
* Added comment:
{{{
Package: mutt
Version: 1.3.22.1i
Severity: normal

-- Please type your report below this line

Hi,

when Czech locale are set, mutt offers in confirmation dialog options [Ano/Ne]
(which is Yes/No in Czech). However, for confirmation it expects Y on stdin
not A.

Thanks

Matej Cepl


-- Mutt Version Information

Mutt 1.3.22.1i (2001-08-30)
Copyright (C) 1996-2001 Michael R. Elkins a dal¹í.
Mutt je roz¹iøován BEZ JAKÉKOLI ZÁRUKY; dal¹í informace získáte pøíkazem
`mutt -vv'.
Mutt je volné programové vybavení. Roz¹iøování tohoto programu je vítáno,
musíte ov¹em dodr¾et urèitá pravidla; dal¹í informace získáte pøíkazem
`mutt -vv'.

System: Linux 2.2.16-22 [using slang 10401]
Pøelo¾eno s volbami:
-DOMAIN
+DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_NNTP  +USE_IMAP  -USE_GSS  +USE_SSL  -USE_SASL  
+HAVE_REGCOMP  -USE_GNU_REGEX  +COMPRESSED  
+HAVE_COLOR  -HAVE_START_COLOR  -HAVE_TYPEAHEAD  -HAVE_BKGDSET  
-HAVE_CURS_SET  -HAVE_META  -HAVE_RESIZETERM  
+HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  -HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/local/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
INEWS="/usr/bin/inews -hS"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
Vývojáøe programu mù¾ete kontaktovat na adrese <mutt-dev@mutt.org> (anglicky).
Chyby v programu oznamujte pomocí utility flea(1).
Pøipomínky k pøekladu zasílejte na adresu <cs@li.org> (èesky).
}}}

--------------------------------------------------------------------------------
2001-10-11 17:59:00 UTC Thomas Roessler <roessler@does-not-exist.org>
* Added comment:
{{{
Please compile and run the program below, and report back the 
program's output. Thanks.

#include <locale.h>
#include <langinfo.h>
#include <stdio.h>

#define langinfo_check(a) printf ("nl_langinfo(" #a ") \t= %s\n", nl_langinfo (a));

int main (int argc, char *argv[])
{
 setlocale (LC_ALL, "");
 setlocale (LC_CTYPE, "");
 langinfo_check (CODESET);
 langinfo_check (YESEXPR);
 langinfo_check (NOEXPR);
 return 0;
}

-- 
Thomas Roessler                        http://log.does-not-exist.org/
}}}

--------------------------------------------------------------------------------
2001-10-17 22:08:06 UTC Matìj Cepl <ceplma00@yahoo.com>
* Added comment:
{{{
Package: mutt
Version: 1.3.22.1i
Severity: normal

-- Please type your report below this line

The same as a bug #814, but with symbols (-g option of compiler), so that it
may be more usefull to you.

Matej


-- Mutt Version Information

Mutt 1.3.22.1i (2001-08-30)
Copyright (C) 1996-2001 Michael R. Elkins a dal¹í.
Mutt je roz¹iøován BEZ JAKÉKOLI ZÁRUKY; dal¹í informace získáte pøíkazem
`mutt -vv'.
Mutt je volné programové vybavení. Roz¹iøování tohoto programu je vítáno,
musíte ov¹em dodr¾et urèitá pravidla; dal¹í informace získáte pøíkazem
`mutt -vv'.

System: Linux 2.2.16-22 [using slang 10401]
Pøelo¾eno s volbami:
-DOMAIN
+DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_NNTP  +USE_IMAP  -USE_GSS  +USE_SSL  -USE_SASL  
+HAVE_REGCOMP  -USE_GNU_REGEX  +COMPRESSED  
+HAVE_COLOR  -HAVE_START_COLOR  -HAVE_TYPEAHEAD  -HAVE_BKGDSET  
-HAVE_CURS_SET  -HAVE_META  -HAVE_RESIZETERM  
+HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  -HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
INEWS="/usr/bin/inews -hS"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
Vývojáøe programu mù¾ete kontaktovat na adrese <mutt-dev@mutt.org> (anglicky).
Chyby v programu oznamujte pomocí utility flea(1).
Pøipomínky k pøekladu zasílejte na adresu <cs@li.org> (èesky).


-- Core Dump Analysis Output

GNU gdb 5.0
Copyright 2000 Free Software Foundation, Inc.
GDB is free software, covered by the GNU General Public License, and you are
welcome to change it and/or distribute copies of it under certain conditions.
Type "show copying" to see the conditions.
There is absolutely no warranty for GDB.  Type "show warranty" for details.
This GDB was configured as "i386-redhat-linux"...
(no debugging symbols found)...
Core was generated by `mutt'.
Program terminated with signal 11, Segmentation fault.
Reading symbols from /usr/lib/libslang.so.1...(no debugging symbols found)...
done.
Loaded symbols for /usr/lib/libslang.so.1
Reading symbols from /lib/libm.so.6...done.
Loaded symbols for /lib/libm.so.6
Reading symbols from /usr/lib/libssl.so.1...done.
Loaded symbols for /usr/lib/libssl.so.1
Reading symbols from /usr/lib/libcrypto.so.1...done.
Loaded symbols for /usr/lib/libcrypto.so.1
Reading symbols from /lib/libc.so.6...done.
Loaded symbols for /lib/libc.so.6
Reading symbols from /lib/libdl.so.2...done.
Loaded symbols for /lib/libdl.so.2
Reading symbols from /lib/ld-linux.so.2...done.
Loaded symbols for /lib/ld-linux.so.2
Reading symbols from /lib/libnss_files.so.2...done.
Loaded symbols for /lib/libnss_files.so.2
Reading symbols from /usr/lib/gconv/ISO8859-2.so...done.
Loaded symbols for /usr/lib/gconv/ISO8859-2.so
Reading symbols from /lib/libnss_nisplus.so.2...done.
Loaded symbols for /lib/libnss_nisplus.so.2
Reading symbols from /lib/libnsl.so.1...done.
Loaded symbols for /lib/libnsl.so.1
Reading symbols from /lib/libnss_nis.so.2...done.
Loaded symbols for /lib/libnss_nis.so.2
Reading symbols from /lib/libnss_dns.so.2...done.
Loaded symbols for /lib/libnss_dns.so.2
Reading symbols from /lib/libresolv.so.2...done.
Loaded symbols for /lib/libresolv.so.2
Reading symbols from /usr/lib/gconv/ISO646.so...done.
Loaded symbols for /usr/lib/gconv/ISO646.so
Reading symbols from /usr/lib/gconv/ISO8859-1.so...done.
Loaded symbols for /usr/lib/gconv/ISO8859-1.so
#0  0x807bc7e in strcpy () at ../sysdeps/generic/strcpy.c:31
#0  0x807bc7e in strcpy () at ../sysdeps/generic/strcpy.c:31
#1  0xbfffdb24 in ?? ()
#2  0x808fd0e in strcpy () at ../sysdeps/generic/strcpy.c:31
#3  0x80901f4 in strcpy () at ../sysdeps/generic/strcpy.c:31
#4  0x8066c3b in strcpy () at ../sysdeps/generic/strcpy.c:31
#5  0x8083cab in strcpy () at ../sysdeps/generic/strcpy.c:31
#6  0x401a1a7c in __libc_start_main (main=0x8082aa0 <strcpy+224192>, argc=1, 
   ubp_av=0xbffff9e4, init=0x804b038 <_init>, fini=0x80e4bac <_fini>, 
   rtld_fini=0x4000d684 <_dl_fini>, stack_end=0xbffff9dc)
   at ../sysdeps/generic/libc-start.c:111
26	in ../sysdeps/generic/strcpy.c
}}}

--------------------------------------------------------------------------------
2001-10-18 12:04:43 UTC Thomas Roessler <roessler@does-not-exist.org>
* Added comment:
{{{
On 2001-10-17 11:08:06 -0400, Matìj Cepl wrote:

>Subject: bug#814: mutt-1.3.22.1i: Core dumped when inbox was changed by isync

Well, the core dump isn't particularly helpful.

However, one question...  Do you have any way to reproduce the 
problem?  What specific kind of behaviour on isync's side crashes 
mutt?

-- 
Thomas Roessler                        http://log.does-not-exist.org/
}}}

--------------------------------------------------------------------------------
2001-10-19 04:26:57 UTC Thomas Roessler <roessler@does-not-exist.org>
* Added comment:
{{{
I suppose this belongs to #830, not to #814.  In order to get #814 
debugged a bit easier (isync-related crashes), I'd like to ask you 
to use a non-NNTP mutt for your IMAP/isync mail.  That way, we don't 
get nntp- and isync-related crashes mixed up.

Thanks.
-- 
Thomas Roessler                        http://log.does-not-exist.org/
}}}

--------------------------------------------------------------------------------
2001-10-24 10:31:25 UTC Thomas Roessler <roessler@does-not-exist.org>
* Added comment:
{{{
OK, I understand now what has happened; sorry for being so dense 
yesterday.

The regular expression returned by nl_langinfo() is so broad mutt 
doesn't like it. More precisely, mutt would expect that expression 
to begin with a '^' character.

In order to fix things for your purposes, you can remove the 
"expr[0] == '^'" clause from lines 167 and 169 in curs_lib.c.

However, I'm not entirely sure we have to require this condition. 

What do you folks on mutt-dev think about this?

-- 
Thomas Roessler                        http://log.does-not-exist.org/
}}}

--------------------------------------------------------------------------------
2001-10-26 14:47:58 UTC Thomas Roessler <roessler@does-not-exist.org>
* Added comment:
{{{
ok, this looks like you were able to reproduce Joey Hess' crash. 
Great.

Can you provide any more details on what actually happened during 
this isync session?  I.e., did isync receive new messages, did it 
change flags, or did it just download messages?

(I'm asking because your bug report clearly indicates that no race 
condition is involved [which is new information], but that mutt gets 
confused upon a certain kind of change isync does to a maildir 
folder.)

Thanks a lot!
-- 
Thomas Roessler                        http://log.does-not-exist.org/
}}}

--------------------------------------------------------------------------------
2001-10-26 15:05:37 UTC Thomas Roessler <roessler@does-not-exist.org>
* Added comment:
{{{
On 2001-10-25 11:41:22 -0400, Matìj Cepl wrote:

>so at last, I believe this should be helpfull. I guess %subj% 
>tells exactly what I was doing. This time no NNTP has been 
>involved.

Bad enough, you're still running an NNTP-enabled version of mutt, 
which includes patches to source files relevant for debugging this 
problem.  In particular, this means that the line numbers in the 
(otherwise quite nice) core dump output are useless to me.


-- 
Thomas Roessler                        http://log.does-not-exist.org/
}}}

--------------------------------------------------------------------------------
2015-12-07 21:35:39 UTC kevin8t8
* Added comment:
As noted in duplicate #3598, the caret check was added in changeset:52ee1788cddf to deal with bogus values in YESEXPR.  See https://groups.google.com/forum/#!topic/mailing.unix.mutt-dev/A9-nA3a1tzU and https://groups.google.com/forum/#!topic/mailing.unix.mutt-dev/NekvNcxoqQg

Short of more information about what was in the YESEXPR, I don't think it's worth keeping this bug open.   If more information is available and there is an alternative to looking for a leading caret, please feel free to reopen this or #3598.

* resolution changed to wontfix
* status changed to closed
