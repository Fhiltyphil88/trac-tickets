Ticket:  3621
Status:  closed
Summary: Unable to install to dir without using sudo, on OSX >= 10.4

Reporter: balderdash
Owner:    mutt-dev

Opened:       2013-01-03 23:01:32 UTC
Last Updated: 2013-01-04 00:21:57 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
I have my ownership for /usr/local set to not require sudo-ing.  My normal username is the owner, and the group is 'staff'.  In other words, the default perms for a new dir one makes in OSX 10.5 and up. When I build mutt and do 'make install' (where the prefix is /usr/local), I get this nonsense:

if test -f /usr/local/bin/mutt_dotlock && test xmail != x ; then \
		chgrp mail /usr/local/bin/mutt_dotlock && \
		chmod 2755 /usr/local/bin/mutt_dotlock || \
		{ echo "Can't fix mutt_dotlock's permissions!" >&2 ; exit 1 ; } \
	fi
chgrp: you are not a member of group mail
Can't fix mutt_dotlock's permissions!
make[4]: *** [install-exec-hook] Error 1
make[3]: *** [install-exec-am] Error 2
make[2]: *** [install-am] Error 2
make[1]: *** [install-recursive] Error 1
make: *** [install] Error 2

This is true for any prefix dir you might pick, so long as you don't use sudo.  It happens on OSX 10.4 and higher, same error every time.

Using Homebrew pretty much requires that /usr/local be sudo-free.  But even that aside, I think mutt should allow installing to an arbitrary prefix (especially, e.g., one in one's own $HOME) without using sudo.

I don't know much about groups re this issue, so I can't offer a patch.

--------------------------------------------------------------------------------
2013-01-03 23:04:19 UTC m-a
* Added comment:
{{{
FWIW, this is not specific to MacOS, I see the same on FreeBSD 9.1 and
Ubuntu Linux 12.04.
}}}

--------------------------------------------------------------------------------
2013-01-04 00:21:57 UTC me
* Added comment:
The chgrp and chmod are not due to the permissions on /usr/local, but rather the configure script has detected that the default mail spool for your system requires the dotlock binary to have elevated permissions.

You can either change the permissions on your default mail spool to world writable, or use the --with-homespool option.

* resolution changed to invalid
* status changed to closed
