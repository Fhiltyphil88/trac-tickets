Ticket:  1105
Status:  closed
Summary: if you delete an attachment in an IMAP folder it kills the whole message

Reporter: Edmund Grimley-Evans <egrimley@arm.com>
Owner:    mutt-dev

Opened:       2002-03-15 01:46:43 UTC
Last Updated: 2005-10-04 05:41:50 UTC

Priority:  minor
Component: mutt
Keywords:  IMAP

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.2i
Severity: important

-- Please type your report below this line

Subject says it all.


-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/lib/gcc-lib/i386-redhat-linux/2.95.1/specs
gcc version 2.95.1 19990816/Linux (release)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.2i (2000-05-09)
Copyright (C) 1996-2000 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.2.14-5.0 [using slang 10202]
Compile options:
-DOMAIN
+DEBUG
-HOMESPOOL  -USE_SETGID  -USE_DOTLOCK  +USE_FCNTL  -USE_FLOCK
+USE_IMAP  +USE_GSS  -USE_SSL  +USE_POP  +HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  +ENABLE_NLS
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/spool/mail"
SHAREDIR="/etc"
SYSCONFDIR="/etc"
-ISPELL
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the muttbug utility.



>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2002-03-15 03:12:59 UTC Edmund GRIMLEY EVANS <egrimley@arm.com>
* Added comment:
{{{
No, it's not that serious. The message disappears from the folder
list, but it's not deleted on the server, so it reappears when you
reopen the folder. It's still a bug, but not so serious as I thought.

Edmund
}}}

--------------------------------------------------------------------------------
2002-03-25 06:15:58 UTC Brendan Cully <brendan@kublai.com>
* Added comment:
{{{
On Thursday, 14 March 2002 at 11:12, Edmund GRIMLEY EVANS wrote:
> No, it's not that serious. The message disappears from the folder
> list, but it's not deleted on the server, so it reappears when you
> reopen the folder. It's still a bug, but not so serious as I thought.

What server are you using? attachment deletion works by uploading a new
message and removing the old - AFAIK there's no way to edit the message
IMAP. So the normal experience is that you see get a spurious 'new
message' alert at the bottom...

Maybe you could send me a transcript.
}}}

--------------------------------------------------------------------------------
2002-03-26 02:09:31 UTC Edmund GRIMLEY EVANS <edmundo@rano.org>
* Added comment:
{{{
Brendan Cully <brendan@kublai.com>:

> > No, it's not that serious. The message disappears from the folder
> > list, but it's not deleted on the server, so it reappears when you
> > reopen the folder. It's still a bug, but not so serious as I thought.
> 
> What server are you using? attachment deletion works by uploading a new
> message and removing the old - AFAIK there's no way to edit the message
> IMAP. So the normal experience is that you see get a spurious 'new
> message' alert at the bottom...
> 
> Maybe you could send me a transcript.

"Domino IMAP4 Server Release 5.0.7".

There's no "new message" alert. I don't get a "new message" alert when
I Fcc a message to myself, either. Uploaded messages don't appear
until real new mail arrives or I reopen the folder. Do other servers
notify the client when a message has been uploaded? Does the IMAP
specification say anything about that?

Edmund
}}}

--------------------------------------------------------------------------------
2005-09-06 22:56:07 UTC brendan
* Added comment:
{{{
I wonder if this bug is still reproducible with current mutt/domino...
}}}

* status changed to assigned

--------------------------------------------------------------------------------
2005-10-04 23:41:50 UTC brendan
* Added comment:
{{{
Ancient bug; no feedback.
}}}

* resolution changed to fixed
* status changed to closed
