Ticket:  3927
Status:  closed
Summary: Mutt crashes when replying and To: is empty

Reporter: subssn594
Owner:    mutt-dev

Opened:       2017-03-27 17:20:40 UTC
Last Updated: 2017-03-29 14:13:35 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
When the To: is (nearly) empty: "To: ;", as may be the case when the original message is sent to a Bcc: list, Mutt will crash when attempting a group reply.

Here's an example of a header for which a group reply fails:

From: "Mills-Curran, Bill" <bill@mills-curran.net>
To: Wood Badge payments:
Subject: Wood Badge payment reminder
Bcc: fake@icloud.com

If I first modify the To: header to contain an email address, the group reply works.  However, the group reply does not include the Bcc: list.


--------------------------------------------------------------------------------
2017-03-27 22:31:19 UTC kevin8t8
* Added comment:
This may have been related to https://dev.mutt.org/trac/ticket/3787 and been fixed by changeset:a6919571eb59 and changeset:1c151d8800de

I can't reproduce the crash in 1.8.  Would you mind testing on a more recent version?


--------------------------------------------------------------------------------
2017-03-28 20:16:44 UTC subssn594
* Added comment:
I'm running Ubuntu 14.04.5 LTS.  I'll try building mutt 1.8 & report the results.

--------------------------------------------------------------------------------
2017-03-28 20:36:42 UTC subssn594
* Added comment:
I've confirmed that the crash does NOT occur with version 1.8.0.

--------------------------------------------------------------------------------
2017-03-29 14:13:35 UTC kevin8t8
* Added comment:
Great news.  Thank you for checking 1.8.0.

* resolution changed to fixed
* status changed to closed
