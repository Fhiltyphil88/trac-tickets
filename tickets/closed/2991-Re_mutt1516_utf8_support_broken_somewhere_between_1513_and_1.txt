Ticket:  2991
Status:  closed
Summary: Re: mutt-1.5.16: utf-8 support broken somewhere between 1.5.13 and 1.5.16

Reporter: Sven Utcke <utcke@informatik.uni-hamburg.de>
Owner:    mutt-dev

Opened:       2007-11-20 20:50:58 UTC
Last Updated: 2007-11-21 07:59:30 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:


{{{
> Up to (and including) 1.5.13 mutt works for me out of the box,
> including utf-8 support.  Starting from (at least) 1.5.16 (haven't
> tried 1.5.14 and 1.5.15) utf-8 support is broken (other encodings
> still work).  This is the case both on the system described below as
> well as an ancient SuSE 7.3 with libiconv/1.7.0.1.

Ok, last working version is 1.5.14.

Sven
}}}

--------------------------------------------------------------------------------
2007-11-21 07:59:30 UTC pdmef
* Added comment:
Duplicate of #2990.

* resolution changed to duplicate
* status changed to closed
