Ticket:  3144
Status:  closed
Summary: Parent and child match pattern modifier

Reporter: jlh
Owner:    mutt-dev

Opened:       2009-01-02 11:37:42 UTC
Last Updated: 2017-07-06 02:14:11 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
This patch implements the < and > pattern modifiers, to match against the following pattern against the parent message and the child messages respectively, instead of the current message itself.

For example, all messages that have at least one duplicate that has been read:
    >(~= !~N)

All replies to one of your mails (whose parent message has been issued by you):
    <~P

--------------------------------------------------------------------------------
2009-01-02 11:38:12 UTC jlh
* Added attachment mutt-parentchildmatch.diff

--------------------------------------------------------------------------------
2009-01-02 11:58:51 UTC jlh
* Added comment:
This feature has been submitted by Jeremie LE HEN on mutt-dev@ on Dec, 05th 2008.  It received a positive feedback but couldn't be committed immediately because of limbo.

See http://marc.info/?t=122848871300007&r=1&w=2.

--------------------------------------------------------------------------------
2009-01-02 17:41:51 UTC kirr
* cc changed to jeremie@le-hen.org, kirr@landau.phys.spbu.ru

--------------------------------------------------------------------------------
2009-01-02 20:05:04 UTC vinc17
* cc changed to jeremie@le-hen.org, kirr@landau.phys.spbu.ru, vincent@vinc17.org

--------------------------------------------------------------------------------
2009-01-06 04:08:44 UTC vinc17
* Added comment:
It seems that <note> and </note> have recently been added in Section patterns-modifier of the manual (manual.xml.head). The patch should be updated to take that into account.

--------------------------------------------------------------------------------
2009-02-19 20:43:32 UTC jlh
* Added comment:
Hi Vincent,

I've just tried to apply the patch for today snapshot (mutt-20090219.tar.gz), and it applies cleanly.  Can you give me more details about the problem you're experiencing please?

Thanks.
Regards,
-- Jeremie

--------------------------------------------------------------------------------
2009-02-20 04:36:48 UTC vinc17
* Added comment:
It applies with fuzz. I don't remember whether there are other problems.

--------------------------------------------------------------------------------
2009-03-20 00:38:39 UTC vinc17
* Added attachment patch-1.5.19hg.jlh-vl.parentchildmatch.1
* Added comment:
New patch due to the changes in the manual (changeset 5716)

--------------------------------------------------------------------------------
2009-03-20 00:41:08 UTC vinc17
* Added comment:
This time, the patch no longer applied. I've updated it to take into account the changes in the manual. I've also corrected childs -> children.

--------------------------------------------------------------------------------
2017-05-18 20:13:39 UTC jlh
* Added attachment mutt18-parentchildmatch.diff

--------------------------------------------------------------------------------
2017-05-18 20:15:45 UTC jlh
* Added comment:
I've updated the patch for Mutt-1.8.  Can we get this submitted finally?  It's been 8 years :-).

--------------------------------------------------------------------------------
2017-05-19 01:07:41 UTC vinc17
* Added attachment patch-r6906.jlh-vl.parentchildmatch.1
* Added comment:
patch for r6906 (still valid for r7046)

--------------------------------------------------------------------------------
2017-05-19 01:14:56 UTC vinc17
* Added comment:
Replying to [comment:8 jlh]:
> I've updated the patch for Mutt-1.8.  Can we get this submitted finally?  It's been 8 years :-).

It seems that your patch is wrong: I don't see why the {{{cache}}} argument should disappear. I've attached the patch I'm currently using (the manual part is a bit different).

--------------------------------------------------------------------------------
2017-05-19 18:12:29 UTC kevin8t8
* Added comment:
Thanks for the ping.  I will take a look at this for 1.9.

* Updated description:
This patch implements the < and > pattern modifiers, to match against the following pattern against the parent message and the child messages respectively, instead of the current message itself.

For example, all messages that have at least one duplicate that has been read:
    >(~= !~N)

All replies to one of your mails (whose parent message has been issued by you):
    <~P

--------------------------------------------------------------------------------
2017-06-22 03:44:02 UTC kevin8t8
* Added comment:
Sorry but I am not willing to apply this in its current form.

The behavior of "not" (!) is unclear and even contradictory in the patch.  Should it apply to the pattern or to the parent/child operator?   Should there be a difference between

{{{
  !>~f foo.com
  !>(~f foo.com)
  !(>~f foo.com)
  >!~f foo.com
  >(!~f foo.com)
}}}

Right now it looks like it's always applied to the pattern.

Related is the behavior when a message doesn't have a parent or child.  It looks like the code is trying to use the "pattern not" to make this decision, but I'm not convinced that's always appropriate.

In the childsmatch logic in mutt_pattern_exec():

{{{
+    for (; t; t = t->next) {
+      if (!t->message)
+       continue;
+      if (pattern_exec (pat, flags, ctx, t->message, cache))
+       return !pat->not;
+    }
+    return pat->not;
}}}

The not is already evaluated in pattern_exec() but then it's returning !pat->not. If the pattern were ">!~f bar", this will return false if one of the children is not from bar (i.e. matches the pattern).  After the loop it's returning pat->not.  With the same pattern ">!~f bar", it will return true if _all_ of the children messages were from bar (i.e. none of the children match the pattern).

The problem is the conflation of trying to apply "not" to the operator logic and the pattern at the same time.

Other notes:

* Does it make sense for a parent/child operator to be applied to ~() operator?  Right now the code is, but this seems a bad idea.

* At an implementation level, I'm not happy with how the patch is structured.  It's not aligned with the rest of the pattern code.  Creating a new outer mutt_pattern_exec() that just deals with this pseudo-operator is ugly.

* Vincent: an important correction to your version of the patch.  Note that the pattern_cache_t applies to a single header: it doesn't deal with threads.  So inside the mutt_pattern_exec(), the first two calls to pattern_exec() should pass NULL for the cache because they are looking at parent/children.  Only the third call should actually pass the cache through.

--------------------------------------------------------------------------------
2017-06-26 01:25:53 UTC vinc17
* Added comment:
Replying to [comment:11 kevin8t8]:
> The behavior of "not" (!) is unclear and even contradictory in the patch.  Should it apply to the pattern or to the parent/child operator?   Should there be a difference between
> 
> {{{
>   !>~f foo.com
>   !>(~f foo.com)
>   !(>~f foo.com)
>   >!~f foo.com
>   >(!~f foo.com)
> }}}

IMHO, the first three should be equivalent (just as they are equivalent without {{{>}}}): this should be all the messages for which {{{>~f foo.com}}} is false. And indeed, the current behavior is incorrect. The last two should be equivalent: all the messages that have at least a child for which {{{~f foo.com}}} is false.

And currently, {{{>(!~P)}}} and {{{>(!~P !~P)}}} return different results!

> Other notes:
> 
> * Does it make sense for a parent/child operator to be applied to ~() operator?  Right now the code is, but this seems a bad idea.

It may be strange to use this, but I don't see any reason to forbid this unless this makes the code too complex. {{{>~(PATTERN)}}} would mean: all the messages that have at least a child for which {{{~(PATTERN)}}} is true. So, if I understand correctly, compared to {{{~(PATTERN)}}}, this would eliminate leaf messages (messages with no children).

> * Vincent: an important correction to your version of the patch.  Note that the pattern_cache_t applies to a single header: it doesn't deal with threads.  So inside the mutt_pattern_exec(), the first two calls to pattern_exec() should pass NULL for the cache because they are looking at parent/children.  Only the third call should actually pass the cache through.

Thanks!

--------------------------------------------------------------------------------
2017-06-26 01:28:05 UTC vinc17
* Added attachment patch-r6906.jlh-vl.parentchildmatch.2
* Added comment:
Corrected patch as suggested by Kevin (but still with issues with the "not" operator)

--------------------------------------------------------------------------------
2017-06-27 02:50:40 UTC kevin8t8
* Added comment:
I'm attaching a (very quickly thrown together) patch to demonstrate a proposal.  It has no documentation and only brief testing.  I'll flush it out if this sounds reasonable.

This version of the patch changes the syntax to:
{{{
  ~<(PATTERN)
  ~>(PATTERN)
}}}
for parent and children pattern matching, respectively.

So the "<" and ">" are no longer pattern modifiers but part of an actual pattern operator, and the parenthesis are required.

It uses the same parsing as "~()", which means it has a proper pattern->child to hold the contained pattern.  This means a "!" in front will apply to the operator, while one inside the parenthesis will apply to the pattern.

It also has a pattern->op, so we can just add it inside the existing mutt_pattern_exec() like the other operators.

--------------------------------------------------------------------------------
2017-06-27 02:51:28 UTC kevin8t8
* Added attachment kevin-proposal-patch-v1.patch
* Added comment:
NOT ready for committing.  needs doc, cleanup, and testing.

--------------------------------------------------------------------------------
2017-06-27 11:33:36 UTC vinc17
* Added comment:
Replying to [comment:13 kevin8t8]:
> I'm attaching a (very quickly thrown together) patch to demonstrate a proposal.

I'll try to find some time to test it.

> So the "<" and ">" are no longer pattern modifiers but part of an actual pattern operator, and the parenthesis are required.

This makes more sense. However, this means that there's more to type. But IMHO, this is a more general issue (e.g. when one wants full-word matching with some other pattern operator), which could be solved in some other way in the future (e.g. as an extension of simple searches).

--------------------------------------------------------------------------------
2017-07-01 23:15:08 UTC kevin8t8
* Added comment:
I've tested it more, and it seems to work properly.

One thing I was concerned about was whether thread->child->prev could be non-null.  The original patch checked for that case, and the match_threadcomplete() function marks to go prev and next when traversing down.

However, I've looked through the thread.c code, and it does not seem to be the case that thread->child ever points to the middle of the children.  You can see this in linearize_tree(), mutt_sort_subthreads(), mutt_sort_threads() and the way in which insert_message() is called, and also in _mutt_traverse_thread().

If it turns out I've missed something, then I'll change the new functions, but I'm not going to add voodoo code just in case.

I'm uploading a v2 patch with documentation.  Unless there are any objections, I'll push it up in a few days.

--------------------------------------------------------------------------------
2017-07-01 23:15:33 UTC kevin8t8
* Added attachment kevin-proposal-patch-v2.patch

--------------------------------------------------------------------------------
2017-07-06 02:14:11 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [changeset:"428e36fb2262e552035800f3544867e627c615f5" 7100:428e36fb2262]:
{{{
#!CommitTicketReference repository="" revision="428e36fb2262e552035800f3544867e627c615f5"
Add ~<() and ~>() immediate parent/children patterns. (closes #3144)

Thanks to Jeremie Le Hen for the original patch, and for his
persistence in getting this feature merged.
}}}

* resolution changed to fixed
* status changed to closed
