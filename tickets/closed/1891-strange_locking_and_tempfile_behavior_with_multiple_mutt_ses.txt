Ticket:  1891
Status:  closed
Summary: strange locking and tempfile behavior with multiple mutt sessions open

Reporter: j-zbiciak1@ti.com
Owner:    mutt-dev

Opened:       2004-05-25 04:24:47 UTC
Last Updated: 2005-09-22 13:26:09 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.4.2.1i
Severity: normal

-- Please type your report below this line


When I run a single instance of mutt, it seems to behave normally.
When I start a second instance, say "mutt -f=folder" in order to
go digging for saved email separately of my currently-open inbox,
I get weird behavior.  Specifically, I see the following symptoms
show up when I try to view, forward or compose messages when there
are multiple mutt sessions open:

 1. Error message:  "Cannot create temporary file!" with no further
    clues.  (Rather frustrating error message.)

 2. Error message:  "No such file or directory
    ~/.mutt/tmp/mutt-durable03-12345-67 (errn"  (the rest gets
    truncated).  The '12345' is replaced with the PID, and the
    '67' increments.  Strange thing is, the file gets created (by
    mutt) and is 0 bytes long.

 3. Error/warning message:  "Lock count exceeded on
    Mail/foldername. Unlock?"

 4. Zero-length emails.  Sometimes when forwarding or replying to a
    message, mutt generates a zero-length email.

These symptoms only arise when I have two mutt sessions open.
Enabling/disabling flock() doesn't seem to change things.  Neither
does enabling the NFS-fix.  (My home area is mounted via NFS.)

I saw the same behavior w/ 1.4.2.  I just tried building 1.4.2.1
today to see if the behavior changed and it does not.  I haven't
tried disabling fcntl() based on the injunction given in the
Mutt FAQ.

Incidentally, the tmpfile problems don't seem to go away if I move
my Mutt temporary dir to /tmp/subdir.  The /tmp filesystem is not
NFS mounted (of course).


-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /db/c6xi/linux/lib/gcc/i686-pc-linux-gnu/3.4.0/specs
Configured with: ../gcc-3.4.0/configure --prefix=/db/c6xi/linux --with-tune=pentium4 --with-arch=pentium4 --with-lang=c,c++,java
Thread model: posix
gcc version 3.4.0

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.4.2.1i (2004-02-12)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.18-27.7.x (i686) [using ncurses 5.2]
Compile options:
-DOMAIN
-DEBUG
+HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_GSS  +USE_SSL  -USE_SASL  
-HAVE_REGCOMP  +USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  +SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/home/jzbiciak/Mail/spool/incoming"
PKGDATADIR="/home/jzbiciak/pub/linux/share/mutt"
SYSCONFDIR="/home/jzbiciak/pub/linux/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.


--- Begin /home/jzbiciak/.mutt/muttrc
ignore "from " received content- mime-version status x-status message-id
ignore sender references return-path lines
macro index \eb '/~b ' 'search in message bodies'
macro index \cb |urlview\n 'call urlview to extract URLs out of a message'
macro pager \cb |urlview\n 'call urlview to extract URLs out of a message'
macro generic <f1> "!less /home/jzbiciak/pub/linux/doc/mutt/manual.txt\n" "Show Mutt documentation"
macro index   <f1> "!less /home/jzbiciak/pub/linux/doc/mutt/manual.txt\n" "Show Mutt documentation"
macro pager   <f1> "!less /home/jzbiciak/pub/linux/doc/mutt/manual.txt\n" "Show Mutt documentation"
set abort_nosubject=ask-yes
set abort_unmodified=ask-yes
set alias_file="~/.mutt/aliases"
source ~/.mutt/aliases-fixed
source ~/.mutt/aliases
set alias_format="%4n %2f %t %-10a   %r"
set allow_8bit=yes
set allow_ansi=no
set arrow_cursor=no
set ascii_chars=no
set askbcc=no
set askcc=no
set attach_split=yes
set attribution="On %d, %n [%a] wrote:"
set autoedit=yes
set beep=no
set beep_new=no
set bounce_delivered=no 
set check_new=yes
set collapse_unread=yes
set uncollapse_jump=no
set confirmappend=no
set confirmcreate=yes
set copy=yes
set delete=ask-yes
set delete_untag=yes
set digest_collapse=no 
set duplicate_threads=yes
set edit_headers=no
set editor="/usr/bin/vim"
set fast_reply=yes
set fcc_attach=yes
set fcc_clear=no
set folder="~/Mail"
set folder_format="%N %F %2l %-8.8u %-8.8g %8s %d %f"
set followup_to=yes
set force_name=yes
set forward_decode=yes
set forward_format="Fwd: %s"
set forward_quote=yes
set hdrs=yes
set header=no
set help=yes
set hidden_host=yes
set history=50
set ignore_list_reply_to=yes
set include=yes
set indent_string="| "
set index_format="%4C%2M %Z %[%b %d] %-16.16F (%6l) %-40.40s"
set mail_check=5
set mark_old=yes
set markers=no 
set mask=".*"
set mbox="=received"
set mbox_type=mbox
set metoo=yes
set menu_scroll=no
set mime_forward=ask-no
set mime_forward_decode=yes
set mime_forward_rest=yes
set move=no
set pager="/usr/local/bin/less -f"
set pager_context=1
set pager_format="[%C/%m] %-18.18n %-30.30s %> %l lines (%c)"
set pager_index_lines=12
set pager_stop=yes
set pgp_replyencrypt=yes
set pgp_replysign=yes
set pgp_replysignencrypted=yes
set pgp_sign_as="0xAFC4FD2B"
set pipe_split=no
set pipe_decode=yes
set postpone=ask-yes
set postponed="~/Mail/postponed"
set print=ask-no
set print_command="lp -dld_4000"
set prompt_after=no 
set quit=ask-yes
set quote_regexp="^([ \t]*[|>:}#])+"
set read_inc=16
set read_only=no
set recall=ask-yes
set record="=out"
set reply_regexp="^(re([\\[0-9\\]+])*|aw):[ \t]*"
set reply_self=no
set reply_to=ask-yes
set resolve=no
set reverse_alias=yes
set reverse_name=yes
set save_address=no
set save_empty=yes
set save_name=yes
set sendmail_wait=-1
set shell="bash"
set sig_dashes=yes
set signature="~/.sig"
set simple_search="~f %s | ~s %s"
set smart_wrap=yes
set smileys="(>From )|(:[-^]?[][)(><}{|/DP])"
set sort=reverse-threads
set sort_alias=unsorted
set sort_aux=last-date-received
set sort_browser=alpha
set sort_re=yes
set spoolfile="~/Mail/spool/incoming"
set status_chars=" *%A"
set status_format="%v@%h:%f%r(%s) [%M/%m(%P of %l)]%> [b=%b|n=%n|u=%u|t=%t|p=%p]"
set status_on_top=yes
set strict_threads=yes
set suspend=yes
set thorough_search=yes
set tilde=yes
set timeout=10
set tmpdir="~/.mutt/tmp"
set to_chars=" +TCFL"
set use_8bitmime=no
set use_domain=yes
set use_from=yes
set visual="/usr/bin/vim"
set wait_key=no 
set wrap_search=yes
set write_inc=16
set write_bcc=yes
save-hook ~l +%B # if message was sent to a known list, default save box is
unhdr_order * # clear system defaults
hdr_order Date: From: Old-Return-Path: Reply-To: Mail-Followup-To: To: Cc: Bcc: Delivered-To: Subject: In-Reply-To: X-Mailing-List: X-Operating-System: X-Mailer: User-Agent:
my_hdr From: Joe Zbiciak <j-zbiciak1@ti.com>
my_hdr Reply-To: Joe Zbiciak <j-zbiciak1@ti.com>
my_hdr Return-Path: Joe Zbiciak <j-zbiciak1@ti.com>
ignore X-Resent resent- x-loop X-Orcpt X-MimeOLE X-MSMail
source ~/.mutt/colors
source ~/.mutt/bindings
source ~/.mutt/macros
source ~/.mutt/my_mailboxes
source ~/.mutt/gpg.rc
auto_view text/html
set implicit_autoview
--- End /home/jzbiciak/.mutt/muttrc


--- Begin /home/jzbiciak/pub/linux/etc/Muttrc
ignore "from " received content- mime-version status x-status message-id
ignore sender references return-path lines
macro index \eb '/~b ' 'search in message bodies'
macro index \cb |urlview\n 'call urlview to extract URLs out of a message'
macro pager \cb |urlview\n 'call urlview to extract URLs out of a message'
macro generic <f1> "!less /home/jzbiciak/pub/linux/doc/mutt/manual.txt\n" "Show Mutt documentation"
macro index   <f1> "!less /home/jzbiciak/pub/linux/doc/mutt/manual.txt\n" "Show Mutt documentation"
macro pager   <f1> "!less /home/jzbiciak/pub/linux/doc/mutt/manual.txt\n" "Show Mutt documentation"
--- End /home/jzbiciak/pub/linux/etc/Muttrc



>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2004-05-25 04:24:47 UTC j-zbiciak1@ti.com
* Added comment:
{{{
Package: mutt
Version: 1.4.2.1i
Severity: normal

-- Please type your report below this line


When I run a single instance of mutt, it seems to behave normally.
When I start a second instance, say "mutt -f=folder" in order to
go digging for saved email separately of my currently-open inbox,
I get weird behavior.  Specifically, I see the following symptoms
show up when I try to view, forward or compose messages when there
are multiple mutt sessions open:

1. Error message:  "Cannot create temporary file!" with no further
   clues.  (Rather frustrating error message.)

2. Error message:  "No such file or directory
   ~/.mutt/tmp/mutt-durable03-12345-67 (errn"  (the rest gets
   truncated).  The '12345' is replaced with the PID, and the
   '67' increments.  Strange thing is, the file gets created (by
   mutt) and is 0 bytes long.

3. Error/warning message:  "Lock count exceeded on
   Mail/foldername. Unlock?"

4. Zero-length emails.  Sometimes when forwarding or replying to a
   message, mutt generates a zero-length email.

These symptoms only arise when I have two mutt sessions open.
Enabling/disabling flock() doesn't seem to change things.  Neither
does enabling the NFS-fix.  (My home area is mounted via NFS.)

I saw the same behavior w/ 1.4.2.  I just tried building 1.4.2.1
today to see if the behavior changed and it does not.  I haven't
tried disabling fcntl() based on the injunction given in the
Mutt FAQ.

Incidentally, the tmpfile problems don't seem to go away if I move
my Mutt temporary dir to /tmp/subdir.  The /tmp filesystem is not
NFS mounted (of course).


-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /db/c6xi/linux/lib/gcc/i686-pc-linux-gnu/3.4.0/specs
Configured with: ../gcc-3.4.0/configure --prefix=/db/c6xi/linux --with-tune=pentium4 --with-arch=pentium4 --with-lang=c,c++,java
Thread model: posix
gcc version 3.4.0

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.4.2.1i (2004-02-12)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.18-27.7.x (i686) [using ncurses 5.2]
Compile options:
-DOMAIN
-DEBUG
+HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_GSS  +USE_SSL  -USE_SASL  
-HAVE_REGCOMP  +USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  +SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/home/jzbiciak/Mail/spool/incoming"
PKGDATADIR="/home/jzbiciak/pub/linux/share/mutt"
SYSCONFDIR="/home/jzbiciak/pub/linux/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.


--- Begin /home/jzbiciak/.mutt/muttrc
ignore "from " received content- mime-version status x-status message-id
ignore sender references return-path lines
macro index \eb '/~b ' 'search in message bodies'
macro index \cb |urlview\n 'call urlview to extract URLs out of a message'
macro pager \cb |urlview\n 'call urlview to extract URLs out of a message'
macro generic <f1> "!less /home/jzbiciak/pub/linux/doc/mutt/manual.txt\n" "Show Mutt documentation"
macro index   <f1> "!less /home/jzbiciak/pub/linux/doc/mutt/manual.txt\n" "Show Mutt documentation"
macro pager   <f1> "!less /home/jzbiciak/pub/linux/doc/mutt/manual.txt\n" "Show Mutt documentation"
set abort_nosubject=ask-yes
set abort_unmodified=ask-yes
set alias_file="~/.mutt/aliases"
source ~/.mutt/aliases-fixed
source ~/.mutt/aliases
set alias_format="%4n %2f %t %-10a   %r"
set allow_8bit=yes
set allow_ansi=no
set arrow_cursor=no
set ascii_chars=no
set askbcc=no
set askcc=no
set attach_split=yes
set attribution="On %d, %n [%a] wrote:"
set autoedit=yes
set beep=no
set beep_new=no
set bounce_delivered=no 
set check_new=yes
set collapse_unread=yes
set uncollapse_jump=no
set confirmappend=no
set confirmcreate=yes
set copy=yes
set delete=ask-yes
set delete_untag=yes
set digest_collapse=no 
set duplicate_threads=yes
set edit_headers=no
set editor="/usr/bin/vim"
set fast_reply=yes
set fcc_attach=yes
set fcc_clear=no
set folder="~/Mail"
set folder_format="%N %F %2l %-8.8u %-8.8g %8s %d %f"
set followup_to=yes
set force_name=yes
set forward_decode=yes
set forward_format="Fwd: %s"
set forward_quote=yes
set hdrs=yes
set header=no
set help=yes
set hidden_host=yes
set history=50
set ignore_list_reply_to=yes
set include=yes
set indent_string="| "
set index_format="%4C%2M %Z %[%b %d] %-16.16F (%6l) %-40.40s"
set mail_check=5
set mark_old=yes
set markers=no 
set mask=".*"
set mbox="=received"
set mbox_type=mbox
set metoo=yes
set menu_scroll=no
set mime_forward=ask-no
set mime_forward_decode=yes
set mime_forward_rest=yes
set move=no
set pager="/usr/local/bin/less -f"
set pager_context=1
set pager_format="[%C/%m] %-18.18n %-30.30s %> %l lines (%c)"
set pager_index_lines=12
set pager_stop=yes
set pgp_replyencrypt=yes
set pgp_replysign=yes
set pgp_replysignencrypted=yes
set pgp_sign_as="0xAFC4FD2B"
set pipe_split=no
set pipe_decode=yes
set postpone=ask-yes
set postponed="~/Mail/postponed"
set print=ask-no
set print_command="lp -dld_4000"
set prompt_after=no 
set quit=ask-yes
set quote_regexp="^([ \t]*[|>:}#])+"
set read_inc=16
set read_only=no
set recall=ask-yes
set record="=out"
set reply_regexp="^(re([\\[0-9\\]+])*|aw):[ \t]*"
set reply_self=no
set reply_to=ask-yes
set resolve=no
set reverse_alias=yes
set reverse_name=yes
set save_address=no
set save_empty=yes
set save_name=yes
set sendmail_wait=-1
set shell="bash"
set sig_dashes=yes
set signature="~/.sig"
set simple_search="~f %s | ~s %s"
set smart_wrap=yes
set smileys="(>From )|(:[-^]?[][)(><}{|/DP])"
set sort=reverse-threads
set sort_alias=unsorted
set sort_aux=last-date-received
set sort_browser=alpha
set sort_re=yes
set spoolfile="~/Mail/spool/incoming"
set status_chars=" *%A"
set status_format="%v@%h:%f%r(%s) [%M/%m(%P of %l)]%> [b=%b|n=%n|u=%u|t=%t|p=%p]"
set status_on_top=yes
set strict_threads=yes
set suspend=yes
set thorough_search=yes
set tilde=yes
set timeout=10
set tmpdir="~/.mutt/tmp"
set to_chars=" +TCFL"
set use_8bitmime=no
set use_domain=yes
set use_from=yes
set visual="/usr/bin/vim"
set wait_key=no 
set wrap_search=yes
set write_inc=16
set write_bcc=yes
save-hook ~l +%B # if message was sent to a known list, default save box is
unhdr_order * # clear system defaults
hdr_order Date: From: Old-Return-Path: Reply-To: Mail-Followup-To: To: Cc: Bcc: Delivered-To: Subject: In-Reply-To: X-Mailing-List: X-Operating-System: X-Mailer: User-Agent:
my_hdr From: Joe Zbiciak <j-zbiciak1@ti.com>
my_hdr Reply-To: Joe Zbiciak <j-zbiciak1@ti.com>
my_hdr Return-Path: Joe Zbiciak <j-zbiciak1@ti.com>
ignore X-Resent resent- x-loop X-Orcpt X-MimeOLE X-MSMail
source ~/.mutt/colors
source ~/.mutt/bindings
source ~/.mutt/macros
source ~/.mutt/my_mailboxes
source ~/.mutt/gpg.rc
auto_view text/html
set implicit_autoview
--- End /home/jzbiciak/.mutt/muttrc


--- Begin /home/jzbiciak/pub/linux/etc/Muttrc
ignore "from " received content- mime-version status x-status message-id
ignore sender references return-path lines
macro index \eb '/~b ' 'search in message bodies'
macro index \cb |urlview\n 'call urlview to extract URLs out of a message'
macro pager \cb |urlview\n 'call urlview to extract URLs out of a message'
macro generic <f1> "!less /home/jzbiciak/pub/linux/doc/mutt/manual.txt\n" "Show Mutt documentation"
macro index   <f1> "!less /home/jzbiciak/pub/linux/doc/mutt/manual.txt\n" "Show Mutt documentation"
macro pager   <f1> "!less /home/jzbiciak/pub/linux/doc/mutt/manual.txt\n" "Show Mutt documentation"
--- End /home/jzbiciak/pub/linux/etc/Muttrc
}}}

--------------------------------------------------------------------------------
2005-08-27 12:24:58 UTC rado
* Added comment:
{{{
Does this happen with 1.5.9+, too?
Can you retry with the same config + folder on another
OS/machine?
}}}

* status changed to assigned

--------------------------------------------------------------------------------
2005-09-23 07:26:09 UTC rado
* Added comment:
{{{
no response for 4 weeks.
}}}

* resolution changed to fixed
* status changed to closed
