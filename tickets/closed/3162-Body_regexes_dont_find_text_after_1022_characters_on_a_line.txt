Ticket:  3162
Status:  closed
Summary: Body regexes don't find text after 1022 characters on a line

Reporter: Smylers
Owner:    mutt-dev

Opened:       2009-01-26 15:38:12 UTC
Last Updated: 2009-04-26 10:01:12 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
If a message has more than 1022 characters on a line then attempting to search for (using the / command) or highlight (using :color ) text after position 1022 will fail.

Such messages are in violation of the SMTP RFC (which imposes a maximum of 998 characters) but are found in the real world; that's how I stumbled up on this limit.

The attached mbox plus muttrc demonstrates the problem -- it shows how terms being highlighted up to position 1022 but not 1023.

--------------------------------------------------------------------------------
2009-01-26 15:39:06 UTC Smylers
* Added attachment Mutt_line_length_demo.tar.gz
* Added comment:
demo of bug

--------------------------------------------------------------------------------
2009-01-27 15:01:24 UTC pdmef
* milestone changed to 1.6

--------------------------------------------------------------------------------
2009-02-20 14:51:31 UTC pdmef
* Added comment:
This is due to a fixed-size buffer of 1k being used. I hacked up a patch using a dynamically growing buffer but obviously don't understand the color code well enough to make it work. Enlarging that buffer would probably work, though it would then cause trouble at longer lines.

--------------------------------------------------------------------------------
2009-04-26 10:01:12 UTC Rocco Rutte <pdmef@gmx.net>
* Added comment:
(In [addbd9145230]) Use dynamic buffer for displaying pager lines. Closes #3162.

With too small fixed-size buffers we can't color/find certain words
that span buffers. This needs to duplicate mutt_read_line with the
adjustment to leave line termination in and not support breaking
long lines using \ at EOL. Other callers may want to use this one
instead, too as we support \-escaping in too many places.

* resolution changed to fixed
* status changed to closed
