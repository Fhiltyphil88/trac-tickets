Ticket:  1572
Status:  closed
Summary: No se actulizan los mail

Reporter: isra@miscorreos.org
Owner:    mutt-dev

Opened:       2003-06-11 08:36:39 UTC
Last Updated: 2005-07-24 23:10:13 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.5.4i
Severity: important

-- Please type your report below this line



-- System Information
System Version: Linux gnosis 2.4.20-kamaxeon #7 jue abr 24 20:44:35 WEST 2003 ppc GNU/Linux

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
cc
Reading specs from /usr/lib/gcc-lib/powerpc-linux/3.3/specs
Configured with: ../src/configure -v --enable-languages=c,c++,java,f77,pascal,objc,ada --prefix=/usr --mandir=/usr/share/man --infodir=/usr/share/info --with-gxx-include-dir=/usr/include/c++/3.3 --enable-shared --with-system-zlib --enable-nls --without-included-gettext --enable-__cxa_atexit --enable-clocale=gnu --enable-java-gc=boehm --enable-java-awt=xlib --enable-objc-gc --disable-multilib powerpc-linux
Thread model: posix
gcc version 3.3 (Debian)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.5.4i (2003-03-19)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.20-kamaxeon (ppc) [using ncurses 5.3] [using libidn 0.1.14 (compiled with 0.1.11)]
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  +IMAP_EDIT_THREADS  -USE_GSS  -USE_SSL  +USE_GNUTLS  +USE_SASL  -USE_SASL2  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +COMPRESSED  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_LIBIDN  +HAVE_GETSID  +HAVE_GETADDRINFO  +USE_CACHE  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
MIXMASTER="mixmaster"
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.

patch-1.5.1.cd.edit_threads.9.2
patch-1.3.23.1.ametzler.pgp_good_sign
patch-1.5.3.Md.gpg_status_fd
patch-1.4.Md.gpg-agent
patch-1.3.27.bse.xtitles.1
patch-1.5.3.Md.etc_mailname_gethostbyname
Md.muttbug
Md.use_debian_editor
patch-1.4.admcd.gnutlsdlopen.53d
patch-1.5.4.Z.hcache.8
patch-1.4.admcd.gnutlsbuild.53d
patch-1.4.admcd.gnutls.55d
patch-1.5.3.rr.compressed.1

--- Begin /home/kamaxeon/.muttrc
set auto_tag
unset beep
set forward_format="Fwd: %s"
set pager="builtin"

folder-hook . set sort=threads

my_hdr From: Israel Santana <isra@miscorreos.org>

set spoolfile = ~/Maildir/
set mbox=~/Maildir/

set editor="vi"

subscribe "list@pureftpd.org"
subscribe "socios@listas.gulic.org"
subscribe "devel@listas.gulic.org"
subscribe "meta-distros@hispalinux.es"



ignore *
unignore From To Cc Subject Date Reply-To Organization X-Mailer User-Agent

mailboxes "~/Maildir/.pureftpd"
mailboxes "~/Maildir"
mailboxes "~/Maildir/.osl"
mailboxes "~/Maildir/.cvs-osl"
mailboxes "~/Maildir/.barrapunto"



--- End /home/kamaxeon/.muttrc


--- Begin /etc/Muttrc
ignore "from " received content- mime-version status x-status message-id
ignore sender references return-path lines
ignore date delivered-to precedence errors-to in-reply-to user-agent
ignore x-loop x-sender x-mailer x-msmail-priority x-mimeole x-priority
ignore x-accept-language x-authentication-warning thread- priority
bind editor    "\e<delete>"    kill-word
bind editor    "\e<backspace>" kill-word
bind editor     <delete>  delete-char
unset use_domain
unset use_from
set sort=threads
unset write_bcc
unset bounce_delivered
macro index \eb '/~b ' 'search in message bodies'
macro index \cb |urlview\n 'call urlview to extract URLs out of a message'
macro pager \cb |urlview\n 'call urlview to extract URLs out of a message'
set pipe_decode
macro generic <f1> "!zcat /usr/share/doc/mutt/manual.txt.gz | sensible-pager\n" "Show Mutt documentation"
macro index   <f1> "!zcat /usr/share/doc/mutt/manual.txt.gz | sensible-pager\n" "Show Mutt documentation"
macro pager   <f1> "!zcat /usr/share/doc/mutt/manual.txt.gz | sensible-pager\n" "Show Mutt documentation"
open-hook	\\.gz$ "gzip -cd %f > %t"
close-hook	\\.gz$ "gzip -c %t > %f"
append-hook	\\.gz$ "gzip -c %t >> %f"
open-hook	\\.bz2$ "bzip2 -cd %f > %t"
close-hook	\\.bz2$ "bzip2 -c %t > %f"
append-hook	\\.bz2$ "bzip2 -c %t >> %f"
color normal	white black
color attachment brightyellow black
color hdrdefault cyan black
color indicator black cyan
color markers	brightred black
color quoted	green black
color signature cyan black
color status	brightgreen blue
color tilde	blue black
color tree	red black
set pgp_decode_command="/usr/bin/gpg  --charset utf-8   --status-fd=2 %?p?--passphrase-fd 0? --no-verbose --quiet  --batch  --output - %f"
set pgp_verify_command="/usr/bin/gpg   --status-fd=2 --no-verbose --quiet  --batch  --output - --verify %s %f"
set pgp_decrypt_command="/usr/bin/gpg   --status-fd=2 --passphrase-fd 0 --no-verbose --quiet  --batch  --output - %f"
set pgp_sign_command="/usr/bin/gpg    --no-verbose --batch --quiet   --output - --passphrase-fd 0 --armor --detach-sign --textmode %?a?-u %a? %f"
set pgp_clearsign_command="/usr/bin/gpg   --charset utf-8 --no-verbose --batch --quiet   --output - --passphrase-fd 0 --armor --textmode --clearsign %?a?-u %a? %f"
set pgp_encrypt_only_command="/usr/lib/mutt/pgpewrap /usr/bin/gpg  --charset utf-8    --batch  --quiet  --no-verbose --output - --encrypt --textmode --armor --always-trust -- -r %r -- %f"
set pgp_encrypt_sign_command="/usr/lib/mutt/pgpewrap /usr/bin/gpg  --charset utf-8 --passphrase-fd 0  --batch --quiet  --no-verbose  --textmode --output - --encrypt --sign %?a?-u %a? --armor --always-trust -- -r %r -- %f"
set pgp_import_command="/usr/bin/gpg  --no-verbose --import -v %f"
set pgp_export_command="/usr/bin/gpg   --no-verbose --export --armor %r"
set pgp_verify_key_command="/usr/bin/gpg   --verbose --batch  --fingerprint --check-sigs %r"
set pgp_list_pubring_command="/usr/bin/gpg   --no-verbose --batch --quiet   --with-colons --list-keys %r" 
set pgp_list_secring_command="/usr/bin/gpg   --no-verbose --batch --quiet   --with-colons --list-secret-keys %r" 
set pgp_good_sign="^\\[GNUPG:\\] GOODSIG"
set smime_ca_location="~/.smime/ca-bundle.crt"
set smime_certificates="~/.smime/certificates"
set smime_keys="~/.smime/keys"
set smime_pk7out_command="openssl smime -verify -in %f -noverify -pk7out"
set smime_get_cert_command="openssl pkcs7 -print_certs -in %f"
set smime_get_signer_cert_command="openssl smime -verify -in %f -noverify -signer %c -out /dev/null"
set smime_get_cert_email_command="openssl x509 -in %f -noout -email"
set smime_import_cert_command="/usr/lib/mutt/smime_keys add_cert %f"
set smime_encrypt_command="openssl smime -encrypt -%a -outform DER -in %f %c"
set smime_sign_command="openssl smime -sign -signer %c -inkey %k -passin stdin -in %f -certfile %i -outform DER"
set smime_decrypt_command="openssl smime -decrypt -passin stdin -inform DER -in %f -inkey %k -recip %c"
set smime_verify_command="openssl smime -verify -inform DER -in %s %C -content %f"
set smime_verify_opaque_command="openssl smime -verify -inform DER -in %s %C"
--- End /etc/Muttrc



>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2003-06-11 08:36:39 UTC isra@miscorreos.org
* Added comment:
{{{
Package: mutt
Version: 1.5.4i
Severity: important

-- Please type your report below this line



-- System Information
System Version: Linux gnosis 2.4.20-kamaxeon #7 jue abr 24 20:44:35 WEST 2003 ppc GNU/Linux

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
cc
Reading specs from /usr/lib/gcc-lib/powerpc-linux/3.3/specs
Configured with: ../src/configure -v --enable-languages=c,c++,java,f77,pascal,objc,ada --prefix=/usr --mandir=/usr/share/man --infodir=/usr/share/info --with-gxx-include-dir=/usr/include/c++/3.3 --enable-shared --with-system-zlib --enable-nls --without-included-gettext --enable-__cxa_atexit --enable-clocale=gnu --enable-java-gc=boehm --enable-java-awt=xlib --enable-objc-gc --disable-multilib powerpc-linux
Thread model: posix
gcc version 3.3 (Debian)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.5.4i (2003-03-19)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.20-kamaxeon (ppc) [using ncurses 5.3] [using libidn 0.1.14 (compiled with 0.1.11)]
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  +IMAP_EDIT_THREADS  -USE_GSS  -USE_SSL  +USE_GNUTLS  +USE_SASL  -USE_SASL2  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +COMPRESSED  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_LIBIDN  +HAVE_GETSID  +HAVE_GETADDRINFO  +USE_CACHE  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
MIXMASTER="mixmaster"
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.

patch-1.5.1.cd.edit_threads.9.2
patch-1.3.23.1.ametzler.pgp_good_sign
patch-1.5.3.Md.gpg_status_fd
patch-1.4.Md.gpg-agent
patch-1.3.27.bse.xtitles.1
patch-1.5.3.Md.etc_mailname_gethostbyname
Md.muttbug
Md.use_debian_editor
patch-1.4.admcd.gnutlsdlopen.53d
patch-1.5.4.Z.hcache.8
patch-1.4.admcd.gnutlsbuild.53d
patch-1.4.admcd.gnutls.55d
patch-1.5.3.rr.compressed.1

--- Begin /home/kamaxeon/.muttrc
set auto_tag
unset beep
set forward_format="Fwd: %s"
set pager="builtin"
}}}

--------------------------------------------------------------------------------
2005-07-25 17:10:13 UTC brendan
* Added comment:
{{{
bad bug report
}}}

* resolution changed to fixed
* status changed to closed
