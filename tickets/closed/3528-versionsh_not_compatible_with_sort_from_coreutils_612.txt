Ticket:  3528
Status:  closed
Summary: version.sh not compatible with sort from coreutils 6.12

Reporter: barsnick
Owner:    dgc

Opened:       2011-06-29 12:47:39 UTC
Last Updated: 2011-06-30 04:18:49 UTC

Priority:  trivial
Component: build
Keywords:  

--------------------------------------------------------------------------------
Description:
When building, version.sh gives me an error message from 'sort', function getdistance_old():
{{{
sort: open failed: +1: No such file or directory
}}}
My 'sort' man page does not explain '+' either.

mutt is latest (as of today) from hg (6187:b477d7c5733e on HEAD/tip).
sort is from Fedora F10:
{{{
barsnick@sunshine:/usr/new/tools/networking/mail/mutt/mutt-hg > sort --version
sort (GNU coreutils) 6.12
}}}


--------------------------------------------------------------------------------
2011-06-29 15:47:40 UTC dgc
* Added comment:
And people ask me why I don't love GNU.

Patch upcoming.

* owner changed to dgc
* status changed to accepted

--------------------------------------------------------------------------------
2011-06-29 16:03:15 UTC m-a
* Added comment:
{{{
You're free to read IEEE Std 1003.1 instead of the system-specific
documentation. It would have told you there's no + option.

Most distributors have some sort of posix manpages, and checking "man 1p
sort" gets you the POSIX (standard) version rather than the system
(non-portable extended) version of the reference material.

:)
}}}

--------------------------------------------------------------------------------
2011-06-29 16:23:08 UTC dgc
* Added comment:
SUSv3 was ratified in 2004.  A plus option existed for decades before that.  I'm absolutely in favor of supporting the -k option.  What I don't like is the removal of harmless backward compatibility.

Most (all?) modern non-GNU systems are SUSv3-compliant ''and'' they support the SUSv3 notation.  Be strict in what you send, but generous in what you receive.

--------------------------------------------------------------------------------
2011-06-29 16:29:46 UTC dgc
* Added comment:
.. someone is going to call me on this and they'll probably be right.  I'm playing fast and loose with specs and dates, and I'm sure I'm wrong.  But my point isn't about dates per se, it's about how long the plus option has existed, doing no harm, before whenever it was eliminated from spec.  What I don't like is that GNU decided to remove a useful compatibility feature and confine themselves to spec.  It's broken production code in my work environment over the years.

All that said, I'm just complaining.  I knew that GNU doesn't support this syntax when I wrote the code (I just didn't think about it), and it's a legitimate bug.  That's why I'm fixing it. :)  But it's hard to break 20 years of shell-script habit.

--------------------------------------------------------------------------------
2011-06-29 20:47:39 UTC m-a
* Added comment:
{{{
Well, the "+" form was already marked "obsolescent" in SUSv2 (1997). :)
}}}

--------------------------------------------------------------------------------
2011-06-30 04:18:49 UTC brendan
* Added comment:
(commit hook didn't fire, closing manually) Fixed in [8b5e41c6a517]

* resolution changed to fixed
* status changed to closed
