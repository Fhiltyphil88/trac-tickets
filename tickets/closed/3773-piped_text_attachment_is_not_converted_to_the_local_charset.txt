Ticket:  3773
Status:  closed
Summary: piped text attachment is not converted to the local charset

Reporter: vinc17
Owner:    kevin8t8

Opened:       2015-09-01 22:30:29 UTC
Last Updated: 2016-10-23 22:28:57 UTC

Priority:  major
Component: charset
Keywords:  

--------------------------------------------------------------------------------
Description:
When I pipe a text attachment, there is no charset conversion, i.e. the charset specified in the MIME header of the attachment is used, which means that the receiver has no reliable way to know which charset was used. It was decided in the past that when there is no information on the charset in a transmission to an external command (e.g. as for mail composing), texts are expected to be transmitted in the local charset.

--------------------------------------------------------------------------------
2016-10-07 08:08:53 UTC vinc17
* Added comment:
See also bug #3886.

--------------------------------------------------------------------------------
2016-10-20 23:05:59 UTC kevin8t8
* Added comment:
It looks like mutt_decode_attachment() checks for the state flag MUTT_CHARCONV and will do charset conversions for text attachments in that case.

There are a couple paths to this through the attachment menu.

One is through mutt_query_pipe_attachment() to mutt_pipe_attachment().  mutt_pipe_attachment() sets up its own STATE to pass through to mutt_decode_attachment().  [mutt_print_attachment() will also invoke mutt_pipe_attachment() when printing text attachments, but I think this would be desired behavior too.]

The other is through directly through pipe_attachment().  This version uses the STATE set up in mutt_pipe_attachment_list().

It seems like all we need to do is add the MUTT_CHARCONV flag to the STATE in mutt_pipe_attachment_list() and mutt_pipe_attachment().


* owner changed to kevin8t8
* status changed to assigned

--------------------------------------------------------------------------------
2016-10-20 23:16:57 UTC kevin8t8
* Added comment:
I'm attaching an untested patch.  I'll have more time to test tomorrow and this weekend.

--------------------------------------------------------------------------------
2016-10-20 23:17:14 UTC kevin8t8
* Added attachment ticket-3773.patch

--------------------------------------------------------------------------------
2016-10-23 22:28:57 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [changeset:"f46ed1718cb4ac38f7c5d5f84b5ca4be576cbb72" 6831:f46ed1718cb4]:
{{{
#!CommitTicketReference repository="" revision="f46ed1718cb4ac38f7c5d5f84b5ca4be576cbb72"
Perform charset conversion on text attachments when piping. (closes #3773) (see #3886)

When piping a text attachment, there is no reliable way to know the
charset used.

Vincent Lefèvre says:
  It was decided in the past that when there is no information on the
  charset in a transmission to an external command (e.g. as for mail
  composing), texts are expected to be transmitted in the local
  charset.

Add a MUTT_CHARSET flag to enable charset conversion on text
attachments for both when $attach_split is set and unset.
}}}

* resolution changed to fixed
* status changed to closed
