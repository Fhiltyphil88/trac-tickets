Ticket:  2308
Status:  closed
Summary: Manual should include the release date

Reporter: vincent@vinc17.org
Owner:    mutt-dev

Opened:       2006-06-25 13:19:40 UTC
Last Updated: 2006-06-25 21:11:18 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
The release date is given by "mutt -v", by the show-version command and in the User-Agent header, but not in the manual.
>How-To-Repeat:
>Fix:
See attached patch.
}}}

--------------------------------------------------------------------------------
2006-06-26 15:11:18 UTC brendan
* Added comment:
{{{
Thanks. I've applied this with a modification to fetch reldate from the builddir, 
not srcdir (it's generated now).
}}}

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2007-04-03 16:43:13 UTC 
* Added attachment patch-1.5.11cvs.vl.manual_reldate.1
* Added comment:
patch-1.5.11cvs.vl.manual_reldate.1
