Ticket:  3555
Status:  closed
Summary: Mutt ASCII signatures in figlet font sent (received) distorted

Reporter: riderplus
Owner:    mutt-dev

Opened:       2011-12-09 22:22:16 UTC
Last Updated: 2012-02-01 08:51:56 UTC

Priority:  minor
Component: mutt
Keywords:  ascii; signature

--------------------------------------------------------------------------------
Description:
The ~/.signature file contains an ASCII signature in figlet font. Tried sending the messages with the signature included in both us-ascii and utf-8 encoding (using vim as editor). While in Mutt's "sent messages" the signature is properly displayed, the recipient of the message (having a web-based client for e-mail, tried with all encodings and plain text) gets them badly wrapped. Most of Mutt's manual suggestions have been tried, but with no result. 

--------------------------------------------------------------------------------
2011-12-10 07:45:42 UTC danf
* Added comment:
{{{
What is most likely happening is that the recipient reader tries to be too
smart and interprets the message incorrectly as flowed text.  You can try
adding the "format=fixed" parameter to the Content-Type: header line
(see RFC 3676). If the reader supports it, then the message should appear
correctly. If not, then there's not a whole lot more you can do, short of
switching to HTML. In any case, this is unlikely to be due to any issue with
mutt itself.
}}}

--------------------------------------------------------------------------------
2012-02-01 08:51:56 UTC riderplus
* resolution changed to wontfix
* status changed to closed
