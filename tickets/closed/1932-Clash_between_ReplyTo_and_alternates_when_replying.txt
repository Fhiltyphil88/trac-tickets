Ticket:  1932
Status:  closed
Summary: Clash between Reply-To and alternates when replying

Reporter: vincent@vinc17.org
Owner:    mutt-dev

Opened:       2004-10-26 16:40:27 UTC
Last Updated: 2010-08-06 18:54:59 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.5.6-20040722
Severity: normal

-- Please type your report below this line

There seems to be a clash between the "Reply-To:" header and the
alternates when the "Reply-To:" header has several e-mail addresses.
I wanted to reply to a mail with:

Reply-To: vincent@vinc17.org, 1931@bugs.guug.de
From: vincent@vinc17.org
To: submit@bugs.guug.de

and though I have $reply_to="ask-yes", when I did a reply, Mutt
proposed:

To: submit@bugs.guug.de

instead of

To: 1931@bugs.guug.de

If the "Reply-To:" header has at least an address that is not matched
by the alternates, the reply should go to the Reply-To addresses,
except the ones matched by the alternates.

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/lib/gcc-lib/powerpc-linux/3.3.4/specs
Configured with: ../src/configure -v --enable-languages=c,c++,java,f77,pascal,objc,ada --prefix=/usr --mandir=/usr/share/man --infodir=/usr/share/info --with-gxx-include-dir=/usr/include/c++/3.3 --enable-shared --with-system-zlib --enable-nls --without-included-gettext --enable-__cxa_atexit --enable-clocale=gnu --enable-debug --enable-java-gc=boehm --enable-java-awt=xlib --enable-objc-gc --disable-multilib powerpc-linux
Thread model: posix
gcc version 3.3.4 (Debian 1:3.3.4-13)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.5.6i (2004-02-01)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.6.8 (ppc) [using ncurses 5.4] [using libidn 0.5.2 (compiled with 0.5.2)]
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  -USE_SETGID  -USE_DOTLOCK  -DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_GSS  +USE_SSL  -USE_SASL  -USE_SASL2  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  +BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_LIBIDN  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/local/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/home/lefevre/Mail"
PKGDATADIR="/home/lefevre/share/mutt"
SYSCONFDIR="/home/lefevre/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.

patch-1.5.1.vl.savehist.1
patch-1.5.5.1.tt.compat.1-cl
patch-1.3.24.ats.parent_match.1


>How-To-Repeat:
	
>Fix:
}}}

--------------------------------------------------------------------------------
2004-10-26 16:40:27 UTC vincent@vinc17.org
* Added comment:
{{{
Package: mutt
Version: 1.5.6-20040722
Severity: normal

-- Please type your report below this line

There seems to be a clash between the "Reply-To:" header and the
alternates when the "Reply-To:" header has several e-mail addresses.
I wanted to reply to a mail with:

Reply-To: vincent@vinc17.org, 1931@bugs.guug.de
From: vincent@vinc17.org
To: submit@bugs.guug.de

and though I have $reply_to="ask-yes", when I did a reply, Mutt
proposed:

To: submit@bugs.guug.de

instead of

To: 1931@bugs.guug.de

If the "Reply-To:" header has at least an address that is not matched
by the alternates, the reply should go to the Reply-To addresses,
except the ones matched by the alternates.

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/lib/gcc-lib/powerpc-linux/3.3.4/specs
Configured with: ../src/configure -v --enable-languages=c,c++,java,f77,pascal,objc,ada --prefix=/usr --mandir=/usr/share/man --infodir=/usr/share/info --with-gxx-include-dir=/usr/include/c++/3.3 --enable-shared --with-system-zlib --enable-nls --without-included-gettext --enable-__cxa_atexit --enable-clocale=gnu --enable-debug --enable-java-gc=boehm --enable-java-awt=xlib --enable-objc-gc --disable-multilib powerpc-linux
Thread model: posix
gcc version 3.3.4 (Debian 1:3.3.4-13)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.5.6i (2004-02-01)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.6.8 (ppc) [using ncurses 5.4] [using libidn 0.5.2 (compiled with 0.5.2)]
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  -USE_SETGID  -USE_DOTLOCK  -DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_GSS  +USE_SSL  -USE_SASL  -USE_SASL2  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  +BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_LIBIDN  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/local/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/home/lefevre/Mail"
PKGDATADIR="/home/lefevre/share/mutt"
SYSCONFDIR="/home/lefevre/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.

patch-1.5.1.vl.savehist.1
patch-1.5.5.1.tt.compat.1-cl
patch-1.3.24.ats.parent_match.1
}}}

--------------------------------------------------------------------------------
2010-08-05 23:05:15 UTC me
* Added comment:
I think the problem here is actually that Mutt sees that you are in the From: address, and always uses the To: line in this case.   From send.c in default_to():

  if (!option(OPTREPLYSELF) && mutt_addr_is_user (env->from))
  {
    /* mail is from the user, assume replying to recipients */
    rfc822_append (to, env->to, 1);
  }

Mutt should probably do this check after checking the reply-to field just below this code?

* Updated description:
{{{
Package: mutt
Version: 1.5.6-20040722
Severity: normal

-- Please type your report below this line

There seems to be a clash between the "Reply-To:" header and the
alternates when the "Reply-To:" header has several e-mail addresses.
I wanted to reply to a mail with:

Reply-To: vincent@vinc17.org, 1931@bugs.guug.de
From: vincent@vinc17.org
To: submit@bugs.guug.de

and though I have $reply_to="ask-yes", when I did a reply, Mutt
proposed:

To: submit@bugs.guug.de

instead of

To: 1931@bugs.guug.de

If the "Reply-To:" header has at least an address that is not matched
by the alternates, the reply should go to the Reply-To addresses,
except the ones matched by the alternates.

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/lib/gcc-lib/powerpc-linux/3.3.4/specs
Configured with: ../src/configure -v --enable-languages=c,c++,java,f77,pascal,objc,ada --prefix=/usr --mandir=/usr/share/man --infodir=/usr/share/info --with-gxx-include-dir=/usr/include/c++/3.3 --enable-shared --with-system-zlib --enable-nls --without-included-gettext --enable-__cxa_atexit --enable-clocale=gnu --enable-debug --enable-java-gc=boehm --enable-java-awt=xlib --enable-objc-gc --disable-multilib powerpc-linux
Thread model: posix
gcc version 3.3.4 (Debian 1:3.3.4-13)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.5.6i (2004-02-01)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.6.8 (ppc) [using ncurses 5.4] [using libidn 0.5.2 (compiled with 0.5.2)]
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  -USE_SETGID  -USE_DOTLOCK  -DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_GSS  +USE_SSL  -USE_SASL  -USE_SASL2  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  +BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_LIBIDN  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/local/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/home/lefevre/Mail"
PKGDATADIR="/home/lefevre/share/mutt"
SYSCONFDIR="/home/lefevre/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.

patch-1.5.1.vl.savehist.1
patch-1.5.5.1.tt.compat.1-cl
patch-1.3.24.ats.parent_match.1


>How-To-Repeat:
	
>Fix:
}}}
* milestone changed to 
* status changed to infoneeded_new

--------------------------------------------------------------------------------
2010-08-06 01:11:30 UTC vinc17
* Added comment:
I think the Reply-To should be checked first and the logic should be the following if there's a Reply-To:

1. If (current mailing-list tests) use the From address.[[BR]]
2. Remove all the alternates from the Reply-To list of addresses. If the remaining list is not empty, use it, otherwise use the recipients.

* status changed to new

--------------------------------------------------------------------------------
2010-08-06 18:54:59 UTC me
* Added comment:
This seems to be a duplicate of bug #2304, which has a patch attached and a *long* discussion.  I am closing this bug and leaving the other one open.

* resolution changed to duplicate
* status changed to closed
