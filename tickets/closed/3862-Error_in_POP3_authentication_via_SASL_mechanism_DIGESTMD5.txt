Ticket:  3862
Status:  closed
Summary: Error in POP3 authentication via SASL mechanism DIGEST-MD5

Reporter: g1pimutt
Owner:    mutt-dev

Opened:       2016-08-25 14:49:51 UTC
Last Updated: 2016-10-11 01:14:21 UTC

Priority:  major
Component: POP
Keywords:  

--------------------------------------------------------------------------------
Description:
According to <​https://tools.ietf.org/html/rfc5034#section-6>, the DIGEST-MD5 authentication should proceed along a sequence similar to the following:

    C: AUTH DIGEST-MD5
    S: + base64-encoded-server-challenge
    C: base64-encoded-client-response
    S: + base64-encoded-server-auth-confirmation
    C:
    S: +OK Maildrop locked and ready 

In fact, even if the server grants access, mutt detects a spurious error, sends the server a standalone "*" to request protocol shutdown, and aborts.

The problem stems from the fact that the pop_auth_sasl() in file pop_auth.c incorrectly terminates the SASL protocol at step 4, then checks that the last message from the server ("+ base64-encoded-server-auth-confirmation") starts with "+OK", and of course fails.

I believe the attached patch fixes the problem.


--------------------------------------------------------------------------------
2016-08-25 14:50:22 UTC g1pimutt
* Added attachment pop-digest-md5.patch

--------------------------------------------------------------------------------
2016-08-25 14:53:14 UTC g1pimutt
* cc changed to g1pi@libero.it
* Added comment:
properly formatted sequence:

1. C: AUTH DIGEST-MD5
2. S: + base64-encoded-server-challenge
3. C: base64-encoded-client-response
4. S: + base64-encoded-server-auth-confirmation
5. C:
6. S: +OK Maildrop locked and ready

--------------------------------------------------------------------------------
2016-08-25 18:58:45 UTC antonio@dyne.org
* cc changed to g1pi@libero.it, antonio@dyne.org

--------------------------------------------------------------------------------
2016-10-07 01:34:11 UTC kevin8t8
* Added comment:
Sorry but this patch doesn't make sense to me.  The sasl code should be making the decisions about when the authentication is done and return that via sasl_client_step().  The sasl_client_step() documentation even says:
{{{
It returns SASL_OK if the whole negotiation is successful
and SASL_CONTINUE if this step is ok but at least one more step is needed.

A client should not assume an authentication negotiation
is  successful  just  because  the server signaled success
via protocol (i.e. if the server said ". OK Authentication
succeeded" in IMAP sasl_client_step should still be called
one more time with a serverinlen of zero.
}}}

What return code are you seeing for the case where you are seeing it abort after step 4?

--------------------------------------------------------------------------------
2016-10-07 15:17:22 UTC g1pimutt
* _comment0 changed to 1475853490221624
* Added comment:
Step 4:

On entry to sasl_client_step(), buf="rspauth=<MD5 sent by server>"; the call
returns 0 (SASL_OK), and sets pc="" and olen=0.

To me, this means SASL authentication was successful: Client and Server have verified
each other, otherwise the return value would be negative,

Step 5:

... yet the client is supposed to send one more blank line to the server.
I'm not sure whether the SASL library or the application should drive this.

The manual for sasl_client_step() says:

    "... in IMAP sasl_client_step should still be called one more time
    with a serverinlen of zero."  (presumably "after it returns SASL_OK")

I believe this applies to POP too: when sasl_client_step returns SASL_OK and sets pc="" and olen=0, another call should be made; unfortunately olen=0 triggers a break in the code a few lines later.

Does the following patch seem reasonable to you?

{{{
--- pop_auth.c  2016-10-07 14:11:54.183776683 +0200
+++ pop_auth.c.new      2016-10-07 12:49:19.026548655 +0200
@@ -117,7 +117,7 @@
       client_start = 0;
     }

-    if (rc != SASL_CONTINUE && (olen == 0 || rc != SASL_OK))
+    if (rc != SASL_CONTINUE && rc != SASL_OK)
       break;

     /* send out response, or line break if none needed */

}}}

--------------------------------------------------------------------------------
2016-10-07 17:12:01 UTC kevin8t8
* Added comment:
> The manual for sasl_client_step() says:
> 
>    "... in IMAP sasl_client_step should still be called one more time with a serverinlen of zero."

> (presumably "after it returns SASL_OK")

No, I don't believe the above presumption is correct.  They are pointing out that you should follow the return codes, not assume that "+OK" in the response means you are done.  If there is another response needed from the client, the sasl_client_step() should return SASL_CONTINUE, even if the clientout is empty.  SASL_OK means "the authentication is complete."

It looks like the code currently tries to handle the case where rc==SASL_OK but the olen is set for some strange reason.

But if what is happening is that olen==0, rc==SASL_OK, and yet there is supposed to be another sasl_client_step(), that is clearly a bug in the SASL libraries, and I don't think there is any good way to work around it in the application (mutt) code.

--------------------------------------------------------------------------------
2016-10-07 18:42:46 UTC kevin8t8
* Added comment:
Wait, it looks like I am wrong!

https://www.cyrusimap.org/docs/cyrus-sasl/2.1.25/programming.php#client_code shows what you are talking about: the need to perform one last sasl_client_step() after getting the SASL_OK.  Let me read a bit more and I'll post a patch to test.

--------------------------------------------------------------------------------
2016-10-07 19:04:53 UTC g1pimutt
* Added comment:
Replying to [comment:5 kevin8t8]:
> > The manual for sasl_client_step() says:
> > 
> >    "... in IMAP sasl_client_step should still be called one more time with a serverinlen of zero."
> 
> > (presumably "after it returns SASL_OK")
> 
> No, I don't believe the above presumption is correct.  They are pointing out that you should follow the return codes, not assume that "+OK" in the response means you are done.  If there is another response needed from the client, the sasl_client_step() should return SASL_CONTINUE, even if the clientout is empty.  SASL_OK means "the authentication is complete."

Unless I'm mistaken, after step 4 returns SASL_OK, authentication IS complete, because the server has responded "I verified that you know the password, here is my rspauth for you to verify that I'm authoritative for the realm", and the client has successfully checked the rspauth.  Now, per the protocol, the client has to send an additional blank line: I'm not sure that has anything to do with authentication.

> 
> It looks like the code currently tries to handle the case where rc==SASL_OK but the olen is set for some strange reason.
> 
> But if what is happening is that olen==0, rc==SASL_OK, and yet there is supposed to be another sasl_client_step(), that is clearly a bug in the SASL libraries, and I don't think there is any good way to work around it in the application (mutt) code.

Perhaps it's a bug for sasl_client_step to return SASL_OK at this stage, and working around it is a mistake.  But as far as I can tell, the code in imap/auth_sasl.c does the same.


--------------------------------------------------------------------------------
2016-10-07 19:10:23 UTC g1pimutt
* Added comment:
Replying to [comment:6 kevin8t8]:
> Wait, it looks like I am wrong!
> 
> https://www.cyrusimap.org/docs/cyrus-sasl/2.1.25/programming.php#client_code shows what you are talking about: the need to perform one last sasl_client_step() after getting the SASL_OK.  Let me read a bit more and I'll post a patch to test.

Sorry, I replied to your previous comment before seeing this one.

If you are going to make a patch, please consider mine, which is as thin as possible.  I've tested it both with CRAM-MD5 and DIGEST-MD5, but I was not able to test it with PLAIN.

--------------------------------------------------------------------------------
2016-10-07 22:59:58 UTC kevin8t8
* Added comment:
Okay I've been trying to dive into it a little deeper, and have been looking more closely at your patch.  I understand your warning: the code is more complicated than it first seems.  :-)

I unrolled the loop a bit, to make it more like the imap code, and tried making other changes to avoid peeking at inbuf.  But in the end, you have to peek at inbuf to know whether to go through another socket_write and socket_read or exit the loop when you receive SASL_OK.

I think the only thing that makes me nervous is that it (theoretically) could exit prematurely now.  Would it make more sense to do:
{{{
if (rc != SASL_CONTINUE &&
    (rc != SASL_OK || !mutt_strncmp(inbuf, "+OK", 3)))
  break;
}}}

Thanks for your patience with me: I wasn't very familiar with this code, and I don't like committing patches I don't understand.

--------------------------------------------------------------------------------
2016-10-08 08:39:39 UTC g1pimutt
* _comment0 changed to 1475916055882235
* Added comment:
I was confused too, because the loop has so many exit points that is not easy to understand what it does.  After spreading dprint()s all over, I came to the conclusion that in the case of DIGEST-MD5 the correct flow is (apart from the interact-ion substeps)

1st iteration: send, receive, call sasl_client_step, get SASL_CONTINUE

2nd iteration: send, receive, call sasl_client_step, get SASL_OK

3rd iteration: send, receive, exit the loop without calling sasl_client_step

After the loop ends, the code checks both rc == SASL_OK and inbuf == "+OK", while failures in the loop (i.e. sasl_whatever returning < 0) are handled by "goto bail": all is fine.

The test as it was, {{{(rc != SASL_CONTINUE && (olen == 0 || rc != SASL_OK))}}}, misinterpreted olen==0 (by which sasl_client_step means "next sent line must be empty") as "we are done", causing premature exit from the loop and skipping the 3rd iteration.  My SECOND patch fixes exactly this defect.

> I think the only thing that makes me nervous is that it (theoretically) could exit prematurely now.

You probably meant "loop forever", since {{{A && (B || C)}}} is certainly true whenever {{{A && B}}} is true, whatever C.  I don't think it can happen, unless sasl_client_step is really buggy and keeps returning SASL_CONTINUE.

I believe you were right in rejecting my first patch: we shouldn't look at the POP3 protocol in the middle of the SASL exchange, even if we know the authentication phase is over and was successful.  That's why I would not add {{{|| !mutt_strncmp(...)}}} to the test.


--------------------------------------------------------------------------------
2016-10-08 14:46:17 UTC kevin8t8
* Added comment:
>> I think the only thing that makes me nervous is that it
>> (theoretically) could exit prematurely now.

> You probably meant "loop forever"

Sorry, my language was unclear.  I was talking about the behavior of your
first patch.

I was thinking there might be a case where rc == SASL_CONTINUE yet
inbuf started with "+OK" or "-ERR".  This would make your first patch
terminate before the sasl code said the interaction was done.  I don't
think this actually would happen, so that's what I meant by
"theoretically".

> I believe you were right in rejecting my first patch: we shouldn't
> look at the POP3 protocol in the middle of the SASL exchange, even if
> we know the authentication phase is over and was successful. That's
> why I would not add || !mutt_strncmp(...) to the test.

My concern with your second patch is the interaction
{{{
        C: AUTH PLAIN
            (note that there is a space following the '+' on the
            following line)
        S: +
        C: dGVzdAB0ZXN0AHRlc3Q=
        S: +OK Maildrop locked and ready
}}}

In this case, the sasl_client_step would return SASL_OK on the 4th step,
but your second patch would send another blank line to the server and
read its response.  My proposed patch would accept the SASL_OK without
further interaction because inbuf started with "+OK".

I need to read more about the POP protocol, but am short on time today
(I need to work on the 1.7.1 release).  Do you know what the server
would return if the client sends one more "\r\n" after the 4th step?

If it is guaranteed to repeat the "+OK" then I guess your patch is okay
but my patch saves a last round trip.  I'd appreciate your thoughts and
testing for this.

--------------------------------------------------------------------------------
2016-10-08 16:09:56 UTC g1pimutt
* _comment0 changed to 1475943382845586
* Added comment:
Please forget my first patch.

I just tested my second patch with PLAIN, and it works fine.  In this case, the code preceding the loop sets rc to SASL_OK and client_start to the length of the auth string.  The loop is entered twice, but sasl_client_step is never called:

1st time (client_start != 0, rc == SASL_OK):

  send "AUTH PLAIN"

  receive "+ "

2nd time (client_start == 0, rc == SASL_OK):

  send base64(auth-string)

  receive "+OK" or "-ERR"

  exit because (!client_start && rc != SASL_CONTINUE)

The exchange between client and server is exactly what it has to be.  The break instruction modified by my patch is never triggered, because rc == SASL_OK all the time.

Despite being very convoluted, the code maintains the invariant that on successful exit from the loop inbuf contains either "+OK" or "-ERR", unless the SASL interaction failed early (goto bail).

--------------------------------------------------------------------------------
2016-10-08 17:36:42 UTC kevin8t8
* Added comment:
Ah!  That's very interesting - I didn't realize the "client_start" case might set rc=SASL_OK.  So that explains the {{{if (!client_start && rc != SASL_CONTINUE)}}} line too: it sends out the data returned from sasl_client_start() and then exits in the next loop because rc=SASL_OK.  The code is really complicated, though, isn't it. :-)

I think the code would have been clearer if they pulled the client_start out of the loop, even though this would have duplicated code.  Something like:

{{{
rc = sasl_client_start()

buf = "AUTH <mech>"
mutt_socket_write(buf)
mutt_socket_readln(inbuf)

if (client_start)
{
  sasl_encode64(pc -> buf)
  mutt_socket_write(buf)
  mutt_socket_readln(inbuf)
}

while (rc == SASL_CONTINUE)
{
  sasl_decode64(inbuf -> buf)
  rc = sasl_client_step()
  if (pc)
    sasl_encode64()
  append \r\n to buf
  mutt_socket_write(buf)
  mutt_socket_readln(inbuf)
}
}}}

But obviously I would probably introduce more bugs if I touched the code that much.

Okay, I will submit your second patch and close this ticket after I finish the 1.7.1 release.
Thank you very much for the patch and for your time to help me understand.

--------------------------------------------------------------------------------
2016-10-09 08:33:08 UTC g1pimutt
* Added comment:
Well, in the process I learnt a lot about mutt and sasl, so thank you for the whole discussion.

The core logic in this code, written by Vsevolod Volkov in 2000, has gone almost unmodified across the standards change of 2007 and still works: this is amazing, and deserves our respect.

--------------------------------------------------------------------------------
2016-10-11 01:14:19 UTC <g1pi@libero.it>
* Added comment:
In [changeset:"a9764761b6929277fa0870aa97c9e9b16f56210b" 6813:a9764761b692]:
{{{
#!CommitTicketReference repository="" revision="a9764761b6929277fa0870aa97c9e9b16f56210b"
Fix POP3 SASL authentication mechanism DIGEST-MD5. (closes #3862)

sasl_client_step() returns SASL_OK after the fourth step: server auth
confirmation.  However, the protocol requires the client send one more
blank line to the server, to which the server then replies with "+OK".
See https://tools.ietf.org/html/rfc5034#section-6.

The code currently only sends a final response if sasl_client_step
returns data to send.  Change it to always send a final client message
after the SASL_OK.
}}}

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2016-10-11 01:14:21 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [changeset:"33d16ccba4cf057487a0ed56d5e1ab4b7cfb7293" 6814:33d16ccba4cf]:
{{{
#!CommitTicketReference repository="" revision="33d16ccba4cf057487a0ed56d5e1ab4b7cfb7293"
Add a few explanatory comments to pop_auth_sasl().  (see #3862)
}}}
