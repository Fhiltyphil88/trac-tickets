Ticket:  3459
Status:  closed
Summary: Mutt hangs when opening large IMAP mailboxes on an Exchange Server

Reporter: me
Owner:    me

Opened:       2010-10-01 15:55:31 UTC
Last Updated: 2012-12-27 04:05:48 UTC

Priority:  major
Component: IMAP
Keywords:  

--------------------------------------------------------------------------------
Description:
Recent Exchange servers (2007+) will sometimes omit FETCH responses when Mutt requests "FETCH 1:n" where "n" is some largish (~2000) number.  This causes Mutt to hang because it expects the server to return all FETCH responses.


--------------------------------------------------------------------------------
2010-10-01 15:56:52 UTC me
* Added comment:
More discussion on the mutt-users list http://marc.info/?t=128497892300001&r=1&w=2

--------------------------------------------------------------------------------
2010-10-01 16:06:40 UTC papo
* cc changed to weyland@library.ethz.ch

--------------------------------------------------------------------------------
2010-10-01 18:51:59 UTC me
* Added comment:
After some testing, it appear that the Exchange server does return all FETCH responses with something like "FETCH 1:n (FLAGS)" (this seems to be what Thunderbird does at startup).  Another user verified that fetching ~2000 messages at a time seemed to not trigger missing FETCH responses, so we probably need to fetch the headers in blocks.

* owner changed to me
* status changed to accepted

--------------------------------------------------------------------------------
2010-10-03 16:06:35 UTC papo
* Added comment:
The user 'scandal' suggested to split the single FETCH of all
messages into several FETCHes of smaller chunks:

{{{
--- imap/message.c.orig 2010-10-03 17:55:58.000000000 +0200
+++ imap/message.c  2010-10-03 17:54:51.000000000 +0200
@@ -240,7 +240,11 @@
     if (msgno + 1 > fetchlast)
     {
       fetchlast = msgend + 1;
-
+      /* Microsoft Exchange 2010 violates the IMAP protocol and 
+       * starts omitting messages if one FETCHes more than 2047 (or
+       * or somewhere around that number. We therefore split the
+       * FETCH into chunks of 2000 messages each */
+      if (fetchlast - msgno - 1 > 2000) fetchlast = msgno+1 + 2000;
       snprintf (buf, sizeof (buf),
         "FETCH %d:%d (UID FLAGS INTERNALDATE RFC822.SIZE %s)", msgno + 1,
         fetchlast, hdrreq);
}}}

I added this line to the version of mutt shipped with Ubuntu lucid (1.5.20,
probably with some patches) and have it running for two days now. This strategy
fixes the problem and I was not able to observe and side effects for now.


--------------------------------------------------------------------------------
2010-11-15 08:53:20 UTC michele
* cc changed to weyland@library.ethz.ch, michele.marcionelli@math.ethz.ch

--------------------------------------------------------------------------------
2011-04-29 12:22:12 UTC village
* cc changed to weyland@library.ethz.ch, michele.marcionelli@math.ethz.ch, bugreports@village.in-berlin.de

--------------------------------------------------------------------------------
2011-04-29 12:36:36 UTC michele
* Added comment:
FYI: this is an IMAP bug of the Exchange Server 2010, that has been solved (I can confirm it). For more information see [http://support.microsoft.com/kb/2494798]

--------------------------------------------------------------------------------
2012-12-27 04:05:48 UTC me
* Added comment:
The bug in Exchange seems to have been addressed, so I am closing this ticket.

* Updated description:
Recent Exchange servers (2007+) will sometimes omit FETCH responses when Mutt requests "FETCH 1:n" where "n" is some largish (~2000) number.  This causes Mutt to hang because it expects the server to return all FETCH responses.
* resolution changed to invalid
* status changed to closed
