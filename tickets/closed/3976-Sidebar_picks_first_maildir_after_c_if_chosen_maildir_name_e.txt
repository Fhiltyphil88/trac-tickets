Ticket:  3976
Status:  closed
Summary: Sidebar picks first maildir after 'c' if chosen maildir name ends with '/'

Reporter: ScarecrowRepair
Owner:    mutt-dev

Opened:       2017-10-10 19:11:00 UTC
Last Updated: 2017-10-18 02:45:50 UTC

Priority:  minor
Component: mutt
Keywords:  sidebar

--------------------------------------------------------------------------------
Description:
I use maildirs.  Suppose I have two maildirs in the sidebar: Main and accounts.  If I am in Main and type 'cacc<TAB><ENTER>', it does switch to the accounts maildir, but the sidebar remains at Main because tab completion added the trailing slash.  Typing in the full name, or backing over the trailing slash, works correctly with the sidebar also selecting accounts.

This is Ubuntu 17.04, neomutt 20170113 (1.7.2).  I won't be surprised if this is already fixed, but a quick search (google and here) didn't show it.

--------------------------------------------------------------------------------
2017-10-15 08:07:11 UTC kevin8t8
* Added comment:
I can't duplicate this in Mutt 1.9.1, and don't recall fixing anything related to this.  I'm closing this out, but if you end up being able to duplicate in Mutt please feel free to reopen.  Otherwise, you may want to file a bug for Ubuntu.

* resolution changed to worksforme
* status changed to closed

--------------------------------------------------------------------------------
2017-10-18 02:45:50 UTC ScarecrowRepair
* Added comment:
Thanks -- it's such a simple bug, it's easy to think something else might have fixed it as a byproduct, and it's so harmless and easy to work around that there's no point in putting more than a minute or two investigation into it.

If Ubuntu releases an update, I'll of course see if it's still around, but I won't file another bug report unless I can make it happen in some version after 1.9.1.
