Ticket:  3778
Status:  closed
Summary: index_format %t does not include all recipients

Reporter: derekschrock
Owner:    mutt-dev

Opened:       2015-09-08 21:35:26 UTC
Last Updated: 2015-09-18 19:12:45 UTC

Priority:  minor
Component: mutt
Keywords:  index_format

--------------------------------------------------------------------------------
Description:
Using index_format's %t in attribution not all recipients are displayed.

set attribution="%t"

When replying to a message with a To: looking like

-----
...[[BR]]
To: user1@ircbsd, user2@ircbsd[[BR]]
...

testing
-----

%t expands to the "To recipients"

-----
To user1@ircbsd
> testing
-----

Expected:

-----
To: user1@ircbsd, user2@ircbsd
> testing
-----

--------------------------------------------------------------------------------
2015-09-18 19:12:45 UTC derekschrock
* Added comment:
After looking at the mutt source it seems this is intentional to not include all the recipients. 

I'm going to close this and submit a patch for an expando to include a listing of To: recipients.  

* resolution changed to invalid
* status changed to closed
