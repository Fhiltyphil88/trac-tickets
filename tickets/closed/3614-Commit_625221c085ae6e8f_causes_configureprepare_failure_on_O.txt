Ticket:  3614
Status:  closed
Summary: Commit 6252:21c085ae6e8f causes configure/prepare failure on OSX 10.4.11 PPC

Reporter: balderdash
Owner:    mutt-dev

Opened:       2012-12-29 06:14:57 UTC
Last Updated: 2012-12-30 19:38:21 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:

Commit 6252:21c085ae6e8f causes configure/prepare failure on OSX 10.4.11 PPC, when the repo clone is fresh.  I noticed failure on tip and traced it back to the named commit.

My prepare and configure flags are, and have for years been:

  --enable-debug  \
  --enable-imap   \
  --enable-pop    \
  --enable-smtp   \
  --enable-hcache \
  --with-ssl=/usr/local/ssl  \
  --with-sasl     \
  --with-tokyocabinet

When I run that I see:

configure.ac:7: error: m4_defn: undefined macro: _m4_divert_diversion
aclocal.m4:90: AM_CONFIG_HEADER is expanded from...
configure.ac:7: the top level
autom4te: /usr/bin/gm4 failed with exit status: 1
configure.ac:7: error: m4_defn: undefined macro: _m4_divert_diversion
aclocal.m4:90: AM_CONFIG_HEADER is expanded from...
configure.ac:7: the top level
autom4te: /usr/bin/gm4 failed with exit status: 1
autoreconf: /usr/bin/autoconf failed with exit status: 1

Running prepare a second time did not help, as it used to (see below).  A configure script is left behind but it fails quickly.

Prior to that, in commit 6251:0703095bf52d and earlier, doing that same 'prepare' line gives me this:

configure.ac:114: error: possibly undefined macro: AC_TYPE_LONG_LONG_INT
      If this token and others are legitimate, please use m4_pattern_allow.
      See the Autoconf documentation.
autoreconf: /usr/bin/autoconf failed with exit status: 1

But, as has been the case for years, running that prepare line a second time makes it work, and I could configure and build just fine.


--------------------------------------------------------------------------------
2012-12-29 06:40:30 UTC me
* Added comment:
{{{
please specify which versions of the following tools you have 
installed:

gm4
autoconf
automake

It must be something older than autoconf 2.61 because it does have 
AC_TYPE_LONG_LONG_INT.
}}}

--------------------------------------------------------------------------------
2012-12-29 22:18:49 UTC me
* status changed to infoneeded_new

--------------------------------------------------------------------------------
2012-12-30 07:59:30 UTC balderdash
* Added comment:
gm4 is "GNU m4 1.4.2"

autoconf is 2.59

automake is 1.6.3

These are the unmodifed dev tools for the XCode package that goes with OSX 10.4.11.

* status changed to new

--------------------------------------------------------------------------------
2012-12-30 19:38:21 UTC me
* Added comment:
You will either need to install a more recent version of autoconf, or use the nightly snapshots located at http://dev.mutt.org/nightlies/

* resolution changed to wontfix
* status changed to closed
