Ticket:  2170
Status:  closed
Summary: delay after wrong key in confirmation prompt for server certificate

Reporter: justinpryzby@users.sourceforge.net
Owner:    mutt-dev

Opened:       2006-01-29 22:04:37 UTC
Last Updated: 2010-08-06 00:19:17 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
This is from Debian Bug#342840; I can reproduce by setting pop_host="pops://user@server", pressing G, and eg. 'x' at the certificate prompt.

--------------------
I have been lazily using 'G' to retrieve mail from a POPS server.

I noticed a reaction after upgrading from mutt_1.5.9-2.  At the
"confirm server certificate prompt":  "WARNING: Server certificate has
expired", "(r)eject, accept (o)nce".

If the response is neither 'r' nor 'o', then there is a long delay,
with no beeps.  After the delay, pressing 'o' causes: "Key is not
bound."  Afterwards, 'r' and 'o' are accepted normally.

I checked the Debian and upstream changelogs, but couldn't see
anything relevant, going to back to 2005 May.
--------------------
>How-To-Repeat:
>Fix:
Unknown
}}}

--------------------------------------------------------------------------------
2006-01-29 22:33:12 UTC owner@bugs.debian.org (Debian Bug Tracking System)
* Added comment:
{{{
Thank you for the additional information you have supplied regarding
this problem report.  It has been forwarded to the package maintainer(s)
and to other interested parties to accompany the original report.

Your message has been sent to the package maintainer(s):
 Adeodato Simó <dato@net.com.org.es>

If you wish to continue to submit further information on your problem,
please send it to 342840@bugs.debian.org, as before.

Please do not reply to the address at the top of this message,
unless you wish to report a problem with the Bug-tracking system.

Debian bug tracking system administrator
(administrator, Debian Bugs database)
}}}

--------------------------------------------------------------------------------
2010-08-06 00:19:17 UTC me
* Added comment:
I believe this bug report is no longer applicable.  Mutt will no longer prompt to accept the expired certificate.  Instead, the $ssl_verify_dates option controls this check.  See http://www.mutt.org/doc/devel/manual.html#ssl-verify-dates

Feel free to reopen this bug if this does not address your issue.

* Updated description:
{{{
This is from Debian Bug#342840; I can reproduce by setting pop_host="pops://user@server", pressing G, and eg. 'x' at the certificate prompt.

--------------------
I have been lazily using 'G' to retrieve mail from a POPS server.

I noticed a reaction after upgrading from mutt_1.5.9-2.  At the
"confirm server certificate prompt":  "WARNING: Server certificate has
expired", "(r)eject, accept (o)nce".

If the response is neither 'r' nor 'o', then there is a long delay,
with no beeps.  After the delay, pressing 'o' causes: "Key is not
bound."  Afterwards, 'r' and 'o' are accepted normally.

I checked the Debian and upstream changelogs, but couldn't see
anything relevant, going to back to 2005 May.
--------------------
>How-To-Repeat:
>Fix:
Unknown
}}}
* milestone changed to 
* resolution changed to invalid
* status changed to closed
