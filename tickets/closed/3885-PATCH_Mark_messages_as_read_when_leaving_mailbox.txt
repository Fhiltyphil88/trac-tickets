Ticket:  3885
Status:  closed
Summary: [PATCH] Mark messages as read when leaving mailbox

Reporter: rsmmr
Owner:    mutt-dev

Opened:       2016-10-06 15:12:55 UTC
Last Updated: 2016-12-28 22:59:18 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
In case this could be interesting to more people, this patch adds a new option 'mark_read' that marks all messages as read when exiting a mailbox. If 'move' is enabled, they will then be moved out of the mailbox as well. I used to do this via a combination of macros but that gets cumbersome when enabling it selectively only for a subset of folders, so I turned it into an option.

--------------------------------------------------------------------------------
2016-10-06 15:13:44 UTC rsmmr
* Added attachment mutt-1.7.0.rs.mark_read.1

--------------------------------------------------------------------------------
2016-12-16 16:24:14 UTC rsmmr
* Added comment:
Just checking in if this is interesting? Otherwise I'll just close the ticket.

* priority changed to minor

--------------------------------------------------------------------------------
2016-12-28 00:14:29 UTC kevin8t8
* Added comment:
Thank you for the ticket and patch.  I apologize for the delayed response: I was thinking about this in the background but then got distracted the past few weeks.

My feeling is that this is a bit too esoteric, and I'm inclined to not apply.  Other devs: do you have an opinion on this?

--------------------------------------------------------------------------------
2016-12-28 00:29:50 UTC vinc17
* Added comment:
I don't see how this could be useful in a general case. This seems for a very specific need. Macros can be used for that. If something is really needed when leaving a mailbox, then a leave-folder hook could be preferable.

--------------------------------------------------------------------------------
2016-12-28 22:59:18 UTC kevin8t8
* Added comment:
Thanks for your input, Vincent.  

I appreciate the patch, but I think this is a bit too specific.  Thank you though, rsmmr!

* resolution changed to wontfix
* status changed to closed
