Ticket:  3380
Status:  closed
Summary: "From " time displayed incorrecly in mutt 1.5.20

Reporter: milvang
Owner:    mutt-dev

Opened:       2010-02-10 10:24:02 UTC
Last Updated: 2010-02-10 22:56:01 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
In mutt 1.5.20, the time in the "From " line is displayed incorrectly, whereas mutt 1.5.12 does it correctly. Example:

1.5.20: From foo@dark-cosmology.dk  Mon Feb  8 18: 5:46 2010
1.5.12: From foo@dark-cosmology.dk  Mon Feb  8 18:35:46 2010

This may not have any consequences for date-received sorting --- not sure.

--------------------------------------------------------------------------------
2010-02-10 22:56:01 UTC antonio@dyne.org
* Added comment:
duplicate of 
mutt http://bugs.mutt.org/3331
D ebian http://bugs.debian.org/542344

* resolution changed to duplicate
* status changed to closed
