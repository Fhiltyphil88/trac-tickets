Ticket:  3727
Status:  closed
Summary: Allow selection of multitple GPG-keys to encrypt to

Reporter: rejozenger
Owner:    mutt-dev

Opened:       2015-01-13 14:25:05 UTC
Last Updated: 2015-04-19 20:25:16 UTC

Priority:  minor
Component: crypto
Keywords:  gpg, encryption, crypt-hook, mailinglist

--------------------------------------------------------------------------------
Description:
With this patch, you can have multiple crypt-hook instances with the same pattern (recipient). This allows one to specify multiple key-ids for a particular pattern (recipient). This comes in handy when one wants to send encrypted email to a mailinglist with a static set subscribers.

--------------------------------------------------------------------------------
2015-01-13 14:25:36 UTC rejozenger
* Added attachment multiple-crypt-hook.patch
* Added comment:
Patch for multiple crypt-hooks's

--------------------------------------------------------------------------------
2015-01-13 14:28:09 UTC rejozenger
* Added comment:
For history, credits and license, see [https://github.com/rejozenger/mutt-multiple-crypt-hook repository at GitHub].

--------------------------------------------------------------------------------
2015-04-04 18:22:16 UTC kevin8t8
* Added comment:
Attaching an updated and revised version of the patch.  It works for classic and gpgme mode, and also changes the behavior when a crypt-hook is declined to allow a subset of the crypt-hooks to be used without reverting to the original recipient for key selection.

--------------------------------------------------------------------------------
2015-04-04 18:22:38 UTC kevin8t8
* Added attachment kevin8t8-revised-multihook.patch

--------------------------------------------------------------------------------
2015-04-19 20:25:15 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [b4c57d3fd7e8375ba563615a6638716565a6f3e3]:
{{{
#!CommitTicketReference repository="" revision="b4c57d3fd7e8375ba563615a6638716565a6f3e3"
Allow multiple crypt-hooks with the same regexp. (closes #3727).

Changes the crypt-hook to accumulate a LIST of hooks with
the same regexp, as opposed to replacing the hook data.
This is useful for the case of encrypted mailing lists.

Update pgp classic and gpgme to process a LIST of crypt-hook
values instead of just one.

This version of the patch creates a new _mutt_list_hook() function that
(in theory) other hooks could use if they were changed to return a list.
It also changes the behavior when a crypt-hook is declined: previously
it would immediately use the original recipient for key selection.  Now
it will only do that if all crypt-hooks for a recipient are declined.
This allows all, a subset, or none of the hooks to be used.

Thanks to Rejo Zenger, Remco Rijnders, and Dale Woolridge for their work
on various versions of this patch.
}}}

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2015-04-19 20:25:16 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [067a3ac42c3b45cc18c4f40938643dd7ab8781e4]:
{{{
#!CommitTicketReference repository="" revision="067a3ac42c3b45cc18c4f40938643dd7ab8781e4"
Add $crypt_confirmhook option. (see #3727)

Allow the confirmation prompt for crypt-hooks to be disabled.  This is
useful for multiple crypt-hook users (e.g. encrypted mailing lists), or
just for people who are certain of their crypt-hooks and don't want to
be prompted every time.

Thanks to Dale Woolridge for the original patch.
}}}
