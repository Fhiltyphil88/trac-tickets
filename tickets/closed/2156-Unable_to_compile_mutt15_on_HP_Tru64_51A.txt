Ticket:  2156
Status:  closed
Summary: Unable to compile mutt-1.5 on HP Tru64 5.1A

Reporter: r.speranza@beltrame.it
Owner:    gnats-admin

Opened:       2005-12-22 13:00:16 UTC
Last Updated: 2006-01-11 06:48:06 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
unable to compile the source distribution
>How-To-Repeat:
>Fix:
Unknown
}}}

--------------------------------------------------------------------------------
2005-12-23 13:41:41 UTC cb
* Added comment:
{{{
move to mutt category
}}}

--------------------------------------------------------------------------------
2005-12-23 17:42:52 UTC Paul Walker <paul@black-sun.demon.co.uk>
* Added comment:
{{{
On Thu, Dec 22, 2005 at 08:45:45PM +0100, Christoph Berg wrote:

> >Description:
> unable to compile the source distribution
> >How-To-Repeat:
> >Fix:

I've pasted the compile output below in plain text, for people who don't
want to load OpenOffice just to read it. (Rich Text was an odd choice...)
You also haven't included any system information - for reference, which
compiler and which version are you using?

I've just checked my source directory. OP_EXIT is defined in keymap_defs.h.
This file is included in keymap.h, which is included in mutt_menu.h, which
is included by addrbook.c, so there should be no error there.

The first thing to do is download 1.5.11 from the FTP site and build that,
just to make sure you're up to date.

The next thing to do is to check that the same chain holds for your source.

If it looks like it should work, then we'll go from there. It would be
useful to see the output from the pre-processor, I think; with gcc, you
should be able to do it by running:

gcc -E -DPKGDATADIR=\"/home/paul/share/mutt\" -DSYSCONFDIR=\"/home/paul/etc\"
-DBINDIR=\"/home/paul/bin\" -DMUTTLOCALEDIR=\"/home/paul/share/locale\"
-DHAVE_CONFIG_H=1 -I. -I./imap  -Iintl  -I./intl
-I/home/paul/include  -Wall -pedantic -g -O2 -c `test -f 'addrbook.c' -o
addrbook-pre.c || echo './'`addrbook.c

from the mutt directory.

make  install-recursive
Making install in m4
No suffix list.
No suffix list.
Making install in po
/bin/sh ase "./mkinstalldirs" in /*) echo "./mkinstalldirs" ;; *) echo
".././m
kinstalldirs" ;; esac /usr/local/muttnew/share
installing de.gmo as /usr/local/muttnew/share/locale/de/LC_MESSAGES/mutt.mo
installing ru.gmo as /usr/local/muttnew/share/locale/ru/LC_MESSAGES/mutt.mo
installing it.gmo as /usr/local/muttnew/share/locale/it/LC_MESSAGES/mutt.mo
installing es.gmo as /usr/local/muttnew/share/locale/es/LC_MESSAGES/mutt.mo
installing uk.gmo as /usr/local/muttnew/share/locale/uk/LC_MESSAGES/mutt.mo
installing fr.gmo as /usr/local/muttnew/share/locale/fr/LC_MESSAGES/mutt.mo
installing pl.gmo as /usr/local/muttnew/share/locale/pl/LC_MESSAGES/mutt.mo
installing nl.gmo as /usr/local/muttnew/share/locale/nl/LC_MESSAGES/mutt.mo
installing cs.gmo as /usr/local/muttnew/share/locale/cs/LC_MESSAGES/mutt.mo
installing id.gmo as /usr/local/muttnew/share/locale/id/LC_MESSAGES/mutt.mo
installing sk.gmo as /usr/local/muttnew/share/locale/sk/LC_MESSAGES/mutt.mo
installing ko.gmo as /usr/local/muttnew/share/locale/ko/LC_MESSAGES/mutt.mo
installing el.gmo as /usr/local/muttnew/share/locale/el/LC_MESSAGES/mutt.mo
installing zh_TW.gmo as
/usr/local/muttnew/share/locale/zh_TW/LC_MESSAGES/mutt.m
o
installing zh_CN.gmo as
/usr/local/muttnew/share/locale/zh_CN/LC_MESSAGES/mutt.m
o
installing pt_BR.gmo as
/usr/local/muttnew/share/locale/pt_BR/LC_MESSAGES/mutt.m
o
installing eo.gmo as /usr/local/muttnew/share/locale/eo/LC_MESSAGES/mutt.mo
installing gl.gmo as /usr/local/muttnew/share/locale/gl/LC_MESSAGES/mutt.mo
installing sv.gmo as /usr/local/muttnew/share/locale/sv/LC_MESSAGES/mutt.mo
installing da.gmo as /usr/local/muttnew/share/locale/da/LC_MESSAGES/mutt.mo
installing lt.gmo as /usr/local/muttnew/share/locale/lt/LC_MESSAGES/mutt.mo
installing tr.gmo as /usr/local/muttnew/share/locale/tr/LC_MESSAGES/mutt.mo
installing ja.gmo as /usr/local/muttnew/share/locale/ja/LC_MESSAGES/mutt.mo
installing hu.gmo as /usr/local/muttnew/share/locale/hu/LC_MESSAGES/mutt.mo
installing et.gmo as /usr/local/muttnew/share/locale/et/LC_MESSAGES/mutt.mo
installing ca.gmo as /usr/local/muttnew/share/locale/ca/LC_MESSAGES/mutt.mo
installing bg.gmo as /usr/local/muttnew/share/locale/bg/LC_MESSAGES/mutt.mo
if test "mutt" = "gettext"; then  /bin/sh ase "./mkinstalldirs" in /*) echo
".
/mkinstalldirs" ;; *) echo ".././mkinstalldirs" ;; esac
/usr/local/muttnew/shar
e/gettext/po;  .././install-sh -c -m 644 ./Makefile.in.in
/usr/local/muttnew/sh
are/gettext/po/Makefile.in.in;  else  : ;  fi
Making install in intl
if test "mutt" = "gettext"  && test 'intl-compat.o' = 'intl-compat.o'; then
/bi
n/sh ase "./mkinstalldirs" in /*) echo "./mkinstalldirs" ;; *) echo
".././mkin
stalldirs" ;; esac /usr/local/muttnew/lib /usr/local/muttnew/include;
.././ins
tall-sh -c -m 644 libintl.h /usr/local/muttnew/include/libintl.h;  @LIBTOOL@
--m
ode=install  .././install-sh -c -m 644 libintl.a
/usr/local/muttnew/lib/libintl.
a;  else  : ;  fi
if test 'yes' = yes; then  /bin/sh ase "./mkinstalldirs" in /*) echo
"./mkinst
alldirs" ;; *) echo ".././mkinstalldirs" ;; esac /usr/local/muttnew/lib;
temp=
/usr/local/muttnew/lib/t-charset.alias;
dest=/usr/local/muttnew/lib/charset.ali
as;  if test -f /usr/local/muttnew/lib/charset.alias; then
orig=/usr/local/mutt
new/lib/charset.alias;  sed -f ref-add.sed $orig > $temp;  .././install-sh
-c -m
 644 $temp $dest;  rm -f $temp;  else  if test no = no; then
orig=charset.alias
;  sed -f ref-add.sed $orig > $temp;  .././install-sh -c -m 644 $temp $dest;
rm
 -f $temp;  fi;  fi;  /bin/sh ase "./mkinstalldirs" in /*) echo
"./mkinstalldi
rs" ;; *) echo ".././mkinstalldirs" ;; esac /usr/local/muttnew/share/locale;
t
est -f /usr/local/muttnew/share/locale/locale.alias  &&
orig=/usr/local/muttnew/
share/locale/locale.alias  || orig=./locale.alias;
temp=/usr/local/muttnew/shar
e/locale/t-locale.alias;  dest=/usr/local/muttnew/share/locale/locale.alias;
se
d -f ref-add.sed $orig > $temp;  .././install-sh -c -m 644 $temp $dest;  rm
-f $
temp;  else  : ;  fi
if test "mutt" = "gettext"; then  /bin/sh ase "./mkinstalldirs" in /*) echo
".
/mkinstalldirs" ;; *) echo ".././mkinstalldirs" ;; esac
/usr/local/muttnew/shar
e/gettext/intl;  .././install-sh -c -m 644 VERSION
/usr/local/muttnew/share/gett
ext/intl/VERSION;  .././install-sh -c -m 644 ChangeLog.inst
/usr/local/muttnew/s
hare/gettext/intl/ChangeLog;  dists="COPYING.LIB-2 COPYING.LIB-2.1
Makefile.in
config.charset locale.alias ref-add.sin ref-del.sin gettext.h gettextP.h
hash-st
ring.h libgnuintl.h libgettext.h loadinfo.h bindtextdom.c dcgettext.c
dgettext.c
 gettext.c  finddomain.c loadmsgcat.c localealias.c textdomain.c l10nflist.c
ex
plodename.c dcigettext.c dcngettext.c dngettext.c ngettext.c plural.y
localchar
set.c intl-compat.c";  for file in $dists; do  .././install-sh -c -m 644
./$file
  /usr/local/muttnew/share/gettext/intl/$file;  done;  chmod a+x
/usr/local/mutt
new/share/gettext/intl/config.charset;  dists="plural.c";  for file in
$dists; d
o  if test -f $file; then dir=.; else dir=.; fi;  .././install-sh -c -m 644
$dir
/$file  /usr/local/muttnew/share/gettext/intl/$file;  done;
dists="xopen-msg.se
d linux-msg.sed po2tbl.sed.in cat-compat.c";  for file in $dists; do  rm -f
/usr
/local/muttnew/share/gettext/intl/$file;  done;  else  : ;  fi
Making install in doc
test -f manual.html || make manual.html || cp ./manual*.html ./
test -f manual.txt || make manual.txt || cp ./manual.txt ./
../mkinstalldirs /usr/local/muttnew/man/man1
../mkinstalldirs /usr/local/muttnew/man/man5
./instdoc mutt.1 /usr/local/muttnew/man/man1/mutt.1
./instdoc ./muttbug.man /usr/local/muttnew/man/man1/flea.1
./instdoc ./muttbug.man /usr/local/muttnew/man/man1/muttbug.1
./instdoc ./dotlock.man  /usr/local/muttnew/man/man1/mutt_dotlock.1
./instdoc muttrc.man /usr/local/muttnew/man/man5/muttrc.5
./instdoc ./mbox.man /usr/local/muttnew/man/man5/mbox.5
./instdoc ./mmdf.man /usr/local/muttnew/man/man5/mmdf.5
../mkinstalldirs /usr/local/muttnew/doc/mutt
for f in COPYRIGHT GPL INSTALL ChangeLog         ChangeLog.old
README NEWS TODO README.SECURITY README.SSL  ; do  .././install-sh -c -m 644
../
$f /usr/local/muttnew/doc/mutt ;  done
for f in PGP-Notes.txt applying-patches.txt      devel-notes.txt
patch-notes.txt
 smime-notes.txt ; do  .././install-sh -c -m 644 ./$f
/usr/local/muttnew/doc/mut
t ;  done
.././install-sh -c -m 644 manual.txt /usr/local/muttnew/doc/mutt || true
../mkinstalldirs /usr/local/muttnew/doc/mutt/html
for file in manual*.html ; do    .././install-sh -c -m 644 $file
/usr/local/mutt
new/doc/mutt/html/ || true ; done
Making install in contrib
../mkinstalldirs /usr/local/muttnew/doc/mutt/samples
/usr/local/muttnew/doc/mutt
/samples/iconv
for f in Mush.rc Pine.rc gpg.rc pgp2.rc pgp5.rc pgp6.rc Tin.rc
sample.muttrc  s
ample.mailcap sample.muttrc-tlr  colors.default colors.linux smime.rc
ca-bundle
.crt smime_keys_test.pl ; do  .././install-sh -c -m 644 ./$f
/usr/local/muttnew/
doc/mutt/samples ;       done
for f in ./iconv/*.rc ; do
.././install-sh -c -m 644 $f /usr/local/muttnew/doc/mutt/samples/iconv   ;      done
source='addrbook.c' object='addrbook.o' libtool=no  DEPDIR=.deps
depmode=tru64 /bin/ksh ./depcomp  cc -DPKGDATADIR=\"/usr/local/muttnew/share/mutt\"
-DSYSCONFDIR=\"/usr/local/muttnew/etc\"  -DBINDIR=\"/usr/local/muttnew/bin\"
-DMUTTLOCALEDIR=\"/usr/local/muttnew/share/locale\"  -DHAVE_CONFIG_H=1 -I. -I. -I. -I. -I. 
-
Iintl   -I./intl -I/usr/local/muttnew/include  -g -c addrbook.c
cc: Error: addrbook.c, line 37: In the initializer for AliasHelp[0].value,
"OP_EXIT" is not declared. (undeclared)
  { N_("Exit"),   OP_EXIT },
------------------^
cc: Error: addrbook.c, line 38: In the initializer for AliasHelp[1].value,
"OP_DELETE" is not declared. (undeclared)
  { N_("Del"),    OP_DELETE },
------------------^
cc: Error: addrbook.c, line 39: In the initializer for AliasHelp[2].value,
"OP_UNDELETE" is not declared. (undeclared)
  { N_("Undel"),  OP_UNDELETE },
------------------^
cc: Error: addrbook.c, line 40: In the initializer for AliasHelp[3].value,
"OP_GENERIC_SELECT_ENTRY" is not declared. (undeclared)
  { N_("Select"), OP_GENERIC_SELECT_ENTRY },
------------------^
cc: Error: addrbook.c, line 41: In the initializer for AliasHelp[4].value,
"OP_HELP" is not declared. (undeclared)
  { N_("Help"),   OP_HELP },
------------------^
*** Exit 1
Stop.
*** Exit 1
Stop.
*** Exit 1
Stop.

-- 
Paul

--0OAP2g/MAC+5xKAE
Content-Type: application/pgp-signature; name="signature.asc"
Content-Description: Digital signature
Content-Disposition: inline

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.1 (GNU/Linux)

iD8DBQFDq1YcP9fOqdxRstoRAhecAKCafaLtREElzRtVXbo/Xhza3jW8SQCfb2jN
vTIFAwXxSdLRWGRwFTQo6ns=XnOq
-----END PGP SIGNATURE-----

--0OAP2g/MAC+5xKAE--
}}}

--------------------------------------------------------------------------------
2006-01-04 06:29:22 UTC r.speranza@beltrame.it
* Added comment:
{{{
This is a multipart message in MIME format.
--=_alternative 00432038C12570EB_=
Content-Type: text/plain; charset="US-ASCII"

Hello 

thanks a lot; with the version 1.5.11 (as suggested on the mutt developer 
forum) I was able to compile without errors.

Have a nice new year 

Riccardo 
--=_alternative 00432038C12570EB_=
Content-Type: text/html; charset="US-ASCII"


<br><font size=2 face="sans-serif">Hello </font>
<br>
<br><font size=2 face="sans-serif">thanks a lot; with the version 1.5.11
(as suggested on the mutt developer forum) I was able to compile without
errors.</font>
<br>
<br><font size=2 face="sans-serif">Have a nice new year </font>
<br>
<br><font size=2 face="sans-serif">Riccardo </font>
--=_alternative 00432038C12570EB_=--
}}}

--------------------------------------------------------------------------------
2006-01-12 00:48:06 UTC brendan
* Added comment:
{{{
This is a multipart message in MIME format.
Reported fixed in 1.5.11.
}}}

* resolution changed to fixed
* status changed to closed

--------------------------------------------------------------------------------
2007-04-03 16:43:09 UTC 
* Added attachment error_compile.rtf
* Added comment:
error_compile.rtf
