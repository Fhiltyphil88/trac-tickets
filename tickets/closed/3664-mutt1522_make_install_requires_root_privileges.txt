Ticket:  3664
Status:  closed
Summary: mutt-1.5.22: make install requires root privileges

Reporter: vc@vc.org.ua
Owner:    Kevin McCarthy <kevin@8t8.us>

Opened:       2013-10-28 17:48:06 UTC
Last Updated: 2015-07-09 21:52:26 UTC

Priority:  major
Component: 
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: mutt-1.5.22-12.el6.i686
Severity: normal

-- Please type your report below this line

Sorry for multiple messages...

make install requires root privileges. It fails on setting sgid bit to mutt_dotlock.

The following patch with 'make DOTLOCK_GROUP="" install' fixes this behaviour:

diff -urN mutt-1.5.22/Makefile.am mutt-1.5.22.vc.dotlock/Makefile.am
--- mutt-1.5.22/Makefile.am     2013-01-15 09:37:15.000000000 +0200
+++ mutt-1.5.22.vc.dotlock/Makefile.am  2013-10-28 19:17:09.278082062 +0200
@@ -154,9 +154,9 @@
                rm -f $(DESTDIR)$(bindir)/mutt.dotlock ;                \
                ln -sf $(DESTDIR)$(bindir)/mutt_dotlock $(DESTDIR)$(bindir)/mutt.dotlock ; \
        fi
-       if test -f $(DESTDIR)$(bindir)/mutt_dotlock && test x@DOTLOCK_GROUP@ != x ; then \
-               chgrp @DOTLOCK_GROUP@ $(DESTDIR)$(bindir)/mutt_dotlock && \
-               chmod @DOTLOCK_PERMISSION@ $(DESTDIR)$(bindir)/mutt_dotlock || \
+       if test -f $(DESTDIR)$(bindir)/mutt_dotlock && test x$(DOTLOCK_GROUP) != x ; then \
+               chgrp $(DOTLOCK_GROUP) $(DESTDIR)$(bindir)/mutt_dotlock && \
+               chmod $(DOTLOCK_PERMISSION) $(DESTDIR)$(bindir)/mutt_dotlock || \
                { echo "Can't fix mutt_dotlock's permissions!  This is required to lock mailboxes in the mail spool directory." >&2 ; exit 1 ; } \
        fi
 

-- System Information
System Version: Linux breaker.hostmaster.ua 2.6.32-358.14.1.el6.i686 #1 SMP Tue Jul 16 14:24:04 CDT 2013 i686 i686 i386 GNU/Linux
RPM Packager: Victor Cheburkin <vc@vc.org.ua>
RedHat Release: Scientific Linux release 6.4 (Carbon)

-- Mutt Version Information

Mutt 1.5.22 (2013-10-16)
Copyright (C) 1996-2009 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.6.32-358.14.1.el6.i686 (i686)
slang: 20201

Compiler:
Using built-in specs.
Target: i686-redhat-linux
Configured with: ../configure --prefix=/usr --mandir=/usr/share/man --infodir=/usr/share/info --with-bugurl=http://bugzilla.redhat.com/bugzilla --enable-bootstrap --enable-shared --enable-threads=posix --enable-checking=release --with-system-zlib --enable-__cxa_atexit --disable-libunwind-exceptions --enable-gnu-unique-object --enable-languages=c,c++,objc,obj-c++,java,fortran,ada --enable-java-awt=gtk --disable-dssi --with-java-home=/usr/lib/jvm/java-1.5.0-gcj-1.5.0.0/jre --enable-libgcj-multifile --enable-java-maintainer-mode --with-ecj-jar=/usr/share/java/eclipse-ecj.jar --disable-libjava-multilib --with-ppl --with-cloog --with-tune=generic --with-arch=i686 --build=i686-redhat-linux
Thread model: posix
gcc version 4.4.7 20120313 (Red Hat 4.4.7-3) (GCC) 

Configure options: 

Compilation CFLAGS: -Wall -pedantic -Wno-long-long -O2 -g -pipe -Wall -Wp,-D_FORTIFY_SOURCE=2 -fexceptions -fstack-protector --param=ssp-buffer-size=4 -m32 -march=i686 -mtune=generic -fasynchronous-unwind-tables

Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  +USE_FCNTL  -USE_FLOCK   
+USE_POP  +USE_IMAP  +USE_SMTP  
+USE_SSL_OPENSSL  -USE_SSL_GNUTLS  +USE_SASL  +USE_GSS  +HAVE_GETADDRINFO  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  -HAVE_START_COLOR  -HAVE_TYPEAHEAD  -HAVE_BKGDSET  
-HAVE_CURS_SET  -HAVE_META  -HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  
-EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  +LOCALES_HACK  -HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  -HAVE_LIBIDN  +HAVE_GETSID  -USE_HCACHE  
-ISPELL
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please visit http://bugs.mutt.org/.

vc.greeting
vvv.initials
}}}


--------------------------------------------------------------------------------
2015-07-09 21:52:26 UTC Kevin McCarthy <kevin@8t8.us>
* Added comment:
In [7e91a8855dc3a918b1d9e1b4fa32254c2c6b99a3]:
{{{
#!CommitTicketReference repository="" revision="7e91a8855dc3a918b1d9e1b4fa32254c2c6b99a3"
Use $(VAR) instead of @VAR@ in Makefile.am files.  (closes #3664)

The @VAR@ form is not overridable, such as the case for #3664 where the
reporter wanted to override DOTLOCK_GROUP.

It's doubtful targets need to be overriden, but it makes sense to be
consistent in the usage of automake substituted variables unless there
is a particular reason/bug to work around.
}}}

* owner changed to Kevin McCarthy <kevin@8t8.us>
* resolution changed to fixed
* status changed to closed
