Ticket:  2103
Status:  closed
Summary: [PATCH] add AES support in S/MIME

Reporter: jeffi@rcn.com
Owner:    mutt-dev

Opened:       2005-10-05 14:36:35 UTC
Last Updated: 2005-10-06 06:12:39 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Mutt does not offer aes{128,192,256} encryption in SMIME.
Ideally if the OpenSSL API supports displaying what algorithms are supported, that would be a better approach for populating this list.
>How-To-Repeat:
>Fix:
--- smime.c.orig        2005-10-05 09:04:43.000000000 -0400
+++ smime.c     2005-10-05 09:02:50.000000000 -0400
@@ -1952,8 +1952,9 @@
   case 3: /* encrypt (w)ith */
     msg->security |= ENCRYPT;
     switch (mutt_multi_choice (_("1: DES, 2: Triple-DES, 3: RC2-40,"
-                                " 4: RC2-64, 5: RC2-128, or (f)orget it? "),
-                              _("12345f"))) {
+                                " 4: RC2-64, 5: RC2-128, 6: AES128,"
+                                "7: AES192, 8: AES256, or (f)orget it? "),
+                              _("12345678f"))) {
     case 1:
        mutt_str_replace (&SmimeCryptAlg, "des");
        break;
@@ -1969,7 +1970,16 @@
     case 5:
        mutt_str_replace (&SmimeCryptAlg, "rc2-128");
        break;
-    case 6: /* forget it */
+    case 6:
+       mutt_str_replace (&SmimeCryptAlg, "aes128");
+       break;
+    case 7:
+       mutt_str_replace (&SmimeCryptAlg, "aes192");
+       break;
+    case 8:
+       mutt_str_replace (&SmimeCryptAlg, "aes256");
+       break;
+    case 9: /* forget it */
        break;
     }
     break;
}}}

--------------------------------------------------------------------------------
2005-10-07 00:12:39 UTC brendan
* Added comment:
{{{
Applied, thanks. Although the encrypt-with query string is getting dangerously 
long...
}}}

* resolution changed to fixed
* status changed to closed
