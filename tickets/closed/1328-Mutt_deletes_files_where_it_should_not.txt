Ticket:  1328
Status:  closed
Summary: Mutt deletes files where it should not.

Reporter: mmj@mmj.dk (Mads Martin Joergensen)
Owner:    mutt-dev

Opened:       2002-08-30 05:24:17 UTC
Last Updated: 2005-08-02 02:13:59 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.4i
Severity: normal

-- Please type your report below this line

Create an attachment
Toggle-unlink
Detach-file

Now the file in the attachment is deleted (unlinked) on disk!


-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/lib/gcc-lib/i486-suse-linux/3.2/specs
Configured with: ../configure --enable-threads=posix --prefix=/usr --with-local-prefix=/usr/local --infodir=/usr/share/info --mandir=/usr/share/man --libdir=/usr/lib --enable-languages=c,c++,f77,objc,java,ada --enable-libgcj --with-gxx-include-dir=/usr/include/g++ --with-slibdir=/lib --with-system-zlib --enable-shared --enable-__cxa_atexit i486-suse-linux
Thread model: posix
gcc version 3.2

- CFLAGS
-Wall -pedantic -Wall -O2 -march=i586 -mcpu=i686 -fmessage-length=0 -pipe -I. -D_GNU_SOURCE

-- Mutt Version Information

Mutt 1.4i (2002-05-29)
Copyright (C) 1996-2001 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.19aa1 (i686) [using ncurses 5.2]
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_GSS  +USE_SSL  +USE_SASL  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +COMPRESSED  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.

patch-1.3.99.rr.compressed.1

--- Begin /etc/Muttrc
ignore "from " received content- mime-version status x-status message-id
ignore sender references return-path lines
macro index \eb '/~b ' 'search in message bodies'
macro index \cb |urlview\n 'call urlview to extract URLs out of a message'
macro pager \cb |urlview\n 'call urlview to extract URLs out of a message'
macro generic <f1> "!/usr/bin/less /usr/share/doc/packages/mutt/manual.txt.gz\n" "Show Mutt documentation"
macro index   <f1> "!/usr/bin/less /usr/share/doc/packages/mutt/manual.txt.gz\n" "Show Mutt documentation"
macro pager   <f1> "!/usr/bin/less /usr/share/doc/packages/mutt/manual.txt.gz\n" "Show Mutt documentation"
set autoedit=yes
set pager_context=4
set pager_index_lines=10
set pager_stop=yes
set reverse_alias=yes
set sort=reverse-date-sent
set tilde=yes
--- End /etc/Muttrc


Received: (at submit) by bugs.guug.de; 30 Aug 2002 11:13:12 +0000
From mmj@suse.de Fri Aug 30 13:13:12 2002
Received: from ns.suse.de ([213.95.15.193] helo=Cantor.suse.de)
	by trithemius.gnupg.org with esmtp (Exim 3.35 #1 (Debian))
	id 17kjiC-0008Jb-00
	for <submit@bugs.guug.de>; Fri, 30 Aug 2002 13:13:12 +0200
Received: from Hermes.suse.de (Charybdis.suse.de [213.95.15.201])
	by Cantor.suse.de (Postfix) with ESMTP id 03E7E1450B
	for <submit@bugs.guug.de>; Fri, 30 Aug 2002 13:14:56 +0200 (MEST)
Date: Fri, 30 Aug 2002 13:14:55 +0200
From: Mads Martin Joergensen <mmj@suse.de>
Message-Id: <200208301114.g7UBEtKZ015843@D251.suse.de>
Subject: mutt-1.4i: Mutt deletes files where it should not.
To: submit@bugs.guug.de

Package: mutt
Version: 1.4i
Severity: important

-- Please type your report below this line


a) create an attachment
b) toggle unlink attachment with "u"
c) detach attachment, because you don't  want to send it

	and oops, the file is gone, even before you send the mail.
	

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/lib/gcc-lib/i486-suse-linux/2.95.3/specs
gcc version 2.95.3 20010315 (SuSE)

- CFLAGS
-Wall -pedantic -Wall -O2 -march=i486 -mcpu=i686 -pipe -I. -D_GNU_SOURCE

-- Mutt Version Information

Mutt 1.4i (2002-05-29)
Copyright (C) 1996-2001 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.18-64GB-SMP (i686) [using ncurses 5.2]
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_GSS  +USE_SSL  +USE_SASL  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +COMPRESSED  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.

patch-1.3.99.rr.compressed.1


>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2005-08-02 20:13:59 UTC brendan
* Added comment:
{{{
Fixed in CVS.
}}}

* resolution changed to fixed
* status changed to closed
