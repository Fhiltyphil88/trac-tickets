Ticket:  2842
Status:  closed
Summary: reply_regexp set in muttrc and during mutt session have different effects

Reporter: gautam@math.stanford.edu
Owner:    mutt-dev

Opened:       2007-03-13 02:41:34 UTC
Last Updated: 2013-04-09 02:07:35 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
When reply_regexp is set to "^re: *" in ~/.mutt/muttrc, the subject generated when replying to emails has one extra space. When reply_regexp is changed during a mutt session, and then changed back to the original value set, the extra space goes away.
>How-To-Repeat:
Put
    set reply_regexp='^re: *'
in ~/.muttrc. Then load mutt. Reply to a message whos subject begins with "Re:". The subject of the new message will be like "Re:  Original subject" (note the extra space).

Now do
   :set reply_regexp='asdf'
   :set reply_regexp='^re: *'
and reply to the same message you replied to earlier. The extra space dissapears, and the subject of the new message is "Re: Original subject" as it should be.
>Fix:
}}}

--------------------------------------------------------------------------------
2007-03-14 03:40:58 UTC rado
* Added comment:
{{{
1.5.14cvs does NOT have this, but I doubt it's mutt-related.
Try "mutt -n -F file" which has only the reply_regexp.
}}}

* status changed to assigned

--------------------------------------------------------------------------------
2007-03-14 03:40:59 UTC rado
* Added comment:
{{{
Removed from notify-list, reporter is enough.
Not reproducible.
Try http://WIKI.mutt.org/?DebugConfig
especially the "clean" muttrc part.
}}}

--------------------------------------------------------------------------------
2013-04-09 02:07:35 UTC kevin8t8
* Added comment:
I am also unable to reproduce this. Closing bug.

* resolution changed to worksforme
* status changed to closed
