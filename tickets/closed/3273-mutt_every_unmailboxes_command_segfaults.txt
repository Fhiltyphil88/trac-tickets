Ticket:  3273
Status:  closed
Summary: mutt: every unmailboxes command segfaults

Reporter: antonio@dyne.org
Owner:    brendan

Opened:       2009-06-17 22:59:57 UTC
Last Updated: 2009-06-17 23:21:14 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
Forwarding from http://bugs.debian.org/533459
Setting it as major because it is a segfault.

How to reproduce:
put the following in your .muttrc
{{{
set spoolfile=~/Mail/mbox
set folder=~/Mail
mailboxes `echo ~/Mail/*`
unmailboxes =from
unmailboxes =junk
unmailboxes =sent.gz
unmailboxes =from
}}}

Then touch ~/Mail/junk

run mutt, it will segfault.

I had a look at the code and tracked down the problem, which is unreproducible in 1.5.19-4.

This commit:
[5778:1dc96cc13a87:5779:7bc332ddd8fc]
replaced safe_strdup of tmp->path with strfcpy and it correctly removed FREE((*tmp)->path) at line 209, unfortunately it forgot to remove the same instruction at line 239, which is causing the segfault.

The attached patch fixes the problem, it was tested and it worked fine

--------------------------------------------------------------------------------
2009-06-17 23:00:22 UTC antonio@dyne.org
* Added attachment 533459-unmailboxes

--------------------------------------------------------------------------------
2009-06-17 23:03:52 UTC antonio@dyne.org
* Added comment:
commit diff URL, it seems that it was not converted:
http://dev.mutt.org/trac/changeset?new=5779%3A7bc332ddd8fc%40buffy.c&old=5778%3A1dc96cc13a87%40buffy.c

--------------------------------------------------------------------------------
2009-06-17 23:06:57 UTC brendan
* owner changed to brendan
* status changed to accepted

--------------------------------------------------------------------------------
2009-06-17 23:21:14 UTC Antonio Radici <antonio@dyne.org>
* Added comment:
(In [25e46aad362b]) Do not free statically-allocated buffy buffer.
Unbreaks unmailboxes, closes #3273.

* resolution changed to fixed
* status changed to closed
