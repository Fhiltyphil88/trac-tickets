Ticket:  3250
Status:  new
Summary: thread-link/-delink messages even when threading is not enabled

Reporter: antonio@dyne.org
Owner:    mutt-dev

Opened:       2009-06-12 23:33:37 UTC
Last Updated: 2009-06-12 23:33:37 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
Forwarding from http://bugs.debian.org/508423

{{{
When thread-sorting is disabled, linking and delinking messages with
link-thread and break-threads does not work but instead produces the
error "Threading is not enabled". Yet, sometimes it would be useful
to be able to thread-link when messages are sorted e.g. by sender or
recipient, not arranged in threads. I can't see how threading is
required to create/break threads, it's really only a display option,
or well, it should only be.

It would be nice if I could use link-thread and break-threads even
when $sort!=threads.
}}}
