Ticket:  1863
Status:  new
Summary: mutt: ispell should check Subject a.o. header fields, too

Reporter: Marco d'Itri <md@linux.it>
Owner:    mutt-dev

Opened:       2004-04-21 03:36:34 UTC
Last Updated: 2005-08-07 06:08:49 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.3.28-2.2
Severity: normal

[NOTE: this bug report has been submitted to the debian BTS as Bug#244812.
Please Cc all your replies to 244812@bugs.debian.org .]

From: Jan Minar <jjminar@fastmail.fm>
Subject: mutt: ispell should check Subject a.o. header fields, too
Date: Tue, 20 Apr 2004 05:28:43 +0200

Hi.

The spell command (bound to `i' in the message composition screen)
should check Subject, as well as other header fields where it is
reasonable: Keywords, Organization, Comments, and the free-form parts in
the other RFC 2822 fields (anything apart from the <>-delimited things,
it seems to me).  There are many other extensions to the standard, and
some of those fields should be spellchecked, some not.  Maybe some of
them should be checked against special dictionaries or patterns, that
would e.g. unveil likely incorrect syntax.

Cheers,
Jan.

-- System Information
Debian Release: 3.0
Architecture: i386
Kernel: Linux kontryhel 2.4.26-jan #3 SMP Mon Apr 19 05:00:00 CEST 2004 i686
Locale: LANG=C, LC_CTYPE=cs_CZ.ISO-8859-2

Versions of packages mutt depends on:
ii  libc6                    2.2.5-11.5      GNU C Library: Shared libraries an
ii  libncurses5              5.2.20020112a-7 Shared libraries for terminal hand
ii  libsasl7                 1.5.27-3        Authentication abstraction library
ii  postfix [mail-transport- 1.1.11-0.woody3 A high-performance mail transport 

-- 
   "To me, clowns aren't funny. In fact, they're kind of scary. I've wondered
 where this started and I think it goes back to the time I went to the circus,
			  and a clown killed my dad."


>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2004-04-21 03:36:34 UTC Marco d'Itri <md@linux.it>
* Added comment:
{{{
Package: mutt
Version: 1.3.28-2.2
Severity: normal

[NOTE: this bug report has been submitted to the debian BTS as Bug#244812.
Please Cc all your replies to 244812@bugs.debian.org .]

From: Jan Minar <jjminar@fastmail.fm>
Subject: mutt: ispell should check Subject a.o. header fields, too
Date: Tue, 20 Apr 2004 05:28:43 +0200

Hi.

The spell command (bound to `i' in the message composition screen)
should check Subject, as well as other header fields where it is
reasonable: Keywords, Organization, Comments, and the free-form parts in
the other RFC 2822 fields (anything apart from the <>-delimited things,
it seems to me).  There are many other extensions to the standard, and
some of those fields should be spellchecked, some not.  Maybe some of
them should be checked against special dictionaries or patterns, that
would e.g. unveil likely incorrect syntax.

Cheers,
Jan.

-- System Information
Debian Release: 3.0
Architecture: i386
Kernel: Linux kontryhel 2.4.26-jan #3 SMP Mon Apr 19 05:00:00 CEST 2004 i686
Locale: LANG=C, LC_CTYPE=cs_CZ.ISO-8859-2

Versions of packages mutt depends on:
ii  libc6                    2.2.5-11.5      GNU C Library: Shared libraries an
ii  libncurses5              5.2.20020112a-7 Shared libraries for terminal hand
ii  libsasl7                 1.5.27-3        Authentication abstraction library
ii  postfix [mail-transport- 1.1.11-0.woody3 A high-performance mail transport 

-- 
  "To me, clowns aren't funny. In fact, they're kind of scary. I've wondered
where this started and I think it goes back to the time I went to the circus,
			  and a clown killed my dad."
}}}

--------------------------------------------------------------------------------
2004-04-21 16:45:47 UTC Paul Walker <paul@black-sun.demon.co.uk>
* Added comment:
{{{
Hi,

> The spell command (bound to `i' in the message composition screen) should
> check Subject, as well as other header fields where it is reasonable:
> Keywords, Organization, Comments, and the free-form parts in the other RFC
> 2822 fields (anything apart from the <>-delimited things, it seems to me). 
> There are many other extensions to the standard, and some of those fields
> should be spellchecked, some not.  Maybe some of them should be checked
> against special dictionaries or patterns, that would e.g. unveil likely
> incorrect syntax.

Forgive the question, but why is this a bug submitted against mutt, rather
than against ispell...? Are you requesting mutt filter the messages before
supplying them to ispell, or that ispell should ignore certain formations?

-- 
Paul

Jesus saves! Allah protects! And Cthulhu thinks you'd make a nice sandwich!
}}}

--------------------------------------------------------------------------------
2004-04-21 19:54:14 UTC Jan Minar <jjminar@fastmail.fm>
* Added comment:
{{{
On Wed, Apr 21, 2004 at 12:45:47AM +0100, Paul Walker wrote:
> Forgive the question, but why is this a bug submitted against mutt, rather
> than against ispell...? Are you requesting mutt filter the messages before
> supplying them to ispell, or that ispell should ignore certain formations?

Well, mutt apparently supplies only the body to ispell, while it should
also pass certain other things.  But the idea that ispell would grok
RFC822 (+MIME and the stuff) messages is good.

It's a mutt's normal bug, while ispell's wishlist, BTW.

Jan.

-- 
  "To me, clowns aren't funny. In fact, they're kind of scary. I've wondered
where this started and I think it goes back to the time I went to the circus,
			  and a clown killed my dad."
}}}

--------------------------------------------------------------------------------
2005-08-08 00:08:49 UTC brendan
* Added comment:
{{{
change-request
}}}
