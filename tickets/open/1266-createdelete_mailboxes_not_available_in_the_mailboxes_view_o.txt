Ticket:  1266
Status:  reopened
Summary: create/delete mailboxes not available in the mailboxes view of browser when using imap

Reporter: "Tabor J. Wells" <twells@fsckit.net>
Owner:    mutt-dev

Opened:       2005-07-24 16:02:25 UTC
Last Updated: 2007-02-07 19:35:57 UTC

Priority:  minor
Component: IMAP
Keywords:  IMAP

--------------------------------------------------------------------------------
Description:
{{{
From twells@fsckit.net Mon Jul 08 20:49:54 2002
Received: from 70.muca.bstn.bstnmaco.dsl.att.net ([12.98.14.70] helo=pulse.fsckit.net)
	by trithemius.gnupg.org with esmtp (Exim 3.35 #1 (Debian))
	id 17Rda6-0007X1-00
	for <submit@bugs.guug.de>; Mon, 08 Jul 2002 20:49:54 +0200
Received: from twells by pulse.fsckit.net with local (Exim)
	id 17Rdbb-0000eg-00
	for submit@bugs.guug.de; Mon, 08 Jul 2002 14:51:27 -0400
Subject: mutt-1.4i: create/delete mailboxes not available in the mailboxes view of browser when using imap
To: submit@bugs.guug.de
Message-Id: <E17Rdbb-0000eg-00@pulse.fsckit.net>
From: "Tabor J. Wells" <twells@fsckit.net>
Date: Mon, 08 Jul 2002 14:51:27 -0400

Package: mutt
Version: 1.4i
Severity: normal

-- Please type your report below this line

In the Mailboxes view of the browser, where it looks like:

  1                            imaps://localhost/INBOX
  2     19                     imaps://localhost/INBOX.folder

attempting to delete (or create) a mailbox results in "Delete is only 
supported for IMAP mailboxes" (or "Create is only supported for IMAP 
mailboxes" as appropriate).

In the Directory view it appears to work as expected.

This is somewhat counter-intuitive. :) 

Thanks,

Tabor

>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2007-02-08 13:35:57 UTC rado
* Added comment:
{{{
Looking for best alternative.
}}}

* status changed to new

--------------------------------------------------------------------------------
2007-02-08 13:35:58 UTC rado
* Added comment:
{{{
Move to IMAP category.

Well, it's counter-intuitive only when you have IMAP-only folders listed:
because this then looks like normal folder browsing.
Which it is _not_! :)

A technical solution would be to have a separate mode for mailboxes, but that's overkill?
Alternatively the help text could be augumented to point this out.
Add 'not in "mailboxes"' after each "(IMAP only)"?

Or check whether it's an IMAP folder and print a warning message for those IMAP-only operations with that extra
note "not for mailboxes-view".
}}}
