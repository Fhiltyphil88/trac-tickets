Ticket:  2073
Status:  new
Summary: mutt should have a CA certs directory option

Reporter: dave@biff.org.uk
Owner:    mutt-dev

Opened:       2005-09-18 23:06:24 UTC
Last Updated: 2010-08-24 17:41:41 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
mutt has the ssl_ca_certificates_file option, but an option like
"ssl_ca_certificates_dir" to pick up all the CA certs in a directory
would be useful.
>How-To-Repeat:
>Fix:
Unknown
}}}

--------------------------------------------------------------------------------
2010-08-24 17:41:41 UTC rsc
* cc changed to 311493@bugs.debian.org, mutt.org@robert-scheck.de
* milestone changed to 
