Ticket:  1360
Status:  new
Summary: folder-leave-hook

Reporter: Oswald Buddenhagen <ossi@kde.org>
Owner:    mutt-dev

Opened:       2002-10-26 10:14:36 UTC
Last Updated: 2008-08-21 13:47:05 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.4i
Severity: wishlist

i want mutt to fork a (wrapped) isync process in the background when i
leave a folder. but there don't seem to be hooks which are invoked when
a context is left, only when one is entered.
>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2002-10-26 10:14:36 UTC Oswald Buddenhagen <ossi@kde.org>
* Added comment:
{{{
Package: mutt
Version: 1.4i
Severity: wishlist

i want mutt to fork a (wrapped) isync process in the background when i
leave a folder. but there don't seem to be hooks which are invoked when
a context is left, only when one is entered.
}}}

--------------------------------------------------------------------------------
2005-08-02 20:23:00 UTC brendan
* Added comment:
{{{
change-request.
}}}

--------------------------------------------------------------------------------
2005-08-24 13:34:20 UTC rado
* Added comment:
{{{
removed gnats tests
}}}

--------------------------------------------------------------------------------
2008-08-21 08:59:24 UTC pdmef
* Added comment:
Entering a folder always means you've just left another one (except when you start mutt). So why not use:

{{{
folder-hook . <cmd>
}}}

?

--------------------------------------------------------------------------------
2008-08-21 13:35:46 UTC ossi
* Added comment:
and how would the script know the name of the folder just left?

--------------------------------------------------------------------------------
2008-08-21 13:47:05 UTC pdmef
* Added comment:
Sorry, I realised that short after writing that comment, too. You could go with macros in the meantime which can be created using folder-hooks so they know the name of the folder. Though that requires to generate one line per folder of config... what folder-leave-hook would require, too.

For a real solution, mutt needs an event model I guess.
