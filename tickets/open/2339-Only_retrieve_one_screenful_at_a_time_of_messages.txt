Ticket:  2339
Status:  new
Summary: Only retrieve one screenful at a time of messages

Reporter: hobart@gmail.com
Owner:    mutt-dev

Opened:       2006-07-07 19:16:53 UTC
Last Updated: 2007-04-07 21:27:19 UTC

Priority:  minor
Component: IMAP
Keywords:  

--------------------------------------------------------------------------------
Description:
When using mutt to access IMAP folders with several thousand messages, it would be nice for an option to behave like current Pine revisions do, grabbing only the most recent screenful-worth of headers, and grabbing headers only as needed (i.e. if the user chooses to change the sort order, etc).

--------------------------------------------------------------------------------
2007-04-07 21:27:19 UTC brendan
* Updated description:
When using mutt to access IMAP folders with several thousand messages, it would be nice for an option to behave like current Pine revisions do, grabbing only the most recent screenful-worth of headers, and grabbing headers only as needed (i.e. if the user chooses to change the sort order, etc).
* milestone changed to 2.0
