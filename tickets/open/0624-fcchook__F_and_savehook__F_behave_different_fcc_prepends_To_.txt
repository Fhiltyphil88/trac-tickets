Ticket:  624
Status:  new
Summary: "fcc-hook  . +%F"   and   "save-hook . +%F" behave different: fcc pre-pends "To "

Reporter: Gregor Zattler <texmex@uni.de>
Owner:    mutt-dev

Opened:       2001-05-26 14:36:35 UTC
Last Updated: 2005-08-18 19:15:02 UTC

Priority:  trivial
Component: mutt
Keywords:  patch

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.3.17i (2001-03-28)
Severity: wishlist

-- Please type your report below this line
This Problem occurs in all Versions of mutt:

 Saving a message using "save-hook . +%F" saves it to        +"Real Name"
Sending a message using "fcc-hook  . +%F" saves/fccs it to   +"To Real Name"
                                                               ^^^
This is annoying, because this "feature" makes it impossible to use
threads in correspondences.

The manual states:
"%F author name, or recipient name if the message is from you" but
this is obviously wrong.

As a work around i use dozends of lines like this in my muttrc:

fcc-save-hook 'texmex@uni.de' +'Gregor Zattler'

But every time i write or reply to a new person i have to do this
*before* writing and source the muttrc. As a matter of fact i don't do
this and end up having dozends of "To Real Name" mail folders and
"Real Name" mail folders. As a consequence i cannot read a whole
correspondence but only my or the recipient's part of it.

I think %F is established with this behavior. So the solution would be
to add a new printf() -like sequence "%R" which does the trick. Or to
add a "%r" printf() -like sequence which does for the recipient what
"%n" does for the author":

%n author's real name (or address if missing)
%r recipient's real name (or address if missing)


This would make mutt much more useable for me because i communicate
with persons not with email addresses.

Ciao, Gregor

-- Mutt Version Information

Mutt 1.3.17i (2001-03-28)
Copyright (C) 1996-2000 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.2.18pre21 [using ncurses 5.0]
Compile options:
-DOMAIN
+DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
-USE_POP  -USE_IMAP  -USE_GSS  -USE_SSL  -USE_SASL  
-HAVE_REGCOMP  +USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+HAVE_PGP  +BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  +LOCALES_HACK  -HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  -HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
SHAREDIR="/usr/local/stow/mutt//share/mutt"
SYSCONFDIR="/usr/local/stow/mutt//etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.



>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2001-07-02 18:18:03 UTC Gregor Zattler <texmex@uni.de>
* Added attachment mutt-without-annoying-To_hdrline.c.patch
* Added attachment mutt-without-annoying-To_init.h.patch
* Added comment:
mutt-without-annoying-To_hdrline.c.patch

* Added comment:
mutt-without-annoying-To_init.h.patch

* Added comment:
{{{
Hi,

with much help from a friend i produced a patch via copy-and-paste to
work around my problem. Attached to this mail are a patch to hdrline.c
which introduces two new format flags %r and %R for ``index_format''
and a patch to init.h (for documentation). %R and %r behave as
described/wished in my bugreport/wishlist which i full quote below.

Perhaps this patch motivates someone who is abel to program C to
produce a nicer solution.

Ciao, Gregor

* Gregor Zattler <texmex@uni.de> [Fre 25 Mai 2001 21:36:35 GMT]:
> Package: mutt
> Version: 1.3.17i
> Severity: wishlist
> 
> This Problem occurs in all Versions of mutt:
> 
>  Saving a message using "save-hook . +%F" saves it to        +"Real Name"
> Sending a message using "fcc-hook  . +%F" saves/fccs it to   +"To Real Name"
>                                                                ^^^
> This is annoying, because this "feature" makes it impossible to use
> threads in correspondences.
> 
> The manual states:
> "%F author name, or recipient name if the message is from you" but
> this is obviously wrong.
> 
> As a work around i use dozends of lines like this in my muttrc:
> 
> fcc-save-hook 'texmex@uni.de' +'Gregor Zattler'
> 
> But every time i write or reply to a new person i have to do this
> *before* writing and source the muttrc. As a matter of fact i don't do
> this and end up having dozends of "To Real Name" mail folders and
> "Real Name" mail folders. As a consequence i cannot read a whole
> correspondence but only my or the recipient's part of it.
> 
> I think %F is established with this behavior. So the solution would be
> to add a new printf() -like sequence "%R" which does the trick. Or to
> add a "%r" printf() -like sequence which does for the recipient what
> "%n" does for the author":
> 
> %n author's real name (or address if missing)
> %r recipient's real name (or address if missing)
> 
> 
> This would make mutt much more useable for me because i communicate
> with persons not with email addresses.
> 
> Ciao, Gregor
> 
> 
> Mutt 1.3.17i (2001-03-28)
> Copyright (C) 1996-2000 Michael R. Elkins and others.
> Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
> Mutt is free software, and you are welcome to redistribute it
> under certain conditions; type `mutt -vv' for details.
> 
> System: Linux 2.2.18pre21 [using ncurses 5.0]
> Compile options:
> -DOMAIN
> +DEBUG
> -HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
> +USE_FCNTL  -USE_FLOCK
> -USE_POP  -USE_IMAP  -USE_GSS  -USE_SSL  -USE_SASL  
> -HAVE_REGCOMP  +USE_GNU_REGEX  
> +HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
> +HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
> +HAVE_PGP  +BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
> +ENABLE_NLS  +LOCALES_HACK  -HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
> +HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  -HAVE_GETADDRINFO  
> ISPELL="/usr/bin/ispell"
> SENDMAIL="/usr/sbin/sendmail"
> MAILPATH="/var/mail"
> SHAREDIR="/usr/local/stow/mutt//share/mutt"
> SYSCONFDIR="/usr/local/stow/mutt//etc"
> EXECSHELL="/bin/sh"
> -MIXMASTER
> To contact the developers, please mail to <mutt-dev@mutt.org>.
> To report a bug, please use the flea(1) utility.
> 
>
}}}

--------------------------------------------------------------------------------
2001-08-27 18:16:52 UTC Gregor Zattler <texmex@uni.de>
* Added comment:
{{{
Hi mutt debuggers and mutt developers,

i reportet a problem (bug #624) and later provided a solution (patch)
but it did'nt get in the source. There were no further comments, so i
don't know why. But i would like to know because it's very useful for
me and i think for others too.

The 2 Diffs i attached in my last posting work too against
mutt-1.3.20 and mutt-1.3.21.

Ciao, Gregor
}}}

--------------------------------------------------------------------------------
2001-08-29 02:25:08 UTC Magnus Bodin <magnus@bodin.org>
* Added comment:
{{{
On Mon, Aug 27, 2001 at 01:16:52AM +0200, Gregor Zattler wrote:
> Hi mutt debuggers and mutt developers,
> 
> i reportet a problem (bug #624) and later provided a solution (patch)
> but it did'nt get in the source. There were no further comments, so i
> don't know why. But i would like to know because it's very useful for
> me and i think for others too.
> 
> The 2 Diffs i attached in my last posting work too against
> mutt-1.3.20 and mutt-1.3.21.

Perhaps of the reason you stated yourself, July 2nd, that a more
experienced c-programmer ought to polish the code. 

I haven't looked at it, but thats my guess.

/magnus

-- 
Word of today: mao jin (towel)
http://x42.com/i/cn/pct/maojin.jpg
}}}

--------------------------------------------------------------------------------
2005-08-18 11:59:22 UTC ab
* Added comment:
{{{
Despam and devirus "unformatted:" text, and tag wishlist.
}}}

--------------------------------------------------------------------------------
2005-08-19 12:06:03 UTC Gregor Zattler <telegraph@gmx.net>
* Added comment:
{{{
Hi Alain,  hi mutt-dev
* Alain Bench <veronatif@free.fr> [17. Aug. 2005]:
> Synopsis: "fcc-hook  . +%F"   and   "save-hook . +%F" behave different: fcc pre-pends "To "
> 
> **** Comment added by ab on Wed, 17 Aug 2005 18:59:22 +0200 ****
>  Despam and devirus "unformatted:" text, and tag wishlist.

Thanks for de-spamming this bug report.  

For the record: This wishlist bug is about saving and fcc-ing
Messages in mail folders according to the real name of your
correspondent.  

It contains a patch to hdrline.c which introduces two new format
flags %r and %R for ``index_format'' and a patch to init.h (for
documentation).  This patch applies to every mutt version from mutt
1.3.19 to 1.5.10 and is very helpful.  Thus my *very* sporadic
email correspondence with Thomas Roessler is automatically stored
in "~/Mail/Thomas Roessler".

I would appreciate it if these patch was accepted in mainstream
mutt.

Ciao, Gregor
-- 
 -... --- .-. . -.. ..--.. ...-.-
}}}
