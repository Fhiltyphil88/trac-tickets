Ticket:  3663
Status:  new
Summary: mutt.org links for screenshots & Michael Elkin's homepage is broken

Reporter: MichaelRay
Owner:    mutt-dev

Opened:       2013-10-23 19:48:37 UTC
Last Updated: 2013-10-23 19:48:37 UTC

Priority:  minor
Component: doc
Keywords:  broken URL

--------------------------------------------------------------------------------
Description:

On http://www.mutt.org/ the link for Michael Elkins links to:

http://www.cs.hmc.edu/~me/ which 403s. 

I am not aware of the correct link to update it to.

Also, 

http://wiki.mutt.org/index.cgi?ConfigList/ScreenShots is non-existent for me.

http://www.mutt.org/screenshots/ appears to be somewhat better (it exists).


