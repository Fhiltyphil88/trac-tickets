Ticket:  3254
Status:  new
Summary: please make postponed searcheable

Reporter: antonio@dyne.org
Owner:    mutt-dev

Opened:       2009-06-13 09:36:56 UTC
Last Updated: 2009-06-13 09:36:56 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
Fowarding from http://bugs.debian.org/492278

{{{
Hello,

I'd like to be able to sort and search messages in the postponed folder
like every other folder. Unfortunately, this does not seem to be
possible. It would make mutt even better if this feature could be added.
}}}

Obviously the user can use c? =postponed but it seems that he wants to have the folder searcheable when he recall a message.
