Ticket:  2520
Status:  new
Summary: Mutt line editor displays nonspacing characters at control escapes

Reporter: dalias@aerifal.cx
Owner:    mutt-dev

Opened:       2006-10-10 08:18:12 UTC
Last Updated: 2007-11-20 14:23:40 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
>How-To-Repeat:
>Fix:
enter.c: lines 38 and 61 should use >= 0 instead of > 0
(line numbers for version 1.4.2.2)
}}}

--------------------------------------------------------------------------------
2007-11-20 14:23:40 UTC pdmef
* Added comment:
These lines are in my_wcwidth() and the my_addwch() functions, the code in both cases is line 2 in:
{{{
int n = wcwidth (wc);
if (IsWPrint (wc) && n > 0)
}}}
I'd rather say that if wcwidth(wc)==0, they should return 0 for the wide null character since I assume wcwidth() for L’\0' should be 0 and iswprint() 0, too.
