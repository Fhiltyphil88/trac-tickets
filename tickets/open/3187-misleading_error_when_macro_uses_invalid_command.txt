Ticket:  3187
Status:  new
Summary: misleading error when macro uses invalid command

Reporter: antonio@dyne.org
Owner:    mutt-dev

Opened:       2009-02-08 14:12:17 UTC
Last Updated: 2009-02-20 18:38:57 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
Forwarding from http://bugs.debian.org/497273

---

I have a macro


  macro index             \Co         '<tag-prefix><save-message> 


and previously, I had index,pager instead of just index in the
above. The result was that when I entered \Co in the pager, I was
told that "Key is not bound. Press '?' for help.". The macro works
fine in the index.

It took me a while to figure out that this was due to <tag-prefix>
being an invalid command in the pager. mutt should either just
ignore it in the pager, or provide a better error message.


--------------------------------------------------------------------------------
2009-02-20 18:38:57 UTC madduck
* cc changed to dev.mutt.org@pobox.madduck.net
