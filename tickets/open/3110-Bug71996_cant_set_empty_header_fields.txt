Ticket:  3110
Status:  new
Summary: Bug#71996: can't set empty header fields

Reporter: myon
Owner:    mutt-dev

Opened:       2008-08-23 16:41:33 UTC
Last Updated: 2009-01-04 20:36:46 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:

{{{
An old one I couldn't find in the bug list:

----- Forwarded message from Gerfried Fuchs <alfie@debian.org> -----

Date: Tue, 19 Sep 2000 09:18:03 +0200
From: Gerfried Fuchs <alfie@debian.org>
Reply-To: Gerfried Fuchs <alfie@debian.org>, 71996@bugs.debian.org
To: Debian Bug Tracking System <submit@bugs.debian.org>
Subject: Bug#71996: can't set empty header fields

Package: mutt
Version: 1.2.5-1
Severity: wishlist

Currently it is not possible to set an epmty header field, which I think
would be great, to have e.g.
Pgp: 
Attach: 
per Default on the screen. There are already other empty headers on the
screen - so why it's not possible to have this also as a user-feature?
}}}

--------------------------------------------------------------------------------
2009-01-04 20:36:46 UTC brendan
* Updated description:

{{{
An old one I couldn't find in the bug list:

----- Forwarded message from Gerfried Fuchs <alfie@debian.org> -----

Date: Tue, 19 Sep 2000 09:18:03 +0200
From: Gerfried Fuchs <alfie@debian.org>
Reply-To: Gerfried Fuchs <alfie@debian.org>, 71996@bugs.debian.org
To: Debian Bug Tracking System <submit@bugs.debian.org>
Subject: Bug#71996: can't set empty header fields

Package: mutt
Version: 1.2.5-1
Severity: wishlist

Currently it is not possible to set an epmty header field, which I think
would be great, to have e.g.
Pgp: 
Attach: 
per Default on the screen. There are already other empty headers on the
screen - so why it's not possible to have this also as a user-feature?
}}}
* milestone changed to 2.0
* priority changed to minor
* type changed to enhancement
