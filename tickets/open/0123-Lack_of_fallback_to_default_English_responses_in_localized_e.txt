Ticket:  123
Status:  new
Summary: Lack of fallback to default English responses in localized env.

Reporter: "Andrey A. Chernov" <ache@nagual.pp.ru>
Owner:    mutt-dev

Opened:       2000-05-14 19:30:16 UTC
Last Updated: 2007-03-27 02:17:13 UTC

Priority:  trivial
Component: mutt
Keywords:  patch

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.2i
Severity: normal

-- Please type your report below this line

The BIG problem in the Mutt is lack of fallback to English responce:
keyboard usually stays in English mode and don't need to be switched to
native mode just for simple yes/no reply (too many keys must be pressed),
moreover, it is too hard to remember all that localized responces (for
mutt_multi_choice case) while English responces are the same from system
to system, i.e. user can use English responces on _any_ computer without
searching a keys. Moreover, since all Mutt commands are English it is very
contr-intuitive to use localized responces on English commands (as you can
guess I am VERY against localized Mutt commands without fallback to
English defaults too). English commands/responces must be enabled in any
case _after_ localized commands/responces considered, even MS Windows
implement this way in their localized software! This problem was discussed
in mutt-dev mailing list long time ago and the next patch got many
positive responses but constantly not included in the next Mutt versions
for reason unknown to me.


--- curs_lib.c.old	Wed Sep  1 19:39:20 1999
+++ curs_lib.c	Sun Sep 26 23:47:03 1999
@@ -156,6 +156,16 @@
       def = 0;
       break;
     }
+    else if (tolower(ch.ch) == 'y')
+    {
+      def = 1;
+      break;
+    }
+    else if (tolower(ch.ch) == 'n')
+    {
+      def = 0;
+      break;
+    }
     else
     {
       BEEP();
@@ -413,8 +423,9 @@
 {
   event_t ch;
   int choice;
-  char *p;
+  char *p, *nletters;
 
+  nletters = _(letters);
   mvaddstr (LINES - 1, 0, prompt);
   clrtoeol ();
   FOREVER
@@ -428,6 +439,12 @@
     }
     else
     {
+      p = strchr (nletters, ch.ch);
+      if (p)
+      {
+	choice = p - nletters + 1;
+	break;
+      }
       p = strchr (letters, ch.ch);
       if (p)
       {
--- browser.c.orig	Fri Sep 24 01:08:08 1999
+++ browser.c	Sun Sep 26 23:46:25 1999
@@ -750,7 +750,7 @@
 	  switch (mutt_multi_choice ((reverse) ?
 	      _("Reverse sort by (d)ate, (a)lpha, si(z)e or do(n)'t sort? ") :
 	      _("Sort by (d)ate, (a)lpha, si(z)e or do(n)'t sort? "),
-	      _("dazn")))
+	      N_("dazn")))
 	  {
 	    case -1: /* abort */
 	      resort = 0;
--- commands.c.orig	Wed Jul  7 02:56:24 1999
+++ commands.c	Sun Sep 26 23:46:25 1999
@@ -337,7 +337,7 @@
   switch (mutt_multi_choice (reverse ?
 			     _("Rev-Sort (d)ate/(f)rm/(r)ecv/(s)ubj/t(o)/(t)hread/(u)nsort/si(z)e/s(c)ore?: ") :
 			     _("Sort (d)ate/(f)rm/(r)ecv/(s)ubj/t(o)/(t)hread/(u)nsort/si(z)e/s(c)ore?: "),
-			     _("dfrsotuzc")))
+			     N_("dfrsotuzc")))
   {
   case -1: /* abort - don't resort */
     return -1;
--- compose.c.orig	Wed Aug 25 10:23:12 1999
+++ compose.c	Sun Sep 26 23:46:25 1999
@@ -130,7 +130,7 @@
   struct pgp_vinfo *pgp = pgp_get_vinfo(PGP_SIGN);
 
   switch (mutt_multi_choice (_("(e)ncrypt, (s)ign, sign (a)s, (b)oth, select (m)ic algorithm, or (f)orget it? "),
-			     _("esabmf")))
+			     N_("esabmf")))
   {
   case 1: /* (e)ncrypt */
     bits |= PGPENCRYPT;
--- muttlib.c.orig	Sun Apr  9 14:39:02 2000
+++ muttlib.c	Fri May 12 11:26:58 2000
@@ -671,7 +671,7 @@
   if (*append == 0 && access (fname, F_OK) == 0)
   {
     switch (mutt_multi_choice
-	    (_("File exists, (o)verwrite, (a)ppend, or (c)ancel?"), _("oac")))
+	    (_("File exists, (o)verwrite, (a)ppend, or (c)ancel?"), N_("oac")))
     {
       case -1: /* abort */
       case 3:  /* cancel */



-- Mutt Version Information

Mutt 1.2i (2000-05-09)
Copyright (C) 1996-2000 Michael R. Elkins É ÄÒÕÇÉÅ.
Mutt ÒÁÓÐÒÏÓÔÒÁÎÑÅÔÓÑ âåú ëáëéè-ìéâï çáòáîôéê; ÄÌÑ ÐÏÌÕÞÅÎÉÑ ÂÏÌÅÅ ÐÏÄÒÏÂÎÏÊ
ÉÎÆÏÒÍÁÃÉÉ ××ÅÄÉÔÅ `mutt -vv'. Mutt Ñ×ÌÑÅÔÓÑ Ó×ÏÂÏÄÎÙÍ ÐÒÏÇÒÁÍÍÎÙÍ ÏÂÅÓÐÅÞÅÎÉÅÍ.
÷Ù ÍÏÖÅÔÅ ÒÁÓÐÒÏÓÔÒÁÎÑÔØ ÅÇÏ ÐÒÉ ÓÏÂÌÀÄÅÎÉÉ ÏÐÒÅÄÅÌÅÎÎÙÈ ÕÓÌÏ×ÉÊ; ÄÌÑ ÐÏÌÕÞÅÎÉÑ
ÂÏÌÅÅ ÐÏÄÒÏÂÎÏÊ ÉÎÆÏÒÍÁÃÉÉ ××ÅÄÉÔÅ `mutt -vv'.

System: FreeBSD 5.0-CURRENT [using ncurses 5.0]
ðÁÒÁÍÅÔÒÙ ËÏÍÐÉÌÑÃÉÉ:
-DOMAIN
+DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  -USE_FCNTL  +USE_FLOCK
+USE_IMAP  -USE_GSS  -USE_SSL  +USE_POP  +HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  +ENABLE_NLS
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
SHAREDIR="/usr/local/share/mutt"
SYSCONFDIR="/usr/local/etc"
ISPELL="/usr/local/bin/ispell"
þÔÏÂÙ Ó×ÑÚÁÔØÓÑ Ó ÒÁÚÒÁÂÏÔÞÉËÁÍÉ, ÉÓÐÏÌØÚÕÊÔÅ ÁÄÒÅÓ <mutt-dev@mutt.org>.
þÔÏÂÙ ÓÏÏÂÝÉÔØ ÏÂ ÏÛÉÂËÅ, ÉÓÐÏÌØÚÕÊÔÅ ÐÒÏÇÒÁÍÍÕ muttbug.



>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2001-06-05 23:12:36 UTC Brendan Cully <brendan@kublai.com>
* Added comment:
{{{
severity 123 wishlist
thanks
}}}

--------------------------------------------------------------------------------
2003-10-08 04:55:06 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
# solved by Lars Hecking
close 676
# no infos, no followups
close 1092
# reporter requested closing
close 1307
# unreproducible, no followups
close 1499
merge 907 1437
merge 1083 1176
severity 1562 wishlist
merge 1385 1562
retitle 1128 new subject breaks thread option
tags 64 patch
tags 123 patch
tags 1484 patch
tags 1489 patch
-- someone speaking Spanish should talk to #1572 reporter
}}}
