Ticket:  2017
Status:  new
Summary: mutt_addr_is_user() "null address is assumed to be the user"

Reporter: ttakah@lapis.plala.or.jp
Owner:    mutt-dev

Opened:       2005-08-01 11:35:55 UTC
Last Updated: 2009-06-30 14:40:33 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
In alias.c, mutt_addr_is_user():
>   /* NULL address is assumed to be the user. */
>   if (!addr)
>   {
>     dprint (5, (debugfile, "mail_addr_is_user: yes, NULL address\n"));
>     return 1;
>   }

This causes a strange effect: A message with no headers is shown
as sent from me, on the index menu.

I'm afraid mutt_addr_is_user() is misused at almost all its
occurences. We should not assume that env->from matches $alternates
just because mutt_addr_is_user(env->from) returns 1.

Why not return 0?

>How-To-Repeat:
Delete FROM header of a message.
The message will be flagged as "F" with the default $to_chars and $index_format.

>Fix:
Simply return 0?
But that could have side effects.

}}}

--------------------------------------------------------------------------------
2008-07-10 01:08:56 UTC vinc17
* cc changed to vinc17

--------------------------------------------------------------------------------
2009-06-30 14:40:33 UTC pdmef
* Added comment:
The same counts for some mailing lists gatewayed from mail to news and back. Some use completely broken From: headers in usenet that end up as messages marked to come from me in the mailing list.

* Updated description:
{{{
In alias.c, mutt_addr_is_user():
>   /* NULL address is assumed to be the user. */
>   if (!addr)
>   {
>     dprint (5, (debugfile, "mail_addr_is_user: yes, NULL address\n"));
>     return 1;
>   }

This causes a strange effect: A message with no headers is shown
as sent from me, on the index menu.

I'm afraid mutt_addr_is_user() is misused at almost all its
occurences. We should not assume that env->from matches $alternates
just because mutt_addr_is_user(env->from) returns 1.

Why not return 0?

>How-To-Repeat:
Delete FROM header of a message.
The message will be flagged as "F" with the default $to_chars and $index_format.

>Fix:
Simply return 0?
But that could have side effects.

}}}
