Ticket:  3535
Status:  new
Summary: check for new mail from the internal viewer in maildir mailboxes

Reporter: miguel
Owner:    mutt-dev

Opened:       2011-08-21 10:36:16 UTC
Last Updated: 2011-08-21 10:36:16 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
When a message is viewed in a maildir mailbox, mutt does not check for new mail in the folders specified by the mailboxes command until back in the index mode. It would be nice if mutt could notify of new mail also in the pager mode.
