Ticket:  498
Status:  new
Summary: mutt does not process PGP/MIME in digests

Reporter: "Marco d'Itri" <md@Linux.IT>
Owner:    mutt-dev

Opened:       2001-03-08 14:47:47 UTC
Last Updated: 2009-06-30 14:38:33 UTC

Priority:  trivial
Component: crypto
Keywords:  #1070

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.3.15-2
Severity: normal

[NOTE: this bug report has been submitted to the debian BTS as Bug#88860.
Please Cc all your replies to 88860@bugs.debian.org.]

From: JP Sugarbroad <taral@taral.net>
Subject: mutt does not process PGP/MIME in digests
Date: Wed, 7 Mar 2001 13:43:05 -0600

When viewing digests with the 'v' key, mutt does not process the
PGP/MIME signed messages that the digest might contain.

-- System Information
Debian Release: testing/unstable
Architecture: i386
Kernel: Linux dragon 2.4.2-ac3 #1 Mon Feb 26 00:01:32 CST 2001 i686

Versions of packages mutt depends on:
ii  libc6                     2.2.2-1        GNU C Library: Shared libraries an
ii  libncurses5               5.0-8          Shared libraries for terminal hand
ii  libsasl7                  1.5.24-5       Authentication abstraction library
ii  postfix [mail-transport-a 0.0.20010228-2 A high-performance mail transport 

-- 
Taral <taral@taral.net>
Please use PGP/GPG to send me mail.
"Never ascribe to malice what can as easily be put down to stupidity."


>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2005-09-06 22:54:20 UTC brendan
* Added comment:
{{{
Cross-reference with #1070.
}}}

--------------------------------------------------------------------------------
2008-01-11 01:28:07 UTC jidanni
* Added comment:
Worst of all, my .muttrc,
{{{
spam "X-Spam-Status: .*score=([-0-9.]+)" %1
set index_format="%H %4C %Z %{%b %d} %-15.15L (%?l?%4l&%4c?) %s"
set sort=spam
}}}
is useless on digests, because after I hit v, there is no "o" etc.
sort commands available anymore.

You should think like gnus, which knows a digest is just like a little
mailbox in itself, with all the same commands available.

--------------------------------------------------------------------------------
2009-06-30 14:38:33 UTC pdmef
* component changed to crypto
* Updated description:
{{{
Package: mutt
Version: 1.3.15-2
Severity: normal

[NOTE: this bug report has been submitted to the debian BTS as Bug#88860.
Please Cc all your replies to 88860@bugs.debian.org.]

From: JP Sugarbroad <taral@taral.net>
Subject: mutt does not process PGP/MIME in digests
Date: Wed, 7 Mar 2001 13:43:05 -0600

When viewing digests with the 'v' key, mutt does not process the
PGP/MIME signed messages that the digest might contain.

-- System Information
Debian Release: testing/unstable
Architecture: i386
Kernel: Linux dragon 2.4.2-ac3 #1 Mon Feb 26 00:01:32 CST 2001 i686

Versions of packages mutt depends on:
ii  libc6                     2.2.2-1        GNU C Library: Shared libraries an
ii  libncurses5               5.0-8          Shared libraries for terminal hand
ii  libsasl7                  1.5.24-5       Authentication abstraction library
ii  postfix [mail-transport-a 0.0.20010228-2 A high-performance mail transport 

-- 
Taral <taral@taral.net>
Please use PGP/GPG to send me mail.
"Never ascribe to malice what can as easily be put down to stupidity."


>How-To-Repeat:
>Fix:
}}}
