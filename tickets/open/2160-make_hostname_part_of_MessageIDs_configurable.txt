Ticket:  2160
Status:  new
Summary: make hostname part of Message-IDs configurable

Reporter: hadmut@danisch.de
Owner:    mutt-dev

Opened:       2006-01-06 09:16:20 UTC
Last Updated: 2015-11-17 11:00:58 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
mutt is lacking a configuration option for the structure of the Message-IDs. It insists on generating the Message-IDs on its own. Therefore I have Message-IDs like
<20060106083603.GA7391@danisch.de>
when sending with mutt. 

Unfortunately, there are plenty of spammers which search through mailing list archives for e-mail-address patterns. They collect these message-ids and put them in their address lists. 

For some special reason I have to accept any mail to my domain (catchall recipient), therefore I get millions of spam to these message-id-addresses. 

Mutt should have an option where to configure the domain part of the Message ID to allow giving it some subdomain of the domain which is not reachable by e-mail.



>How-To-Repeat:
>Fix:
Allow a configuration option to configure the domain part of the message ID. Or better: Allow an sprintf-like String to freely configure it.

}}}

--------------------------------------------------------------------------------
2006-01-07 09:43:39 UTC Michael Tatge <Michael.Tatge@web.de>
* Added comment:
{{{
* On Fri, Jan 06, 2006 hadmut@danisch.de (hadmut@danisch.de) muttered:
> >Number:         2160
> >Synopsis:       annoying Message-IDs
> >Originator:     Hadmut Danisch
> mutt is lacking a configuration option for the structure of the
> Message-IDs. It insists on generating the Message-IDs on its own.
> Therefore I have Message-IDs like <20060106083603.GA7391@danisch.de>
> when sending with mutt. 
> Mutt should have an option where to configure the domain part of the
> Message ID.

It kind of has. $hostname is used as the domain part.

> For some special reason I have to accept any mail to my domain
> (catchall recipient), therefore I get millions of spam to these
> message-id-addresses. 

Catchall is never a good idea. If you have to use it - and you should
rethink if you really have too - combine it with greylisting.

HTH,

Michael
-- 
Little known fact about Middle Earth: The Hobbits had a very sophisticated
computer network!  It was a Tolkien Ring...

PGP-Key-ID: 0xDC1A44DD
Jabber:     init[0]@amessage.de
}}}

--------------------------------------------------------------------------------
2006-01-07 10:04:39 UTC Vincent Lefevre <vincent@vinc17.org>
* Added comment:
{{{
On 2006-01-06 16:43:39 +0100, Michael Tatge wrote:
> * On Fri, Jan 06, 2006 hadmut@danisch.de (hadmut@danisch.de) muttered:
> > >Number:         2160
> > >Synopsis:       annoying Message-IDs
> > >Originator:     Hadmut Danisch
> > mutt is lacking a configuration option for the structure of the
> > Message-IDs. It insists on generating the Message-IDs on its own.
> > Therefore I have Message-IDs like <20060106083603.GA7391@danisch.de>
> > when sending with mutt.=20
> > Mutt should have an option where to configure the domain part of the
> > Message ID.
>=20
> It kind of has. $hostname is used as the domain part.

However, $hostname is also use for local e-mail addresses;
so, one does generally not want to modify it!

--=20
Vincent Lef=E8vre <vincent@vinc17.org> - Web: <http://www.vinc17.org/>
100% accessible validated (X)HTML - Blog: <http://www.vinc17.org/blog/>
Work: CR INRIA - computer arithmetic / SPACES project at LORIA
}}}

--------------------------------------------------------------------------------
2006-01-08 06:55:36 UTC Nicolas Rachinsky <mutt-devel-0@ml.turing-complete.org>
* Added comment:
{{{
* Vincent Lefevre <vincent@vinc17.org> [2006-01-06 17:04 +0100]:
> On 2006-01-06 16:43:39 +0100, Michael Tatge wrote:
> > > Mutt should have an option where to configure the domain part of the
> > > Message ID.
> > 
> > It kind of has. $hostname is used as the domain part.
> 
> However, $hostname is also use for local e-mail addresses;
> so, one does generally not want to modify it!

set hostname="mid.pc5.i.0x5.de"
set hidden_host
does solve that problem for me. But I think, an option to set the
domain part of the MIDs would be a good idea.

Nicolas

-- 
http://www.rachinsky.de/nicolas
}}}

--------------------------------------------------------------------------------
2007-03-02 13:26:04 UTC cb
* Added comment:
{{{
* On Fri, Jan 06, 2006 hadmut@danisch.de (hadmut@danisch.de) muttered:
 On 2006-01-06 16:43:39 +0100, Michael Tatge wrote:
 * Vincent Lefevre <vincent@vinc17.org> [2006-01-06 17:04 +0100]:
 update title
}}}

--------------------------------------------------------------------------------
2015-11-17 11:00:58 UTC Safari
* Added comment:
https://github.com/Safari77/mutt-sidebar/commit/3a1b9f10ac15745a25b254dfd97ddc28acb16439
does not apply cleanly to 1.5.24 release but easy to fix.
