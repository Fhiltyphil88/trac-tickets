Ticket:  1263
Status:  reopened
Summary: establish variable categories prefix naming scheme

Reporter: rado
Owner:    mutt-dev

Opened:       2002-07-03 14:17:05 UTC
Last Updated: 2008-04-08 19:22:47 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
Often it happens that people ask where they have to look for (a)
variable(s) controlling a certain feature. It would save a lot of
time finding related variables controlling a distinct area of mutt
if we changed the organization of the reference for the variables
in the TOC (table of contents) of the manual by putting them into
categories.
 It will ease the time of newbies when searching for help, and that
of veterans pointing people to an overviewable (?) portion of the
_big_ manual, so they don't lose hope and interest in checking it
out themselves.

The idea is to prefix all vars with a common category name.
For this I offer a suggestion at

        http://WIKI.mutt.org/?ManualVarNames

where it can easily be changed by everyone and discussed why
or why not the change should be done in some way or the other or
at all. Corrections and additions (especially for post 1.5.6
version) are welcome.


Rado S

--------------------------------------------------------------------------------
2002-10-19 14:14:01 UTC "Rado S." <rado@math.uni-hamburg.de>
* Added comment:
{{{
Hi dear mutt-dev crew.

Finally I've come around to do the major work myself. I made up categories
and placed the variables into them (based on 1.5.6).

After Michael E. pointed out that to fix the manual I had to change init.h
and makedoc.c, I grabbed and finished init.h for v1.5.6 (took me a few
hours).
However, I'd appreciate if somebody familiar with makedoc.c could contact
me so we can extend makedoc.c together to make it understand a new tag for
subsections. Extending it shouldn't take long when you know well how
makedoc.c works (I don't know well, that's why I ask for help. Especially I
have no clue about nroff format and how to implement subsections there ;).

[*** Post-edit note by Rado: the idea of changing makedoc is obsolete, the
suggestion in the followups of prefixing the vars is the current path ***]
}}}

--------------------------------------------------------------------------------
2002-10-19 23:01:46 UTC Eduardo Pérez Ureta <eperez@it.uc3m.es>
* Added comment:
{{{
>  However, I'd appreciate if somebody familiar with makedoc.c could contact
> me so we can extend makedoc.c together to make it understand a new tag for
> subsections. Extending it shouldn't take long when you know well how
> makedoc.c works (I don't know well, that's why I ask for help. Especially I
> have no clue about nroff format and how to implement subsections there ;).

Hi Rado,

Why instead sorting the variables in the manual, you prefix the
variables with the correct category so they are automatically sorted?

For example, while none of:
 system, headers, message, folders, compose,
 attachments, remote, display-index, threads, display-pager,
 display-formats, key-commands, encoding, misc
are prefixed, most of:
 crypt, ssl
are prefixed, and all of:
 pgp, smime, imap, pop
are prefixed.

Some variables like display-formats are suffixed instead of prefixed thus
making them difficult to sort. Changing the suffix to prefix makes them
easily sortable.

I know this would break compatibility with old variable names but
a transition where the two variable names is available as in mutt-1.6.x
would make the transition smooth.

This prefix would also help people to quickly know the variable
purpose.

What do you think?
}}}

--------------------------------------------------------------------------------
2002-10-22 09:52:25 UTC "Rado S." <rado@math.uni-hamburg.de>
* Added comment:
{{{
> This prefix would also help people to quickly know the variable purpose.
>
> What do you think?

I like the idea.

Now, since I've ordered the variables, it's easy to prefix the names
and to get rid of the subsection/makedoc.c problem.

When we are up to changing names, you might go over all of them
and check whether some variables could get better names, i.e. in
line with naming of other variables (name -> semantics,
convention to stick with, e.g.: attribution, indent_string,
post_indent_string).
Fix names ?

Then there are several synonyms, i.e. multiple names controlling the same
feature (like hdr_format and index_format). I guess this is an old
compatibility issue.
Can those old synonyms be removed then ?

However, before I do any of this, I need confirmation that you people want
it that way (prefixed instead of categories in the manual, fixed names,
cleaned of old synonyms) and the final set (i.e. better suggestions).
}}}

--------------------------------------------------------------------------------
2002-10-22 12:09:44 UTC Eduardo Pérez Ureta <eperez@it.uc3m.es>
* Added comment:
{{{
Rado S. wrote:
> When we are up to changing names, you might go over all of them and check
> whether some variables could get better names, i.e. in line with naming of
> other variables (name -> semantics, convention to stick with, e.g.:
> attribution, indent_string, post_indent_string).
> Fix names ?

Better names for variables can help very much new mutt people and make
the use of mutt easier for all mutt users.

> Then there are several synonyms, i.e. multiple names controlling the same
> feature (like hdr_format and index_format). I guess this is an old
> compatibility issue.
> Can those old synonyms be removed then ?

We would need a transition between old variable names and new ones.
Either using a script that changes old muttrc variables names to new
ones or having both in a stable version of mutt.
}}}

--------------------------------------------------------------------------------
2002-10-23 08:57:00 UTC "Rado S." <rado@math.uni-hamburg.de>
* Added comment:
{{{
> > Can those old synonyms be removed then ?
>
> We would need a transition between old variable names and new ones.
> Either using a script that changes old muttrc variables names to new ones
> or having both in a stable version of mutt.

Yes, this is clear for the current transition of current official
names and the wannabe prefixed names (next generation, done by me).

But, there exist even other names (older, previous generation to
current ?), that aren't documented [anymore ?]. I _guess_ they
are remains of a previous transition, long before 1.4 (or even
1.3, or 1.2 or 1.0 ;).
Should those "stone age" names still be kept or disappear finally ?

(if they aren't old but put there intentionally for some reason, let me
know, so I don't remove them)

Here the list of the old synonyms:
 { "edit_hdrs",        DT_SYN,  R_NONE, UL "edit_headers", 0 },
 { "forw_format",      DT_SYN,  R_NONE, UL "forward_format", 0 },
 { "forw_quote",       DT_SYN,  R_NONE, UL "forward_quote", 0 },
 { "indent_str",       DT_SYN,  R_NONE, UL "indent_string", 0 },
 { "post_indent_str",  DT_SYN,  R_NONE, UL "post_indent_string", 0 },
 { "forw_decode",      DT_SYN,  R_NONE, UL "forward_decode", 0 },
 { "mime_fwd",         DT_SYN,  R_NONE, UL "mime_forward", 0 },
 { "forw_decrypt",     DT_SYN,  R_NONE, UL "forward_decrypt", 0 },
 { "smime_sign_as",    DT_SYN,  R_NONE, UL "smime_default_key", 0 },
 { "hdr_format",       DT_SYN,  R_NONE, UL "index_format", 0 },
 { "msg_format",       DT_SYN,  R_NONE, UL "message_format", 0 },
 { "print_cmd",        DT_SYN,  R_NONE, UL "print_command", 0 },

Most of them look like abbrevations (do we really need to save 3 bytes ?).
hdr_format is unnecessary and confusing.
Is the smime-ambiguity necessary ?

> > However, before I do any of this, I need confirmation that you people
> > want it that way (prefixed instead of categories in the manual, fixed
> > names, cleaned of old [unused/-documented] synonyms) and the final set
> > (i.e. better suggestions).

> [ for prefices or names ]

It took me already quite some time bundling the variables, and it might
take quite some more time fixing, cleaning and aliasing them for a
transition.

Before I spend this more time on it, I'd like to have a confirmation that
the final result will go through a process of verification and finally be
approved to go into the official release.
(It makes no sense to end up just as an optional patch. ;)
}}}

--------------------------------------------------------------------------------
2002-10-24 11:17:48 UTC "Rado S." <rado@math.uni-hamburg.de>
* Added comment:
{{{
[=-  David Ellement <ellement@sdd.hp.com>  -=]
[=-  Wed 23.Oct'02 at  7:43:33 -0700  -=]

> On 021022, at 15:57:00, Rado S. wrote
> > But, there exist even other names (older, previous generation to
> > current ?), that aren't documented [anymore ?]. I _guess_ they are remains
> > of a previous transition, long before 1.4 (or even 1.3, or 1.2 or 1.0 ;).
> > 
> > Here the list of the old synonyms:
> >   { "edit_hdrs",        DT_SYN,  R_NONE, UL "edit_headers", 0 },
> >   { "forw_format",      DT_SYN,  R_NONE, UL "forward_format", 0 },  
> >   { "forw_quote",       DT_SYN,  R_NONE, UL "forward_quote", 0 },
> >   { "indent_str",       DT_SYN,  R_NONE, UL "indent_string", 0 },
> >   { "post_indent_str",  DT_SYN,  R_NONE, UL "post_indent_string", 0 },
> >   { "forw_decode",      DT_SYN,  R_NONE, UL "forward_decode", 0 },
> >   { "mime_fwd",         DT_SYN,  R_NONE, UL "mime_forward", 0 },
> >   { "forw_decrypt",     DT_SYN,  R_NONE, UL "forward_decrypt", 0 },
> >   { "smime_sign_as",    DT_SYN,  R_NONE, UL "smime_default_key", 0 },
> >   { "hdr_format",       DT_SYN,  R_NONE, UL "index_format", 0 },
> >   { "msg_format",       DT_SYN,  R_NONE, UL "message_format", 0 },
> >   { "print_cmd",        DT_SYN,  R_NONE, UL "print_command", 0 },
> > 
> > Most of them look like abbrevations (do we really need to save 3 bytes ?).
> 
> These are ancient names (pre 1.0), for the most part.  They remained to
> ease the transition (your first guess), not as abreviations.  It should
> be safe to remove them, but it prudent to make a note in the change log.

Ok, they'll be dropped.

> The smime synonym is much newer (1.3); but the original name wasn't in
> use very long, so it can probably be removed also.

Erhm, which one can be removed ?
smime_sign_as or smime_default_key

For the sake of similarity to pgp_sign_as I'd prefer to keep smime_sign_as.

> -- 
> David Ellement
}}}

--------------------------------------------------------------------------------
2005-08-07 09:07:23 UTC rado
* Added comment:
{{{
Clean up, replace long list reposts with link to WIKI for easier discussion and corrections.
Removed excessive quotes, left just what matters on-topic and provides input.
Responsible-Changed-From-To: mutt-dev->rado
Responsible-Changed-By: rado
Responsible-Changed-When: Sat, 20 Aug 2005 17:48:41 +0200
Responsible-Changed-Why:
I'm willing to apply the changes myself (incl. conversion
scripts for muttrcs ...), since I brought it up.
}}}

--------------------------------------------------------------------------------
2005-08-21 10:48:41 UTC rado
* Added comment:
{{{
Willing to finish it to make it go into next official
release if approved.
}}}

* status changed to assigned

--------------------------------------------------------------------------------
2005-08-21 10:48:42 UTC rado
* Added comment:
{{{
Please have a look at
      http://WIKI.mutt.org/?ManualVarNames
for corrections, discussion and hopefully an approval so I can finish init.h .
See original report for history.
}}}

--------------------------------------------------------------------------------
2005-08-25 09:16:51 UTC rado
* Added comment:
{{{
lower severity, higher priority
}}}

--------------------------------------------------------------------------------
2005-10-08 08:41:44 UTC rado
* Added comment:
{{{
due to lack of interested by mutt-dev
}}}

* resolution changed to wontfix
* status changed to closed

--------------------------------------------------------------------------------
2005-10-08 08:41:45 UTC rado
* Added comment:
{{{
simplify subject.
"now or never". Seeing that occasionally varnames are changed
and new vars added, a transition would be better earlier
than later.
}}}

--------------------------------------------------------------------------------
2007-06-24 13:54:43 UTC rado
* Added comment:
wrong status, "suspended" lost in translation to trac.

* milestone changed to 2.0
* resolution changed to 
* status changed to reopened
* type changed to enhancement

--------------------------------------------------------------------------------
2008-04-08 19:22:47 UTC brendan
* Updated description:
Often it happens that people ask where they have to look for (a)
variable(s) controlling a certain feature. It would save a lot of
time finding related variables controlling a distinct area of mutt
if we changed the organization of the reference for the variables
in the TOC (table of contents) of the manual by putting them into
categories.
 It will ease the time of newbies when searching for help, and that
of veterans pointing people to an overviewable (?) portion of the
_big_ manual, so they don't lose hope and interest in checking it
out themselves.

The idea is to prefix all vars with a common category name.
For this I offer a suggestion at

        http://WIKI.mutt.org/?ManualVarNames

where it can easily be changed by everyone and discussed why
or why not the change should be done in some way or the other or
at all. Corrections and additions (especially for post 1.5.6
version) are welcome.


Rado S
* reporter changed to rado
