Ticket:  627
Status:  new
Summary: wanted: way to force interactive mode

Reporter: Marco d'Itri <md@linux.it>
Owner:    mutt-dev

Opened:       2001-05-30 04:19:53 UTC
Last Updated: 2010-09-15 14:09:43 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.3.18-1
Severity: wishlist

[NOTE: this bug report has been submitted to the debian BTS as Bug#99104.
Please Cc all your replies to 99104@bugs.debian.org.]

From: Joey Hess <joeyh@debian.org>
Subject: wanted: way to force interactive mode
Date: Mon, 28 May 2001 21:48:19 -0400

With the -e switch, mutt can be used for a lot of useful, automated
mailbox munging tasks. Unfortunatly, none of these tasks work if mutt's
standard input is not connected to a tty. If stdin is anything else,
mutt enters noninteractive mail sending mode.

This means that none of the handy things I have recipies to make mutt do
with mutt -e commands work in cron jobs.

I would like a new flag to be added, that forces mutt to enter
interactive mode even if there is no controlling terminal.

-- System Information
Debian Release: testing/unstable
Architecture: i386
Kernel: Linux silk 2.4.4 #2 Fri May 11 22:25:05 EDT 2001 i686
Locale: LANG=C, LC_CTYPE=C

Versions of packages mutt depends on:
ii  exim                      3.22-4         Exim Mailer                       
ii  exim [mail-transport-agen 3.22-4         Exim Mailer                       
ii  libc6                     2.2.3-3        GNU C Library: Shared libraries an
ii  libncurses5               5.2.20010318-1 Shared libraries for terminal hand
ii  libsasl7                  1.5.24-6       Authentication abstraction library




>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2010-09-15 14:09:43 UTC me
* Updated description:
{{{
Package: mutt
Version: 1.3.18-1
Severity: wishlist

[NOTE: this bug report has been submitted to the debian BTS as Bug#99104.
Please Cc all your replies to 99104@bugs.debian.org.]

From: Joey Hess <joeyh@debian.org>
Subject: wanted: way to force interactive mode
Date: Mon, 28 May 2001 21:48:19 -0400

With the -e switch, mutt can be used for a lot of useful, automated
mailbox munging tasks. Unfortunatly, none of these tasks work if mutt's
standard input is not connected to a tty. If stdin is anything else,
mutt enters noninteractive mail sending mode.

This means that none of the handy things I have recipies to make mutt do
with mutt -e commands work in cron jobs.

I would like a new flag to be added, that forces mutt to enter
interactive mode even if there is no controlling terminal.

-- System Information
Debian Release: testing/unstable
Architecture: i386
Kernel: Linux silk 2.4.4 #2 Fri May 11 22:25:05 EDT 2001 i686
Locale: LANG=C, LC_CTYPE=C

Versions of packages mutt depends on:
ii  exim                      3.22-4         Exim Mailer                       
ii  exim [mail-transport-agen 3.22-4         Exim Mailer                       
ii  libc6                     2.2.3-3        GNU C Library: Shared libraries an
ii  libncurses5               5.2.20010318-1 Shared libraries for terminal hand
ii  libsasl7                  1.5.24-6       Authentication abstraction library




>How-To-Repeat:
>Fix:
}}}
* milestone changed to 
* type changed to enhancement
