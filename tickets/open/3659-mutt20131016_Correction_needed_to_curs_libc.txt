Ticket:  3659
Status:  new
Summary: mutt-(2013-10-16): Correction needed to curs_lib.c

Reporter: jpr
Owner:    

Opened:       2013-10-19 21:51:33 UTC
Last Updated: 2013-10-20 00:30:27 UTC

Priority:  major
Component: 
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: (2013-10-16)
Severity: normal

-- Please type your report below this line

A problem which I reported for 1.5.21 is still a problem in 1.5.22;
I think line 178 of curs_lib.c, which reads

        if (mutt_system (cmd))

should instead be:

        if (mutt_system (cmd) == -1)

Otherwise, after composing a message and exiting the editor, I get
this error:

        Error running "/usr/bin/vi '/usr/tmp/mutt-jpradley-0-23020-1'"!


-- System Information
System Version: SCO_SV jpradley 5 6.0.0 i386

-- Mutt Version Information


Mutt  (2013-10-16)               <<< NOTE: '1.5.22' should be included here <<<


Copyright (C) 1996-2009 Michael R. Elkins and others.  <<< 2009??? <<<

Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: SCO_SV 5 (i386)

Compiler:
Using built-in specs.
Target: i586-pc-sysv5r6.0
Configured with: /u/gnu/src/gnutools/gcc4/configure --prefix=/usr/gnu --host=i586-pc-sysv5r6.0 --build=i586-pc-sysv5r6.0 --enable-threads=posix --with-gnu-as --with-as=/usr/gnu/bin/gas --with-gnu-ld --with-ld=/usr/gnu/bin/gld --libexecdir=/usr/gnu/lib --disable-shared --target=i586-pc-sysv5r6.0
Thread model: single
gcc version 4.0.2

Configure options: '--disable-nls' '--disable-iconv' 'CC=gcc -D_REENTRANT' 'CFLAGS=-O2 -Wall -Dsnprintf=_xsnprintf'

Compilation CFLAGS: -Wall -pedantic -Wno-long-long -O2 -Wall -Dsnprintf=_xsnprintf

Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  +USE_FCNTL  -USE_FLOCK   
-USE_POP  -USE_IMAP  -USE_SMTP  
-USE_SSL_OPENSSL  -USE_SSL_GNUTLS  -USE_SASL  -USE_GSS  -HAVE_GETADDRINFO  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  -HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  
-EXACT_ADDRESS  -SUN_ATTACHMENT  
-ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
-HAVE_ICONV  -ICONV_NONTRANS  -HAVE_LIBIDN  +HAVE_GETSID  -USE_HCACHE  
ISPELL="/usr/local/bin/ispell"
SENDMAIL="/usr/lib/sendmail"
MAILPATH="/var/spool/mail"
PKGDATADIR="/usr/local/share/mutt"
SYSCONFDIR="/usr/local/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please visit http://bugs.mutt.org/.
}}}


--------------------------------------------------------------------------------
2013-10-20 00:30:27 UTC me
* Added comment:
{{{
On Sat, Oct 19, 2013 at 09:51:33PM -0000, Mutt wrote:

I think you need to determine why nvi is reporting an error 
instead of making Mutt ignore it.  Testing for -1 only catches the 
case where the editor failed to launch, and not when the editor is 
reporting error while editing the file.  That is not the correct 
solution.
}}}
