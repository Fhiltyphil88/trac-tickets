Ticket:  2895
Status:  new
Summary: mutt should keep a deletion log

Reporter: jhawk
Owner:    mutt-dev

Opened:       2007-05-27 00:34:45 UTC
Last Updated: 2008-10-12 13:27:00 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
After spending the past hour or so deleting piles of old email, and then watching mutt crash because of some sort of bizarre server-side IMAP bug, and losing track of all my deletions, I am somewhat frustrated.

It would be nice if mutt could keep a deletion log of messages deleted, such that, when this case happens, it would be possible to restore the log in a new mutt and not have to painstakingly find those deleted messages and delete them again.

Of course it would be nice to fix all the bugs that could cause mutt to crash and people to lose work. In this case, it is a difficult-to-debug IMAP interaction that I think is really a server-side problem (but of course mutt shouldn't crash in any case)...

So this enhancement request is really a band-aid.

Still, I think it would be a very helpful band-aid to have.

--------------------------------------------------------------------------------
2007-05-27 00:38:28 UTC Brendan Cully
* Added comment:
{{{
On Sunday, 27 May 2007 at 00:34, Mutt wrote:
> #2895: mutt should keep a deletion log
> 
>  After spending the past hour or so deleting piles of old email, and then
>  watching mutt crash because of some sort of bizarre server-side IMAP bug,
>  and losing track of all my deletions, I am somewhat frustrated.
> 
>  It would be nice if mutt could keep a deletion log of messages deleted,
>  such that, when this case happens, it would be possible to restore the log
>  in a new mutt and not have to painstakingly find those deleted messages
>  and delete them again.
> 
>  Of course it would be nice to fix all the bugs that could cause mutt to
>  crash and people to lose work. In this case, it is a difficult-to-debug
>  IMAP interaction that I think is really a server-side problem (but of
>  course mutt shouldn't crash in any case)...
> 
>  So this enhancement request is really a band-aid.
> 
>  Still, I think it would be a very helpful band-aid to have.

Feel free to sync-mailbox whenever you want to commit your work. As
long as you don't expunged the messages, they will simply be marked
deleted on the server.
}}}

--------------------------------------------------------------------------------
2007-05-27 00:43:08 UTC John Hawkinson
* Added comment:
{{{
Mutt <fleas@mutt.org> wrote on Sun, 27 May 2007
at 00:38:28 -0000 in <046.6cdbdfee0853f719e5056e252a4009d0@mutt.org>:

> Comment (by Brendan Cully):

>  Feel free to sync-mailbox whenever you want to commit your work. As
>  long as you don't expunged the messages, they will simply be marked
>  deleted on the server.

This does not work :(

It does tend to improve the imap problems somewhat. But if it crashes,
my deletion state gets lost. (server bug? This is cyrus.)

(Though also, if I remembered to do that, which I sometimes do,
then I probably would have remembered that deleting this many messages
in one go would probably be bad, and have not done it...).

--jhawk@mit.edu
  John Hawkinson
}}}

--------------------------------------------------------------------------------
2007-05-27 00:53:19 UTC jhawk
* Added comment:
Actually, I just got "Fatal: word too long" while running sync-mailbox ($ n). mutt (1.5.12) didn't crash, but, well, deletion state lost. These deletions much less painfully earned, though.

--------------------------------------------------------------------------------
2008-05-26 22:16:02 UTC myon
* Added comment:
See also #3000.

--------------------------------------------------------------------------------
2008-10-12 13:27:00 UTC Patrick Welche
* Added comment:
{{{
On Sun, May 27, 2007 at 12:53:20AM -0000, Mutt wrote:

#3000 makes this much happier though..
}}}
