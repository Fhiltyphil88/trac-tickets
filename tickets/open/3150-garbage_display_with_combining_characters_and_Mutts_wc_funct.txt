Ticket:  3150
Status:  accepted
Summary: garbage display with combining characters and Mutt's wc functions (--without-wc-funcs)

Reporter: vinc17
Owner:    me

Opened:       2009-01-06 14:08:18 UTC
Last Updated: 2015-11-23 21:08:03 UTC

Priority:  major
Component: charset
Keywords:  patch

--------------------------------------------------------------------------------
Description:
When building Mutt with --without-wc-funcs, I get garbage output on combining characters.

--------------------------------------------------------------------------------
2009-01-06 14:08:57 UTC vinc17
* Added attachment combining-wcwidth
* Added comment:
testcase: mailbox file

--------------------------------------------------------------------------------
2009-01-06 14:10:03 UTC vinc17
* Added comment:
FYI, this is the same testcase as in bug #2820.

--------------------------------------------------------------------------------
2009-01-06 20:44:32 UTC brendan
* milestone changed to 1.6

--------------------------------------------------------------------------------
2009-06-30 14:06:56 UTC pdmef
* Added comment:
Can you still reproduce it? I get the same combined chars output in mutt as in a web browser when viewing the attachment...

--------------------------------------------------------------------------------
2009-07-01 08:02:23 UTC vinc17
* Added comment:
The problem occurs when I use
{{{
set charset=UTF-8//TRANSLIT
}}}
Note: I could modify the script that sets my $charset to avoid the useless //TRANSLIT in case of UTF-8, but this is still a bug.

Anyway, output is also incorrect with
{{{
set charset=UTF-8
}}}
though more readable. In a 80-column terminal, I get
{{{
é.1 é.2 é.3 é.4 é.5 é.6 é.7 é.8 é.9 é.10 é.11 é.12 é.13 é.14 é.15
 é.16 é.17 é.18+é.19 é.20
}}}
instead of
{{{
é.1 é.2 é.3 é.4 é.5 é.6 é.7 é.8 é.9 é.10 é.11 é.12 é.13 é.14 é.15 é.16 é.17 é.18
+é.19 é.20
}}}

--------------------------------------------------------------------------------
2009-07-02 10:13:28 UTC pdmef
* Added comment:
So this is a display/wrapping issue? And not mutt outputting garbage in the pager?

--------------------------------------------------------------------------------
2009-07-04 22:53:39 UTC vinc17
* Added comment:
There seem to be two different problems (perhaps related): one with UTF-8//TRANSLIT (where I get garbage) and one with UTF-8 (wrapping problem).

--------------------------------------------------------------------------------
2009-07-05 09:55:02 UTC pdmef
* Added comment:
What does "garbage" mean? I get:

{{{
e\01.1 e\01.2 e\01.3 e\01.4 e\01.5 e\01.6 e\01.7 e\01.8 e\01.9                                                                                        
+e\01.10 e\01.11 e\01.12 e\01.13 e\01.14 e\01.15 e\01.16 e\01.17                                                                                      
+e\01.18 e\01.19 e\01.20 
}}}

with //TRANSLIT, regardless of --without-wc-funcs.

--------------------------------------------------------------------------------
2009-07-05 09:57:27 UTC pdmef
* Added comment:
Err, no, used the wrong build to test that. Without system wc funcs it works even with //TRANSLIT...

--------------------------------------------------------------------------------
2009-07-06 23:16:48 UTC vinc17
* Added comment:
I get the following:
{{{
e 201.1 e 201.2 e 201.3 e 201.4 e 201.5 e 201.6 e 201.7 e 201.8 e 201.9+e 201.10
 e 201.11 e 201.12 e 201.13 e 201.14 e 201.15 e 201.16 e 201.17+e 201.18 e 201.1
9 e 201.20
}}}
with //TRANSLIT.

--------------------------------------------------------------------------------
2009-07-08 11:57:18 UTC pdmef
* Added comment:
Can you please test the patch in http://bitbucket.org/pdmef/muttfixesmq/src/tip/ticket-3150 on bitbucket?

The problem is that mutt doesn't know of charset extensions at all, but the system does and thus wide character stuff magically/accidentally work with extensions. However, with --without-wc-funcs mutt uses wide character functions that make decisions on hard-coded characters/ranges ''if and only if'' the charset is utf8 -- else they use the singlebyte system functions like `isprint()` etc.

However, as mutt never knew about extensions it never classified 'utf-8//TRANSLIT' as utf8... which eventually makes it use single-byte functions as said.

The patch tries to canonicalize the charset without extension, and uses string prefix matching rather than exact matching when comparing character sets.

* keywords changed to patch
* owner changed to pdmef
* status changed to accepted
* version changed to 1.5.20

--------------------------------------------------------------------------------
2009-07-08 11:57:31 UTC pdmef
* status changed to started

--------------------------------------------------------------------------------
2009-07-10 21:36:16 UTC Vincent Lefevre
* Added comment:
{{{
On 2009-07-08 11:57:22 -0000, Mutt wrote:

Thanks, this fixes the garbage problem. But as expected, the wrapping
problem (like with UTF-8) is still there.

BTW, I get the following warning:

curs_lib.c: In function 'mutt_format_string':
curs_lib.c:730: warning: implicit declaration of function 'iswblank'

(but even without the patch).
}}}

--------------------------------------------------------------------------------
2009-07-11 12:54:25 UTC Rocco Rutte <pdmef@gmx.net>
* Added comment:
(In [6f942afe60b5]) Recognize charset extensions, see #3150.

With utf-8//TRANSLIT, we internally didn't recognize it as utf-8. This
leads to badly broken behaviour if --without-wc-funcs is used for some
reason. In that case, if we have utf-8 as charset, we implement our own
wide char functions; for all other charsets, we use the system
single-byte locale functions. And using these with utf-8 is broken.

--------------------------------------------------------------------------------
2011-04-13 11:19:45 UTC m-a
* owner changed to 
* status changed to new

--------------------------------------------------------------------------------
2013-01-17 00:29:43 UTC me
* Added comment:
I believe this bug has been fixed by [a51df78218e8] in which the $mail_check_recent option controls whether or not we use UNSEEN or RECENT to report the number of new messages in the mailbox.

* owner changed to me
* status changed to accepted

--------------------------------------------------------------------------------
2013-01-17 11:54:42 UTC vinc17
* Added comment:
Replying to [comment:15 me]:
> I believe this bug has been fixed by [a51df78218e8] in which the $mail_check_recent option controls whether or not we use UNSEEN or RECENT to report the number of new messages in the mailbox.
I think you didn't reply to the right bug report.

--------------------------------------------------------------------------------
2015-11-23 21:08:03 UTC kevin8t8
* milestone changed to 
