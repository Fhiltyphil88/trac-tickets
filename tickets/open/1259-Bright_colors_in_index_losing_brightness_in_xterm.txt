Ticket:  1259
Status:  new
Summary: Bright* colors in index losing brightness in xterm.

Reporter: ecoffey@alphalink.com.au
Owner:    mutt-dev

Opened:       2002-06-27 19:08:15 UTC
Last Updated: 2007-04-02 02:03:42 UTC

Priority:  minor
Component: display
Keywords:  

--------------------------------------------------------------------------------
Description:
I have bright foreground colors in my index to highlight mails from
certain people, and my indicator bar has a non-default (red) background
and a bright (brightyellow) foreground.  When I move the indicator
upward, over one of the bright entries, in an xterm, the bright color
reverts to its plain, non-bright version.  This also occurs when
returning to the index from another screen when the indicator is placed
immediately above a bright entry.

This does not occur when moving the indicator down, or in a text
console, or with a default background for the indicator bar, or with a
non-bright foreground for the indicator bar.

Obvious workaround: don't use bright foregrounds for the indicator bar.

Relevant parts of .muttrc:
{{{
color indicator brightyellow red
color index     brightred    default Person's\ Name
}}}


--------------------------------------------------------------------------------
2000-07-07 08:24:48 UTC mgedmin@pub.osf.lt
* Added comment:
{{{
Package: mutt
Version: 1.2i
Severity: normal

-- Please type your report below this line

There's a problem in Mutt that prevents it from using colors properly
with ncurses.  Attributes such as bold set via bkgdset() override those
set via attrset.  A simple muttrc consisting of these two lines is
enough to reproduce the error:

 color normal brightcyan blue
 color status black cyan

Both help and status lines are displayed in bold when they should not
be.

Last time I raised this question on mutt-dev@mutt.org (Message-ID:
<20000626001818.B6915@mg.home>) there was some discussion (which
quickly wandered off-topic), but the problem hasn't been solved yet.

-- Mutt Version Information

Mutt 1.2i (2000-05-09)
Copyright (C) 1996-2000 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.2.16 [using ncurses 5.0]
Compile options:
-DOMAIN
+DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +USE_FCNTL  -USE_FLOCK
-USE_IMAP  -USE_GSS  -USE_SSL  -USE_POP  +HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  +ENABLE_NLS
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/spool/mail"
SHAREDIR="/usr/local/share/mutt"
SYSCONFDIR="/usr/local/etc"
ISPELL="/usr/bin/ispell"
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the muttbug utility.
}}}

--------------------------------------------------------------------------------
2007-04-02 02:03:42 UTC brendan
* component changed to display
* Updated description:
I have bright foreground colors in my index to highlight mails from
certain people, and my indicator bar has a non-default (red) background
and a bright (brightyellow) foreground.  When I move the indicator
upward, over one of the bright entries, in an xterm, the bright color
reverts to its plain, non-bright version.  This also occurs when
returning to the index from another screen when the indicator is placed
immediately above a bright entry.

This does not occur when moving the indicator down, or in a text
console, or with a default background for the indicator bar, or with a
non-bright foreground for the indicator bar.

Obvious workaround: don't use bright foregrounds for the indicator bar.

Relevant parts of .muttrc:
{{{
color indicator brightyellow red
color index     brightred    default Person's\ Name
}}}
