Ticket:  3437
Status:  new
Summary: separate sorting settings for folder, mailbox, and IMAP browsers

Reporter: gber
Owner:    mutt-dev

Opened:       2010-08-04 10:12:23 UTC
Last Updated: 2010-08-04 10:12:23 UTC

Priority:  minor
Component: browser
Keywords:  

--------------------------------------------------------------------------------
Description:
Currently mutt only supports only a single sorting option for the folder, mailbox, and IMAP browsers, I would find it useful to have three separate sort settings for each of them.
Specifically, I'd like to be able to have filenames in the folder browser sorted alphabetically while leaving the mailboxes in the mailbox browser unsorted, that is in the order specified in the configuration file which is also the order used by the change-folder function to traverse those mailboxes with new mail.

