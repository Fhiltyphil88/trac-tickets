Ticket:  3519
Status:  new
Summary: Select nearest message when changing filters

Reporter: ericpruitt
Owner:    mutt-dev

Opened:       2011-05-26 11:31:15 UTC
Last Updated: 2011-05-26 11:39:21 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
When a message is visible before and after changing mutt's current message limiting filter, the row cursor remains on the same message, but if the message does not exist, the cursor is reset to the top-most message. Instead, the nearest message without rolling over to the top of the screen should be selected. This could be made an optional setting using something like filtered_message_selection (a terrible name) that could be set to "off", "before", "after" to determine if the nearest message before or after is selected. Another variable, "limit_rollover", would be used to determine whether or not mutt was allowed to wrap around to the next message if using "before" at the top of the mailbox or "after" at the end of the mailbox.

--------------------------------------------------------------------------------
2011-05-26 11:39:21 UTC ericpruitt
* Added comment:
Something else I forgot to add -- when no message is selected when focused on an empty filter, the most recent selected message could be used when going to a non-empty filter, or "after" / "before" could determine whether or not to jump to the end or beginning of the non-empty filter.
