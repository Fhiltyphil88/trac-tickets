Ticket:  2201
Status:  new
Summary: <view-attach> a message/rfc822 misses $display_filter

Reporter: mutt-bugs@luisflorit.endjunk.com
Owner:    mutt-dev

Opened:       2006-03-29 00:38:31 UTC
Last Updated: 2009-06-30 14:25:03 UTC

Priority:  minor
Component: display
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
There are two issues regarding display filters:
1) I did a small perl script to strip multiple To: and Cc:
lines (yes, I know there are other scripts that do the same
but I wanted a simple one to add to my custom "adds remover"
filter). It's quite simple:

#####################################################

#!/usr/bin/perl -p

undef $/;
s/((To:|Cc:).*\n)(^[\t\s].*\n)+(^[^ ].*)/\1    \[---=| STRIPPED |=---\]\n\4/mg;

#####################################################
                                                     
It works very well stripping the anoying multiple lines
in those headers BUT ONLY when mutt does not weed headers
('h' keyword). With stripped headers, it just does not work. I would expect to behave consistently.

2) I also use another filter to strip adds in messages,
like the ones added by Yahoo and other mailing lists, 
or virus scan programs. I call the filters with

  set display_filter='mutt-display.pl'

in my .muttrc file.

    However, I realized that when I receive a
message diggest (many messages in one, that appear
as separate 'message/rfc822' attachments), and I
try to see one attachment, the filter programs
are not called, and the adds are shown. I tried
to configure my .mailcap with:

message/rfc822; cat %s | mutt-display.pl ; copiousoutput
    
but that didn't make any difference.
>How-To-Repeat:
For (1), just toggle the view of all the headers with 'h' of a mail with many To: addresses. When all headers are shown, the multiple To: and Cc: lines are stripped. When not shown, they are not stripped (i.e., the filter does not work).
For (2), just try to see the attachments using 'v' and then ENTER in the corresponding message. For example, you can use the mutt mailing list daily diggest... :)
>Fix:
For (1), perhaps this is some silly regexp problem.
Not a clue for (2).
}}}

--------------------------------------------------------------------------------
2006-06-05 03:59:44 UTC "Luis A. Florit" <mutt-bugs@luisflorit.endjunk.com>
* Added comment:
{{{
Dear Alain and all,

    Sorry for sending an email, but my post was in march, 
and I cannot remember my username at mutt. And I couldn't
find a way to retrieve the data...

* El 04/06/06 a las 18:13, Alain Bench chamullaba:

> Synopsis: wish: <view-attach> a message/rfc822 applies $display_filter
> 
> **** Comment added by ab on Sun, 04 Jun 2006 18:13:58 +0200 ****
>  
>     Hello Luis, and thank you for this report.
> 
> The issue #1 is not a Mutt bug, but a problem in your
> Perl script which doesn't work correctly even alone.
 
Sorry, but I don't know why you said it does not work.
For me the script works very well when applied to messages with
'unweeded' headers, that is, when full headers are showed.
Or even 'alone', i.e., when applied to a mail with a command 
such as 'cat <messagefile> | script.pl'.
BUT it doesn't work when the headers are weeded: it does 
nothing then. This inconsistency must be due to mutt.

Could you please send me a simple perl line like mine that 
does work? I tried for a long time trying to make it work,
trying to guess what mutt was doing (adding tabs or something
like that), without any success. That is what took me to the
conclusion that this is a mutt bug...

> Issue #2 seems true: $display_filter is not applied when
> in attachments menu one <view-attach> a message/rfc822 part.
> I'm not really sure if it's bug or feature lack, but it
> could seem to make sense: After all things like $weed,
> ignore, hdr_order, auto_view, mailcap, and such do apply
> there. Why not $display_filter?

Certainly.

> So I turn this bug into a
> wish to make use of $display_filter in attachments menu.

I would prefer to call it a bug because it is inconsistent.

>   Only for message/rfc822, or also for text/* parts?

Any part. Display-filter does not work for any attachments.
 
> > I tried to configure my .mailcap with:
> > | message/rfc822; cat %s | mutt-display.pl ; copiousoutput
> 
>     Can't work: Mutt displays message/rfc822 itself
> internally, without mailcap. 

Ok.

> Furthermore it's a useless use of cat %s |

Oops, sorry.
 
    Thanks!!!

        Luis.


PS:
Is mankind ready for mutt 1.6 ? Because mutt 
1.6 should already be ready for mankind... :)

PPS:
BTW, mutt shows for the mutt-users mailing 
list the attachments like this:

  I     1 Index                      [text/plain, 8bit, us-ascii, 0.5K]
  I     2 <no description>                  [multipa/digest, 8bit, 26K]
  I     3 |->200605/478                    [message/rfc822, 8bit, 2.5K]
  I     4 |->200605/479                    [message/rfc822, 8bit, 3.0K]
  I     5 |->200605/480                    [message/rfc822, 8bit, 2.5K]
  I     6 |->200605/481                    [message/rfc822, 8bit, 3.0K]
  I     7 |->200605/482                    [message/rfc822, 8bit, 2.0K]
  I     8 |->200606/1                      [message/rfc822, 8bit, 1.6K]
  I     9 |->200606/2                      [message/rfc822, 8bit, 1.6K]
  I    10 |->200606/3                      [message/rfc822, 8bit, 3.3K]
  I    11 |->200606/4                      [message/rfc822, 8bit, 1.4K]
  I    12 `->200606/5                      [message/rfc822, 8bit, 3.0K]

These numbers '2000605' are useless. It would be much better
if the subject is shown (like with vim mailing list).
Why is this? Am I doing something stupid?
}}}

--------------------------------------------------------------------------------
2006-06-05 11:13:58 UTC ab
* Added comment:
{{{
Hello Luis, and thank you for this report.

    The issue #1 is not a Mutt bug, but a problem in your
Perl script which doesn't work correctly even alone.

    Issue #2 seems true: $display_filter is not applied when
in attachments menu one <view-attach> a message/rfc822 part.
I'm not really sure if it's bug or feature lack, but it
could seem to make sense: After all things like $weed,
ignore, hdr_order, auto_view, mailcap, and such do apply
there. Why not $display_filter? So I turn this bug into a
wish to make use of $display_filter in attachments menu.

    Only for message/rfc822, or also for text/* parts?
}}}

--------------------------------------------------------------------------------
2006-06-05 16:23:10 UTC Nicolas Rachinsky <mutt-devel-0@ml.turing-complete.org>
* Added comment:
{{{
* Alain Bench <veronatif@free.fr> [2006-06-04 18:13 +0200]:
>     Issue #2 seems true: $display_filter is not applied when
> in attachments menu one <view-attach> a message/rfc822 part.
> I'm not really sure if it's bug or feature lack, but it
> could seem to make sense: After all things like $weed,
> ignore, hdr_order, auto_view, mailcap, and such do apply
> there. Why not $display_filter? So I turn this bug into a
> wish to make use of $display_filter in attachments menu.
> 
>     Only for message/rfc822, or also for text/* parts?

I think it should only used for message/rfc822, I don't think it's
that useful for other text parts. And the filter would have to
recognize wether it's used for an arbitrary text or for an mail if
it's used to manipulate the headers.

Nicolas

-- 
http://www.rachinsky.de/nicolas
}}}

--------------------------------------------------------------------------------
2006-06-07 07:42:51 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
On Sunday, June 4, 2006 at 22:15:02 +0200, Luis A. Florit wrote:

> Sorry for sending an email

    No worries: That's the intended way for reporters to followup on
their bug. Such mails are recorded in the BTS and forwarded to mutt-dev.
Note: I read mutt-dev, so CCing me is useless (as indicated by my MFT).

    OTOS responders by mail to bug-any@bmo should not CC mutt-dev
(duplication), but should CC the reporter and other interested parties
not reading mutt-dev. Unfortunately MFT doesn't help that.

    Responses thru the BTS website itself automatically go to mutt-dev,
the reporter, and any additional interested parties.


> For me the script works very well

    No. The script is unreliable. Depending on fields order, suite, and
spelling, it strips too much or not enough, matches wrong positives in
header and in body, and sometimes happens to strip the empty line
separating header and body. Completely FUBAR.

    The difference you see between complete and weeded headers is only
an effect of the script brokenness, and of its unwanted sensitivity to
fields sequence. Example: Pipe a full header where "Cc:" immediatly
follows "To:", both multiline. The To is stripped, but not the Cc.


> trying to guess what mutt was doing (adding tabs or something like
> that)

    Weeding doesn't change the filter input, outside of the removal of
ignored fields, and reordering of the remaining ones by hdr_order.


> * El 04/06/06 a las 18:13, Alain Bench chamullaba:
>> wish to make use of $display_filter in attachments menu.
> I would prefer to call it a bug because it is inconsistent.

    Agreed: I'll change severity. What means "chamullaba"?


> mutt shows for the mutt-users [digest]
>|   I     2 <no description>                  [multipa/digest, 8bit, 26K]
>|   I     3 |->200605/478                    [message/rfc822, 8bit, 2.5K]
> These numbers '2000605' are useless. It would be much better if the
> subject is shown (like with vim mailing list).

    I never saw an MJ2 digest, but would guess that those parts have a
"Content-Description: YYYYMM/NNN" field, that the %d expando
(_%d_escription) in $attach_format is intended to display, in priority
to the embedded "Subject:".


 On Sunday, June 4, 2006 at 23:25:01 +0200, Nicolas Rachinsky wrote:

>> make use of $display_filter in attachments menu. Only for
>> message/rfc822, or also for text/* parts?
> I think it should only used for message/rfc822, I don't think it's
> that useful for other text parts. And the filter would have to
> recognize wether it's used for an arbitrary text or for an mail if
> it's used to manipulate the headers.

    Hum... OTOS some kind of simple filters (rot13, removal of ^M, or
such) do not care about header/body. Users may expect they apply to text
parts also.


Bye!	Alain.
-- 
A: Because it messes up the order in which people normally read text.
Q: Why is top-posting such a bad thing?
}}}

--------------------------------------------------------------------------------
2006-06-11 12:17:06 UTC "Luis A. Florit" <mutt-bugs@luisflorit.endjunk.com>
* Added comment:
{{{
* El 06/06/06 a las 14:42, Alain Bench chamullaba:

>  On Sunday, June 4, 2006 at 22:15:02 +0200, Luis A. Florit wrote:
> 
> > Sorry for sending an email
> 
> No worries: That's the intended way for reporters to followup on
> their bug. Such mails are recorded in the BTS and forwarded to
> mutt-dev. Note: I read mutt-dev, so CCing me is useless (as
> indicated by my MFT).
> 
> OTOS responders by mail to bug-any@bmo should not CC mutt-dev
> (duplication), but should CC the reporter and other interested
> parties not reading mutt-dev. Unfortunately MFT doesn't help that.
> 
> Responses thru the BTS website itself automatically go to mutt-dev,
> the reporter, and any additional interested parties.
 
Ok.
 
> > For me the script works very well
> 
> No. The script is unreliable. Depending on fields order, suite, and
> spelling, it strips too much or not enough, matches wrong positives
> in header and in body, and sometimes happens to strip the empty line
> separating header and body. Completely FUBAR.

Sorry, I wasn't clear enough. 
The script is not just FUBAR: it is a complete suicide!!!

I didn't try to say that the script was a good one, 
but that it showed that there was a (supposedly) bug. 
Don't be afraid, I don't use that as a script to strip headers. 
A good script to do so cannot be a one line script.

> The difference you see between complete and weeded headers is only
> an effect of the script brokenness, and of its unwanted sensitivity
> to fields sequence. Example: Pipe a full header where "Cc:"
> immediatly follows "To:", both multiline. The To is stripped, but
> not the Cc.
 
Not really the problem, but you were right, it was 
a mistake in the script that was responsible for this: 
I have the 'To:' header as the first one in 'hdr_order'.
And '^' does not match for the beginning of line in perl IF
the line is the first one! Stupid me, sorry!! 50 lashes here.

> > trying to guess what mutt was doing (adding tabs or something like
> > that)
> 
> Weeding doesn't change the filter input, outside of the removal of
> ignored fields, and reordering of the remaining ones by hdr_order.

Exactly because of this reordering of the headers the problem arose. 
 
> > * El 04/06/06 a las 18:13, Alain Bench chamullaba:
> > > wish to make use of $display_filter in attachments menu.
> > I would prefer to call it a bug because it is inconsistent.
> 
> Agreed: I'll change severity. 

Ok.

> What means "chamullaba"?

Would you believe if I tell you that you're
the first person in a year that asked?? 

It is a funny Argentinian slang, that means "said".
Just a silly thing.

> > mutt shows for the mutt-users [digest]
> > |   I     2 <no description>      [multipa/digest, 8bit, 26K]
> > |   I     3 |->200605/478        [message/rfc822, 8bit, 2.5K]
> > These numbers '2000605' are useless. It would be much better if the
> > subject is shown (like with vim mailing list).
> 
> I never saw an MJ2 digest, but would guess that those parts have a
> "Content-Description: YYYYMM/NNN" field, that the %d expando
> (_%d_escription) in $attach_format is intended to display,
> in priority to the embedded "Subject:".

Exactly! Good.
And it seems that I can do nothing to correct this by only
using mutt... (of course I can play with my .procmailrc).
 
>  On Sunday, June 4, 2006 at 23:25:01 +0200, Nicolas Rachinsky wrote:
> 
> > > make use of $display_filter in attachments menu.
> > > Only for message/rfc822, or also for text/* parts?
> > I think it should only used for message/rfc822, I don't think it's
> > that useful for other text parts. And the filter would have to
> > recognize wether it's used for an arbitrary text or for an mail if
> > it's used to manipulate the headers.
> 
> Hum... OTOS some kind of simple filters (rot13, removal of ^M, or
> such) do not care about header/body. Users may expect they apply
> to text parts also.
 
Yes, you're right.
 
    Thanks for your patience!

        Luis.
}}}

--------------------------------------------------------------------------------
2006-06-16 07:55:45 UTC ab
* Added comment:
{{{
Dear Alain and all,
 * Alain Bench <veronatif@free.fr> [2006-06-04 18:13 +0200]:
  On Sunday, June 4, 2006 at 22:15:02 +0200, Luis A. Florit wrote:
 * El 06/06/06 a las 14:42, Alain Bench chamullaba:
 recategorize sw-bug, amend title
}}}

--------------------------------------------------------------------------------
2006-07-22 05:38:52 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
Hello Nicolas,

 On Sunday, June 4, 2006 at 23:25:01 +0200, Nicolas Rachinsky wrote:

> * Alain Bench <veronatif@free.fr> [2006-06-04 18:13 +0200]:
>> wish to make use of $display_filter in attachments menu. Only for
>> message/rfc822, or also for text/* parts?
> the filter would have to recognize wether it's used for an arbitrary
> text or for an mail if it's used to manipulate the headers.

    Hum... Changing to apply $display_filter to text/* parts would
possibly perturbate existing filters: Bad. What if Mutt would, only for
non-messages, do:

 -1) prepend \n
 -2) apply $display_filter
 -3) strip 1st char if it's still a \n

    This would hopefully not perturbate existing full filters, and the
empty first line would probably be a sufficient end-of-header indication
for all filters needing header/body distinction.


Bye!	Alain.
}}}

--------------------------------------------------------------------------------
2006-07-25 08:58:19 UTC Gary Johnson <garyjohn@spk.agilent.com>
* Added comment:
{{{
On 2006-07-21, Alain Bench <veronatif@free.fr> wrote:
> The following reply was made to PR mutt/2201; it has been noted by GNATS.
> 
> From: Alain Bench <veronatif@free.fr>
> To: bug-any@bugs.mutt.org
> Cc: 
> Subject: Re: mutt/2201: wish: <view-attach> a message/rfc822 applies $display_filter
> Date: Fri, 21 Jul 2006 12:38:52 +0200 (CEST)
> 
>  Hello Nicolas,
>  
>   On Sunday, June 4, 2006 at 23:25:01 +0200, Nicolas Rachinsky wrote:
>  
>  > * Alain Bench <veronatif@free.fr> [2006-06-04 18:13 +0200]:
>  >> wish to make use of $display_filter in attachments menu. Only for
>  >> message/rfc822, or also for text/* parts?
>  > the filter would have to recognize wether it's used for an arbitrary
>  > text or for an mail if it's used to manipulate the headers.
>  
>      Hum... Changing to apply $display_filter to text/* parts would
>  possibly perturbate existing filters: Bad. What if Mutt would, only for
>  non-messages, do:
>  
>   -1) prepend \n
>   -2) apply $display_filter
>   -3) strip 1st char if it's still a \n
>  
>      This would hopefully not perturbate existing full filters, and the
>  empty first line would probably be a sufficient end-of-header indication
>  for all filters needing header/body distinction.

Since the content of a text/* part display is different from that of 
a message or message/rfc822 part display, it might be better to 
create a new display filter variable for the former.  I am concerned 
that applying $display_filter in too many applications with 
different requirements will make it difficult to write a simple yet 
robust filter.

Regards,
Gary

-- 
Gary Johnson                               | Agilent Technologies
garyjohn@spk.agilent.com                   | Wireless Division
http://www.spocom.com/users/gjohnson/mutt/ | Spokane, Washington, USA
}}}

--------------------------------------------------------------------------------
2009-06-30 14:25:03 UTC pdmef
* component changed to display
* Updated description:
{{{
There are two issues regarding display filters:
1) I did a small perl script to strip multiple To: and Cc:
lines (yes, I know there are other scripts that do the same
but I wanted a simple one to add to my custom "adds remover"
filter). It's quite simple:

#####################################################

#!/usr/bin/perl -p

undef $/;
s/((To:|Cc:).*\n)(^[\t\s].*\n)+(^[^ ].*)/\1    \[---=| STRIPPED |=---\]\n\4/mg;

#####################################################
                                                     
It works very well stripping the anoying multiple lines
in those headers BUT ONLY when mutt does not weed headers
('h' keyword). With stripped headers, it just does not work. I would expect to behave consistently.

2) I also use another filter to strip adds in messages,
like the ones added by Yahoo and other mailing lists, 
or virus scan programs. I call the filters with

  set display_filter='mutt-display.pl'

in my .muttrc file.

    However, I realized that when I receive a
message diggest (many messages in one, that appear
as separate 'message/rfc822' attachments), and I
try to see one attachment, the filter programs
are not called, and the adds are shown. I tried
to configure my .mailcap with:

message/rfc822; cat %s | mutt-display.pl ; copiousoutput
    
but that didn't make any difference.
>How-To-Repeat:
For (1), just toggle the view of all the headers with 'h' of a mail with many To: addresses. When all headers are shown, the multiple To: and Cc: lines are stripped. When not shown, they are not stripped (i.e., the filter does not work).
For (2), just try to see the attachments using 'v' and then ENTER in the corresponding message. For example, you can use the mutt mailing list daily diggest... :)
>Fix:
For (1), perhaps this is some silly regexp problem.
Not a clue for (2).
}}}
