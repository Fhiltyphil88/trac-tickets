Ticket:  3453
Status:  new
Summary: mutt-1.5.18: wrap line as they can be selected with a double click

Reporter: fraff@free.fr
Owner:    mutt-dev

Opened:       2010-09-20 14:32:05 UTC
Last Updated: 2012-10-30 05:27:11 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.5.18-6
Severity: wishlist

-- Please type your report below this line

it would be nice if mutt could wrap long line (url for exemple) as irssi does,
that is, selectable with a double click and pastable as this in a browser.


-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Using built-in specs.
Target: i486-linux-gnu
Configured with: ../src/configure -v --with-pkgversion='Debian 4.3.2-1.1' --with-bugurl=file:///usr/share/doc/gcc-4.3/README.Bugs --enable-languages=c,c++,fortran,objc,obj-c++ --prefix=/usr --enable-shared --with-system-zlib --libexecdir=/usr/lib --without-included-gettext --enable-threads=posix --enable-nls --with-gxx-include-dir=/usr/include/c++/4.3 --program-suffix=-4.3 --enable-clocale=gnu --enable-libstdcxx-debug --enable-objc-gc --enable-mpfr --enable-targets=all --enable-cld --enable-checking=release --build=i486-linux-gnu --host=i486-linux-gnu --target=i486-linux-gnu
Thread model: posix
gcc version 4.3.2 (Debian 4.3.2-1.1) 

- CFLAGS
-Wall -pedantic -Wno-long-long -g -O2

-- Mutt Version Information

Mutt 1.5.18 (2008-05-17)
Copyright (C) 1996-2008 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.6.18-6-686 (i686)
ncurses: ncurses 5.7.20081213 (compiled with 5.7)
libidn: 1.8 (compiled with 1.10)
hcache backend: GDBM version 1.8.3. 10/15/2002 (built Apr 24 2006 03:25:20)
Compile options:
-DOMAIN
+DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK   
+USE_POP  +USE_IMAP  +USE_SMTP  +USE_GSS  -USE_SSL_OPENSSL  +USE_SSL_GNUTLS  +USE_SASL  +HAVE_GETADDRINFO  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  
-EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +COMPRESSED  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_LIBIDN  +HAVE_GETSID  +USE_HCACHE  
-ISPELL
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
MIXMASTER="mixmaster"
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please visit http://bugs.mutt.org/.

patch-1.5.13.cd.ifdef.2
patch-1.5.13.cd.purge_message.3.4
patch-1.5.13.nt+ab.xtitles.4
patch-1.5.4.vk.pgp_verbose_mime
patch-1.5.6.dw.maildir-mtime.1
patch-1.5.8.hr.sensible_browser_position.3
}}}


--------------------------------------------------------------------------------
2010-09-20 14:32:05 UTC fraff@free.fr
* Added comment:

This message has 0 attachment(s)


--------------------------------------------------------------------------------
2010-09-20 22:08:07 UTC dickey@his.com
* Added comment:
{{{
On Mon, 20 Sep 2010, Mutt wrote:


It's lost by cursor-movement optimization (in ncurses, curses or slang).

xterm sets the line-wrap flag when text is written automatically wrapping 
on the right margin.  Optimization may notice that it's faster to jump to 
the next line by some other way.
}}}

--------------------------------------------------------------------------------
2010-09-24 12:34:59 UTC fraff@free.fr
* Added comment:
{{{
do you mean it's impossible to implement it the way mutt is designed ?

why does it work in irssi ?

On Mon, Sep 20, 2010 at 22:08:07 -0000, Mutt wrote:







}}}

--------------------------------------------------------------------------------
2010-09-24 21:25:03 UTC dickey@his.com
* Added comment:
{{{
On Fri, 24 Sep 2010, Francois Lallart wrote:


I'm not sure.  I just took a quick look at its source, and see a module 
that uses curses (no termcap, etc.), and don't see any workarounds for
the newline optimization.  However, a comment in the changelog suggests
that it can be built with termcap - which wouldn't do the optimization.
I don't think that's likely though (you could check by doing a 'strings'
on its binaries and looking for "initscr" to prove that it's using 
curses).

Perhaps it's mostly-working since irssi is basically a scrolling window,
and there's little prior text that could be optimized.

}}}

--------------------------------------------------------------------------------
2010-09-25 00:25:36 UTC me
* Added comment:
{{{
On Mon, Sep 20, 2010 at 06:07:59PM -0400, Thomas Dickey wrote:

Does the fact that mutt writes to the screen 1 character at a time have any 
effect?  I just verified that the pager code does not do addch('\n') when line 
wrapping (pager.c:1490), it lets ncurses do the wrapping.  mutt *does* reset 
the color attributes before writing the next line.

me
}}}

--------------------------------------------------------------------------------
2010-09-25 00:57:21 UTC dickey@his.com
* Added comment:
{{{
On Fri, 24 Sep 2010, Michael Elkins wrote:


no - using addch has no effect.  curses collects all of that information 
and writes out the changes to the screen when "refresh" is called (which 
is also a side-effect of calling getch).
}}}

--------------------------------------------------------------------------------
2010-11-30 10:25:21 UTC fraff@free.fr
* Added comment:
{{{
I think pager code does addch('\n') when line wraping (pager.c:1233 then
pager.c:1490). I modified wrap_cols (pager.c:1100) to avoid that
addch('\n') but it still doesn't allow to select a multi lines url.


On Fri, Sep 24, 2010 at 17:26:22 -0700, Michael Elkins wrote:



}}}

--------------------------------------------------------------------------------
2010-11-30 10:46:30 UTC dickey@his.com
* Added comment:
{{{
On Tue, 30 Nov 2010, Francois Lallart wrote:


That's expected behavior.  It would only change if there were some way
to tell curses to not optimize the wrapped lines.

}}}

--------------------------------------------------------------------------------
2012-10-24 03:20:00 UTC powerman
* Added comment:
Looks like Vim somehow solved this issue (clicking on long urls wrapped over several lines with ":set wrap" works ok in konsole and urxvt), so this annoying bug can be fixed in mutt.

--------------------------------------------------------------------------------
2012-10-24 03:20:24 UTC powerman
* cc changed to powerman-asdf@ya.ru

--------------------------------------------------------------------------------
2012-10-30 05:27:11 UTC wyardley
* cc changed to powerman-asdf@ya.ru, mutt-dev@veggiechinese.net
