Ticket:  1841
Status:  new
Summary: message-hook NOOP For Attached Message

Reporter: lists+mutt_bugs_submit@bigfatdave.com
Owner:    mutt-dev

Opened:       2004-03-30 21:30:16 UTC
Last Updated: 2005-08-26 16:55:42 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.5.6i
Severity: normal

-- Please type your report below this line

Well, I've got message-hook mania, setting and unsetting stuff based
on all sorts of criteria when viewing a message.  I just noticed that
when somebody forwards me a message as an attachment and I view it,
the settings of the parent message hold, not of the child one (the one
I'm viewing).  Considering the fact that these types of attachments are
first-class messages in almost all other ways, I find it particularly
annoying that they aren't in this most obvious (and rather basic, when
you think of it for a second) way.

Thanks,
 - Dave

BTW - My entire Mutt configuration directory is online:
http://www.bigfatdave.com/dave/mutt/muttdir/

-- System Information
System Version: Linux dave2 2.4.25+fuse #5 SMP Sun Mar 14 00:57:36 EST 2004 i686 unknown unknown GNU/Linux

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Leyendo especificaciones de /usr/local/lib/gcc-lib/i686-pc-linux-gnu/3.2.3/specs
Configurado con: ./configure --enable-version-specific-runtime-libs --enable-languages=c,c++
Modelo de hilos: posix
gcc versión 3.2.3

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.5.6i (2004-02-01)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.25+fuse (i686) [using ncurses 5.3]
Opciones especificadas al compilar:
-DOMAIN
+DEBUG
-HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
-USE_POP  +USE_IMAP  -USE_GSS  +USE_SSL  -USE_SASL  -USE_SASL2  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  -HAVE_LIBIDN  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/bin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/deta/mutt_cvs/usr/share/mutt"
SYSCONFDIR="/deta/mutt_cvs/usr/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
Para contactar a los desarrolladores mande un mensaje a <mutt-dev@mutt.org>.
Para reportar un fallo use la utilería flea(1) por favor.


--- Begin /home/dave/.muttrc
source ~/.mutt/muttrc
--- End /home/dave/.muttrc


--- Begin /deta/mutt_cvs/usr/etc/Muttrc
ignore "from " received content- mime-version status x-status message-id
ignore sender references return-path lines
macro index \eb '/~b ' 'search in message bodies'
macro index \cb |urlview\n 'call urlview to extract URLs out of a message'
macro pager \cb |urlview\n 'call urlview to extract URLs out of a message'
macro generic <f1> "!less /beta/mutt_cvs/usr/doc/mutt/manual.txt\n" "Show Mutt documentation"
macro index   <f1> "!less /beta/mutt_cvs/usr/doc/mutt/manual.txt\n" "Show Mutt documentation"
macro pager   <f1> "!less /beta/mutt_cvs/usr/doc/mutt/manual.txt\n" "Show Mutt documentation"
--- End /deta/mutt_cvs/usr/etc/Muttrc




>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2004-03-30 21:30:16 UTC lists+mutt_bugs_submit@bigfatdave.com
* Added comment:
{{{
Package: mutt
Version: 1.5.6i
Severity: normal

-- Please type your report below this line

Well, I've got message-hook mania, setting and unsetting stuff based
on all sorts of criteria when viewing a message.  I just noticed that
when somebody forwards me a message as an attachment and I view it,
the settings of the parent message hold, not of the child one (the one
I'm viewing).  Considering the fact that these types of attachments are
first-class messages in almost all other ways, I find it particularly
annoying that they aren't in this most obvious (and rather basic, when
you think of it for a second) way.

Thanks,
- Dave

BTW - My entire Mutt configuration directory is online:
http://www.bigfatdave.com/dave/mutt/muttdir/

-- System Information
System Version: Linux dave2 2.4.25+fuse #5 SMP Sun Mar 14 00:57:36 EST 2004 i686 unknown unknown GNU/Linux

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Leyendo especificaciones de /usr/local/lib/gcc-lib/i686-pc-linux-gnu/3.2.3/specs
Configurado con: ./configure --enable-version-specific-runtime-libs --enable-languages=c,c++
Modelo de hilos: posix
gcc versión 3.2.3

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.5.6i (2004-02-01)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.25+fuse (i686) [using ncurses 5.3]
Opciones especificadas al compilar:
-DOMAIN
+DEBUG
-HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
-USE_POP  +USE_IMAP  -USE_GSS  +USE_SSL  -USE_SASL  -USE_SASL2  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  -HAVE_LIBIDN  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/bin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/deta/mutt_cvs/usr/share/mutt"
SYSCONFDIR="/deta/mutt_cvs/usr/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
Para contactar a los desarrolladores mande un mensaje a <mutt-dev@mutt.org>.
Para reportar un fallo use la utilería flea(1) por favor.


--- Begin /home/dave/.muttrc
source ~/.mutt/muttrc
--- End /home/dave/.muttrc


--- Begin /deta/mutt_cvs/usr/etc/Muttrc
ignore "from " received content- mime-version status x-status message-id
ignore sender references return-path lines
macro index \eb '/~b ' 'search in message bodies'
macro index \cb |urlview\n 'call urlview to extract URLs out of a message'
macro pager \cb |urlview\n 'call urlview to extract URLs out of a message'
macro generic <f1> "!less /beta/mutt_cvs/usr/doc/mutt/manual.txt\n" "Show Mutt documentation"
macro index   <f1> "!less /beta/mutt_cvs/usr/doc/mutt/manual.txt\n" "Show Mutt documentation"
macro pager   <f1> "!less /beta/mutt_cvs/usr/doc/mutt/manual.txt\n" "Show Mutt documentation"
--- End /deta/mutt_cvs/usr/etc/Muttrc
}}}

--------------------------------------------------------------------------------
2004-04-13 14:09:43 UTC Thomas Roessler <roessler@does-not-exist.org>
* Added comment:
{{{
priority 1841 wishlist
}}}

--------------------------------------------------------------------------------
2005-08-27 10:55:42 UTC rado
* Added comment:
{{{
change-request
}}}
