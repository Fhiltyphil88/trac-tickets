Ticket:  3031
Status:  new
Summary: Handle multiple folders at the same time (via <add-folder> command)

Reporter: memoryhole
Owner:    mutt-dev

Opened:       2008-02-15 15:50:19 UTC
Last Updated: 2009-09-26 10:16:26 UTC

Priority:  minor
Component: mutt
Keywords:  multiple folders

--------------------------------------------------------------------------------
Description:
This addresses a complaint/question that comes up every now and then, and would probably provide a lot of side-benefits that we can't even think of at the moment. It would be excellent if mutt could view the contents of several folders at the same time, in an integrated display (i.e. there's not necessarily any visible distinction between messages in one folder and another).

If this worked, one could (for example) set limits and search within multiple folders.

It MAY even be useful to add another % option to index_format that allows each message to display which folder it's from, and a new sorting option to sort (or sort_aux) by folder.

Here's the way I imagine the interface could work (just a suggestion). <change-folder> works as it currently does (namely, it clears the index and rebuilds the index from the contents of the new folder), however we can add a new command: <add-folder>. This command would behave similar to <change-folder> (i.e. the prompt, the cycling through mailboxes, etc.) but instead of clearing the index and rebuilding it with the contents of the new folder, it adds the contents of the new folder to the existing index list, and re-sorts the index to merge the two together.

If this could work generically, one could theoretically view both the local mailspool and an IMAP INBOX at the same time (for example). You could also view all new mail messages at the same time in all of your mailboxes (by adding them all, and then limiting the view to just new messages). You could even search all mailboxes by adding all mailboxes and then searching.

Thinking beyond this, really blue-sky, would be if mailboxes were handled similar to the way threading is handled: that all messages in a given mailbox could be "collapsed" the way a thread can be collapsed.

I don't know how difficult any of this would be to implement. It would be awesome, though: very mutt-like, very simple interface, and extraordinarily powerful (better than any other mail reader out there, bar none).

--------------------------------------------------------------------------------
2008-02-15 16:43:40 UTC memoryhole
* summary changed to Handle multiple folders at the same time (via <add-folder> command)

--------------------------------------------------------------------------------
2008-02-15 18:02:01 UTC vinc17
* Added comment:
This would be very useful. There should also be a way to define folder lists and names associated with them, e.g. in the .muttrc:
{{{
folder-list my_folder path_to_folder1 path_to_folder2 path_to_folder3
}}}
and something like
{{{
mutt -f @my_folder
}}}
to open a folder list (not sure about the best prefix, but + and = are already taken).

--------------------------------------------------------------------------------
2008-02-15 18:57:45 UTC Kyle Wheeler
* Added attachment part0001.pgp
* Added comment:
Added by email2trac

* Added comment:
{{{
On Friday, February 15 at 06:02 PM, quoth Mutt:
>#3031: Handle multiple folders at the same time (via <add-folder> command)
>
>Comment (by vinc17):
>
> This would be very useful. There should also be a way to define folder
> lists and names associated with them, e.g. in the .muttrc:
> {{{
> folder-list my_folder path_to_folder1 path_to_folder2 path_to_folder3
> }}}
> and something like
> {{{
> mutt -f @my_folder
> }}}
> to open a folder list (not sure about the best prefix, but + and = are
> already taken).

Well, that sort of thing could be done with a macro, so I'd be content 
if mutt didn't do that (though your suggestion would be convenient). 
For what it's worth, the current reserved prefixes are: !, !!, ~, +, 
=, <, >, -, @, and ^. I think a better prefix for what you describe 
would be &.

~Kyle
}}}

--------------------------------------------------------------------------------
2008-02-16 23:50:53 UTC Vincent Lefevre
* Added comment:
{{{
On 2008-02-15 12:57:50 -0600, Kyle Wheeler wrote:
[folder lists]
> Well, that sort of thing could be done with a macro,

With many limitations, e.g. not if one wants to handle folder list
a bit like normal folders. Similarly, mail aliases can be done with
a macro.

> so I'd be content if mutt didn't do that (though your suggestion
> would be convenient). For what it's worth, the current reserved
> prefixes are: !, !!, ~, +, =, <, >, -, @, and ^. I think a better
> prefix for what you describe would be &.

But most prefixes are used in other contexts. Or could there be
conflicts with some commands?
}}}

--------------------------------------------------------------------------------
2009-09-26 10:16:26 UTC ZmjbS
* cc changed to martin@swift.is
* Added comment:
Has there been any progress on this?
