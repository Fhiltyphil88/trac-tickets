Ticket:  2147
Status:  new
Summary: Unbale to see the image as a part of email message body - mutt with -i option

Reporter: jagtapr@yahoo.com
Owner:    mutt-dev

Opened:       2005-12-08 15:21:27 UTC
Last Updated: 2005-12-14 21:45:01 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Unbale to see the image as a part of email message body - mutt with -i option

I am new to mutt. I need help with one of the mutt option. The option is -i
My requirement is to send the image as a part of the message body instead of an attachment.
 
This is what I am doing
mutt -s "test email" myemail@email.com -i file.jpg
 
I am not getting any problem while sending this but while reading I am not seeing this image as a part of message body instead I am getting binary message (some junk characters)
 
I am using yahoo mail and Microsoft outlook to read this email.
Can you please help me so that I can see the image as a part of message body?
 
Note: 
1) When I send this image as an attachment I can see the image while reading the email in yahoo as well as Microsoft outlook (When I click to open the image)
 
2) mutt version = mutt-1.4.1-10
3) Red Hat Enterprise Linux ES release 4
 
Thanks for the help.
Raj

>How-To-Repeat:
mutt -s "test email" myemail@email.com -i file.jpg
>Fix:
Unknown
}}}

--------------------------------------------------------------------------------
2005-12-08 23:56:52 UTC Derek Martin <invalid@pizzashack.org>
* Added comment:
{{{
On Thu, Dec 08, 2005 at 04:21:27PM +0100, jagtapr@yahoo.com wrote:
> Unbale to see the image as a part of email message body - mutt with -i option
> 
> I am new to mutt. I need help with one of the mutt option. The
> option is -i My requirement is to send the image as a part of the
> message body instead of an attachment.

I think this is not at all what you want to do.  I believe other mail
programs handle this by creating an HTML message which refers to the
image, and including the image as an attachment.  Mutt isn't really
designed to do this (though it probably should be), though IIRC there
are patches to handle creation of HTML mail.  I don't know where to
find them, or if they will help you do what you want to do.  If you
need to do this a lot, I suspect you will be better off with a
different mail program...
}}}

--------------------------------------------------------------------------------
2005-12-09 08:44:46 UTC Paul Walker <paul@black-sun.demon.co.uk>
* Added comment:
{{{
On Thu, Dec 08, 2005 at 04:21:27PM +0100, jagtapr@yahoo.com wrote:

> I am not getting any problem while sending this but while reading I am not
> seeing this image as a part of message body instead I am getting binary
> message (some junk characters)

Well, yes - that would be the image you asked it to include.

Try installing the uudeview program, and something like

uuenview image.jpg | mutt -s "Test" address

should do. (uuenview can apparently also send mails itself, if you wanted to
investigate that.)

-- 
Paul

I don't mind food that bites back, a little, but I draw the line at food
that commits assault with intent to maim. -- Dave Luckett

--2Z2K0IlrPCVsbNpk
Content-Type: application/pgp-signature; name="signature.asc"
Content-Description: Digital signature
Content-Disposition: inline

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.1 (GNU/Linux)

iD8DBQFDmGL+P9fOqdxRstoRArJOAJ0SPIWNI5RLHuoWImJfEjrnZTWGygCgnBQc
k0B+vFy6Biv6rYPMV5kAUhI=Y2c9
-----END PGP SIGNATURE-----

--2Z2K0IlrPCVsbNpk--
}}}

--------------------------------------------------------------------------------
2005-12-09 13:44:58 UTC Moritz Barsnick <moritz+mutt@barsnick.net>
* Added comment:
{{{
Hi,

On Thu, Dec 08, 2005 at 16:21:27 +0100, jagtapr@yahoo.com wrote:

> I am new to mutt. I need help with one of the mutt option. The option
> is -i My requirement is to send the image as a part of the message
> body instead of an attachment.
[...]
> Can you please help me so that I can see the image as a part of
> message body?

As others have said: It is probably usual to attach an image, and make
the body of the email in text/html, just displaying the said image.

What you can do (but you'll have to wait until tomorrow until I see the
result in Outlook ;->):

Write an email with the image attached. In the compose menu (before
finally sending the mail), you should see two "attachments": The "body"
(as text/plain, hopefully), and the attached image (as image/*,
probably base64 encoded).

Now, just delete the body "attachment" (default: 'D') -> bingo, you
have an email consisting only of the image!

It is declared as an "attachment" though, I don't know whether that
matters:

> Content-Type: image/jpeg
> Content-Disposition: attachment; filename="myfile.jpg"

And, as I said, I refuse to check the result in Outlook before
tomorrow. :)

Hope this helps,
Moritz
}}}

--------------------------------------------------------------------------------
2005-12-09 15:15:38 UTC Raj <jagtapr@yahoo.com>
* Added comment:
{{{
--0-1171117096-1134141338=:51999
Content-Type: text/plain; charset=iso-8859-1
Content-Transfer-Encoding: 8bit

  Thank you so much for the hits and the updates. If createing the HTML e-mail is the only option that I have then is this something I can do with mutt ? Does mutt will support this?
   
  Also Do you have any example that I can loot at?
   
  Thank you so must for the help.
  

			
---------------------------------
Yahoo! Shopping
 Find Great Deals on Holiday Gifts at Yahoo! Shopping 
--0-1171117096-1134141338=:51999
Content-Type: text/html; charset=iso-8859-1
Content-Transfer-Encoding: 8bit

<FONT size=2>  <div>Thank you so much for the hits and the updates. If createing the HTML e-mail is the only option that I have then is this something I can do with mutt ? Does mutt will support this?</div>  <div>&nbsp;</div>  <div>Also Do you have any example that I can loot at?</div>  <div>&nbsp;</div>  <div>Thank you so must for the help.</div></FONT>  <DIV id=RTEContent></DIV><p>
	
		<hr size=1>Yahoo! Shopping<br> 
Find Great Deals on Holiday Gifts at <a href="http://us.rd.yahoo.com/mail_us/footer/shopping/*http://shopping.yahoo.com/;_ylc=X3oDMTE2bzVzaHJtBF9TAzk1OTQ5NjM2BHNlYwNtYWlsdGFnBHNsawNob2xpZGF5LTA1 
">Yahoo! Shopping</a> 
--0-1171117096-1134141338=:51999--
}}}

--------------------------------------------------------------------------------
2005-12-10 02:30:16 UTC Moritz Barsnick <moritz+mutt@barsnick.net>
* Added comment:
{{{
On Thu, Dec 08, 2005 at 21:35:01 +0100, Moritz Barsnick wrote:
>  What you can do (but you'll have to wait until tomorrow until I see the
>  result in Outlook ;->):
[...]
>  Now, just delete the body "attachment" (default: 'D') -> bingo, you
>  have an email consisting only of the image!

Okay, here at work today, I can say that Outlook displays this mail
consisting only of an an empty mail (body) with an image attachment,
which is not what you wanted.

>  > Content-Type: image/jpeg
>  > Content-Disposition: attachment; filename="myfile.jpg"

Removing the "Content-Disposition:" line (albeit manually editing the
mbox, and then bouncing or moving it to Outlook) does not change this
behavior.

So I finally believe that what you "want" (do you really??) is an HTML
e-mail. Blah!  ;-)

Moritz
}}}

--------------------------------------------------------------------------------
2005-12-11 16:31:56 UTC TAKAHASHI Tamotsu <ttakah@lapis.plala.or.jp>
* Added comment:
{{{
* Fri Dec  9 2005 Raj <jagtapr@yahoo.com>
>    Thank you so much for the hits and the updates. If createing
> the HTML e-mail is the only option that I have then is this
> something I can do with mutt ? Does mutt will support this?

I don't think so.

HTML messages use ``<img src="cid:foobar">'' to show images,
but ``grep -i content-id ~/mutt/*.c'' returns nothing.

We can modify Content-Type and Content-Description with mutt,
but mutt has no function to modify/create Content-Id.

If you write a sendmail-wrapper to
 * add a CID to each attachments
 * and s/attach1/cid:<the-CID-of-the-first-attachment>/,
you can use ``<img src="attach1">'' to send HTML messages.

-- 
tamo
}}}

--------------------------------------------------------------------------------
2005-12-14 20:40:24 UTC Raj <jagtapr@yahoo.com>
* Added comment:
{{{
--0-395425824-1134592824=:86646
Content-Type: text/plain; charset=iso-8859-1
Content-Transfer-Encoding: 8bit

Hi,
   
  Thank you so much for the udpates and the examples. I am trying the example as mentioned here. Will post the reply as soon as I have the results.
   
  Thanks again
  Raj

Moritz Barsnick <moritz+mutt@barsnick.net> wrote:
  [Cc:ing to Raj for readability, as the BTS->mutt-dev make break some
things.]

Hi,

On Sat, Dec 10, 2005 at 07:35:01 +0100, TAKAHASHI Tamotsu wrote:
> HTML messages use ``'' to show images,
> but ``grep -i content-id ~/mutt/*.c'' returns nothing.

I think it's sufficient to attach an image
(Content-Disposition: attachment; filename="1.jpg")
and refer to that image with the HTML  tag. At least
for Outlook.

> We can modify Content-Type and Content-Description with mutt,
> but mutt has no function to modify/create Content-Id.

Can this (the former) be done from mutt's command line? I couldn't
achieve it, and it seems such an HTML needs a few more quirks than just
Content-Type.

> If you write a sendmail-wrapper to
> * add a CID to each attachments
> * and s/attach1/cid:/,
> you can use ``'' to send HTML messages.

As a "POC" (proof of concept), I wrote this one line wrapper. It needs
mimencode for the base64 though, and required me to do some fiddling
with headers and boundaries - something mutt is much better at. ;-)

Here comes the really long line (feel free to hack it into a script
with parameters). I broke it at the '\$' places for readability:

( echo -e 'From: my_email\nTo: recipient@domain.com\n\
Subject: embedded image test\n\
Content-Type: multipart/mixed; boundary="bound"\n\n\n\
--bound\nContent-Type: text/html; charset=us-ascii\n\
Content-Disposition: inline\n\n\
\n--bound\n\
Content-Type: image/jpeg\n\
Content-Disposition: attachment; filename="1.jpg"\n\
Content-Transfer-Encoding: base64\n'; \
mimencode sendmail -oi -t -v

Only flaw: 
The image will display inline _and_ be presented as an attachment in
Outlook...

HTH,
Moritz
  


			
---------------------------------
Yahoo! Shopping
 Find Great Deals on Holiday Gifts at Yahoo! Shopping 
--0-395425824-1134592824=:86646
Content-Type: text/html; charset=iso-8859-1
Content-Transfer-Encoding: 8bit

<DIV>Hi,</DIV>  <DIV>&nbsp;</DIV>  <DIV>Thank you so much for the udpates and the examples. I am trying the example as mentioned here. Will post the reply as soon as I have the results.</DIV>  <DIV>&nbsp;</DIV>  <DIV>Thanks again</DIV>  <DIV>Raj<BR><BR><B><I>Moritz Barsnick &lt;moritz+mutt@barsnick.net&gt;</I></B> wrote:</DIV>  <BLOCKQUOTE class=replbq style="PADDING-LEFT: 5px; MARGIN-LEFT: 5px; BORDER-LEFT: #1010ff 2px solid">[Cc:ing to Raj for readability, as the BTS-&gt;mutt-dev make break some<BR>things.]<BR><BR>Hi,<BR><BR>On Sat, Dec 10, 2005 at 07:35:01 +0100, TAKAHASHI Tamotsu wrote:<BR>&gt; HTML messages use ``<IMG src="cid:foobar">'' to show images,<BR>&gt; but ``grep -i content-id ~/mutt/*.c'' returns nothing.<BR><BR>I think it's sufficient to attach an image<BR>(Content-Disposition: attachment; filename="1.jpg")<BR>and refer to that image with the HTML <IMG src="http://us.f511.mail.yahoo.com/ym/1.jpg"> tag. At least<BR>for Outlook.<BR><BR>&gt; We can modify Content-Type
 and Content-Description with mutt,<BR>&gt; but mutt has no function to modify/create Content-Id.<BR><BR>Can this (the former) be done from mutt's command line? I couldn't<BR>achieve it, and it seems such an HTML needs a few more quirks than just<BR>Content-Type.<BR><BR>&gt; If you write a sendmail-wrapper to<BR>&gt; * add a CID to each attachments<BR>&gt; * and s/attach1/cid:<THE-CID-OF-THE-FIRST-ATTACHMENT>/,<BR>&gt; you can use ``<IMG src="http://us.f511.mail.yahoo.com/ym/attach1">'' to send HTML messages.<BR><BR>As a "POC" (proof of concept), I wrote this one line wrapper. It needs<BR>mimencode for the base64 though, and required me to do some fiddling<BR>with headers and boundaries - something mutt is much better at. ;-)<BR><BR>Here comes the really long line (feel free to hack it into a script<BR>with parameters). I broke it at the '\$' places for readability:<BR><BR>( echo -e 'From: my_email\nTo: recipient@domain.com\n\<BR>Subject: embedded image test\n\<BR>Content-Type:
 multipart/mixed; boundary="bound"\n\n\n\<BR>--bound\nContent-Type: text/html; charset=us-ascii\n\<BR>Content-Disposition: inline\n\n\<BR><IMG src="http://us.f511.mail.yahoo.com/ym/1.jpg">\n--bound\n\<BR>Content-Type: image/jpeg\n\<BR>Content-Disposition: attachment; filename="1.jpg"\n\<BR>Content-Transfer-Encoding: base64\n'; \<BR>mimencode <THE_IMAGE.JPG; \<br | ) ?\n--bound--\n? -e echo>sendmail -oi -t -v<BR><BR>Only flaw: <BR>The image will display inline _and_ be presented as an attachment in<BR>Outlook...<BR><BR>HTH,<BR>Moritz<BR></BLOCKQUOTE>  <DIV><BR></DIV><p>
	
		<hr size=1>Yahoo! Shopping<br> 
Find Great Deals on Holiday Gifts at <a href="http://us.rd.yahoo.com/mail_us/footer/shopping/*http://shopping.yahoo.com/;_ylc=X3oDMTE2bzVzaHJtBF9TAzk1OTQ5NjM2BHNlYwNtYWlsdGFnBHNsawNob2xpZGF5LTA1 
">Yahoo! Shopping</a> 
--0-395425824-1134592824=:86646--
}}}

--------------------------------------------------------------------------------
2005-12-15 11:12:22 UTC Moritz Barsnick <moritz+mutt@barsnick.net>
* Added comment:
{{{
[Cc:ing to Raj for readability, as the BTS->mutt-dev make break some
things.]

Hi,

On Sat, Dec 10, 2005 at 07:35:01 +0100, TAKAHASHI Tamotsu wrote:
>  HTML messages use ``<img src="cid:foobar">'' to show images,
>  but ``grep -i content-id ~/mutt/*.c'' returns nothing.

I think it's sufficient to attach an image
(Content-Disposition: attachment; filename="1.jpg")
and refer to that image with the HTML <IMG SRC="1.jpg"> tag. At least
for Outlook.

>  We can modify Content-Type and Content-Description with mutt,
>  but mutt has no function to modify/create Content-Id.

Can this (the former) be done from mutt's command line? I couldn't
achieve it, and it seems such an HTML needs a few more quirks than just
Content-Type.

>  If you write a sendmail-wrapper to
>   * add a CID to each attachments
>   * and s/attach1/cid:<the-CID-of-the-first-attachment>/,
>  you can use ``<img src="attach1">'' to send HTML messages.

As a "POC" (proof of concept), I wrote this one line wrapper. It needs
mimencode for the base64 though, and required me to do some fiddling
with headers and boundaries - something mutt is much better at.  ;-)

Here comes the really long line (feel free to hack it into a script
with parameters). I broke it at the '\$' places for readability:

( echo -e 'From: my_email\nTo: recipient@domain.com\n\
Subject: embedded image test\n\
Content-Type: multipart/mixed; boundary="bound"\n\n\n\
--bound\nContent-Type: text/html; charset=us-ascii\n\
Content-Disposition: inline\n\n\
<HTML><IMG SRC="1.jpg"></HTML>\n--bound\n\
Content-Type: image/jpeg\n\
Content-Disposition: attachment; filename="1.jpg"\n\
Content-Transfer-Encoding: base64\n'; \
mimencode <the_image.jpg; echo -e '\n--bound--\n' ) | \
sendmail -oi -t -v

Only flaw: 
The image will display inline _and_ be presented as an attachment in
Outlook...

HTH,
Moritz
}}}
