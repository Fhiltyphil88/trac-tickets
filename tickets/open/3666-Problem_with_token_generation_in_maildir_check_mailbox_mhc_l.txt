Ticket:  3666
Status:  new
Summary: Problem with token generation in maildir_check_mailbox (mh.c line ~ 1900)

Reporter: louis
Owner:    mutt-dev

Opened:       2013-11-08 17:29:50 UTC
Last Updated: 2013-11-12 10:43:42 UTC

Priority:  critical
Component: maildir/mh
Keywords:  

--------------------------------------------------------------------------------
Description:
Hi,

'''Situation:'''

The function maildir_check_mailbox (mh.c:1900) detects "real" new mail
("real" as opposed to mails that have only been changed by another
program but are already in ctx) by the following way:

 - it starts by adding a "token" into a hashtable called "fnames" for each mail detected as "new";

 - then for all mails in ctx->hdrs[], if the token of ctx->hdrs[i] appears in "fnames" that means the i-th mail has been changed and is not a real new mail.

'''Problem is:''' 

the token for a mail cur/msg.KKKK:2,S is
msg.KKKK. Therefore if we have a mail A in the file new/msg.KKKK and a
mail B in the file cur/msg.KKKK, the mail A will not be detected as
new and will only be considered as an updated version of
B.

Furthermore any action towards the mail B in the index will, in reality,
occur to mail A. (If mail B has "B" for subject, then you will see "B"
in the index view and if you delete it you will in reality delete A.)

I know mail names are supposed to be random and this problem should
not appear to often but my fetchmail seems to produce a *lot* of these
collisions.

'''Step to reproduce:'''

'''With the tar I will try to attach:''' open mutt and point it to the
Maildir in the tar. Move the message at the root of the Maildir to
new and see that mutt doest not show it. Open mail A in mutt and see
that mail B is opened.

'''In general:''' Take a mail A from Maildir/cur/ and copy it to
/tmp/. Edit the mail to change message-id, subject and body. Open mutt
and mv the message B (the one from /tmp/) to Maildir/new. You should
not be able to view the subject from B but if you open the mail A, B
should appear.



'''Proposed solution:'''

adding the new or the cur in the token?



--------------------------------------------------------------------------------
2013-11-08 17:31:08 UTC louis
* Added attachment Maildir.tar
* Added comment:
buggy maildir

--------------------------------------------------------------------------------
2013-11-12 10:43:42 UTC louis
* Added comment:
I found out why my names were not random : [http://wiki.dovecot.org/MailboxFormat/Maildir#Procmail_Problems].

The problem still exists with MH…
