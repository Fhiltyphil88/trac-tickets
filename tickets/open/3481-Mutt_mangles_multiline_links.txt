Ticket:  3481
Status:  new
Summary: Mutt mangles multiline links

Reporter: hukka
Owner:    mutt-dev

Opened:       2010-12-20 07:44:41 UTC
Last Updated: 2010-12-20 09:29:31 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
Even after getting rid of the "+" at the end of lines, multiline links still don't work in gnome-terminal (ie. clicking it only opens the link as it would be only on the first line). Piping to less works, so the gnome-terminal isn't at fault. Even irssi works, so ncurses in general should be ok.

Seems like mutt is somehow handling the linewrap itself instead of letting ncurses take care of it, and that additional linebreak confuses the link recognition of terminals.

--------------------------------------------------------------------------------
2010-12-20 09:11:03 UTC vinc17
* Added comment:
Dup of bug #3453?

--------------------------------------------------------------------------------
2010-12-20 09:17:56 UTC hukka
* Added comment:
Hmm, might be. The reporter of #3453 refers to xterm and selecting by double clicking, which is different, but might be two symptoms of the same cause.

--------------------------------------------------------------------------------
2010-12-20 09:29:31 UTC dickey@his.com
* Added comment:
{{{
On Mon, 20 Dec 2010, Mutt wrote:


as I commented a few months ago, ncurses's cursor-movement optimization 
can be a cause of this...
}}}
