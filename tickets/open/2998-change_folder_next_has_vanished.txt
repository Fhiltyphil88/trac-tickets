Ticket:  2998
Status:  reopened
Summary: change_folder_next has vanished

Reporter: cjs
Owner:    mutt-dev

Opened:       2007-12-06 04:46:21 UTC
Last Updated: 2007-12-06 23:19:19 UTC

Priority:  minor
Component: mutt
Keywords:  change folder next

--------------------------------------------------------------------------------
Description:
When upgrading from 1.4.something to 1.5.17, I noticed that the setting change_folder_next appears to have vanished, and not been replaced with equivalant functionality. Can we have it back?

--------------------------------------------------------------------------------
2007-12-06 06:50:30 UTC brendan
* Added comment:
bind something to {{{next-unread-mailbox}}}. I don't remember change_folder_next in 1.4 either.

* resolution changed to invalid
* status changed to closed

--------------------------------------------------------------------------------
2007-12-06 08:46:07 UTC cjs
* Added comment:
Ah, I'd not noticed that it's actually a patch from NetBSD pkgsrc.

next-unread-mailbox is different functionality; it moves you to the next mailbox immediately, whereas change_folder_next simply starts the default list of folders the change-folder command gives you (the one you flip through with the space bar) with the next new folder after the current folder, rather than always at the beginning. I find it useful. I will attach patches.

* resolution changed to 
* status changed to reopened

--------------------------------------------------------------------------------
2007-12-06 08:47:52 UTC cjs
* Added attachment z
* Added comment:
Add change_folder_next option.

--------------------------------------------------------------------------------
2007-12-06 09:52:46 UTC vinc17
* Added comment:
Yes, that would be very useful when one wants to skip one or two folders temporarily and read all the other ones.

--------------------------------------------------------------------------------
2007-12-06 10:01:59 UTC Fabian Groffen
* Added comment:
{{{
On 06-12-2007 09:52:46 -0000, Mutt wrote:
> #2998: change_folder_next has vanished
> 
> Comment (by vinc17):
> 
>  Yes, that would be very useful when one wants to skip one or two folders
>  temporarily and read all the other ones.

See also
http://thread.gmane.org/gmane.mail.mutt.devel/12457/focus=13073
}}}

--------------------------------------------------------------------------------
2007-12-06 19:35:29 UTC Elimar Riesebieter
* Added comment:
{{{
On Thu, 06 Dec 2007 the mental interface of
Mutt told:

> #2998: change_folder_next has vanished
> 
>  When upgrading from 1.4.something to 1.5.17, I noticed that the setting
>  change_folder_next appears to have vanished, and not been replaced with
>  equivalant functionality. Can we have it back?

bind pager ,n    next-unread-mailbox
bind index ,n    next-unread-mailbox

Elimar
}}}

--------------------------------------------------------------------------------
2007-12-06 23:19:19 UTC cjs
* Added comment:
Replying to [comment:5 Elimar Riesebieter]:

> bind pager ,n    next-unread-mailbox
> bind index ,n    next-unread-mailbox

Can you explain what you're trying to say here? As described in the second comment, next-unread-mailbox is rather different functionality from what I'm looking for and what this patch implements. I don't like next-unread-mailbox myself, and don't use it regardless of whether change_folder_next is available or not.
