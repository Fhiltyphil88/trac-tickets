Ticket:  1048
Status:  new
Summary: Wishlist: New mail notification: notify in pager, report key headers in status

Reporter: pmak@lina.aaanime.net
Owner:    mutt-dev

Opened:       2002-02-13 09:17:34 UTC
Last Updated: 2007-04-07 14:36:04 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.5.0i
Severity: wishlist

-- Please type your report below this line

One thing that I've always missed since I switched from pine to mutt            
was pine's more verbose e-mail notification. Instead of just saying
"New mail in this mailbox." it would say something like:

      [New mail from Philip Mak re New mail notification ideas]                 

Also, pine checks for new mail when it is in the pager. In mutt's
case, it doesn't, so if I stay inside the pager for hours then I won't
see any new messages.

I would like to suggest that these features (verbose new e-mail
notification line, and check for new mail while in the pager) be
implemented in mutt.

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/lib/gcc-lib/i386-redhat-linux/2.96/specs
gcc version 2.96 20000731 (Red Hat Linux 7.1 2.96-98)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.5.0i (2002-01-22)
Copyright (C) 1996-2001 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.7-10 (i686) [using ncurses 5.2]
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
-USE_POP  -USE_IMAP  -USE_GSS  -USE_SSL  -USE_SASL  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+HAVE_PGP  +HAVE_SMIME  +HAVE_SMIME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  -HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/local/share/mutt"
SYSCONFDIR="/usr/local/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.



>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2005-08-21 08:08:16 UTC rado
* Added comment:
{{{
- More helpful subject.
- How should verbose status line report behave when there are many new mails since last check?
(see mail_check & timeout for big values)
}}}
