Ticket:  3583
Status:  new
Summary: mutt-1.5.21: extract-keys does not extract PGP keys from S/MIME signed mail

Reporter: matthias@towiski.de
Owner:    mutt-dev

Opened:       2012-05-23 18:54:06 UTC
Last Updated: 2012-05-23 18:54:06 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.5.21
Severity: normal

-- Please type your report below this line

When viewing a mail with a PGP key attachment named *.asc but with MIME type
text/plain that has been signed with S/MIME (weird combination, I know, but my
bank sends this stuff and I think it should work), the extract-keys command
correctly extracts the S/MIME certificate but ignores the PGP key. Just a minor
annoyance as people who use mutt for this kind of stuff will probably know what
to do, but perhaps mutt should just try all candidate attachments in this case
instead of quitting after the first one.

-- System Information
System Version: Linux aldous 2.6.38-xen #2 SMP Mon Nov 28 17:45:50 GALT 2011 x86_64 AMD Phenom(tm) II X6 1090T Processor AuthenticAMD GNU/Linux

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
x86_64-pc-linux-gnu-gcc
Using built-in specs.
COLLECT_GCC=/usr/x86_64-pc-linux-gnu/gcc-bin/4.5.3/x86_64-pc-linux-gnu-gcc
COLLECT_LTO_WRAPPER=/usr/libexec/gcc/x86_64-pc-linux-gnu/4.5.3/lto-wrapper
Target: x86_64-pc-linux-gnu
Configured with: /tmp/portage/sys-devel/gcc-4.5.3-r2/work/gcc-4.5.3/configure --prefix=/usr --bindir=/usr/x86_64-pc-linux-gnu/gcc-bin/4.5.3 --includedir=/usr/lib/gcc/x86_64-pc-linux-gnu/4.5.3/include --datadir=/usr/share/gcc-data/x86_64-pc-linux-gnu/4.5.3 --mandir=/usr/share/gcc-data/x86_64-pc-linux-gnu/4.5.3/man --infodir=/usr/share/gcc-data/x86_64-pc-linux-gnu/4.5.3/info --with-gxx-include-dir=/usr/lib/gcc/x86_64-pc-linux-gnu/4.5.3/include/g++-v4 --host=x86_64-pc-linux-gnu --build=x86_64-pc-linux-gnu --disable-altivec --disable-fixed-point --without-ppl --without-cloog --disable-lto --enable-nls --without-included-gettext --with-system-zlib --disable-werror --enable-secureplt --enable-multilib --enable-libmudflap --disable-libssp --enable-libgomp --with-python-dir=/share/gcc-data/x86_64-pc-linux-gnu/4.5.3/python --enable-checking=release --enable-java-awt=gtk --enable-languages=c,c++,java,objc --enable-shared --enable-threads=posix --enable-__cxa_atexit --enable-clocale=gnu
  --enable-targets=all --with-bugurl=http://bugs.gentoo.org/ --with-pkgversion='Gentoo 4.5.3-r2 p1.2, pie-0.4.7'
Thread model: posix
gcc version 4.5.3 (Gentoo 4.5.3-r2 p1.2, pie-0.4.7) 

- CFLAGS
-Wall -pedantic -Wno-long-long -Os -march=amdfam10 -pipe -fomit-frame-pointer -fno-ident -fforce-addr

-- Mutt Version Information

Mutt 1.5.21 (2010-09-15, Gentoo 1.5.21-r9)
Copyright (C) 1996-2009 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.6.38-xen (x86_64)
ncurses: ncurses 5.9.20110404 (compiled with 5.9)
libidn: 1.24 (compiled with 1.24)
hcache backend: tokyocabinet 1.4.47
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  -USE_FCNTL  +USE_FLOCK   
+USE_POP  +USE_NNTP  +USE_IMAP  +USE_SMTP  
-USE_SSL_OPENSSL  +USE_SSL_GNUTLS  +USE_SASL  -USE_GSS  +HAVE_GETADDRINFO  
-HAVE_REGCOMP  +USE_GNU_REGEX  +COMPRESSED  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  +CRYPT_BACKEND_GPGME  
-EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_LIBIDN  +HAVE_GETSID  +USE_HCACHE  
-ISPELL
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/spool/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc/mutt"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please visit http://bugs.mutt.org/.

fg.sidebar-dotpathsep
ar.sidebar-utf8
sidebar
fg.gpg_smime_output
patch-1.5.11.vk.pgp_verbose_mime
cd.trash_folder.3.4
dgc.subjrx
fg.change_folder_next
fg.smarttime
vvv.initials
vvv.quote
vvv.nntp
dgc.xterm.titles.v3
patch-1.5.20hg.pdmef.progress.vl.2
rr.compressed
patch-1.5.4.lpr.collapse_flagged Lukas P. Ruf <lukas.ruf@lpr.ch>
}}}


--------------------------------------------------------------------------------
2012-05-23 18:54:06 UTC matthias@towiski.de
* Added comment:

This message has 0 attachment(s)

