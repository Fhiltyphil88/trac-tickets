Ticket:  3107
Status:  new
Summary: Traditional PGP checking isn't robust enough

Reporter: pdmef
Owner:    mutt-dev

Opened:       2008-08-20 15:58:32 UTC
Last Updated: 2015-11-23 21:14:54 UTC

Priority:  minor
Component: crypto
Keywords:  

--------------------------------------------------------------------------------
Description:
When viewing messages with $pgp_auto_decode set or using check-traditional-pgp, messages like http://marc.info/?l=mutt-users&m=121916523318263&w=2 look completely broken in mutt:

{{{
-----BEGIN PGP SIGNATURE-----
[-- BEGIN PGP SIGNED MESSAGE --]



[-- END PGP SIGNED MESSAGE --]
}}}

...which may be quite misleading.

Sure, it's and an edge-case with plain text that has parts looking like PGP, but displaying an empty message is wrong in this case.

--------------------------------------------------------------------------------
2009-04-19 13:53:53 UTC pdmef
* Added comment:
As marc.info is currently not available, the message-id is:

{{{
56b92dda0808190959h6d3b8dcdk924d05f49894e9ca@mail.gmail.com
}}}

--------------------------------------------------------------------------------
2015-11-23 21:14:54 UTC kevin8t8
* milestone changed to 
