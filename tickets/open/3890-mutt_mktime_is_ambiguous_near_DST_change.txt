Ticket:  3890
Status:  new
Summary: mutt_mktime is ambiguous near DST change

Reporter: vinc17
Owner:    mutt-dev

Opened:       2016-10-25 12:02:54 UTC
Last Updated: 2016-11-17 10:17:48 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
When {{{local}}} is nonzero, meaning that the local timezone is taken into account, {{{mutt_mktime}}} (from {{{date.c}}}) is ambiguous. Indeed, the time components from the second to the year and the local timezone are not sufficient to determine a {{{time_t}}} value, due to dates near DST change. For this purpose, there is a {{{tm_isdst}}} component, but it is currently not used in Mutt's implementation.

--------------------------------------------------------------------------------
2016-10-25 12:44:55 UTC vinc17
* Added comment:
Concerning the implementation, the GNU C library (glibc) provides a {{{timegm}}} function, but obviously, it is not portable. The advice from the glibc manual is:

     *Portability note:* 'mktime' is essentially universally available.
     'timegm' is rather rare.  For the most portable conversion from a
     UTC broken-down time to a simple time, set the 'TZ' environment
     variable to UTC, call 'mktime', then set 'TZ' back.

Note: This is not thread-safe, but I don't think this would matter for Mutt.

The caller must make sure that the {{{tm_isdst}}} is properly set (this is needed anyway).

--------------------------------------------------------------------------------
2016-10-25 18:01:51 UTC derekmartin
* Added comment:
This seems to be the right idea.  But Mutt should not assume TZ is set:  These days I think TZ is typically unset, and the system libraries rely on settings from /etc/localtime, at least on Linux.  So Mutt will probably need to do something like:

{{{
    char *tz = getenv("TZ");
    setenv("TZ", "GMT", 1);
    /* do time conversion stuff */
    ...
    if (tz) setenv("TZ", tz, 1);
    else unsetenv("TZ");
    ...
}}}

There are places in Mutt where it needs to convert the sender's local time, but may not have enough information to accurately determine what the sender's DST context was for the date in question.  It does have the time offset, however, so it's probably sufficient to add the GMT offset to the struct tm before calling mktime on it.

Note:  According to the man page, mktime() is supposed to "normalize" a struct tm that has fields with values that are out of the normal range for the field.  This program demonstrates that:

{{{
#include <time.h>
#include <stdio.h>

int main(int argc, char **argv)
{
    struct tm now;
    struct tm later;
    struct tm *tmp;
    time_t start;

    start = time(NULL);
    tmp = gmtime(&start);
    now = *tmp;
    later = now;
    later.tm_sec += 3600;

    printf("Now = %ld\n", mktime(&now));
    printf("Later = %ld\n", mktime(&later));
    return 0;
}

$ gcc -o tm time.c
$ ./tm
Now = 1477436206
Later = 1477439806

}}}

If this normalization is reliable on all platforms, it may be useful for part of what Mutt needs to do here.


--------------------------------------------------------------------------------
2016-10-26 11:55:58 UTC vinc17
* Added comment:
Replying to [comment:2 derekmartin]:
> This seems to be the right idea.  But Mutt should not assume TZ is set:  These days I think TZ is typically unset, and the system libraries rely on settings from /etc/localtime, at least on Linux.

I agree. But I suppose that by saying "set 'TZ' back", the manual implicitly means to restore the TZ status (i.e. unset it if it was unset, else set it to the old value).

>  So Mutt will probably need to do something like:
> 
> {{{
>     char *tz = getenv("TZ");
>     setenv("TZ", "GMT", 1);
>     /* do time conversion stuff */
>     ...
>     if (tz) setenv("TZ", tz, 1);
>     else unsetenv("TZ");
>     ...
> }}}

Instead of "GMT", shouldn't "GMT0" (for compatibility with old systems) or "UTC0" be used? http://pubs.opengroup.org/onlinepubs/9699919799/utilities/date.html says for the {{{date -u}}}:

  Perform operations as if the TZ environment variable was set to the string "UTC0", or its equivalent historical value of "GMT0". Otherwise, date shall use the timezone indicated by the TZ environment variable or the system default if that variable is unset or null.

and in the rationale:

  Some of the new options for formatting are from the ISO C standard. The -u option was introduced to allow portable access to Coordinated Universal Time (UTC). The string "GMT0" is allowed as an equivalent TZ value to be compatible with all of the systems using the BSD implementation, where this option originated.

--------------------------------------------------------------------------------
2016-10-26 19:41:49 UTC derekmartin
* Added comment:
Replying to [comment:3 vinc17]:
> Replying to [comment:2 derekmartin]:
> I agree. But I suppose that by saying "set 'TZ' back", the manual implicitly means to 
restore the TZ status (i.e. unset it if it was unset, else set it to the old value).

Sure...  But it's a detail that could get overlooked, so I wanted to call it out explicitly.

> Instead of "GMT", shouldn't "GMT0" (for compatibility with old systems) or "UTC0" be used?

Honestly I'm unsure if GMT0 is better than GMT or not...  It's been a long while now since I regularly had occasion to access a wide variety of Unix flavors, but s far as I was aware, "GMT" was the older, longer supported value, and seemed more likely to work everywhere to me for that reason.  But if you think GMT0 is widely adopted, go with that.  If either turns out to cause problems it's easy enough to fix later.  Timezone values was one of those annoyingly non-standard things on older Unix systems, but I thought GMT always worked..



--------------------------------------------------------------------------------
2016-10-27 15:11:30 UTC vinc17
* Added comment:
Replying to [comment:4 derekmartin]:
> Honestly I'm unsure if GMT0 is better than GMT or not...  It's been a long while now since I regularly had occasion to access a wide variety of Unix flavors, but s far as I was aware, "GMT" was the older, longer supported value, and seemed more likely to work everywhere to me for that reason.  But if you think GMT0 is widely adopted, go with that.  If either turns out to cause problems it's easy enough to fix later.  Timezone values was one of those annoyingly non-standard things on older Unix systems, but I thought GMT always worked..

Well, according to https://www.postgresql.org/message-id/15040.57233.197701.455081@umbra.northsea.sevenseas.org "GMT" does not work on Tru64 Unix. It is said: "In particular, where Linux accepts GMT and reads it to be GMT0, under Tru64 Unix the correct behaviour _requires_ the use of GMT0."

--------------------------------------------------------------------------------
2016-11-05 05:05:33 UTC derekmartin
* Added attachment test_mutt_mktime.c

--------------------------------------------------------------------------------
2016-11-05 05:27:07 UTC derekmartin
* Added comment:
Actually it seems this bug is invalid.  The test program I just attached has 4 test cases: DST, non-DST, crossing the start of DST, crossing the end of DST.  It produces correct results in all four cases.

To run the test program, copy it to the mutt source tree, compile mutt, and then compile the test as:

  gcc -o ttz date.o test_mutt_mktime.c
  ./ttz

It's got the 2016 United States DST start and end dates hard-coded, so if you're in another country you may need to adjust those values.

--------------------------------------------------------------------------------
2016-11-05 05:39:31 UTC derekmartin
* Added comment:
Oh, the reason it's invalid is that the system's timezone databases know when DST start and end in your timezone.  In fact, the time_t returned by mktime() is the same whether or not your current time is in DST. THis is not worded precisely, I'll try again:

In other words, the time_t returned by mktime() called on the result of localtime() is always the GMT time plus your GMT offset IN STANDARD TIME.  The system just interprets that differently (e.g. when you call ctime() on it) depending on whether your system's configured time zone is honoring DST or not at the specified time.

I hope that makes sense, even as I fear it does not... :-)

At the least, this is how it behaves on Linux.  I tried it on a couple of recent Ubuntu installs, as well as an older CentOS install.  I have no other Unixen to test on, sadly. :-(

--------------------------------------------------------------------------------
2016-11-06 20:41:10 UTC vinc17
* Added comment:
Replying to [comment:7 derekmartin]:
> Oh, the reason it's invalid is that the system's timezone databases know when DST start and end in your timezone.

This is not sufficient. From the definition of DST, the local time without DST information is ambiguous for 1 hour in the year. So, during this hour, it is not possible to know the offset.

For instance, in France, in the night of the DST-to-NoDST change, at 3:00, one goes back to 2:00. So, if I give the local time 2:30 without additional information, one cannot know whether it was 2:30 before the change or 2:30 after the change.

--------------------------------------------------------------------------------
2016-11-06 23:49:48 UTC derekmartin
* Added comment:
OK I see, but as I've already pointed out, Mutt still won't be able to get this right in the overwhelming majority of cases, because the required information is simply unavailable.  For instance, in is_from() (in from.c) it's NOT POSSIBLE to get this right, because in general there is no indication of whether or not DST is in effect in the date in the from line.  Likewise in the headers, the time zone need only be represented as the offset from GMT; the symbolic time zone may or may not be present.

A cursory examination of my mail indicates the time zone info is NEVER present on the From line, and in the date field it is by far the majority of cases that it is not presented with any indication of DST.  I guess we can get it right in the minority of cases where it IS available...  Basically, parse.c is the only place this matters, I believe.  In all other cases Mutt can set isdst = -1 but this doesn't really help in the broken case.

You can perhaps improve the chances of getting it right by parsing the Received headers... but this is not guaranteed (since often the important received header is spoofed, or in a different time zone than the sender, or otherwise insufficient).  And it seems like an awful lot of trouble to go through for an oddball edge case that is very unlikely to occur anyway (since at 2am most people are sleeping, and it only happens 1 hr a year).

I think this isn't worth the effort, and should be closed WONTFIX.

I believe, FWIW, that there are worse problems... Basically anywhere that mutt_mktime() is called and local=0, it's assumed that the date is a GMT time, which as best as I can tell is generally not the case.  In some cases this is fixable (because the time zone is actually available) but in others it's not.  When the time zone is available, Mutt would need to convert that essentially to a pair of (offset, is_dst) in order to get the time right, but unless the symbolic form of the time zone is given (which we've already established that it mostly isn't) there's no good way to do that, AFAIK.  In particular, Mutt can't practically maintain its own database of the world timezones, and I don't see any portable way to manipulate the system's timezone database other than mktime(), localtime(), and their various friends... none of which get you what you need, AFAICT, since they require the DST flag as input.

--------------------------------------------------------------------------------
2016-11-08 09:23:50 UTC vinc17
* Added comment:
But when the offset is provided, there is no ambiguity and it is not the local timezone that should be used. And when no timezone information is provided in a mail, assuming the current local timezone is incorrect. It would be simpler to remove the {{{local}}} argument.

--------------------------------------------------------------------------------
2016-11-08 16:47:11 UTC derekmartin
* Added comment:
Replying to [comment:10 vinc17]:
> But when the offset is provided, there is no ambiguity and it is not the local timezone that should be used. 

I agree, but AFAICT in many of these cases, Mutt is treating the time as GMT when it is in fact not.

> And when no timezone information is provided in a mail, assuming the current local timezone is incorrect. 

Is it?  Is that stated in the RFC?  It seems pretty unintuitive that assuming the local time zone is wrong...  I'd guess that the far more common case is for people to receive e-mail from friends and coworkers who live/work in the same time zone.  Assuming GMT will guarantee that most of their mail will be treated as having a wrong time.

> It would be simpler to remove the {{{local}}} argument.

Local is only used in a couple of spots, and I *think* it's used appropriately.  So I don't believe that helps the situation at all.  I'd really need to analyze how the time_t's that are produced are being used a bit more carefully than I have, to be sure.




--------------------------------------------------------------------------------
2016-11-17 10:17:48 UTC vinc17
* Added comment:
Replying to [comment:11 derekmartin]:
> Replying to [comment:10 vinc17]:
> > And when no timezone information is provided in a mail, assuming the current local timezone is incorrect. 
> 
> Is it?  Is that stated in the RFC?

I suspect that timezone information is mandatory in general, so that this is out of scope of RFC's. But there's still the following specific case ([https://tools.ietf.org/html/rfc5322#section-3.3 RFC 5322, 3.3. Date and Time Specification]):

    Though "-0000" also indicates Universal Time, it is
    used to indicate that the time was generated on a system that may be
    in a local time zone other than Universal Time and that the date-time
    contains no information about the local time zone.

which makes the difference between UTC and no timezone information ambiguous.

But the main problem is the inconsistency / non reproducibility: If you travel, the dates would appear as different from before.

> I'd guess that the far more common case is for people to receive e-mail from friends and coworkers who live/work in the same time zone.

In the most common case, the timezone is provided. And I wouldn't deduce anything from an unusual case where the timezone is not provided.
