Ticket:  3539
Status:  new
Summary: User Input is often ignored

Reporter: ManDay
Owner:    mutt-dev

Opened:       2011-09-21 12:43:03 UTC
Last Updated: 2011-09-21 12:43:03 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
Mutt often ignores the user or does not allow for interactivity. Many operations cannot be forced to cancel (such as downloading an email from a remote or closing a connection) and Mutt will perform them or infinitly try to perform then, completely ignoring the wish of the user.

This is a somewhat general problem throughout Mutt. A start to remdedy that would be to allow the user to cancel operations such as connecting, disconnecting, performing operations on the remote and downloading data from the remote.
