Ticket:  2893
Status:  new
Summary: fcc_attach=no should record filenames (like deleted attachments)

Reporter: myon
Owner:    mutt-dev

Opened:       2007-05-23 08:26:35 UTC
Last Updated: 2009-06-04 23:26:20 UTC

Priority:  major
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:


{{{
Forwarding to trac so we don't forget it...

----- Forwarded message from Louis-David Mitterrand <vindex+lists-mutt-dev@apartia.org> -----

Date: Thu, 3 May 2007 17:21:26 +0200
From: Louis-David Mitterrand <vindex+lists-mutt-dev@apartia.org>
To: mutt-dev@mutt.org
Subject: fcc_attach=no improvement suggestion

Hello,

I am following the exciting new rythm of mutt's development and this 
leads me to humbly submit an improvement request.

When fcc_attach=no outgoing attachements are not saved in the 'Sent' 
mailbox however it would be convenient to have a record of what was 
attached at the time it was sent: file path, size and last modification. 
This would nicely improve the audit'ability of one's sent mail without 
inflating it.

Thanks for your consideration,

----- End forwarded message -----
}}}

--------------------------------------------------------------------------------
2008-06-30 17:03:54 UTC peter
* cc changed to pmlists@free.fr

--------------------------------------------------------------------------------
2008-07-21 05:05:42 UTC danny
* Added comment:
Yes, this would be very useful!

--------------------------------------------------------------------------------
2009-06-04 23:26:20 UTC antonio@dyne.org
* Added comment:
a similar request came from http://bugs.debian.org/519445
