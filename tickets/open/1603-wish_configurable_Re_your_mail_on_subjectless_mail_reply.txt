Ticket:  1603
Status:  new
Summary: wish configurable "Re: your mail" on subject-less mail reply

Reporter: Mathieu Dussier <muttbug@ghosthack.net>
Owner:    mutt-dev

Opened:       2003-08-01 16:36:13 UTC
Last Updated: 2007-04-07 14:35:12 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.5.4-1
Severity: wishlist

-- Please type your report below this line
I wonder if it can be possible to change this default behavior when
replying to an email where the Subject: line was empty and replace
"Re: your mail" by something more elaborate like "Re: (your mail from
$DATE $HOUR)" or somethink like that.
It'll be very appreciate, I think. At least by me! ;-)
(sorry, I really don't have the capabilities to write a patch myself!)

-- System Information
System Version: Linux navi 2.4.20 #1 Thu Jun 12 18:22:15 CEST 2003 i686 GNU/Linux

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
cc
Lecture des spécification à partir de /usr/lib/gcc-lib/i386-linux/3.2.3/specs
Configuré avec: ../src/configure -v --enable-languages=c,c++,f77,objc,ada --prefix=/usr --mandir=/usr/share/man --infodir=/usr/share/info --with-gxx-include-dir=/usr/include/c++/3.2 --enable-shared --with-system-zlib --enable-nls --without-included-gettext --enable-__cxa_atexit --enable-clocale=gnu --enable-objc-gc i386-linux
Modèle de thread: posix
version gcc 3.2.3 (Debian)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.5.4i (2003-03-19)
Copyright (C) 1996-2002 Michael R. Elkins et autres.
Mutt ne fournit ABSOLUMENT AUCUNE GARANTIE ; pour les détails tapez `mutt -vv'.
Mutt est un logiciel libre, et vous êtes libre de le redistribuer
sous certaines conditions ; tapez `mutt -vv' pour les détails.

System: Linux 2.4.20 (i686) [using ncurses 5.3] [using libidn 0.1.14 (compiled with 0.1.11)]
Options de compilation :
-DOMAIN
-DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  +IMAP_EDIT_THREADS  -USE_GSS  -USE_SSL  +USE_GNUTLS  +USE_SASL  -USE_SASL2  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +COMPRESSED  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_LIBIDN  +HAVE_GETSID  +HAVE_GETADDRINFO  +USE_CACHE  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/etc"
EXECSHELL="/bin/sh"
MIXMASTER="mixmaster"
Pour contacter les développeurs, veuillez écrire à <mutt-dev@mutt.org>.
Pour signaler un bug, veuillez utiliser l'utilitaire flea(1).

patch-1.5.3.Md.gpg_status_fd
patch-1.3.23.1.ametzler.pgp_good_sign
patch-1.4.Md.gpg-agent
patch-1.5.1.cd.edit_threads.9.2
patch-1.5.3.Md.etc_mailname_gethostbyname
patch-1.3.27.bse.xtitles.1
Md.muttbug
Md.use_debian_editor
patch-1.4.admcd.gnutlsdlopen.53d
patch-1.5.4.Z.hcache.8
patch-1.4.admcd.gnutlsbuild.53d
patch-1.4.admcd.gnutls.55d
patch-1.5.3.rr.compressed.1

Received: (at submit) by bugs.guug.de; 18 Jan 2004 16:06:06 +0000
From cri@linux.it Sun Jan 18 17:06:04 2004
Received: from smtp1.libero.it ([193.70.192.51])
	by trithemius.gnupg.org with esmtp (Exim 3.35 #1 (Debian))
	id 1AiFR1-0001mK-00
	for <submit@bugs.guug.de>; Sun, 18 Jan 2004 17:05:59 +0100
Received: from wamozart.buliga.it (151.30.201.87) by smtp1.libero.it (7.0.020-DD01)
        id 3F6F0E480219A017; Sun, 18 Jan 2004 17:08:30 +0100
Received: by wamozart.buliga.it (Postfix, from userid 1000)
	id 6DB7A24586; Sun, 18 Jan 2004 17:07:33 +0100 (CET)
From: Cristian Rigamonti <cri@linux.it>
Subject: mutt-1.5.5.1i: i18n of "Re: your mail" string
To: submit@bugs.guug.de
Message-Id: <20040118160733.6DB7A24586@wamozart.buliga.it>
Date: Sun, 18 Jan 2004 17:07:33 +0100 (CET)
X-Spam-Status: No, hits=-3.7 required=4.0
	tests=BAYES_10,UPPERCASE_25_50
	version=2.55
X-Spam-Level: 
X-Spam-Checker-Version: SpamAssassin 2.55 (1.174.2.19-2003-05-19-exp)

Package: mutt
Version: 1.5.5.1i
Severity: normal

-- Please type your report below this line

When answering a message with an empty "Subject: " field, mutt automatically
puts "Re: your mail" in the answering message's "Subject: " field.
This string (contained in send.c) should be gettextized (or it could be made
user-configurable).


-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/lib/gcc-lib/i386-linux/2.95.4/specs
gcc version 2.95.4 20011002 (Debian prerelease)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.5.5.1i (2003-11-05)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.18-bf2.4 (i686) [using ncurses 5.2]
Opzioni di compilazione:
-DOMAIN
-DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
-USE_POP  -USE_IMAP  -IMAP_EDIT_THREADS  -USE_GSS  -USE_SSL  -USE_SASL  -USE_SASL2  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  -HAVE_LIBIDN  +HAVE_GETSID  -HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/local/share/mutt"
SYSCONFDIR="/usr/local/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
Per contattare gli sviluppatori scrivi a <mutt-dev@mutt.org>.
Per segnalare un bug usa il programma flea(1).

patch-1.5.5.1.cd.edit_threads.9.5


>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2004-02-01 19:50:15 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
severity 1613 wishlist
merge 800 1613
merge 1427 1747
severity 1765 wishlist
merge 1603 1765
tags 1334 patch
tags 1642 patch
tags 1778 patch
-- are 1662/1663 fixed and closable?
}}}

--------------------------------------------------------------------------------
2005-09-05 13:11:14 UTC brendan
* Added comment:
{{{
Refile as change-request.
}}}

--------------------------------------------------------------------------------
2005-09-08 04:13:52 UTC ab
* Added comment:
{{{
Deduppe unform, retitle, add second reporter to notifieds.
}}}

--------------------------------------------------------------------------------
2005-09-09 01:56:39 UTC ab
* Added comment:
{{{
Dhetypoed, tanks Moritz!
}}}
