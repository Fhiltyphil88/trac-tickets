Ticket:  2460
Status:  new
Summary: mutt does not write the maildir P flag

Reporter: Michael.Tatge@web.de
Owner:    mutt-dev

Opened:       2006-08-31 00:16:51 UTC
Last Updated: 2009-06-30 14:57:09 UTC

Priority:  trivial
Component: maildir/mh
Keywords:  

--------------------------------------------------------------------------------
Description:
Maildir has the P flag for messages that are handed off to other users:
Flag "P" (passed): the user has resent/forwarded/bounced this message to someone else.
Mutt doesn't make use of this flag but it should at least write it, so that other clients that might use the flag have the info available.
>How-To-Repeat:
resent/forward/bounce a message. sync-mailbox. check the maildir flags for that message.

--------------------------------------------------------------------------------
2007-04-30 02:02:00 UTC brendan
* Updated description:
Maildir has the P flag for messages that are handed off to other users:
Flag "P" (passed): the user has resent/forwarded/bounced this message to someone else.
Mutt doesn't make use of this flag but it should at least write it, so that other clients that might use the flag have the info available.
>How-To-Repeat:
resent/forward/bounce a message. sync-mailbox. check the maildir flags for that message.

--------------------------------------------------------------------------------
2009-06-30 14:57:09 UTC pdmef
* component changed to maildir
