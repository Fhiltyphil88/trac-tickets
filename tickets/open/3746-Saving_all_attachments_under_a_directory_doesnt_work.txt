Ticket:  3746
Status:  new
Summary: Saving (a)ll attachments under a directory doesn't work

Reporter: L29Ah
Owner:    mutt-dev

Opened:       2015-04-06 23:29:57 UTC
Last Updated: 2016-06-12 16:26:44 UTC

Priority:  minor
Component: user interface
Keywords:  

--------------------------------------------------------------------------------
Description:
When i `s`, type in a directory and pick `a`, the first file gets stored under a picked directory, but all the next ones go to cwd.

--------------------------------------------------------------------------------
2016-06-12 12:03:36 UTC L29Ah
* _comment0 changed to 1465733051207724
* Added comment:
Quick and ugly fix:

{{{
diff -r 123e36398eea muttlib.c
--- a/muttlib.c	Sat Jun 11 17:59:37 2016 +0200
+++ b/muttlib.c	Sun Jun 12 14:59:13 2016 +0300
@@ -975,6 +975,8 @@
       {
 	case 3:		/* all */
 	  mutt_str_replace (directory, fname);
+	  if (chdir (*directory) != 0)
+	    mutt_perror("chdir");
 	  break;
 	case 1:		/* yes */
 	  FREE (directory);		/* __FREE_CHECKED__ */
}}}

* version changed to 1.6.1

--------------------------------------------------------------------------------
2016-06-12 16:26:44 UTC kevin8t8
* Added comment:
When you tag multiple attachments and save them, it follows this flow:

{{{
   Save to file: ~/temp/foo/<cr>

   File is a directory, save under it? [(y)es, (n)o, (a)ll]  a

   File under directory: att1.pdf<cr>

   Save to file: /home/kevin/temp/foo/att2.pdf<cr>

   Save to file: /home/kevin/temp/foo/att3.pdf<cr>
}}}

Notice that starting with the second attachment, it has prepended the directory you specified.

Yes, this is somewhat clunky, but it does work as intended.

* component changed to user interface
* priority changed to minor
* type changed to enhancement
