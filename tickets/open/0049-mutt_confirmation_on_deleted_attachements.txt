Ticket:  49
Status:  new
Summary: mutt: confirmation on deleted attachements

Reporter: "Marco d'Itri" <md@linux.it>
Owner:    mutt-dev

Opened:       2000-02-24 19:44:23 UTC
Last Updated: 2005-08-01 05:12:21 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.0pre4-1
Severity: wishlist

[NOTE: this bug report has been submitted to the debian BTS as Bug#48477.
Please Cc all your replies to 48477@bugs.debian.org.]


From: Ulf Jaenicke-Roessler <ujr@pbtrs2.phy.tu-dresden.de>
Subject: mutt: confirmation on deleted attachements
Date: Wed, 27 Oct 1999 17:03:22 +0200

Hi,

 mutt doesn't ask for confirmation before it stores mails into mailboxes
 after attachements were deleted.

 IMHO it should do so, in order to prevent accidential data loss.

 Thanks,

  Ulf


-- System Information
Debian Release: potato
Kernel Version: Linux pkfp20 2.2.13 #1 Tue Oct 26 11:11:53 CEST 1999 i586 unknown

Versions of the packages mutt depends on:
ii  libc6           2.1.2-6        GNU C Library: Shared libraries and timezone
ii  libncurses4     4.2-3.4        Shared libraries for terminal handling
ii  exim            3.03-1         Exim Mailer
	^^^ (Provides virtual package mail-transport-agent)



>How-To-Repeat:
>Fix:
}}}
