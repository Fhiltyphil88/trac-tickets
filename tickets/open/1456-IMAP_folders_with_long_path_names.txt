Ticket:  1456
Status:  new
Summary: IMAP folders with long path names

Reporter: robl@linx.net
Owner:    mutt-dev

Opened:       2005-07-24 16:02:44 UTC
Last Updated: 2007-04-12 16:59:57 UTC

Priority:  trivial
Component: IMAP
Keywords:  IMAP

--------------------------------------------------------------------------------
Description:
{{{
From robl@linx.net Mon Feb 03 22:11:06 2003
Received: from london.linx.net ([195.66.232.34] ident=root)
	by trithemius.gnupg.org with esmtp (Exim 3.35 #1 (Debian))
	id 18fnrk-0007B0-00
	for <submit@bugs.guug.de>; Mon, 03 Feb 2003 22:10:56 +0100
Received: from beige.linx.net ([195.66.233.94])
	by london.linx.net with esmtp (Exim 3.36 #1)
	id 18fntS-0007gD-00; Mon, 03 Feb 2003 21:12:42 +0000
Received: from robl by beige.linx.net with local (Exim 3.36 #2)
	id 18fntR-0002v2-00; Mon, 03 Feb 2003 21:12:41 +0000
From: robl@linx.net
Subject: mutt-1.4i: IMAP folders with long path names
To: submit@bugs.guug.de
Message-Id: <E18fntR-0002v2-00@beige.linx.net>
Date: Mon, 03 Feb 2003 21:12:41 +0000

Package: mutt
Version: mutt-1.4i-1.cfp.rhl7
Severity: wishlist

-- Please type your report below this line

We have several shared "read only" type IMAP folders on our server
for things like common mailing lists that we are all on, rather than
sending e-mails to all users, we can access one copy of the messages
from the shared IMAP folders.

Unfortunately, the folders are in a stupidly long deep location
in the IMAP tree, like:

imaps://mail/Other Users/lists~services.server.net/external-customers/long_folder_name

(And I've shortened it here by removing the FQDN of the mail server!)

There should be a way to hide all the cruft before the actual
folder name and/or just list it as:

imaps://[...]/external-customers/long_folder_name

Quite often these are >80 columns wide and so changing folders
when there is new mail in them truncates the end of the folder
name (I think you can press ">" to make it show you the end
of the folder's name.

An IMAP listing at 80 Columns looks like:

 6   0                                 imaps://mail/Other Users/lists~services.
 7   0                                 imaps://mail/Other Users/lists~services.
 8   0                                 imaps://mail/Other Users/lists~services.
 9   0                                 imaps://mail/Other Users/lists~services.
10   0                                 imaps://mail/Other Users/lists~services.
11   0                                 imaps://mail/Other Users/lists~services.
12   0                                 imaps://mail/Other Users/lists~services.


... which is not particularly helpful selecting the correct folder!

The workaround is I have to make my terminal a lot wider at the moment.

Is it possible to, like I have defined for set folder="imaps://mail/mail"
and then:

mailboxes =foo =bar =folders are listed in the list as "=foo"

To do a similar thing to that like:

imap_list_hide="imaps://mail/Other Users/lists~services.server.net/"

which would cause them to be listed as, say:


6   0                                  </external-customers/long_folder_name




>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2005-09-05 12:55:39 UTC brendan
* Added comment:
{{{
Refile as change-request.
}}}

--------------------------------------------------------------------------------
2007-02-08 14:30:51 UTC rado
* Added comment:
{{{
Move to IMAP category
}}}

--------------------------------------------------------------------------------
2007-04-12 16:59:57 UTC brendan
* Updated description:
{{{
From robl@linx.net Mon Feb 03 22:11:06 2003
Received: from london.linx.net ([195.66.232.34] ident=root)
	by trithemius.gnupg.org with esmtp (Exim 3.35 #1 (Debian))
	id 18fnrk-0007B0-00
	for <submit@bugs.guug.de>; Mon, 03 Feb 2003 22:10:56 +0100
Received: from beige.linx.net ([195.66.233.94])
	by london.linx.net with esmtp (Exim 3.36 #1)
	id 18fntS-0007gD-00; Mon, 03 Feb 2003 21:12:42 +0000
Received: from robl by beige.linx.net with local (Exim 3.36 #2)
	id 18fntR-0002v2-00; Mon, 03 Feb 2003 21:12:41 +0000
From: robl@linx.net
Subject: mutt-1.4i: IMAP folders with long path names
To: submit@bugs.guug.de
Message-Id: <E18fntR-0002v2-00@beige.linx.net>
Date: Mon, 03 Feb 2003 21:12:41 +0000

Package: mutt
Version: mutt-1.4i-1.cfp.rhl7
Severity: wishlist

-- Please type your report below this line

We have several shared "read only" type IMAP folders on our server
for things like common mailing lists that we are all on, rather than
sending e-mails to all users, we can access one copy of the messages
from the shared IMAP folders.

Unfortunately, the folders are in a stupidly long deep location
in the IMAP tree, like:

imaps://mail/Other Users/lists~services.server.net/external-customers/long_folder_name

(And I've shortened it here by removing the FQDN of the mail server!)

There should be a way to hide all the cruft before the actual
folder name and/or just list it as:

imaps://[...]/external-customers/long_folder_name

Quite often these are >80 columns wide and so changing folders
when there is new mail in them truncates the end of the folder
name (I think you can press ">" to make it show you the end
of the folder's name.

An IMAP listing at 80 Columns looks like:

 6   0                                 imaps://mail/Other Users/lists~services.
 7   0                                 imaps://mail/Other Users/lists~services.
 8   0                                 imaps://mail/Other Users/lists~services.
 9   0                                 imaps://mail/Other Users/lists~services.
10   0                                 imaps://mail/Other Users/lists~services.
11   0                                 imaps://mail/Other Users/lists~services.
12   0                                 imaps://mail/Other Users/lists~services.


... which is not particularly helpful selecting the correct folder!

The workaround is I have to make my terminal a lot wider at the moment.

Is it possible to, like I have defined for set folder="imaps://mail/mail"
and then:

mailboxes =foo =bar =folders are listed in the list as "=foo"

To do a similar thing to that like:

imap_list_hide="imaps://mail/Other Users/lists~services.server.net/"

which would cause them to be listed as, say:


6   0                                  </external-customers/long_folder_name




>How-To-Repeat:
>Fix:
}}}
* milestone changed to 2.0
