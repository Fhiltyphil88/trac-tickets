Ticket:  3628
Status:  new
Summary: Woudl liek to have sendmail variable usable when smtp_url set

Reporter: pfortuny
Owner:    mutt-dev

Opened:       2013-01-23 11:37:14 UTC
Last Updated: 2013-01-23 11:37:14 UTC

Priority:  major
Component: mutt
Keywords:  sendmail, variable, pipe, filter

--------------------------------------------------------------------------------
Description:
Hi,

I would like to pre-filter an already formatted message before sending, like one does when setting
$sendmail
to a command different from the raw sendmail of the system, like they do in [http://dev.mutt.org/trac/wiki/ConfigTricks/CheckAttach] (i.e. having the whole formatted message sent through a pipe to the $sendmail command). However, as using the SMTP extension renders the $sendmail variable useless, there is no (easy) way to do this.

The fact is I am planning on writing an extension and with the standard setting ($sendmail useful) it works pretty easily but if smtp_url is set, then it is a terrible hassle.

Thanks.

--------------------------------------------------------------------------------
2013-01-23 14:53:27 UTC pfortuny
* Added attachment smtp_filter.diff
* Added comment:
diff with dummy implementation 
