Ticket:  2939
Status:  new
Summary: next-unread-mailbox does not add much new functionality

Reporter: amp
Owner:    mutt-dev

Opened:       2007-08-07 07:04:24 UTC
Last Updated: 2009-08-31 16:05:01 UTC

Priority:  trivial
Component: mutt
Keywords:  patch

--------------------------------------------------------------------------------
Description:
Currently ''$next-unread-mailbox'' will change to the next mailbox with '''new''' mail, but not old unread (as the name implies). If I have ''$mark_old'' set and I exit mutt for some reason I found no easy way to jump to folders containing unread mail. ''$next-unread-mailbox'' could (should?) do this.

Thanks for reading,
Andrei

--------------------------------------------------------------------------------
2009-08-27 07:18:29 UTC pierre
* Added attachment next-unread-mailbox.patch

--------------------------------------------------------------------------------
2009-08-27 07:21:26 UTC pierre
* Added comment:
Please consider the following patch that implements this behavior.

Tested both under Debian Lenny and Ubuntu Jaunty.

Since the function parses all mailboxes, the delay can be longer. I tested it with  Maildirs with ~ 1,000 messages. The delay was noticeable, but not too bad (~1-2 seconds).

An optimization would be to add a global unread in BUFFY. If that's acceptable, I'll be happy to recook the patch.

Thread started on mutt-users@:

    http://marc.info/?l=mutt-users&m=125119464023276&w=2

--------------------------------------------------------------------------------
2009-08-27 07:22:39 UTC pierre
* cc changed to pierre@mouraf.org

--------------------------------------------------------------------------------
2009-08-31 16:05:01 UTC pierre
* keywords changed to patch
