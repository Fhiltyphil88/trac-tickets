Ticket:  2110
Status:  new
Summary: decode-copy decode-save create empty parts / messages in case of decoding errors

Reporter: michael.tatge@web.de
Owner:    mutt-dev

Opened:       2005-10-10 21:07:14 UTC
Last Updated: 2005-10-11 01:15:02 UTC

Priority:  minor
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Mutt creates empty messages if there's some error while decode-{copy,save} e.g. wrong passphrase.
It gives an error for pgp/mime types (Could not decrypt PGP message / Could not copy message) or if check-traditional-pgp is used.
Nontheless a message with an empty body is copied/saved.
This should not happen. In case of decode-save it could result in mail loss.
Mutt should warn the user about the error and either copy the message as is (encoded) or refuse to write the message at all.
>How-To-Repeat:
decode-copy or decode-save an encrpted message and enter a wrong passphrase.
>Fix:
}}}

--------------------------------------------------------------------------------
2005-10-12 09:12:36 UTC TAKAHASHI Tamotsu <ttakah@lapis.plala.or.jp>
* Added comment:
{{{
> >Number:         2110
> >Category:       mutt
> >Synopsis:       decode-copy decode-save create empty parts / messages in case of decoding errors

Thanks for the report.
I don't like this behaviour, too.
But I don't know how to fix it.

> >Description:
> Mutt creates empty messages if there's some error while decode-{copy,save} e.g. wrong passphrase.

True.

> It gives an error for pgp/mime types (Could not decrypt PGP message / Could not copy message) or if check-traditional-pgp is used.

True.

> Nontheless a message with an empty body is copied/saved.

True.
While decode-copying, mutt tries to decrypt the message *after*
copying the header. So, only the header is left even if aborted.
Hmmm...

> This should not happen. In case of decode-save it could result in mail loss.

No. The original message won't be deleted if decode-save fails.
So both decode-copy and decode-save don't lose any messages.
(Because Brendan fixed bug #1919, IIRC.)

> Mutt should warn the user about the error and either copy the message as is (encoded) or refuse to write the message at all.

IMHO, "copy-as-is" would cause more confusion.

-- 
tamo
}}}
