Ticket:  1552
Status:  new
Summary: removing signature part when replying to message

Reporter: 0dq94rkr001@sneakemail.com
Owner:    mutt-dev

Opened:       2003-04-27 02:49:33 UTC
Last Updated: 2007-04-07 14:22:46 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.5.4i
Severity: wishlist

-- Please type your report below this line
It would be nice if mutt had a variable, which, when set, would cause
the removal of the signature part (ie. everything from '-- \n' to the
end of file) when one replies to a message, and the original message is
included (quoted) in the reply.

P.S. The copyright dates in the version string need to be updated.
	Mutt 1.5.4i (2003-03-19)
	Copyright (C) 1996-2002 Michael R. Elkins and others.
                           ^^^^  

-- System Information
System Version: Linux unknown 2.2.14 #1 Fri Apr 26 00:00:00 GMT 2002 i386 unknown

-- Build environment information

- gcc version information

- CFLAGS

-- Mutt Version Information

Mutt 1.5.4i (2003-03-19)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.18 (i686) [using slang 10401]
Compile options:
-DOMAIN
-DEBUG
+HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
-USE_POP  -USE_IMAP  -USE_GSS  -USE_SSL  -USE_SASL  -USE_SASL2  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  -HAVE_START_COLOR  -HAVE_TYPEAHEAD  -HAVE_BKGDSET  
-HAVE_CURS_SET  -HAVE_META  -HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  -CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
-ENABLE_NLS  +LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  -HAVE_LIBIDN  +HAVE_GETSID  -HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="Mailbox"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/usr/share/mutt"
EXECSHELL="/bin/sh"
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.



>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2003-04-27 02:49:33 UTC 0dq94rkr001@sneakemail.com
* Added comment:
{{{
Package: mutt
Version: 1.5.4i
Severity: wishlist

-- Please type your report below this line
It would be nice if mutt had a variable, which, when set, would cause
the removal of the signature part (ie. everything from '-- \n' to the
end of file) when one replies to a message, and the original message is
included (quoted) in the reply.

P.S. The copyright dates in the version string need to be updated.
	Mutt 1.5.4i (2003-03-19)
	Copyright (C) 1996-2002 Michael R. Elkins and others.
                          ^^^^  

-- System Information
System Version: Linux unknown 2.2.14 #1 Fri Apr 26 00:00:00 GMT 2002 i386 unknown

-- Build environment information

- gcc version information

- CFLAGS

-- Mutt Version Information

Mutt 1.5.4i (2003-03-19)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.18 (i686) [using slang 10401]
Compile options:
-DOMAIN
-DEBUG
+HOMESPOOL  -USE_SETGID  +USE_DOTLOCK  -DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
-USE_POP  -USE_IMAP  -USE_GSS  -USE_SSL  -USE_SASL  -USE_SASL2  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  -HAVE_START_COLOR  -HAVE_TYPEAHEAD  -HAVE_BKGDSET  
-HAVE_CURS_SET  -HAVE_META  -HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  -CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
-ENABLE_NLS  +LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  -HAVE_LIBIDN  +HAVE_GETSID  -HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="Mailbox"
PKGDATADIR="/usr/share/mutt"
SYSCONFDIR="/usr/share/mutt"
EXECSHELL="/bin/sh"
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.
}}}

--------------------------------------------------------------------------------
2003-04-28 22:23:55 UTC Bob Bell <bobbell@zk3.dec.com>
* Added comment:
{{{
On Sat, Apr 26, 2003 at 11:49:33AM -0000, 0dq94rkr001@sneakemail.com <0dq94rkr001@sneakemail.com> wrote:
> It would be nice if mutt had a variable, which, when set, would cause
> the removal of the signature part (ie. everything from '-- \n' to the
> end of file) when one replies to a message, and the original message is
> included (quoted) in the reply.

This is usually left as a feature for the editor to provide (or a filter
script you provide if your editor is incapable of that).

-- 
Bob Bell <bobbell@zk3.dec.com>
-------------------------------------------------------------------------
"The popularization of the open-source movement continues to pose a
 significant challenge to [Microsoft's] business model."
  -- Microsoft SEC filing
}}}

--------------------------------------------------------------------------------
2003-04-28 22:23:56 UTC Bob Bell <bobbell@zk3.dec.com>
* Added comment:
{{{
On Sat, Apr 26, 2003 at 11:49:33AM -0000, 0dq94rkr001@sneakemail.com <0dq94rkr001@sneakemail.com> wrote:
> It would be nice if mutt had a variable, which, when set, would cause
> the removal of the signature part (ie. everything from '-- \n' to the
> end of file) when one replies to a message, and the original message is
> included (quoted) in the reply.

This is usually left as a feature for the editor to provide (or a filter
script you provide if your editor is incapable of that).

-- 
Bob Bell <bobbell@zk3.dec.com>
-------------------------------------------------------------------------
"The popularization of the open-source movement continues to pose a
 significant challenge to [Microsoft's] business model."
  -- Microsoft SEC filing
}}}

--------------------------------------------------------------------------------
2003-04-29 13:34:25 UTC <0dq94rkr001@sneakemail.com>
* Added comment:
{{{
> > It would be nice if mutt had a variable, which, when set, would cause
> > the removal of the signature part (ie. everything from '-- \n' to the
> > end of file) when one replies to a message, and the original message
is
> > included (quoted) in the reply.
>
> This is usually left as a feature for the editor to provide (or a filter
> script you provide if your editor is incapable of that).

Yes, but unfortunately the editor has currently no way to tell in
advance whether it's responding to message (with part of it quoted) or
composing a new message, and has to use heuristics based on the
message content (which makes the whole thing somewhat brittle)...

Let's pass this info to the editor then by means of an environment
variable
or something.
}}}

--------------------------------------------------------------------------------
2005-08-25 13:51:00 UTC rado
* Added comment:
{{{
change-request

You can specify whatever you need to $editor, including scripts taking care of the body itself
or invoking the real editor with desired setup (incl. SHELL-env vars).

As for the wish:
Since signature is already detected for coloring, it might as well get dropped before being passed to editor.
}}}
