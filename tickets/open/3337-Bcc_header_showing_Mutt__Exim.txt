Ticket:  3337
Status:  new
Summary: [Bcc header showing] Mutt + Exim

Reporter: will177
Owner:    mutt-dev

Opened:       2009-09-21 09:54:25 UTC
Last Updated: 2012-02-09 21:46:39 UTC

Priority:  major
Component: mutt
Keywords:  bcc, exim

--------------------------------------------------------------------------------
Description:
Hi,

I got stung by the following mutt+exim issue recently:

http://sysmonblog.co.uk/2009/04/mutt-exim4-debian-unbuntu-and-bcc-fail-fix-included.html

which starts:

"Take a default Ubuntu/Debian install using the Mutt mail client (MUA) then switch to exim4. You get the most monumental FAIL - your bcc addresses will be included in the message to everyone else. Dangerous and embarrassing!
..."

This page does offer a workaround, but I think this is really a bug in mutt that should be fixed.  I cannot see why the Bcc information should EVER go out to all recipients.

I have spoken to the original designer of Exim, Philip Hazel, who is now retired, about this and he tells me the following, i.e. it is a mutt bug.

"Exim will only remove Bcc headers when it is being used as a message *submission* agent, that is, if the -t (I think it is) option is used to ask it to take recipients from header lines. In all other cases, when Exim is a transport agent,
it doesn't mess with headers. This has been argued before to be the
correct approach. 
So, if mutt is calling Exim, not using -t, and
expecting it to remove headers, it is mutt's problem."

Given I want mutt to save sent emails with the Bcc information but not send out the Bcc information to all recipients, I think that this is a mutt bug.  (i.e. write_bcc=no is not the answer here.)

I am using the sendmail -t and exim workaround mentioned in the URL above for now, but when I next upgrade I might forget to apply this workaround so I'll like mutt to be fixed, if you agree.

Thanks in advance.


--------------------------------------------------------------------------------
2009-09-21 10:02:55 UTC Thomas Roessler
* Added comment:
{{{
LEt's say that this is a longstanding disagreement between mutt and  
exim on whose job it is to drop that header.  Let's just notice that  
exim delivers a poor imitation of sendmail's command line interface  
here.

'nuff said.
--
Thomas Roessler  <roessler@does-not-exist.org>











On 21 Sep 2009, at 11:54, Mutt wrote:
}}}

--------------------------------------------------------------------------------
2009-09-21 10:09:42 UTC antonio@dyne.org
* Added comment:
For the next Debian release of the mutt package this won't be true anymore, please see http://bugs.debian.org/467432

--------------------------------------------------------------------------------
2009-09-21 10:58:58 UTC grobian
* Added comment:
Thanks for the pointer, Antonio!

Next Gentoo release of mutt will have the same patch applied.

--------------------------------------------------------------------------------
2012-02-09 21:46:39 UTC idl0r
* cc changed to idl0r@qasl.de
* Added comment:
*push*
