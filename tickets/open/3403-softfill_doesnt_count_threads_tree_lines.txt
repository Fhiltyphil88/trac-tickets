Ticket:  3403
Status:  assigned
Summary: soft-fill doesn't count threads' tree lines

Reporter: daniell
Owner:    me

Opened:       2010-04-09 20:01:26 UTC
Last Updated: 2010-04-27 11:41:59 UTC

Priority:  minor
Component: mutt
Keywords:  soft fill index format

--------------------------------------------------------------------------------
Description:
I'm using an index_format like this:
{{{
set index_format='%3C[%e] %Z %-15.15F %s%*  (%4.4c) %{%y.%m.%d %H:%M}'                                              
}}}
When getting a message with a very long subject, the soft-fill (%*) takes care that the date and the message size is always shown. However, when looking at threads, soft-fill fails to count in the tree branches of the threads in the index, and pushes the characters on the right side out of the window space.
eg.:
{{{
  1[1]     N.      L.      Re: [munin-users] U when fetching plugin values from master, but re (4,9K) 10.04.09 13:29
  2[2]     G.       P.     └─>[munin-users] Maintenance of Munins SELinux policies (was: Re: U when  (4,2K) 10.04.09
}}}
You can see that the "time" string has disappeared in the second message, while with the first message soft-fill was succesful displaying everything that was meant to be displayed.

--------------------------------------------------------------------------------
2010-04-11 23:22:05 UTC me
* Added attachment patch-1.5.20-softfill-arrow-cursor.1
* Added comment:
patch fixing softfill problem with 'set arrow_cursor'

--------------------------------------------------------------------------------
2010-04-11 23:23:15 UTC me
* Added comment:
Do you happen to be using 'set arrow_cursor' ?  The attached patch fixes that bug.  I didn't find any obvious problem with the thread tree display.  Let me know if this patch doesn't fix the problem.

* owner changed to me
* status changed to assigned

--------------------------------------------------------------------------------
2010-04-12 07:18:01 UTC daniell
* Added comment:
I've applied the patch, but I don't use the arrow_cursor option (it is at the default state: 'no'), and it seems in the patch that the "offset" is only other than zero when the arrow_cursor option is used. Anyway I'm still experiencing the problem:
{{{
  1[1]  sF To leva@ecentru aaaaaaaaasd qwd wqd qwd qwdwqd qwd qwdqw qwasdasdqwdqwdwqdqwdqwdqwd (1,3K) 10.04.12 09:11
  2[2]  sF To leva@ecentru ??>Re: aaaaaaaaasd qwd wqd qwd qwdwqd qwd qwdqw qwasdasdqwdqwdwqdqwdqwdqw (1,5K) 10.04.12
  3[3] NsF To leva@ecentru ? ?=>                                                               (2,2K) 10.04.12 09:12
  4[4] rsF To leva@ecentru ?=>aaaaaaaaasd qwd wqd qwd qwdwqd qwd qwdqw qwasdasdqwdqwdwqdqwdqwdqwdwqd (1,2K) 10.04.12
}}}

--------------------------------------------------------------------------------
2010-04-12 16:55:04 UTC Michael Elkins
* Added comment:
{{{
I notice the question-marks in your excerpt.  Does mutt actually display 
those, or was that an artifact of the bug reporting system?  Mutt will display 
a ? when it can't convert a character to something that is printable in your 
current locale.
}}}

--------------------------------------------------------------------------------
2010-04-12 17:02:55 UTC Michael Elkins
* Added comment:
{{{
Just for reference, I found a similar bug here
http://dev.mutt.org/trac/ticket/3364

I think the issue for this bug report might be that the assumption that the 
tree characters are 1 cell wide might not be true in your locale.

Can you please state what your $LANG and charset are?
}}}

--------------------------------------------------------------------------------
2010-04-12 19:16:38 UTC me
* Added attachment patch-1.5.20-pad-with-soft-fill.1
* Added comment:
fix bug with wide pad char

--------------------------------------------------------------------------------
2010-04-12 19:17:44 UTC me
* Added comment:
I found another potential bug where the width of the pad char wasn't used properly when truncating the left part of the string.  I'm not sure this will help your problem, because it looks like you are using a space character.  Please test it and let me know.

--------------------------------------------------------------------------------
2010-04-13 09:05:02 UTC daniell
* Added comment:
Replying to [comment:5 me]:
> I found another potential bug where the width of the pad char wasn't used properly when truncating the left[[BR]]
 part of the string.  I'm not sure this will help your problem, because it looks like you are using a space[[BR]]
 character.  Please test it and let me know.

This doesn't apply clean for me; I have that line at 1266. However, if I apply it manually (ie. replace that line in muttlib.c) I get this error during make:
{{{
gcc -DPKGDATADIR=\"/usr/share/mutt\" -DSYSCONFDIR=\"/etc/mutt\" -DBINDIR=\"/usr/bin\" -DMUTTLOCALEDIR=\"/usr/share/locale\" -DHAVE_CONFIG_H=1 -I.  -I. -I. -I./imap  -Iintl -I/usr/include// -I./intl  -Wall -pedantic -Wno-long-long -march=i486 -mtune=i686 -ggdb -MT muttlib.o -MD -MP -MF .deps/muttlib.Tpo -c -o muttlib.o muttlib.c
muttlib.c: In function â:
muttlib.c:1266: error: â undeclared (first use in this function)
muttlib.c:1266: error: (Each undeclared identifier is reported only once
muttlib.c:1266: error: for each function it appears in.)
make[2]: *** [muttlib.o] Error 1
}}}

--------------------------------------------------------------------------------
2010-04-13 09:07:10 UTC daniell
* Added comment:
Replying to [comment:3 Michael Elkins]:
> {{{
> I notice the question-marks in your excerpt.  Does mutt actually display 
> those, or was that an artifact of the bug reporting system?  Mutt will display 
> a ? when it can't convert a character to something that is printable in your 
> current locale.
> }}}
No, those question marks are artifacts of the trac system I think, because in I have the tree branches in place of the question marks. If you'd like, I could post a screenshot too.

--------------------------------------------------------------------------------
2010-04-13 13:33:17 UTC me
* Added comment:
Replying to [comment:6 daniell]:
> Replying to [comment:5 me]:
> > I found another potential bug where the width of the pad char wasn't used properly when truncating the left[[BR]]
>  part of the string.  I'm not sure this will help your problem, because it looks like you are using a space[[BR]]
>  character.  Please test it and let me know.
> 
> This doesn't apply clean for me; I have that line at 1266. However, if I apply it manually (ie. replace that line in muttlib.c) I get this error during make:

Sorry, that patch was meant to be applied over the first one.  If you would like to just try the second patch by itself, just remove the -offset from those two lines.  The important change for the second patch is changing "pad" to "pad*pw".

--------------------------------------------------------------------------------
2010-04-13 15:44:35 UTC daniell
* Added comment:
With the two patches applied, I still got this behaviour :\
{{{
  1[1]   T Mutt            Re: [Mutt] #3403: soft-fill doesn't count threads' tree lines       (1,4K) 10.04.13 13:33
  2[1]  sF To leva@ecentru aaaaaaaaasd qwd wqd qwd qwdwqd qwd qwdqw qwasdasdqwdqwdwqdqwdqwdqwd (1,3K) 10.04.12 09:11
  3[2]  sF To leva@ecentru ├─>Re: aaaaaaaaasd qwd wqd qwd qwdwqd qwd qwdqw qwasdasdqwdqwdwqdqwdqwdqw (1,5K) 10.04.12
  4[3]  sF To leva@ecentru │ └=>                                                               (2,2K) 10.04.12 09:12
  5[4] rsF To leva@ecentru └=>aaaaaaaaasd qwd wqd qwd qwdwqd qwd qwdqw qwasdasdqwdqwdwqdqwdqwdqwdwqd (1,2K) 10.04.12
}}}

--------------------------------------------------------------------------------
2010-04-13 16:16:34 UTC me
* Added comment:
Could you please provide some information:

1) which version of mutt are you using?

2) which terminal emulator are you using?

3) what is the value of your $LANG environment variable?

4) what is the value of ':set charset' inside of Mutt?

5) what is the value of the $TERM environment variable?

Thanks

--------------------------------------------------------------------------------
2010-04-15 09:07:05 UTC daniell
* Added comment:
Replying to [comment:10 me]:
> Could you please provide some information:
Sure!


> 1) which version of mutt are you using?
Mutt 1.5.20 (2009-06-14)  + the patches here.

> 2) which terminal emulator are you using?
xterm

> 3) what is the value of your $LANG environment variable?
hu_HU.utf8

> 4) what is the value of ':set charset' inside of Mutt?
utf-8

> 5) what is the value of the $TERM environment variable?
screen

--------------------------------------------------------------------------------
2010-04-27 11:41:59 UTC daniell
* Added comment:
Is there anything more I can do to help?
