Ticket:  3494
Status:  new
Summary: <enter> cannot be bound (only <return>)

Reporter: rafael
Owner:    mutt-dev

Opened:       2011-01-03 04:01:04 UTC
Last Updated: 2017-01-18 20:34:41 UTC

Priority:  major
Component: mutt
Keywords:  patch

--------------------------------------------------------------------------------
Description:
This bug has been reported to debian at http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=547661 . I tried it and I could reproduce it as well. I even compiled the official version and still the error was reproducible there.

The problem happens when I try to bind \Cj, mutt interprets that as <Return>. Even if execute ``bind -r '\C-j''' in my shell first, I still get the same error. I used mutt's what-key function and both C-j and <Return> report: Char = <Return>, Octal = 12, Decimal = 10

There was one person on Debian's bug report who couldn't reproduce the bug. So I suspect it might be related to the terminal or the shell. I'm using rxvt-unicode (urxvt) v9.07 and bash 4.1.5, both stock from Debian squeeze repository. Though I've also tried from tty and xterm using both csh and bash.

I made a small curses program (it's attached) that reads a key press and prints the character. Running it in my setup gives me 10 for C-j and 13 for <Return>. Afaik 10 is the character of \n and 13 is the character number for \r. So I guess I can see where the confusion arises from. But note how that differs from what-key output.

--------------------------------------------------------------------------------
2011-01-03 04:01:45 UTC rafael
* Added attachment test.c

--------------------------------------------------------------------------------
2011-01-03 14:51:30 UTC vinc17
* cc changed to vincent@vinc17.org

--------------------------------------------------------------------------------
2014-10-21 04:54:49 UTC vapier
* Added comment:
incoming brain dump, but i'll reward you with a patch to make it work :P

https://en.wikipedia.org/wiki/Control_character#How_control_characters_map_to_keyboards

{{{<enter>}}} is {{{0xa (10)}}}, aka {{{\n}}}, aka newline, and is what {{{ctrl+j (^Cj)}}} sends.[[BR]]
{{{<return>}}} is {{{0xd (13)}}}, aka {{{\r}}}, aka carriage return, and is what {{{ctrl+m (^Cm)}}} sends.

iow, it is not possible to bind {{{^Cj}}} and {{{<enter>}}} to different operations (same goes for {{{^Cm}}} and {{{<return>}}}) because they are the same key stroke.

most likely, your X11 keyboard map has it so that the keyboard sends {{{<return>}}} (regardless of what is printed on the physical keyboard -- "enter" or "return").  the {{{<enter>}}} key is typically part of the numpad.  see the pic on the right side:[[BR]]
https://en.wikipedia.org/wiki/Enter_key

legacy wise, this has long been so inconsistent across keyboards/OS's, that many terminal related programs have options like "enter is return" and "return is enter" so that both keys get interpreted the same way (which is easier than figuring out how to change your keyboard maps for most people), and why a lot of code bases accept either \r or \n as the "confirm" key.  including mutt -- grep for the CI_is_return func as an example.

the rabbit hole gets even deeper as this is baked into the terminal itself.  the termios options you can tweak on the fly via {{{stty}}}:
 * {{{inlcr}}}: translate newline to carriage return on input
 * {{{icrnl}}}: translate carriage return to newline on input
 * {{{ocrnl}}}: translate newline to carriage return on output
 * {{{onlcr}}}: translate carriage return to newline on output
most likely your terminal line settings have it set to {{{-inlcr icrnl -ocrnl onlcr}}} by default.

of course, when you use ncurses, it helpfully takes over the terminal fully and lets the programmer bend it to its will.  mutt has not used the {{{nonl()}}} func which means ncurses takes care of rewriting the enter/return keystrokes into {{{\n}}} everywhere.  this is further compounded by the mutt code defining {{{M_ENTER_C}}} to {{{\n}}} in {{{mutt_curses.h}}} (in the ncurses code path), then aliasing {{{<return>}}} to {{{M_ENTER_C}}}.  so whenever you hit either enter or return, it all gets turned into the {{{<return>}}} binding.

the attached patch works for me in the ncurses code path, although i can see the {{{M_ENTER_C/M_ENTER_S}}} code paths need updating.  i'll post updates as i notice problems.

--------------------------------------------------------------------------------
2014-10-21 04:55:09 UTC vapier
* Added attachment mutt-return-enter.patch
* Added comment:
split <return> and <enter>

--------------------------------------------------------------------------------
2014-10-21 04:59:20 UTC vapier
* cc changed to vincent@vinc17.org, vapier@gentoo.org
* summary changed to <enter> cannot be bound (only <return>)

--------------------------------------------------------------------------------
2016-05-31 18:30:14 UTC vapier
* keywords changed to patch

--------------------------------------------------------------------------------
2016-05-31 18:34:49 UTC vapier
* Added attachment mutt-enter-return-hg-6658.patch
* Added comment:
refreshed patch to latest hg tip

--------------------------------------------------------------------------------
2016-08-31 07:40:17 UTC antonio@dyne.org
* Added comment:
This patch will be applied to the Debian release 1.7.0-2

--------------------------------------------------------------------------------
2016-09-03 17:33:43 UTC kevin8t8
* Added comment:
@dgc, I'd appreciate your input on this.

It looks like the patch maintains the default \r and \n bindings.

However, I worry about confusion that could be caused by this.  Someone who wants to rebind the behavior of their return key could get quite confused which they want to rebind.

Would have to do both for consistent behavior across platforms, or does adding the nonl() fix this problem?

* Updated description:
This bug has been reported to debian at http://bugs.debian.org/cgi-bin/bugreport.cgi?bug=547661 . I tried it and I could reproduce it as well. I even compiled the official version and still the error was reproducible there.

The problem happens when I try to bind \Cj, mutt interprets that as <Return>. Even if execute ``bind -r '\C-j''' in my shell first, I still get the same error. I used mutt's what-key function and both C-j and <Return> report: Char = <Return>, Octal = 12, Decimal = 10

There was one person on Debian's bug report who couldn't reproduce the bug. So I suspect it might be related to the terminal or the shell. I'm using rxvt-unicode (urxvt) v9.07 and bash 4.1.5, both stock from Debian squeeze repository. Though I've also tried from tty and xterm using both csh and bash.

I made a small curses program (it's attached) that reads a key press and prints the character. Running it in my setup gives me 10 for C-j and 13 for <Return>. Afaik 10 is the character of \n and 13 is the character number for \r. So I guess I can see where the confusion arises from. But note how that differs from what-key output.

--------------------------------------------------------------------------------
2016-10-19 17:28:20 UTC grobian
* Added comment:
FWIW Gentoo applies this patch for a while without any reported bugs sofar.

--------------------------------------------------------------------------------
2017-01-18 20:34:41 UTC cri
* Added comment:
Maybe a related bug? I can't bind `\C<Enter>`, it seems it's treated as just `<Enter>`.
Indeed `<what-key>` for Ctrl-Enter returns: `Car = <Return>, Octal = 15, Decimal = 13`
