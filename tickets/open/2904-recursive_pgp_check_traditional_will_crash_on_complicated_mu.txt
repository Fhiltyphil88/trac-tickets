Ticket:  2904
Status:  new
Summary: recursive pgp_check_traditional() will crash on complicated multipart messages

Reporter: taviso
Owner:    mutt-dev

Opened:       2007-06-02 13:11:57 UTC
Last Updated: 2009-06-30 14:25:47 UTC

Priority:  minor
Component: crypto
Keywords:  

--------------------------------------------------------------------------------
Description:
pgp_check_traditional() is implemented recursively, so deeply nested multipart messages will cause a stack overflow, which can be annoying on systems with limited stack space.

Reproduce:

$ bash mime.sh > crash
$ mutt -F /dev/null -f crash -e 'set pgp_auto_decode'
Segmentation fault




--------------------------------------------------------------------------------
2007-06-02 13:12:22 UTC taviso
* Added attachment crash.gz
* Added comment:
demonstration mbox

--------------------------------------------------------------------------------
2007-06-02 13:12:52 UTC taviso
* Added attachment mime.sh
* Added comment:
script used to generate mbox

--------------------------------------------------------------------------------
2009-06-30 14:25:47 UTC pdmef
* component changed to crypto
