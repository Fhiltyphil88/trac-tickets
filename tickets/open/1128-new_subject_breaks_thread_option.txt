Ticket:  1128
Status:  reopened
Summary: new subject breaks thread option

Reporter: Stephen Patterson <steve@localhost.localdomain>
Owner:    mutt-dev

Opened:       2002-03-26 12:00:57 UTC
Last Updated: 2005-08-20 19:35:02 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.5.0i
Severity: wishlist

-- Please type your report below this line

On some mailing lists that I am subscribed to, people sometimes send
out completely new messages by replying to previous messages even when
the new message has nothing to do with the previous message. I try to
educate people not to do this, but it would be really useful if mutt
could create new threads when the subject changes.

This is a sample from my mailbox, the thread "ylug: Some printing
issues" is not related to the other thread "ylug: Next meeting,
where". 

 226   F 11 Oct 01    8:To ylug@halibut    ylug: Next meeting, where
 227   L 11 Oct 01   24:Richard G. Cleg    |-> ylug: Some printing issues
 228 r L 12 Oct 01   34:Arthur Clune       |->
 229   L 12 Oct 01   36:Richard G. Cleg      |->
 230   F 12 Oct 01  266:To ylug@halibut      |->
 231   L 12 Oct 01   15:Richard G. Cleg        |->
 232   L 12 Oct 01   18:Ewan Mac Mahon     |-> Re: ylug: Next meeting, where
 233   L 12 Oct 01   20:Arthur Clune         |->
 234   L 12 Oct 01   46:GIBBS, PAUL        |*>
 235   L 12 Oct 01   22:Ewan Mac Mahon       |->


-- System Information
Debian Release: testing/unstable
Kernel Version: Linux bloodnok 2.4.16 #1 Fri Mar 22 14:30:53 GMT 2002 i686 unknown


-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
gcc
Reading specs from /usr/lib/gcc-lib/i386-linux/2.95.4/specs
gcc version 2.95.4 20011006 (Debian prerelease)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.5.0i (2002-01-22)
Copyright (C) 1996-2001 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.16 (i686) [using ncurses 5.2]
Compile options:
-DOMAIN
+DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
-USE_POP  -USE_IMAP  -USE_GSS  -USE_SSL  -USE_SASL  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+HAVE_PGP  +HAVE_SMIME  -BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_GETSID  -HAVE_GETADDRINFO  
ISPELL="/usr/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/mail"
PKGDATADIR="/usr/local/stow/mutt-cvs/share/mutt"
SYSCONFDIR="/usr/local/stow/mutt-cvs/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.


--- Begin /home/steve/.muttrc
set folder = ~/.Mail
set reverse_alias
set alias_file = ~/.mail.addresses
source ~/.mail.addresses
set sort_alias = alias
set quit = yes 	# exit without prompting 
set realname='Stephen Patterson'
set alternates="s\.patterson@freeuk\.com|cassat@freeuk\.com|lexx\.uklinux\.net"
unset beep	# be quiet (don't beep)
unset prompt_after # don't prompt after external commands
set edit_headers	# allow me to edit message headers
set editor = "jmacs +8"
set postponed = +postponed	# where to save postponed messages
set attribution="On %d, %n (%a) wrote:"
set fast_reply	# don't prompt for recipient address
set abort_unmodified = ask-yes	# ask if i want to send unmodified replies
set record = +sent-mail	# where to save sent messages
send-hook . my_hdr From: steve@lexx.uklinux.net
send-hook . unmy_hdr Reply-To:
send-hook perl-win32 my_hdr From: cassat@freeuk.com
send-hook perl-win32 my_hdr Reply-To: perl-win32-users@listserv.activestate.com
fcc-hook '~s "Job Application"' +jobs # job mail gets seperated
fcc-hook '~s Application' +jobs
fcc-hook '~s CV' +jobs
set sendmail_wait = -1 # don't wait for sendmail to finish
set forward_quote	# quote forwarded text
unset confirmappend	# don't ask when appending messages to a mailbox
unset confirmcreate	# don't ask when creating mailboxes
unset save_empty # delete empty mailboxes
set read_inc = 25
mailboxes =mbox =ylug =wylug =CPAN =procmail =perl-win32 =jobs =mutt =root =thinkpad
unset collapse_unread	# don't collapse threads containing unread messages
unset mark_old		# mark old unread messages as new
set wrap_search		# goto top when search hits bottom
set delete = yes	# delete messages without asking
set mask = '!(^\.)|(\.log$)|(\.cache$)'  # don't show . .. and *.log in mailbox
set mailcap_path = ~/.mailcap:/etc/mailcap
set print_command = 'a2ps -1 --borders=no -Email -q --left-title=mutt'
set print = yes	# print without asking
subscribe wylug-help wylug-announce wylug-discuss ylug perl-win32 mutt-dev procmail linux-thinkpad
set followup_to		    # keep list traffic on lists
set honor_followup_to = yes # ditto
bind index l list-reply
bind pager l list-reply
bind index y print-message
bind pager y print-message
bind browser M enter-mask
macro browser m <select-entry><mail> "Compose message"
macro index S s+spam\n
macro pager S s+spam\n
macro index G "!fetchmail\n" "Check for new mail"
macro pager G "!fetchmail\n" "Check for new mail"
macro index A "!jmacs  ~/.mail.addresses\n:source ~/.mail.addresses\n" "edit aliases"
macro pager A "!jmacs ~/.mail.addresses\n:source ~/.mail.addresses\n" "edit aliases"
set pager_stop  	# don't move to next msg automatically
unset markers		# don't mark wrapped lines
set pager_index_lines=4	# msgs to show above pager
ignore *
unignore from: subject date
unhdr_order *
hdr_order from to date subject
folder-hook sent-mail ignore *
folder-hook sent-mail unignore to subject date
folder-hook jobs ignore *
folder-hook jobs unignore to subject date
color attachment cyan blue
color bold white default
color hdrdefault green default
color header yellow default .*subject.*
color indicator black cyan
color markers cyan default
color quoted white default
color quoted1 green default
color quoted2 red default
color quoted3 cyan default
color search cyan magenta
color tree brightred default
color signature yellow default 
color status yellow blue
color body white default "(ftp|http|https|mailto)://[^ ]+" 
color body white default "mailto:[-a-z_0-9.]+@[-a-z_0-9.]+"
color body white default "[-a-z_0-9.]+@[-a-z_0-9.]+"
color index green default .*[Ww]ylug.*[Dd]iscuss.*
color index cyan default .*[Ww]ylug.*[Hh]elp.*
color index default default .*[Ww]ylug.*[Aa]nnounce.*
folder-hook . set sort=threads
folder-hook . set sort_aux=date
folder-hook mbox set sort=date
folder-hook mbox set sort_aux=subject
folder-hook sent-mail set sort=date-sent
folder-hook . 'exec collapse-all' # collapse all threads
set date_format = "%d %b %y"
set index_format="%4C %Z %d %4l:%-15.15F %2M %s"
set status_on_top	# statbar on top of screen
set pgp_replysign = yes    # sign replies to signed messages
set pgp_verify_sig = yes   # verify signed messages
send-hook . 'set pgp_autosign'
send-hook perl-win32 'unset pgp_autosign'
send-hook wylug 'unset pgp_autosign'
send-hook ~sJob 'unset pgp_autosign'
send-hook ~sCV  'unset pgp_autosign'
set pgp_decode_command="gpg --quiet --no-verbose %?p?--passphrase-fd 0? --batch --output - %f"
set pgp_verify_command="gpg --no-verbose --batch --output - --verify %s %f"
set pgp_decrypt_command="gpg -q --passphrase-fd 0 --batch --output - %f"
set pgp_sign_command="gpg -q --batch --output - --passphrase-fd 0 --armor --detach-sign --textmode %?a?-u %a? %f"
set pgp_encrypt_only_command="pgpewrap gpg -v --batch --output - --encrypt --textmode --armor --always-trust -- -r %r -- %f"
set pgp_encrypt_sign_command="pgpewrap gpg --passphrase-fd 0 -v --batch --output - --encrypt --sign %?a?-u %a? --armor --always-trust -- -r %r -- %f"
set pgp_import_command="gpg --quiet --import -v %f"
set pgp_export_command="gpg --quiet --export --armor %r"
set pgp_verify_key_command="gpg --quiet --batch --fingerprint --check-sigs %r"
set pgp_list_pubring_command="gpg --quiet --batch --with-colons --list-keys %r" 
set pgp_list_secring_command="gpg --quiet --batch --with-colons --list-secret-keys %r" 
--- End /home/steve/.muttrc


--- Begin /usr/local/stow/mutt-cvs/etc/Muttrc
ignore "from " received content- mime-version status x-status message-id
ignore sender references return-path lines
macro index \eb '/~b ' 'search in message bodies'
macro index \cb |urlview\n 'call urlview to extract URLs out of a message'
macro pager \cb |urlview\n 'call urlview to extract URLs out of a message'
macro generic <f1> "!less /usr/local/stow/mutt-cvs/doc/mutt/manual.txt\n" "Show Mutt documentation"
macro index   <f1> "!less /usr/local/stow/mutt-cvs/doc/mutt/manual.txt\n" "Show Mutt documentation"
macro pager   <f1> "!less /usr/local/stow/mutt-cvs/doc/mutt/manual.txt\n" "Show Mutt documentation"
--- End /usr/local/stow/mutt-cvs/etc/Muttrc


Received: (at submit) by bugs.guug.de; 8 Aug 2000 16:15:00 +0000
From PeterBartosch@t-online.de  Tue Aug  8 18:15:00 2000
Received: from mailout05.sul.t-online.com (mailout05.sul.t-online.com [194.25.134.82])
	by sigtrap.guug.de (8.9.3/8.9.3/Debian/GNU) with ESMTP id SAA12528
	for <submit@bugs.guug.de>; Tue, 8 Aug 2000 18:15:00 +0200
Received: from fwd05.sul.t-online.com 
	by mailout05.sul.t-online.com with smtp 
	id 13MC1r-0000wf-06; Tue, 8 Aug 2000 18:14:59 +0200
Received: from mind.bartosch.net (320089164316-0001@[62.156.23.175]) by fwd05.sul.t-online.com
	with esmtp id 13MC1l-25ZssSC; Tue, 8 Aug 2000 18:14:53 +0200
Received: from peter by mind.bartosch.net with local (Exim 3.11 #1 (Debian))
	id 13MBx3-0000t2-00; Tue, 08 Aug 2000 18:10:01 +0200
Reply-To: peter@bartosch.net
Subject: mutt-1.2i: wish: ability to devide a thread if needed
To: submit@bugs.guug.de
Message-Id: <E13MBx3-0000t2-00@mind.bartosch.net>
From: PeterBartosch@t-online.de (Peter Bartosch)
Date: Tue, 08 Aug 2000 18:10:01 +0200
X-Sender: 320089164316-0001@t-dialin.net

Package: mutt
Version: 1.2-1
Severity: wishlist

-- Please type your report below this line

i want to devide a thread in parts if somebody starts a new topic 
right in the middle of a thread
-> start a new thread when the topic (mail) becomes of topic (thread)

the debian-package maintainer wrote that a patch for such funktionality was
declined ... please add this, its usefull 


Peter Bartosch ...

-- System Information
Debian Release: 2.2
Kernel Version: Linux mind 2.2.13ac3 #1 SMP Mon Mar 13 13:32:29 CET 2000 i686 unknown

Versions of the packages mutt depends on:
ii  libc6            2.1.3-10         GNU C Library: Shared libraries and Timezone dat
ii  libncurses5      5.0-6            Shared libraries for terminal handling
ii  exim             3.12-10          Exim Mailer
	^^^ (Provides virtual package mail-transport-agent)

-- Mutt Version Information


System: Linux 2.2.13ac3 [using ncurses 5.0]
Einstellungen bei der Compilierung:
-DOMAIN
+DEBUG
-HOMESPOOL  +USE_SETGID  +USE_DOTLOCK  +USE_FCNTL  -USE_FLOCK
+USE_IMAP  +USE_GSS  +USE_SSL  +USE_POP  +HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_PGP  -BUFFY_SIZE -EXACT_ADDRESS  +ENABLE_NLS
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/var/spool/mail"
SHAREDIR="/usr/share/mutt"
SYSCONFDIR="/etc"
ISPELL="/usr/bin/ispell"



>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2002-03-27 13:32:23 UTC Daniel Eisenbud <eisenbud+mutt@cs.swarthmore.edu>
* Added comment:
{{{
This is not a bug.  You could use the thread-editing patch to detach
these by hand if you wanted.  I guess mutt could have an option that
would break threads if the subject had changed, but I don't see that as
being that generally useful -- subjects often legitimately change in the
same thread.  This is at most a wishlist item.  And to the submitter: if
you don't supply a valid email address when you submit the bug, it's
hard to keep you informed.  But maybe you're reading this through the
bug tracking system.

-Daniel
}}}

--------------------------------------------------------------------------------
2005-07-25 20:11:04 UTC brendan
* Added comment:
{{{
Fixed in CVS
}}}

* status changed to closed

--------------------------------------------------------------------------------
2005-07-29 18:57:28 UTC Brendan Cully <brendan@kublai.com>
* Added comment:
{{{
On Friday, 29 July 2005 at 18:55, Alain Bench wrote:
> The following reply was made to PR mutt/1128; it has been noted by GNATS.
> 
> From: Alain Bench <veronatif@free.fr>
> To: bug-any@bugs.mutt.org
> Cc: 
> Subject: Re: mutt/1128
> Date: Fri, 29 Jul 2005 17:25:07 +0200 (CEST)
> 
>  Hello Brendan,
>  
>   On Monday, July 25, 2005 at 3:11:05 AM +0200, Brendan Cully wrote:
>  
>  > Synopsis: new subject breaks thread option
>  > State-Changed-From-To: open->closed
>  > Fixed in CVS
>  
>      I missed this new feature? Or is there confusion with the
>  <break-thread> function of C=E9dric Duval's cd.edit_threads patch?

Hmm, yes. I had assumed break-thread was satisfactory, but I guess it
isn't quite the automatic break that was requested. I suppose I should
reopen the bug.
}}}

--------------------------------------------------------------------------------
2005-07-30 10:25:07 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
Hello Brendan,

 On Monday, July 25, 2005 at 3:11:05 AM +0200, Brendan Cully wrote:

> Synopsis: new subject breaks thread option
> State-Changed-From-To: open->closed
> Fixed in CVS

    I missed this new feature? Or is there confusion with the
<break-thread> function of C=E9dric Duval's cd.edit_threads patch?


Bye!=09Alain.
--=20
Give your computer's unused idle processor cycles to a scientific goa=
l:
The Folding@home project at <URL:http://folding.stanford.edu/>.
}}}

--------------------------------------------------------------------------------
2005-07-30 13:12:45 UTC brendan
* Added comment:
{{{
Hello Brendan,
Almost, but not quite, resolved.
}}}

* status changed to new

--------------------------------------------------------------------------------
2005-07-30 13:12:46 UTC brendan
* Added comment:
{{{
Hmm, I suppose I thought break-thread should be sufficient. But it isn't as automatic as this requester would like, so I guess I'll reopen it.
}}}

--------------------------------------------------------------------------------
2005-07-30 17:40:18 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
On Friday, July 29, 2005 at 8:12:45 PM +0200, Brendan Cully wrote:

> Hmm, I suppose I thought break-thread should be sufficient. But it
> isn't as automatic as this requester would like, so I guess I'll
> reopen it.

    Yes: <break-thread> is a manual operation, and non-reversibly
modifies the mail. The here wished feature would be a display option,
just like sorting, limiting, or collapsing: Harmless for the mail.
Ideally the detached branch would still be linked in some way to parent
thread: Sorted just below, and perhaps linked with a new thread symbol,
or such. But they would collapse as 2 separate threads.

    After all a changing title in thread can be a hijacking new mail, an
artifact, or just a topic that evolved. IMHO this wish makes sense.


Bye!	Alain.
-- 
When you post a new message, beginning a new topic, use the "mail" or
"post" or "new message" functions.
When you reply or followup, use the "reply" or "followup" functions.
Do not do the one for the other, this breaks or hijacks threads.
}}}

--------------------------------------------------------------------------------
2005-08-21 08:45:16 UTC rado
* Added comment:
{{{
On Friday, 29 July 2005 at 18:55, Alain Bench wrote:
  On Friday, July 29, 2005 at 8:12:45 PM +0200, Brendan Cully wrote:
 - Isn't strict_threads supposed to answer the request?
- How to change the reporter's eMail to the correct value?
}}}

--------------------------------------------------------------------------------
2005-08-21 12:13:37 UTC Alain Bench <veronatif@free.fr>
* Added comment:
{{{
Hi Rado, welcome to mutt-dev.

 On Saturday, August 20, 2005 at 3:45:16 PM +0200, Rado Smiljanic wrote:

> Synopsis: new subject breaks thread option
> Isn't strict_threads supposed to answer the request?

    No: $strict_threads breaks threads where subject is same but
references lack. The request is breaking threads where references exist
but subject changed.


> How to change the reporter's eMail to the correct value?

    I haven't found? Workaround: Put name and email in Notify-List.


Bye!	Alain.
-- 
Software should be written to deal with every conceivable error
	RFC 1122 / Robustness Principle
}}}
