Ticket:  1775
Status:  new
Summary: Threading should be deterministic

Reporter: vincent@vinc17.org
Owner:    mutt-dev

Opened:       2004-01-23 17:54:11 UTC
Last Updated: 2005-10-08 16:22:58 UTC

Priority:  trivial
Component: mutt
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
Package: mutt
Version: 1.5.5.1-200401
Severity: wishlist

-- Please type your report below this line

Mutt's threading isn't deterministic. For instance, in my archive mailbox,
I have initially:

   5583   + 2002-01-25 xxxxxx xxxxxxx  (  41) linux
   5584   F 2002-01-25 To xxxxxx xxxxx (  62) `->
   5585   F 2002-01-25 To xxxxxx xxxxx (  17)   `->
   5586   F 2002-01-25 To xxxxxx xxxxx (  25)     `->
   5587   F 2002-01-25 To xxxxxx       (  16) Piles du Psion (was: Le futur
   5588   F 2002-01-25 To mutt-dev@mut (  15) C99 comments
[...]
   14979   + 2003-07-23 bugzilla-daemon (  12) [Bug 213444] Crash due to Ja
   14980   F 2002-01-25 To xxxxxx       (  18) Re: Le futur en question.
   14981 r L 2002-01-25 xxx             (  55) |->
   14982   F 2002-01-25 To xxxxxx       (  14) | `->
   14983 r L 2002-01-25 xxx             (  17) `--->
   14984   L 2002-01-25 xxx             (  40)   |->
   14985   L 2002-07-31 xxxxxx xxxxx    (  12)   |->Re: Psion (5mx...) et L
   14986   L 2003-07-23 xxxxx xxxxxx    (  27)   `->Re: Tr: ARM Conference:
   14987  sL 2003-07-23 xxxxxx xxxxx    ( 141) Re: mutt sets charset to utf

But if I modify a message (e.g. flag-message twice on the latest
message) and synchronize, I get:

   5583   + 2002-01-25 xxxxxx xxxxxxx  (  41) linux
   5584   F 2002-01-25 To xxxxxx xxxxx (  62) `->
   5585   F 2002-01-25 To xxxxxx xxxxx (  17)   `->
   5586   F 2002-01-25 To xxxxxx xxxxx (  25)     `->
   5587   F 2002-01-25 To mutt-dev@mut (  15) C99 comments
[...]
   14978   + 2003-07-23 bugzilla-daemon (  12) [Bug 213444] Crash due to Ja
   14979   F 2002-01-25 To xxxxxx       (  16) -->Piles du Psion (was: Le f
   14980   L 2002-01-25 xxx             (  40) `--->Re: Le futur en questio
   14981 r L 2002-01-25 xxx             (  17)   |->Re: Le futur en questio
   14982   F 2002-01-25 To xxxxxx       (  18)   | `->
   14983 r L 2002-01-25 xxx             (  55)   |   `->
   14984   F 2002-01-25 To xxxxxx       (  14)   |     `->
   14985   L 2002-07-31 xxxxxx xxxxx    (  12)   |->Re: Psion (5mx...) et L
   14986   L 2003-07-23 xxxxx xxxxxx    (  27)   `->Re: Tr: ARM Conference:
   14987  sL 2003-07-23 xxxxxx xxxxx    ( 141) Re: mutt sets charset to utf

If I do another modification, I get the first threading.

-- Build environment information

(Note: This is the build environment installed on the system
muttbug is run on.  Information may or may not match the environment
used to build mutt.)

- gcc version information
cc
Reading specs from /usr/lib/gcc-lib/powerpc-linux/3.3.2/specs
Configured with: ../src/configure -v --enable-languages=c,c++,java,f77,pascal,objc,ada --prefix=/usr --mandir=/usr/share/man --infodir=/usr/share/info --with-gxx-include-dir=/usr/include/c++/3.3 --enable-shared --with-system-zlib --enable-nls --without-included-gettext --enable-__cxa_atexit --enable-clocale=gnu --enable-java-gc=boehm --enable-java-awt=xlib --enable-objc-gc --disable-multilib powerpc-linux
Thread model: posix
gcc version 3.3.2 (Debian)

- CFLAGS
-Wall -pedantic -g -O2

-- Mutt Version Information

Mutt 1.5.5.1i (2003-11-05)
Copyright (C) 1996-2002 Michael R. Elkins and others.
Mutt comes with ABSOLUTELY NO WARRANTY; for details type `mutt -vv'.
Mutt is free software, and you are welcome to redistribute it
under certain conditions; type `mutt -vv' for details.

System: Linux 2.4.18-newpmac (ppc) [using ncurses 5.3] [using libidn 0.1.14 (compiled with 0.1.14)]
Compile options:
-DOMAIN
-DEBUG
-HOMESPOOL  -USE_SETGID  -USE_DOTLOCK  -DL_STANDALONE  
+USE_FCNTL  -USE_FLOCK
+USE_POP  +USE_IMAP  -USE_GSS  +USE_SSL  -USE_SASL  -USE_SASL2  
+HAVE_REGCOMP  -USE_GNU_REGEX  
+HAVE_COLOR  +HAVE_START_COLOR  +HAVE_TYPEAHEAD  +HAVE_BKGDSET  
+HAVE_CURS_SET  +HAVE_META  +HAVE_RESIZETERM  
+CRYPT_BACKEND_CLASSIC_PGP  +CRYPT_BACKEND_CLASSIC_SMIME  -CRYPT_BACKEND_GPGME  +BUFFY_SIZE -EXACT_ADDRESS  -SUN_ATTACHMENT  
+ENABLE_NLS  -LOCALES_HACK  +HAVE_WC_FUNCS  +HAVE_LANGINFO_CODESET  +HAVE_LANGINFO_YESEXPR  
+HAVE_ICONV  -ICONV_NONTRANS  +HAVE_LIBIDN  +HAVE_GETSID  +HAVE_GETADDRINFO  
ISPELL="/usr/local/bin/ispell"
SENDMAIL="/usr/sbin/sendmail"
MAILPATH="/home/lefevre/Mail"
PKGDATADIR="/home/lefevre/share/mutt"
SYSCONFDIR="/home/lefevre/etc"
EXECSHELL="/bin/sh"
-MIXMASTER
To contact the developers, please mail to <mutt-dev@mutt.org>.
To report a bug, please use the flea(1) utility.

patch-1.3.24.ats.parent_match.1
patch-1.5.1.vl.savehist.1


>How-To-Repeat:
>Fix:
}}}

--------------------------------------------------------------------------------
2005-10-09 10:22:58 UTC ab
* Added comment:
{{{
deduppe unform
}}}
