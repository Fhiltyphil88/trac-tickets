Ticket:  2222
Status:  new
Summary: gpgme - diffence in key selection

Reporter: alex-mutt@zeitform.de
Owner:    mutt-dev

Opened:       2006-05-04 18:38:55 UTC
Last Updated: 2009-06-30 14:44:11 UTC

Priority:  trivial
Component: crypto
Keywords:  

--------------------------------------------------------------------------------
Description:
{{{
I have noticed a difference in gpg key selection (using gpgme and mutt cvs) for the recipient "xxx@domain" versus "<xxx@domain>".

This is my example:

> gpg --list-keys Pleiner
pub   1024D/613C21EA 2001-07-30
uid                  Alex Pleiner <alex@zeitform.de>
uid                  Alex Pleiner <pleiner@zeitform.de>
uid                  Alex Pleiner <a.pleiner@zeitform.de>
uid                  Alex Pleiner <ciphelp@igd.fhg.de>
sub   1024g/2C55D552 2001-07-30

If I compose an e-mail to "alex@zeitform.de" I will not be asked for the recipient key. The correct one is automatically selected. 
If I write to "Alex Pleiner <alex@zeitform.de>" or even
"<alex@zeitform.de"> I get to the pgp menu with all those above entries and have to manually select the right one. Is this correct behaviour? Feature or bug?
This does not happen if I disable gpgme.
>How-To-Repeat:
encrypt mail to recipient with multiple subkeys (e-mail addresses) via gpgme an use backets around recipient address.
>Fix:
Unknown
}}}

--------------------------------------------------------------------------------
2009-06-30 14:44:11 UTC pdmef
* component changed to crypto
* Updated description:
{{{
I have noticed a difference in gpg key selection (using gpgme and mutt cvs) for the recipient "xxx@domain" versus "<xxx@domain>".

This is my example:

> gpg --list-keys Pleiner
pub   1024D/613C21EA 2001-07-30
uid                  Alex Pleiner <alex@zeitform.de>
uid                  Alex Pleiner <pleiner@zeitform.de>
uid                  Alex Pleiner <a.pleiner@zeitform.de>
uid                  Alex Pleiner <ciphelp@igd.fhg.de>
sub   1024g/2C55D552 2001-07-30

If I compose an e-mail to "alex@zeitform.de" I will not be asked for the recipient key. The correct one is automatically selected. 
If I write to "Alex Pleiner <alex@zeitform.de>" or even
"<alex@zeitform.de"> I get to the pgp menu with all those above entries and have to manually select the right one. Is this correct behaviour? Feature or bug?
This does not happen if I disable gpgme.
>How-To-Repeat:
encrypt mail to recipient with multiple subkeys (e-mail addresses) via gpgme an use backets around recipient address.
>Fix:
Unknown
}}}
