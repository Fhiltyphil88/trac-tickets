# HG changeset patch
# User Kevin McCarthy <kevin@8t8.us>
# Date 1452309779 28800
#      Fri Jan 08 19:22:59 2016 -0800
# Node ID 1eef88a038dcb07056e5338034fcb108df1602d6
# Parent  7c0bd34546f4bbd9bd8f9584e69947bc7e5d4b6e
Make sasl authentication buffers dynamically sized. (see #3804)

The reporter found that the current buffer of HUGE_STRING was
insufficient in his case to encode the clientout response back to the
server in imap_auth_sasl().

Since sasl gives us the size of "clientout", we can dynamically malloc
and resize the buffer as needed.  This patch uses max(LONG_STRING,
(clientoutlen*2)).  This is sufficient to hold the base64 encoded
value plus additional prefix/suffix needed in each protocol.

The size is rechecked after each sasl_client_step() and resized as
needed.

Similar code is in pop_auth_sasl() and smtp_auth_sasl(), so convert
all three to use a dynamic buffer.

diff --git a/imap/auth_sasl.c b/imap/auth_sasl.c
--- a/imap/auth_sasl.c
+++ b/imap/auth_sasl.c
@@ -31,17 +31,18 @@
 #include <sasl/saslutil.h>
 
 /* imap_auth_sasl: Default authenticator if available. */
 imap_auth_res_t imap_auth_sasl (IMAP_DATA* idata, const char* method)
 {
   sasl_conn_t* saslconn;
   sasl_interact_t* interaction = NULL;
   int rc, irc;
-  char buf[HUGE_STRING];
+  char *buf = NULL;
+  size_t bufsize = 0;
   const char* mech;
   const char *pc = NULL;
   unsigned int len, olen;
   unsigned char client_start;
 
   if (mutt_sasl_client_new (idata->conn, &saslconn) < 0)
   {
     dprint (1, (debugfile,
@@ -94,22 +95,25 @@
       dprint (1, (debugfile, "imap_auth_sasl: Failure starting authentication exchange. No shared mechanisms?\n"));
     /* SASL doesn't support LOGIN, so fall back */
 
     return IMAP_AUTH_UNAVAIL;
   }
 
   mutt_message (_("Authenticating (%s)..."), mech);
 
-  snprintf (buf, sizeof (buf), "AUTHENTICATE %s", mech);
+  bufsize = ((olen * 2) > LONG_STRING) ? (olen * 2) : LONG_STRING;
+  buf = safe_malloc (bufsize);
+
+  snprintf (buf, bufsize, "AUTHENTICATE %s", mech);
   if (mutt_bit_isset (idata->capabilities, SASL_IR) && client_start)
   {
     len = mutt_strlen (buf);
     buf[len++] = ' ';
-    if (sasl_encode64 (pc, olen, buf + len, sizeof (buf) - len, &olen) != SASL_OK)
+    if (sasl_encode64 (pc, olen, buf + len, bufsize - len, &olen) != SASL_OK)
     {
       dprint (1, (debugfile, "imap_auth_sasl: error base64-encoding client response.\n"));
       goto bail;
     }
     client_start = olen = 0;
   }
   imap_cmd_start (idata, buf);
   irc = IMAP_CMD_CONTINUE;
@@ -127,21 +131,31 @@
     if (irc == IMAP_CMD_RESPOND)
     {
       /* Exchange incorrectly returns +\r\n instead of + \r\n */
       if (idata->buf[1] == '\0')
       {
 	buf[0] = '\0';
 	len = 0;
       }
-      else if (sasl_decode64 (idata->buf+2, strlen (idata->buf+2), buf,
-			      LONG_STRING-1, &len) != SASL_OK)
+      else
       {
-	dprint (1, (debugfile, "imap_auth_sasl: error base64-decoding server response.\n"));
-	goto bail;
+        len = strlen (idata->buf + 2);
+        if (len > bufsize)
+        {
+          bufsize = len;
+          safe_realloc (&buf, bufsize);
+        }
+        /* For sasl_decode64, the fourth parameter, outmax, doesn't
+         * include space for the trailing null */
+        if (sasl_decode64 (idata->buf+2, len, buf, bufsize - 1, &len) != SASL_OK)
+        {
+          dprint (1, (debugfile, "imap_auth_sasl: error base64-decoding server response.\n"));
+          goto bail;
+        }
       }
     }
 
     /* client-start is only available with the SASL-IR extension, but
      * SASL 2.1 seems to want to use it regardless, at least for DIGEST
      * fast reauth. Override if the server sent an initial continuation */
     if (!client_start || buf[0])
     {
@@ -154,26 +168,31 @@
       while (rc == SASL_INTERACT);
     }
     else
       client_start = 0;
 
     /* send out response, or line break if none needed */
     if (olen)
     {
-      if (sasl_encode64 (pc, olen, buf, sizeof (buf), &olen) != SASL_OK)
+      if ((olen * 2) > bufsize)
+      {
+        bufsize = olen * 2;
+        safe_realloc (&buf, bufsize);
+      }
+      if (sasl_encode64 (pc, olen, buf, bufsize, &olen) != SASL_OK)
       {
 	dprint (1, (debugfile, "imap_auth_sasl: error base64-encoding client response.\n"));
 	goto bail;
       }
     }
     
     if (irc == IMAP_CMD_RESPOND)
     {
-      strfcpy (buf + olen, "\r\n", sizeof (buf) - olen);
+      strfcpy (buf + olen, "\r\n", bufsize - olen);
       mutt_socket_write (idata->conn, buf);
     }
 
     /* If SASL has errored out, send an abort string to the server */
     if (rc < 0)
     {
       mutt_socket_write (idata->conn, "*\r\n");
       dprint (1, (debugfile, "imap_auth_sasl: sasl_client_step error %d\n",rc));
@@ -187,21 +206,23 @@
       break;
 
   if (rc != SASL_OK)
     goto bail;
 
   if (imap_code (idata->buf))
   {
     mutt_sasl_setup_conn (idata->conn, saslconn);
+    FREE (&buf);
     return IMAP_AUTH_SUCCESS;
   }
 
  bail:
   sasl_dispose (&saslconn);
+  FREE (&buf);
 
   if (method)
   {
     dprint (2, (debugfile, "imap_auth_sasl: %s failed\n", method));
     return IMAP_AUTH_UNAVAIL;
   }
 
   mutt_error _("SASL authentication failed.");
diff --git a/pop_auth.c b/pop_auth.c
--- a/pop_auth.c
+++ b/pop_auth.c
@@ -37,17 +37,18 @@
 
 #ifdef USE_SASL
 /* SASL authenticator */
 static pop_auth_res_t pop_auth_sasl (POP_DATA *pop_data, const char *method)
 {
   sasl_conn_t *saslconn;
   sasl_interact_t *interaction = NULL;
   int rc;
-  char buf[LONG_STRING];
+  char *buf = NULL;
+  size_t bufsize = 0;
   char inbuf[LONG_STRING];
   const char* mech;
   const char *pc = NULL;
   unsigned int len, olen, client_start;
 
   if (mutt_sasl_client_new (pop_data->conn, &saslconn) < 0)
   {
     dprint (1, (debugfile, "pop_auth_sasl: Error allocating SASL connection.\n"));
@@ -72,36 +73,40 @@
     /* SASL doesn't support suggested mechanisms, so fall back */
     return POP_A_UNAVAIL;
   }
 
   client_start = olen;
 
   mutt_message _("Authenticating (SASL)...");
 
-  snprintf (buf, sizeof (buf), "AUTH %s", mech);
+  bufsize = ((olen * 2) > LONG_STRING) ? (olen * 2) : LONG_STRING;
+  buf = safe_malloc (bufsize);
+
+  snprintf (buf, bufsize, "AUTH %s", mech);
   olen = strlen (buf);
 
   /* looping protocol */
   FOREVER
   {
-    strfcpy (buf + olen, "\r\n", sizeof (buf) - olen);
+    strfcpy (buf + olen, "\r\n", bufsize - olen);
     mutt_socket_write (pop_data->conn, buf);
     if (mutt_socket_readln (inbuf, sizeof (inbuf), pop_data->conn) < 0)
     {
       sasl_dispose (&saslconn);
       pop_data->status = POP_DISCONNECTED;
+      FREE (&buf);
       return POP_A_SOCKET;
     }
 
     if (!client_start && rc != SASL_CONTINUE)
       break;
 
     if (!mutt_strncmp (inbuf, "+ ", 2)
-        && sasl_decode64 (inbuf+2, strlen (inbuf+2), buf, LONG_STRING-1, &len) != SASL_OK)
+        && sasl_decode64 (inbuf+2, strlen (inbuf+2), buf, bufsize - 1, &len) != SASL_OK)
     {
       dprint (1, (debugfile, "pop_auth_sasl: error base64-decoding server response.\n"));
       goto bail;
     }
 
     if (!client_start)
       FOREVER
       {
@@ -117,44 +122,54 @@
     }
 
     if (rc != SASL_CONTINUE && (olen == 0 || rc != SASL_OK))
       break;
 
     /* send out response, or line break if none needed */
     if (pc)
     {
-      if (sasl_encode64 (pc, olen, buf, sizeof (buf), &olen) != SASL_OK)
+      if ((olen * 2) > bufsize)
+      {
+        bufsize = olen * 2;
+        safe_realloc (&buf, bufsize);
+      }
+      if (sasl_encode64 (pc, olen, buf, bufsize, &olen) != SASL_OK)
       {
 	dprint (1, (debugfile, "pop_auth_sasl: error base64-encoding client response.\n"));
 	goto bail;
       }
     }
   }
 
   if (rc != SASL_OK)
     goto bail;
 
   if (!mutt_strncmp (inbuf, "+OK", 3))
   {
     mutt_sasl_setup_conn (pop_data->conn, saslconn);
+    FREE (&buf);
     return POP_A_SUCCESS;
   }
 
 bail:
   sasl_dispose (&saslconn);
 
   /* terminate SASL session if the last response is not +OK nor -ERR */
   if (!mutt_strncmp (inbuf, "+ ", 2))
   {
-    snprintf (buf, sizeof (buf), "*\r\n");
+    snprintf (buf, bufsize, "*\r\n");
     if (pop_query (pop_data, buf, sizeof (buf)) == -1)
+    {
+      FREE (&buf);
       return POP_A_SOCKET;
+    }
   }
 
+  FREE (&buf);
   mutt_error _("SASL authentication failed.");
   mutt_sleep (2);
 
   return POP_A_FAILURE;
 }
 #endif
 
 /* Get the server timestamp for APOP authentication */
diff --git a/smtp.c b/smtp.c
--- a/smtp.c
+++ b/smtp.c
@@ -563,17 +563,18 @@
 
 static int smtp_auth_sasl (CONNECTION* conn, const char* mechlist)
 {
   sasl_conn_t* saslconn;
   sasl_interact_t* interaction = NULL;
   const char* mech;
   const char* data = NULL;
   unsigned int len;
-  char buf[HUGE_STRING];
+  char *buf = NULL;
+  size_t bufsize = 0;
   int rc, saslrc;
 
   if (mutt_sasl_client_new (conn, &saslconn) < 0)
     return SMTP_AUTH_FAIL;
 
   do
   {
     rc = sasl_client_start (saslconn, mechlist, &interaction, &data, &len, &mech);
@@ -587,68 +588,78 @@
     dprint (2, (debugfile, "smtp_auth_sasl: %s unavailable\n", mech));
     sasl_dispose (&saslconn);
     return SMTP_AUTH_UNAVAIL;
   }
 
   if (!option(OPTNOCURSES))
     mutt_message (_("Authenticating (%s)..."), mech);
 
-  snprintf (buf, sizeof (buf), "AUTH %s", mech);
+  bufsize = ((len * 2) > LONG_STRING) ? (len * 2) : LONG_STRING;
+  buf = safe_malloc (bufsize);
+
+  snprintf (buf, bufsize, "AUTH %s", mech);
   if (len)
   {
-    safe_strcat (buf, sizeof (buf), " ");
+    safe_strcat (buf, bufsize, " ");
     if (sasl_encode64 (data, len, buf + mutt_strlen (buf),
-                       sizeof (buf) - mutt_strlen (buf), &len) != SASL_OK)
+                       bufsize - mutt_strlen (buf), &len) != SASL_OK)
     {
       dprint (1, (debugfile, "smtp_auth_sasl: error base64-encoding client response.\n"));
       goto fail;
     }
   }
-  safe_strcat (buf, sizeof (buf), "\r\n");
+  safe_strcat (buf, bufsize, "\r\n");
 
   do {
     if (mutt_socket_write (conn, buf) < 0)
       goto fail;
-    if ((rc = mutt_socket_readln (buf, sizeof (buf), conn)) < 0)
+    if ((rc = mutt_socket_readln (buf, bufsize, conn)) < 0)
       goto fail;
     if (smtp_code (buf, rc, &rc) < 0)
       goto fail;
 
     if (rc != smtp_ready)
       break;
 
-    if (sasl_decode64 (buf+4, strlen (buf+4), buf, sizeof (buf), &len) != SASL_OK)
+    if (sasl_decode64 (buf+4, strlen (buf+4), buf, bufsize - 1, &len) != SASL_OK)
     {
       dprint (1, (debugfile, "smtp_auth_sasl: error base64-decoding server response.\n"));
       goto fail;
     }
 
     do
     {
       saslrc = sasl_client_step (saslconn, buf, len, &interaction, &data, &len);
       if (saslrc == SASL_INTERACT)
         mutt_sasl_interact (interaction);
     }
     while (saslrc == SASL_INTERACT);
 
     if (len)
     {
-      if (sasl_encode64 (data, len, buf, sizeof (buf), &len) != SASL_OK)
+      if ((len * 2) > bufsize)
+      {
+        bufsize = len * 2;
+        safe_realloc (&buf, bufsize);
+      }
+      if (sasl_encode64 (data, len, buf, bufsize, &len) != SASL_OK)
       {
         dprint (1, (debugfile, "smtp_auth_sasl: error base64-encoding client response.\n"));
         goto fail;
       }
     }
-    strfcpy (buf + len, "\r\n", sizeof (buf) - len);
+    strfcpy (buf + len, "\r\n", bufsize - len);
   } while (rc == smtp_ready && saslrc != SASL_FAIL);
 
   if (smtp_success (rc))
   {
     mutt_sasl_setup_conn (conn, saslconn);
+    FREE (&buf);
     return SMTP_AUTH_SUCCESS;
   }
 
 fail:
   sasl_dispose (&saslconn);
+  FREE (&buf);
   return SMTP_AUTH_FAIL;
 }
 #endif /* USE_SASL */
