diff --git a/doc/manual.xml.head b/doc/manual.xml.head
index 18a5a0d..09ac109 100644
--- a/doc/manual.xml.head
+++ b/doc/manual.xml.head
@@ -5201,6 +5201,22 @@ recipients from Germany.
 </example>
 
 <para>
+<emphasis role="bold">Parent and child match</emphasis>.
+You can tell mutt that the following pattern has to be matched against
+the parent message with &lt; or one of its children with &gt;.
+This example matches all mails which have at least an unread duplicate
+message:
+</para>
+
+<para>
+
+<screen>
+>(~= ~N)
+</screen>
+
+</para>
+
+<para>
 You can restrict address pattern matching to aliases that you have
 defined with the "@" modifier.  This example matches messages whose
 recipients are all from Germany, and who are known to your alias list.
diff --git a/mutt.h b/mutt.h
index 10d5ab6..3f02ee0 100644
--- a/mutt.h
+++ b/mutt.h
@@ -863,6 +863,8 @@ typedef struct pattern_t
   unsigned int alladdr : 1;
   unsigned int stringmatch : 1;
   unsigned int groupmatch : 1;
+  unsigned int parentmatch : 1;
+  unsigned int childsmatch : 1;
   unsigned int ign_case : 1;		/* ignore case for local stringmatch searches */
   unsigned int isalias : 1;
   int min;
diff --git a/pattern.c b/pattern.c
index 8462e4d..b67e159 100644
--- a/pattern.c
+++ b/pattern.c
@@ -46,6 +46,7 @@ static int eat_regexp (pattern_t *pat, BUFFER *, BUFFER *);
 static int eat_date (pattern_t *pat, BUFFER *, BUFFER *);
 static int eat_range (pattern_t *pat, BUFFER *, BUFFER *);
 static int patmatch (const pattern_t *pat, const char *buf);
+static int pattern_exec (struct pattern_t *pat, pattern_exec_flag flags, CONTEXT *ctx, HEADER *h);
 
 static const struct pattern_flags
 {
@@ -781,6 +782,8 @@ pattern_t *mutt_pattern_comp (/* const */ char *s, int flags, BUFFER *err)
   pattern_t *last = NULL;
   int not = 0;
   int alladdr = 0;
+  int parentmatch = 0;
+  int childsmatch = 0;
   int or = 0;
   int implicit = 1;	/* used to detect logical AND operator */
   int isalias = 0;
@@ -806,6 +809,23 @@ pattern_t *mutt_pattern_comp (/* const */ char *s, int flags, BUFFER *err)
 	ps.dptr++;
 	not = !not;
 	break;
+      case '<':
+	ps.dptr++;
+	if (childsmatch) {
+	  snprintf (err->data, err->dsize, _("cannot use both < and > as a pattern modifier"));
+	  mutt_pattern_free (&curlist);
+	  return NULL;
+	}
+	parentmatch = 1;
+	break;
+      case '>':
+	ps.dptr++;
+	if (parentmatch) {
+	  snprintf (err->data, err->dsize, _("cannot use both < and > as a pattern modifier"));
+	  mutt_pattern_free (&curlist);
+	  return NULL;
+	}
+	childsmatch = 1;
       case '@':
 	ps.dptr++;
 	isalias = !isalias;
@@ -835,6 +855,8 @@ pattern_t *mutt_pattern_comp (/* const */ char *s, int flags, BUFFER *err)
 	implicit = 0;
 	not = 0;
 	alladdr = 0;
+	parentmatch = 0;
+	childsmatch = 0;
 	isalias = 0;
 	break;
       case '%':
@@ -865,9 +887,13 @@ pattern_t *mutt_pattern_comp (/* const */ char *s, int flags, BUFFER *err)
 	  last = tmp;
 	  tmp->not ^= not;
 	  tmp->alladdr |= alladdr;
+	  tmp->parentmatch |= parentmatch;
+	  tmp->childsmatch |= childsmatch;
 	  tmp->isalias |= isalias;
 	  not = 0;
 	  alladdr = 0;
+	  parentmatch = 0;
+	  childsmatch = 0;
 	  isalias = 0;
 	  /* compile the sub-expression */
 	  buf = mutt_substrdup (ps.dptr + 1, p);
@@ -896,11 +922,15 @@ pattern_t *mutt_pattern_comp (/* const */ char *s, int flags, BUFFER *err)
 	tmp = new_pattern ();
 	tmp->not = not;
 	tmp->alladdr = alladdr;
+	tmp->parentmatch = parentmatch;
+	tmp->childsmatch = childsmatch;
 	tmp->isalias = isalias;
         tmp->stringmatch = (*ps.dptr == '=') ? 1 : 0;
         tmp->groupmatch  = (*ps.dptr == '%') ? 1 : 0;
 	not = 0;
 	alladdr = 0;
+	parentmatch = 0;
+	childsmatch = 0;
 	isalias = 0;
 
 	if (last)
@@ -967,9 +997,13 @@ pattern_t *mutt_pattern_comp (/* const */ char *s, int flags, BUFFER *err)
 	last = tmp;
 	tmp->not ^= not;
 	tmp->alladdr |= alladdr;
+	tmp->parentmatch |= parentmatch;
+	tmp->childsmatch |= childsmatch;
 	tmp->isalias |= isalias;
 	not = 0;
 	alladdr = 0;
+	parentmatch = 0;
+	childsmatch = 0;
 	isalias = 0;
 	ps.dptr = p + 1; /* restore location */
 	break;
@@ -1129,13 +1163,44 @@ static int is_pattern_cache_set (int cache_entry)
 }
 
 
+/* Handles parent/children matching. */
+int
+mutt_pattern_exec (struct pattern_t *pat, pattern_exec_flag flags, CONTEXT *ctx, HEADER *h,
+                   pattern_cache_t *cache)
+{
+  THREAD *t;
+
+  if (pat->parentmatch) {
+    if (h->thread && h->thread->parent && h->thread->parent->message)
+      return pattern_exec (pat, flags, ctx, h->thread->parent->message);
+    else
+      return pat->not;
+  }
+  if (pat->childsmatch) {
+    if (!h->thread)
+      return pat->not;
+    if (!h->thread->child)
+      return pat->not;
+    t = h->thread->child;
+    while (t->prev)
+      t = t->prev;
+    for (; t; t = t->next) {
+      if (!t->message)
+	continue;
+      if (pattern_exec (pat, flags, ctx, t->message))
+	return !pat->not;
+    }
+    return pat->not;
+  }
+  return pattern_exec (pat, flags, ctx, h);
+}
+
 /*
  * flags: MUTT_MATCH_FULL_ADDRESS - match both personal and machine address
  * cache: For repeated matches against the same HEADER, passing in non-NULL will
  *        store some of the cacheable pattern matches in this structure. */
-int
-mutt_pattern_exec (struct pattern_t *pat, pattern_exec_flag flags, CONTEXT *ctx, HEADER *h,
-                   pattern_cache_t *cache)
+static int
+pattern_exec (struct pattern_t *pat, pattern_exec_flag flags, CONTEXT *ctx, HEADER *h)
 {
   int result;
   int *cache_entry;
