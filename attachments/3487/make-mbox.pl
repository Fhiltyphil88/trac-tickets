#!/usr/bin/perl

use strict;
use warnings;

sub print_mail( $ );
sub print_mail_encrypted( $ );

sub print_mail( $ ) { return print_mail_plain($_[0]) }

my $size = 0;
my $index = 1;

# * repeat the mail until the mailbox is bigger than 2GiB
# * customize the Message-ID: of each mail with the number of the mail so mutt
#   will not try to put them all into one thread (way too slow)
while($size < 2*1024**3) {
    $size += print_mail($index);
    ++$index;
}

# print one more mail that starts after the 2GiB boundary
print_mail($index);

sub print_mail_plain( $ ) {
    my $text = <<EOT;
From jorrit\@jorrit.de Mon Nov  1 22:36:59 2010
Received: from joe by paranoia.fritz.box with local (Exim 4.72)
	(envelope-from <jorrit\@jorrit.de>)
	id 1PD23n-0002Ym-RU
	for joe\@localhost; Mon, 01 Nov 2010 22:36:59 +0100
Date: Mon, 1 Nov 2010 22:36:59 +0100
From: =?iso-8859-1?Q?J=F6?= Fahlke <jorrit\@jorrit.de>
To: =?iso-8859-1?Q?J=F6?= <joe\@localhost>
Subject: test
Message-ID: <$_[0].20101101213659.GE29893\@paranoia>
MIME-Version: 1.0
Content-Type: text/plain; charset=iso-8859-1
Content-Disposition: inline
Content-Transfer-Encoding: quoted-printable
User-Agent: Mutt/1.5.20 (2009-06-14)
Content-Length: 195

test

--=20
Interpunktion, Orthographie und Grammatik der Email ist frei erfunden.
Eine =DCbereinstimmung mit aktuellen oder ehemaligen Regeln w=E4re rein
zuf=E4llig und ist nicht beabsichtigt.


EOT

    print $text;
    return length $text;
}

sub print_mail_encrypted( $ ) {
    my $text = <<EOT;
From jorrit\@jorrit.de Mon Nov  1 16:23:21 2010
Received: from joe by paranoia.fritz.box with local (Exim 4.72)
	(envelope-from <jorrit\@jorrit.de>)
	id 1PCwED-0007od-ED
	for joe\@localhost; Mon, 01 Nov 2010 16:23:21 +0100
Date: Mon, 1 Nov 2010 16:23:21 +0100
From: =?iso-8859-1?Q?J=F6?= Fahlke <jorrit\@jorrit.de>
To: =?iso-8859-1?Q?J=F6?= <joe\@localhost>
Subject: test
Message-ID: <$_[0].20101101152321.GA29893\@paranoia>
MIME-Version: 1.0
Content-Type: multipart/signed; micalg=pgp-sha256;
	protocol="application/pgp-signature"; boundary="HlL+5n6rz5pIUxbD"
Content-Disposition: inline
User-Agent: Mutt/1.5.20 (2009-06-14)
Status: RO
Content-Length: 1262


--HlL+5n6rz5pIUxbD
Content-Type: text/plain; charset=us-ascii
Content-Disposition: inline
Content-Transfer-Encoding: quoted-printable

test

--=20
Q: How does a Unix guru have sex?
A: unzip;strip;touch;finger;mount;fsck;more;yes;umount;sleep
-- unknown source

--HlL+5n6rz5pIUxbD
Content-Type: application/pgp-signature; name="signature.asc"
Content-Description: Digital signature
Content-Disposition: inline

-----BEGIN PGP SIGNATURE-----
Version: GnuPG v1.4.10 (GNU/Linux)

iQIVAwUBTM7baemx9CKgc0OWAQgKEQ//a2zxjK66T/zKCMHpR1/BXbhS1KG8J6aN
pi5QlwooiWuDX0jxjx31xbV0CwsQ/XUD5MZAQUBad7UMdmpxzgEC4uQsdT5OgyZP
1XTlHAuAbdg5tlbwoERjVVLJDDy39HqSUXNl9s43WjRx5luGBl5vE9AXN3T2WXwM
vC6RgIMQulhhgR64B1k1Z9lfOYCbvWO+M0YhtxZ0rgmo4lCMKj6dU1xLS4pZxxUJ
40vDKd991941jQN4VMvulS+jMuO4StPaIg6bWfEeVfPS3m9uFlxEuTQ0FXZ5RYEB
6PEuc0dNCwgCpuLR6xdMbpWcDJZMcbUki1xic8J9TOl9FW/ggjYv+WAXhg8XjPMd
yFlpbb97Whh7BDxGnlnnVHVUHyQJppG3Foq5YjKa7UMRpQMjPk2f1CaQYWbHfjBp
scbG1FnTGF2BuTycEq2Du2hA30zJ/GFA+5lUd7/vr1tJHGmfeRCd7eI09YEnpCiG
r5eoSy2p5id+qZkW+U+kA9yDVDaeKLORNF+hLgh/8V/KkS6g3APdqfFxkedHHUxI
iDMmFHLuGD6MY7DqI7PfnvuxaferALLCdaSuH2iiqjA16wWjjXe8aX3NN296L4Bn
ZKfdEGL4uDK7am9kHWusQP/ZrW5mUH566Ay95oZQcNB7k8i5ySmfLMPjLB4NJgpg
qaEk0K/VEYU=
=a48j
-----END PGP SIGNATURE-----

--HlL+5n6rz5pIUxbD--


EOT
# un-confuse emacs' syntax highlighting

=pod

=cut

    print $text;
    return length $text;
}
