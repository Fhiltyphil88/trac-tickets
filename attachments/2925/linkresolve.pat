--- mutt-1.5.15cvs/browser.c	Tue Apr 10 09:00:08 2007
+++ mgo/browser.c	Sun Jun 24 17:40:52 2007
@@ -308,7 +308,7 @@
 }
 
 static void add_folder (MUTTMENU *m, struct browser_state *state,
-			const char *name, const struct stat *s, int new)
+			const char *name, struct stat *s, char *buffer, int new)
 {
   if (state->entrylen == state->entrymax)
   {
@@ -324,6 +324,7 @@
   if (s != NULL)
   {
     (state->entry)[state->entrylen].mode = s->st_mode;
+    if (S_ISLNK (s->st_mode)) { stat(buffer,s); } /* keep mode for links, but use linked to time + size */
     (state->entry)[state->entrylen].mtime = s->st_mtime;
     (state->entry)[state->entrylen].size = s->st_size;
     
@@ -415,7 +416,7 @@
     tmp = Incoming;
     while (tmp && mutt_strcmp (buffer, tmp->path))
       tmp = tmp->next;
-    add_folder (menu, state, de->d_name, &s, (tmp) ? tmp->new : 0);
+    add_folder (menu, state, de->d_name, &s, buffer, (tmp) ? tmp->new : 0);
   }
   closedir (dp);  
   browser_sort (state);
@@ -443,14 +444,14 @@
     if (mx_is_imap (tmp->path))
     {
       imap_mailbox_state (tmp->path, &mbox);
-      add_folder (menu, state, tmp->path, NULL, mbox.new);
+      add_folder (menu, state, tmp->path, NULL, buffer, mbox.new);
       continue;
     }
 #endif
 #ifdef USE_POP
     if (mx_is_pop (tmp->path))
     {
-      add_folder (menu, state, tmp->path, NULL, tmp->new);
+      add_folder (menu, state, tmp->path, NULL, buffer, tmp->new);
       continue;
     }
 #endif
@@ -464,7 +465,7 @@
     strfcpy (buffer, NONULL(tmp->path), sizeof (buffer));
     mutt_pretty_mailbox (buffer);
 
-    add_folder (menu, state, buffer, &s, tmp->new);
+    add_folder (menu, state, buffer, &s, tmp->path, tmp->new);
   }
   while ((tmp = tmp->next));
   browser_sort (state);
