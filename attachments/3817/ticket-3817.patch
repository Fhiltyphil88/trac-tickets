# HG changeset patch
# User Kevin McCarthy <kevin@8t8.us>
# Date 1458854394 25200
#      Thu Mar 24 14:19:54 2016 -0700
# Node ID 5a96c243ab5c621af943b6779d9f42fbdb706b9d
# Parent  484b34e23f2a14ed3cfc9db160bd58ce56ea4f46
Fix error handling in sync_helper() and imap_sync_mailbox(). (closes #3817)

This patch is based on the one Richard Russon found in the Fedora package.

If an error occurs during one of the imap_exec() calls in
imap_sync_mailbox(), the mailbox could end up being closed.  This
would cause idata->ctx to be NULL.  Add a check in sync_helper() for
the case where idata->ctx == NULL.

In imap_sync_mailbox(), check the return value of sync_helper().  To
keep the code simple, change rc from being the sum of the calls to the
bitwise-OR of the calls.  (We only need to know if a single flag needs
to be updated, and bitwise-OR will detect negatives.)

Below the calls to sync_helper(), if the call to imap_exec() fails,
make sure rc is set to -1.

diff --git a/imap/imap.c b/imap/imap.c
--- a/imap/imap.c
+++ b/imap/imap.c
@@ -1115,18 +1115,20 @@
 
   return 0;
 }
 
 static int sync_helper (IMAP_DATA* idata, int right, int flag, const char* name)
 {
   int count = 0;
   int rc;
+  char buf[LONG_STRING];
 
-  char buf[LONG_STRING];
+  if (!idata->ctx)
+    return -1;
 
   if (!mutt_bit_isset (idata->ctx->rights, right))
     return 0;
 
   if (right == M_ACL_WRITE && !imap_has_flag (idata->flags, name))
     return 0;
 
   snprintf (buf, sizeof(buf), "+FLAGS.SILENT (%s)", name);
@@ -1233,37 +1235,39 @@
       }
     }
   }
 
 #if USE_HCACHE
   imap_hcache_close (idata);
 #endif
 
-  /* sync +/- flags for the five flags mutt cares about */
-  rc = 0;
-
   /* presort here to avoid doing 10 resorts in imap_exec_msgset */
   oldsort = Sort;
   if (Sort != SORT_ORDER)
   {
     hdrs = ctx->hdrs;
     ctx->hdrs = safe_malloc (ctx->msgcount * sizeof (HEADER*));
     memcpy (ctx->hdrs, hdrs, ctx->msgcount * sizeof (HEADER*));
 
     Sort = SORT_ORDER;
     qsort (ctx->hdrs, ctx->msgcount, sizeof (HEADER*),
            mutt_get_sort_func (SORT_ORDER));
   }
 
-  rc += sync_helper (idata, M_ACL_DELETE, M_DELETED, "\\Deleted");
-  rc += sync_helper (idata, M_ACL_WRITE, M_FLAG, "\\Flagged");
-  rc += sync_helper (idata, M_ACL_WRITE, M_OLD, "Old");
-  rc += sync_helper (idata, M_ACL_SEEN, M_READ, "\\Seen");
-  rc += sync_helper (idata, M_ACL_WRITE, M_REPLIED, "\\Answered");
+  rc = 0;
+  if (((rc |= sync_helper (idata, M_ACL_DELETE, M_DELETED, "\\Deleted")) < 0) ||
+      ((rc |= sync_helper (idata, M_ACL_WRITE, M_FLAG, "\\Flagged")) < 0) ||
+      ((rc |= sync_helper (idata, M_ACL_WRITE, M_OLD, "Old")) < 0) ||
+      ((rc |= sync_helper (idata, M_ACL_SEEN, M_READ, "\\Seen")) < 0) ||
+      ((rc |= sync_helper (idata, M_ACL_WRITE, M_REPLIED, "\\Answered")) < 0))
+  {
+    mutt_error _("Error saving flags");
+    goto out;
+  }
 
   if (oldsort != Sort)
   {
     Sort = oldsort;
     FREE (&ctx->hdrs);
     ctx->hdrs = hdrs;
   }
 
@@ -1275,16 +1279,17 @@
       {
         rc = 0;
         idata->state = IMAP_AUTHENTICATED;
         goto out;
       }
     }
     else
       mutt_error _("Error saving flags");
+    rc = -1;
     goto out;
   }
 
   /* Update local record of server state to reflect the synchronization just
    * completed.  imap_read_headers always overwrites hcache-origin flags, so
    * there is no need to mutate the hcache after flag-only changes. */
   for (n = 0; n < ctx->msgcount; n++)
   {
