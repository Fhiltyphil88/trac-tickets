# HG changeset patch
# User Matthias Andree <matthias.andree@gmx.de>
# Date 1488578839 28800
#      Fri Mar 03 14:07:19 2017 -0800
# Node ID 279fb726995bd038483e29b82187a9fb63089c88
# Parent  d15de76f71232147aef90afaf1431b5fa4f972d2
Add $ssl_verify_partial_chains option for OpenSSL.  (closes #3916)

The reworked OpenSSL certificate validation took away a "feature" of
the previous implementation: the ability to reject a node in the chain
and yet continue to the next node.

If this new option is set to 'yes', enables OpenSSL's
X509_V_FLAG_PARTIAL_CHAIN flag to reinstate the functionality and permit
to use a non-root certificate as the trust anchor.

This option is only available if OpenSSL offers the
X509_V_FLAG_PARTIAL_CHAIN macro, which should be the case as of 1.0.2b
or later.

Code written by Kevin McCarthy and Matthias Andree.

diff --git a/doc/makedoc-defs.h b/doc/makedoc-defs.h
--- a/doc/makedoc-defs.h
+++ b/doc/makedoc-defs.h
@@ -14,16 +14,19 @@
 #  define USE_POP
 # endif
 # ifndef USE_SMTP
 #  define USE_SMTP
 # endif
 # ifndef USE_SSL_OPENSSL
 #  define USE_SSL_OPENSSL
 # endif
+# ifndef X509_V_FLAG_PARTIAL_CHAIN
+#  define X509_V_FLAG_PARTIAL_CHAIN
+# endif
 # ifndef USE_SSL_GNUTLS
 #  define USE_SSL_GNUTLS
 # endif
 # ifndef USE_SSL
 #  define USE_SSL
 # endif
 # ifndef USE_SOCKET
 #  define USE_SOCKET
diff --git a/init.h b/init.h
--- a/init.h
+++ b/init.h
@@ -74,16 +74,20 @@
   short type;
   short flags;
   unsigned long data;
   unsigned long init; /* initial value */
 };
 
 #define UL (unsigned long)
 
+#ifdef USE_SSL_OPENSSL
+/* need to check X509_V_FLAG_PARTIAL_CHAIN later */
+# include <openssl/x509_vfy.h>
+#endif
 #endif /* _MAKEDOC */
 
 #ifndef ISPELL
 #define ISPELL "ispell"
 #endif
 
 struct option_t MuttVars[] = {
   /*++*/
@@ -3372,16 +3376,34 @@
   { "ssl_verify_host", DT_BOOL, R_NONE, OPTSSLVERIFYHOST, 1 },
   /*
   ** .pp
   ** If \fIset\fP (the default), mutt will not automatically accept a server
   ** certificate whose host name does not match the host used in your folder
   ** URL. You should only unset this for particular known hosts, using
   ** the \fC$<account-hook>\fP function.
   */
+# ifdef USE_SSL_OPENSSL
+#  ifdef X509_V_FLAG_PARTIAL_CHAIN
+  { "ssl_verify_partial_chains", DT_BOOL, R_NONE, OPTSSLVERIFYPARTIAL, 0 },
+  /*
+  ** .pp
+  ** This option should not be changed from the default unless you understand
+  ** what you are doing.
+  ** .pp
+  ** Setting this variable to \fIyes\fP will permit verifying partial
+  ** certification chains, i. e. a certificate chain where not the root,
+  ** but an intermediate certificate CA, or the host certificate, are
+  ** marked trusted (in $$certificate_file), without marking the root
+  ** signing CA as trusted.
+  ** .pp
+  ** (OpenSSL 1.0.2b and newer only).
+  */
+#  endif /* defined X509_V_FLAG_PARTIAL_CHAIN */
+# endif /* defined USE_SSL_OPENSSL */
   { "ssl_ciphers", DT_STR, R_NONE, UL &SslCiphers, UL 0 },
   /*
   ** .pp
   ** Contains a colon-seperated list of ciphers to use when using SSL.
   ** For OpenSSL, see ciphers(1) for the syntax of the string.
   ** .pp
   ** For GnuTLS, this option will be used in place of "NORMAL" at the
   ** start of the priority string.  See gnutls_priority_init(3) for the
diff --git a/mutt.h b/mutt.h
--- a/mutt.h
+++ b/mutt.h
@@ -391,16 +391,19 @@
 # endif /* USE_SSL_GNUTLS */
   OPTSSLV3,
   OPTTLSV1,
   OPTTLSV1_1,
   OPTTLSV1_2,
   OPTSSLFORCETLS,
   OPTSSLVERIFYDATES,
   OPTSSLVERIFYHOST,
+# ifdef USE_SSL_OPENSSL
+  OPTSSLVERIFYPARTIAL,
+# endif /* USE_SSL_OPENSSL */
 #endif /* defined(USE_SSL) */
   OPTIMPLICITAUTOVIEW,
   OPTINCLUDEONLYFIRST,
   OPTKEEPFLAGGED,
   OPTMAILCAPSANITIZE,
   OPTMAILCHECKRECENT,
   OPTMAILCHECKSTATS,
   OPTMAILDIRTRASH,
diff --git a/mutt_ssl.c b/mutt_ssl.c
--- a/mutt_ssl.c
+++ b/mutt_ssl.c
@@ -18,16 +18,17 @@
 
 #if HAVE_CONFIG_H
 # include "config.h"
 #endif
 
 #include <openssl/ssl.h>
 #include <openssl/x509.h>
 #include <openssl/x509v3.h>
+#include <openssl/x509_vfy.h>
 #include <openssl/err.h>
 #include <openssl/rand.h>
 #include <openssl/evp.h>
 
 #undef _
 
 #include <string.h>
 
@@ -52,16 +53,22 @@
 #else
 static int entropy_byte_count = 0;
 /* OpenSSL fills the entropy pool from /dev/urandom if it exists */
 #define HAVE_ENTROPY()	(!access(DEVRANDOM, R_OK) || entropy_byte_count >= 16)
 #endif
 
 /* index for storing hostname as application specific data in SSL structure */
 static int HostExDataIndex = -1;
+
+/* Index for storing the "skip mode" state in SSL structure.  When the
+ * user skips a certificate in the chain, the stored value will be
+ * non-null. */
+static int SkipModeExDataIndex = -1;
+
 /* keep a handle on accepted certificates in case we want to
  * open up another connection to the same server in this session */
 static STACK_OF(X509) *SslSessionCerts = NULL;
 
 typedef struct
 {
   SSL_CTX *ctx;
   SSL *ssl;
@@ -77,17 +84,17 @@
 static int ssl_socket_write (CONNECTION* conn, const char* buf, size_t len);
 static int ssl_socket_open (CONNECTION * conn);
 static int ssl_socket_close (CONNECTION * conn);
 static int tls_close (CONNECTION* conn);
 static void ssl_err (sslsockdata *data, int err);
 static void ssl_dprint_err_stack (void);
 static int ssl_cache_trusted_cert (X509 *cert);
 static int ssl_verify_callback (int preverify_ok, X509_STORE_CTX *ctx);
-static int interactive_check_cert (X509 *cert, int idx, int len);
+static int interactive_check_cert (X509 *cert, int idx, int len, SSL *ssl);
 static void ssl_get_client_cert(sslsockdata *ssldata, CONNECTION *conn);
 static int ssl_passwd_cb(char *buf, int size, int rwflag, void *userdata);
 static int ssl_negotiate (CONNECTION *conn, sslsockdata*);
 
 /* ssl certificate verification can behave strangely if there are expired
  * certs loaded into the trusted store.  This function filters out expired
  * certs.
  * Previously the code used this form:
@@ -131,16 +138,45 @@
   ERR_clear_error();
 
   X509_free (cert);
   safe_fclose (&fp);
 
   return rv;
 }
 
+static int ssl_set_verify_partial (SSL_CTX *ctx)
+{
+  int rv = 0;
+#ifdef X509_V_FLAG_PARTIAL_CHAIN
+  X509_VERIFY_PARAM *param;
+
+  if (option (OPTSSLVERIFYPARTIAL))
+  {
+    param = X509_VERIFY_PARAM_new();
+    if (param)
+    {
+      X509_VERIFY_PARAM_set_flags(param, X509_V_FLAG_PARTIAL_CHAIN);
+      if (0 == SSL_CTX_set1_param(ctx, param))
+      {
+        dprint (2, (debugfile, "ssl_set_verify_partial: SSL_CTX_set1_param() failed."));
+        rv = -1;
+      }
+      X509_VERIFY_PARAM_free(param);
+    }
+    else
+    {
+      dprint (2, (debugfile, "ssl_set_verify_partial: X509_VERIFY_PARAM_new() failed."));
+      rv = -1;
+    }
+  }
+#endif
+  return rv;
+}
+
 /* mutt_ssl_starttls: Negotiate TLS over an already opened connection.
  *   TODO: Merge this code better with ssl_socket_open. */
 int mutt_ssl_starttls (CONNECTION* conn)
 {
   sslsockdata* ssldata;
   int maxbits;
   long ssl_options = 0;
 
@@ -201,16 +237,22 @@
 
   if (SslCiphers) {
     if (!SSL_CTX_set_cipher_list (ssldata->ctx, SslCiphers)) {
       dprint (1, (debugfile, "mutt_ssl_starttls: Could not select preferred ciphers\n"));
       goto bail_ctx;
     }
   }
 
+  if (ssl_set_verify_partial (ssldata->ctx))
+  {
+    mutt_error (_("Warning: error enabling ssl_verify_partial_chains"));
+    mutt_sleep (2);
+  }
+
   if (! (ssldata->ssl = SSL_new (ssldata->ctx)))
   {
     dprint (1, (debugfile, "mutt_ssl_starttls: Error allocating SSL\n"));
     goto bail_ctx;
   }
 
   if (SSL_set_fd (ssldata->ssl, conn->fd) != 1)
   {
@@ -448,16 +490,22 @@
     dprint (1, (debugfile, "ssl_socket_open: Error loading trusted certificates\n"));
 
   ssl_get_client_cert(data, conn);
 
   if (SslCiphers) {
     SSL_CTX_set_cipher_list (data->ctx, SslCiphers);
   }
 
+  if (ssl_set_verify_partial (data->ctx))
+  {
+    mutt_error (_("Warning: error enabling ssl_verify_partial_chains"));
+    mutt_sleep (2);
+  }
+
   data->ssl = SSL_new (data->ctx);
   SSL_set_fd (data->ssl, conn->fd);
 
   if (ssl_negotiate(conn, data))
   {
     mutt_socket_close (conn);
     return -1;
   }
@@ -484,16 +532,28 @@
   }
 
   if (! SSL_set_ex_data (ssldata->ssl, HostExDataIndex, conn->account.host))
   {
     dprint (1, (debugfile, "failed to save hostname in SSL structure\n"));
     return -1;
   }
 
+  if ((SkipModeExDataIndex = SSL_get_ex_new_index (0, "skip", NULL, NULL, NULL)) == -1)
+  {
+    dprint (1, (debugfile, "failed to get index for application specific data\n"));
+    return -1;
+  }
+
+  if (! SSL_set_ex_data (ssldata->ssl, SkipModeExDataIndex, NULL))
+  {
+    dprint (1, (debugfile, "failed to save skip mode in SSL structure\n"));
+    return -1;
+  }
+
   SSL_set_verify (ssldata->ssl, SSL_VERIFY_PEER, ssl_verify_callback);
   SSL_set_mode (ssldata->ssl, SSL_MODE_AUTO_RETRY);
   ERR_clear_error ();
 
   if ((err = SSL_connect (ssldata->ssl)) != 1)
   {
     switch (SSL_get_error (ssldata->ssl, err))
     {
@@ -942,83 +1002,95 @@
  * certificate is trusted, returning 0 immediately aborts the SSL connection */
 static int ssl_verify_callback (int preverify_ok, X509_STORE_CTX *ctx)
 {
   char buf[STRING];
   const char *host;
   int len, pos;
   X509 *cert;
   SSL *ssl;
+  int skip_mode;
 
   if (! (ssl = X509_STORE_CTX_get_ex_data (ctx, SSL_get_ex_data_X509_STORE_CTX_idx ())))
   {
     dprint (1, (debugfile, "ssl_verify_callback: failed to retrieve SSL structure from X509_STORE_CTX\n"));
     return 0;
   }
   if (! (host = SSL_get_ex_data (ssl, HostExDataIndex)))
   {
     dprint (1, (debugfile, "ssl_verify_callback: failed to retrieve hostname from SSL structure\n"));
     return 0;
   }
 
+  /* This is true when a previous entry in the certificate chain did
+   * not verify and the user manually chose to skip it via the
+   * $ssl_verify_partial_chains option.
+   * In this case, all following certificates need to be treated as non-verified
+   * until one is actually verified.
+   */
+  skip_mode = (SSL_get_ex_data (ssl, SkipModeExDataIndex) != NULL);
+
   cert = X509_STORE_CTX_get_current_cert (ctx);
   pos = X509_STORE_CTX_get_error_depth (ctx);
   len = sk_X509_num (X509_STORE_CTX_get_chain (ctx));
 
-  dprint (1, (debugfile, "ssl_verify_callback: checking cert chain entry %s (preverify: %d)\n",
-              X509_NAME_oneline (X509_get_subject_name (cert),
-                                 buf, sizeof (buf)), preverify_ok));
+  dprint (1, (debugfile,
+              "ssl_verify_callback: checking cert chain entry %s (preverify: %d skipmode: %d)\n",
+              X509_NAME_oneline (X509_get_subject_name (cert), buf, sizeof (buf)),
+              preverify_ok, skip_mode));
 
   /* check session cache first */
   if (check_certificate_cache (cert))
   {
     dprint (2, (debugfile, "ssl_verify_callback: using cached certificate\n"));
+    SSL_set_ex_data (ssl, SkipModeExDataIndex, NULL);
     return 1;
   }
 
   /* check hostname only for the leaf certificate */
   buf[0] = 0;
   if (pos == 0 && option (OPTSSLVERIFYHOST) != MUTT_NO)
   {
     if (!check_host (cert, host, buf, sizeof (buf)))
     {
       mutt_error (_("Certificate host check failed: %s"), buf);
       mutt_sleep (2);
-      return interactive_check_cert (cert, pos, len);
+      return interactive_check_cert (cert, pos, len, ssl);
     }
     dprint (2, (debugfile, "ssl_verify_callback: hostname check passed\n"));
   }
 
-  if (!preverify_ok)
+  if (!preverify_ok || skip_mode)
   {
     /* automatic check from user's database */
     if (SslCertFile && check_certificate_by_digest (cert))
     {
       dprint (2, (debugfile, "ssl_verify_callback: digest check passed\n"));
+      SSL_set_ex_data (ssl, SkipModeExDataIndex, NULL);
       return 1;
     }
 
 #ifdef DEBUG
     /* log verification error */
     {
       int err = X509_STORE_CTX_get_error (ctx);
       snprintf (buf, sizeof (buf), "%s (%d)",
          X509_verify_cert_error_string (err), err);
       dprint (2, (debugfile, "X509_verify_cert: %s\n", buf));
     }
 #endif
 
-    /* prompt user */
-    return interactive_check_cert (cert, pos, len);
+   /* prompt user */
+    return interactive_check_cert (cert, pos, len, ssl);
   }
 
   return 1;
 }
 
-static int interactive_check_cert (X509 *cert, int idx, int len)
+static int interactive_check_cert (X509 *cert, int idx, int len, SSL *ssl)
 {
   static const int part[] =
     { NID_commonName,             /* CN */
       NID_pkcs9_emailAddress,     /* Email */
       NID_organizationName,       /* O */
       NID_organizationalUnitName, /* OU */
       NID_localityName,           /* L */
       NID_stateOrProvinceName,    /* ST */
@@ -1027,16 +1099,17 @@
   X509_NAME *x509_issuer;
   char helpstr[LONG_STRING];
   char buf[STRING];
   char title[STRING];
   MUTTMENU *menu = mutt_new_menu (MENU_GENERIC);
   int done, row, i;
   unsigned u;
   FILE *fp;
+  int allow_always = 0, allow_skip = 0;
 
   menu->max = mutt_array_size (part) * 2 + 10;
   menu->dialog = (char **) safe_calloc (1, menu->max * sizeof (char *));
   for (i = 0; i < menu->max; i++)
     menu->dialog[i] = (char *) safe_calloc (1, SHORT_STRING * sizeof (char));
 
   row = 0;
   strfcpy (menu->dialog[row], _("This certificate belongs to:"), SHORT_STRING);
@@ -1068,28 +1141,48 @@
   buf[0] = '\0';
   x509_fingerprint (buf, sizeof (buf), cert, EVP_md5);
   snprintf (menu->dialog[row++], SHORT_STRING, _("MD5 Fingerprint: %s"), buf);
 
   snprintf (title, sizeof (title),
 	    _("SSL Certificate check (certificate %d of %d in chain)"),
 	    len - idx, len);
   menu->title = title;
+
+  /* The leaf/host certificate can't be skipped. */
+#ifdef X509_V_FLAG_PARTIAL_CHAIN
+  if ((idx != 0) &&
+      (option (OPTSSLVERIFYPARTIAL)))
+    allow_skip = 1;
+#endif
+
+  /* L10N:
+   * These four letters correspond to the choices in the next four strings:
+   * (r)eject, accept (o)nce, (a)ccept always, (s)kip.
+   * These prompts are the interactive certificate confirmation prompts for
+   * an OpenSSL connection.
+   */
+  menu->keys = _("roas");
   if (SslCertFile
       && (option (OPTSSLVERIFYDATES) == MUTT_NO
 	  || (X509_cmp_current_time (X509_get_notAfter (cert)) >= 0
 	      && X509_cmp_current_time (X509_get_notBefore (cert)) < 0)))
   {
-    menu->prompt = _("(r)eject, accept (o)nce, (a)ccept always");
-    menu->keys = _("roa");
+    allow_always = 1;
+    if (allow_skip)
+      menu->prompt = _("(r)eject, accept (o)nce, (a)ccept always, (s)kip");
+    else
+      menu->prompt = _("(r)eject, accept (o)nce, (a)ccept always");
   }
   else
   {
-    menu->prompt = _("(r)eject, accept (o)nce");
-    menu->keys = _("ro");
+    if (allow_skip)
+      menu->prompt = _("(r)eject, accept (o)nce, (s)kip");
+    else
+      menu->prompt = _("(r)eject, accept (o)nce");
   }
 
   helpstr[0] = '\0';
   mutt_make_help (buf, sizeof (buf), _("Exit  "), MENU_GENERIC, OP_EXIT);
   safe_strcat (helpstr, sizeof (helpstr), buf);
   mutt_make_help (buf, sizeof (buf), _("Help"), MENU_GENERIC, OP_HELP);
   safe_strcat (helpstr, sizeof (helpstr), buf);
   menu->help = helpstr;
@@ -1101,16 +1194,18 @@
     switch (mutt_menuLoop (menu))
     {
       case -1:			/* abort */
       case OP_MAX + 1:		/* reject */
       case OP_EXIT:
         done = 1;
         break;
       case OP_MAX + 3:		/* accept always */
+        if (!allow_always)
+          break;
         done = 0;
         if ((fp = fopen (SslCertFile, "a")))
 	{
 	  if (PEM_write_X509 (fp, cert))
 	    done = 1;
 	  safe_fclose (&fp);
 	}
 	if (!done)
@@ -1121,18 +1216,25 @@
 	else
         {
 	  mutt_message (_("Certificate saved"));
 	  mutt_sleep (0);
 	}
         /* fall through */
       case OP_MAX + 2:		/* accept once */
         done = 2;
+        SSL_set_ex_data (ssl, SkipModeExDataIndex, NULL);
 	ssl_cache_trusted_cert (cert);
         break;
+      case OP_MAX + 4:          /* skip */
+        if (!allow_skip)
+          break;
+        done = 2;
+        SSL_set_ex_data (ssl, SkipModeExDataIndex, &SkipModeExDataIndex);
+        break;
     }
   }
   unset_option(OPTIGNOREMACROEVENTS);
   mutt_menuDestroy (&menu);
   set_option (OPTNEEDREDRAW);
   dprint (2, (debugfile, "ssl interactive_check_cert: done=%d\n", done));
   return (done == 2);
 }
