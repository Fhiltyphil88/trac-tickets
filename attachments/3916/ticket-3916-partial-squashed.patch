exporting patch:
# HG changeset patch
# User Matthias Andree <matthias.andree@gmx.de>
# Date 1488443711 -3600
#      Thu Mar 02 09:35:11 2017 +0100
# Node ID a09eaea693af19ea6c337dab82ec7bbde6478c81
# Parent  66056fb5f434dbbf5c3588f1ac4d8efd96401e5c
Add $ssl_verify_partial_chains option for OpenSSL.  (closes #3916)

The reworked OpenSSL certificate validation took away a "feature" of
the previous implementation: the ability to reject a node in the chain
and yet continue to the next node.

If this new option is set to 'yes', enables OpenSSL's
X509_V_FLAG_PARTIAL_CHAIN flag to reinstate the functionality and permit
to use a non-root certificate as the trust anchor.

This option is only available if OpenSSL offers the
X509_V_FLAG_PARTIAL_CHAIN macro, which should be the case as of 1.0.2b
or later.

Code written by Kevin McCarthy and Matthias Andree.

diff --git a/init.h b/init.h
--- a/init.h
+++ b/init.h
@@ -79,6 +79,10 @@
 
 #define UL (unsigned long)
 
+#ifdef USE_SSL_OPENSSL
+/* need to check X509_V_FLAG_PARTIAL_CHAIN later */
+# include <openssl/x509_vfy.h>
+#endif
 #endif /* _MAKEDOC */
 
 #ifndef ISPELL
@@ -3377,6 +3381,23 @@
   ** URL. You should only unset this for particular known hosts, using
   ** the \fC$<account-hook>\fP function.
   */
+# ifdef USE_SSL_OPENSSL
+#  ifdef X509_V_FLAG_PARTIAL_CHAIN
+  { "ssl_verify_partial_chains", DT_BOOL, R_NONE, OPT_SSLVERIFYPARTIAL, 0 },
+  /*
+  ** .pp
+  ** This option should not be changed from the default unless you understand
+  ** what you are doing.
+  ** .pp
+  ** Setting this variable to \fIyes\fP will permit verifying partial
+  ** certification chains, i. e. when an intermediate signing CA, or the
+  ** host certificate, are marked as trusted (in $$certificate_file),
+  ** without marking the root signing CA as trusted.
+  ** .pp
+  ** (OpenSSL 1.0.2b and newer only).
+  */
+#  endif /* defined X509_V_FLAG_PARTIAL_CHAIN */
+# endif /* defined USE_SSL_OPENSSL */
   { "ssl_ciphers", DT_STR, R_NONE, UL &SslCiphers, UL 0 },
   /*
   ** .pp
diff --git a/mutt.h b/mutt.h
--- a/mutt.h
+++ b/mutt.h
@@ -396,6 +396,9 @@
   OPTSSLFORCETLS,
   OPTSSLVERIFYDATES,
   OPTSSLVERIFYHOST,
+# ifdef USE_SSL_OPENSSL
+  OPT_SSLVERIFYPARTIAL,
+# endif /* USE_SSL_OPENSSL */
 #endif /* defined(USE_SSL) */
   OPTIMPLICITAUTOVIEW,
   OPTINCLUDEONLYFIRST,
diff --git a/mutt_ssl.c b/mutt_ssl.c
--- a/mutt_ssl.c
+++ b/mutt_ssl.c
@@ -23,6 +23,7 @@
 #include <openssl/ssl.h>
 #include <openssl/x509.h>
 #include <openssl/x509v3.h>
+#include <openssl/x509_vfy.h>
 #include <openssl/err.h>
 #include <openssl/rand.h>
 #include <openssl/evp.h>
@@ -57,6 +58,12 @@
 
 /* index for storing hostname as application specific data in SSL structure */
 static int HostExDataIndex = -1;
+
+/* Index for storing the "skip mode" state in SSL structure.  When the
+ * user skips a certificate in the chain, the stored value will be
+ * non-null. */
+static int SkipModeExDataIndex = -1;
+
 /* keep a handle on accepted certificates in case we want to
  * open up another connection to the same server in this session */
 static STACK_OF(X509) *SslSessionCerts = NULL;
@@ -82,7 +89,7 @@
 static void ssl_dprint_err_stack (void);
 static int ssl_cache_trusted_cert (X509 *cert);
 static int ssl_verify_callback (int preverify_ok, X509_STORE_CTX *ctx);
-static int interactive_check_cert (X509 *cert, int idx, int len);
+static int interactive_check_cert (X509 *cert, int idx, int len, SSL *ssl);
 static void ssl_get_client_cert(sslsockdata *ssldata, CONNECTION *conn);
 static int ssl_passwd_cb(char *buf, int size, int rwflag, void *userdata);
 static int ssl_negotiate (CONNECTION *conn, sslsockdata*);
@@ -453,6 +460,24 @@
     SSL_CTX_set_cipher_list (data->ctx, SslCiphers);
   }
 
+#ifdef X509_V_FLAG_PARTIAL_CHAIN
+  if (option (OPT_SSLVERIFYPARTIAL)) {
+    X509_VERIFY_PARAM *param;
+    param = X509_VERIFY_PARAM_new();
+    if (param) {
+      X509_VERIFY_PARAM_set_flags(param, X509_V_FLAG_PARTIAL_CHAIN);
+      if (0 == SSL_CTX_set1_param(data->ctx, param)) {
+	mutt_error (_("Warning: ssl_socket_open: couldn't set X509_V_FLAG_PARTIAL_CHAIN."));
+	mutt_sleep (2);
+      }
+      X509_VERIFY_PARAM_free(param);
+    } else {
+      mutt_error (_("Warning: ssl_socket_open: X509_VERIFY_PARAM_new() failed."));
+      mutt_sleep (2);
+    }
+  }
+#endif
+
   data->ssl = SSL_new (data->ctx);
   SSL_set_fd (data->ssl, conn->fd);
 
@@ -489,6 +514,18 @@
     return -1;
   }
 
+  if ((SkipModeExDataIndex = SSL_get_ex_new_index (0, "skip", NULL, NULL, NULL)) == -1)
+  {
+    dprint (1, (debugfile, "failed to get index for application specific data\n"));
+    return -1;
+  }
+
+  if (! SSL_set_ex_data (ssldata->ssl, SkipModeExDataIndex, NULL))
+  {
+    dprint (1, (debugfile, "failed to save skip mode in SSL structure\n"));
+    return -1;
+  }
+
   SSL_set_verify (ssldata->ssl, SSL_VERIFY_PEER, ssl_verify_callback);
   SSL_set_mode (ssldata->ssl, SSL_MODE_AUTO_RETRY);
   ERR_clear_error ();
@@ -946,6 +983,7 @@
   int len, pos;
   X509 *cert;
   SSL *ssl;
+  int skip_mode;
 
   if (! (ssl = X509_STORE_CTX_get_ex_data (ctx, SSL_get_ex_data_X509_STORE_CTX_idx ())))
   {
@@ -958,18 +996,28 @@
     return 0;
   }
 
+  /* This is true when a previous entry in the certificate chain did not verify
+   * and the user either automatically or manually chose to skip it via
+   * the $ssl_verify_partial_chains quadoption.
+   * In this case, all following certificates need to be treated as non-verified
+   * until one is actually verified.
+   */
+  skip_mode = (SSL_get_ex_data (ssl, SkipModeExDataIndex) != NULL);
+
   cert = X509_STORE_CTX_get_current_cert (ctx);
   pos = X509_STORE_CTX_get_error_depth (ctx);
   len = sk_X509_num (X509_STORE_CTX_get_chain (ctx));
 
-  dprint (1, (debugfile, "ssl_verify_callback: checking cert chain entry %s (preverify: %d)\n",
-              X509_NAME_oneline (X509_get_subject_name (cert),
-                                 buf, sizeof (buf)), preverify_ok));
+  dprint (1, (debugfile,
+              "ssl_verify_callback: checking cert chain entry %s (preverify: %d skipmode: %d)\n",
+              X509_NAME_oneline (X509_get_subject_name (cert), buf, sizeof (buf)),
+              preverify_ok, skip_mode));
 
   /* check session cache first */
   if (check_certificate_cache (cert))
   {
     dprint (2, (debugfile, "ssl_verify_callback: using cached certificate\n"));
+    SSL_set_ex_data (ssl, SkipModeExDataIndex, NULL);
     return 1;
   }
 
@@ -981,17 +1029,18 @@
     {
       mutt_error (_("Certificate host check failed: %s"), buf);
       mutt_sleep (2);
-      return interactive_check_cert (cert, pos, len);
+      return interactive_check_cert (cert, pos, len, ssl);
     }
     dprint (2, (debugfile, "ssl_verify_callback: hostname check passed\n"));
   }
 
-  if (!preverify_ok)
+  if (!preverify_ok || skip_mode)
   {
     /* automatic check from user's database */
     if (SslCertFile && check_certificate_by_digest (cert))
     {
       dprint (2, (debugfile, "ssl_verify_callback: digest check passed\n"));
+      SSL_set_ex_data (ssl, SkipModeExDataIndex, NULL);
       return 1;
     }
 
@@ -1005,14 +1054,20 @@
     }
 #endif
 
+    if ((pos != 0) && (quadoption (OPT_SSLVERIFYPARTIAL) == MUTT_YES))
+    {
+      SSL_set_ex_data (ssl, SkipModeExDataIndex, &SkipModeExDataIndex);
+      return 1;
+    }
+
     /* prompt user */
-    return interactive_check_cert (cert, pos, len);
+    return interactive_check_cert (cert, pos, len, ssl);
   }
 
   return 1;
 }
 
-static int interactive_check_cert (X509 *cert, int idx, int len)
+static int interactive_check_cert (X509 *cert, int idx, int len, SSL *ssl)
 {
   static const int part[] =
     { NID_commonName,             /* CN */
@@ -1031,6 +1086,7 @@
   int done, row, i;
   unsigned u;
   FILE *fp;
+  int allow_always = 0, allow_skip = 0;
 
   menu->max = mutt_array_size (part) * 2 + 10;
   menu->dialog = (char **) safe_calloc (1, menu->max * sizeof (char *));
@@ -1072,18 +1128,36 @@
 	    _("SSL Certificate check (certificate %d of %d in chain)"),
 	    len - idx, len);
   menu->title = title;
+
+  /* The leaf/host certificate can't be skipped. */
+  if ((idx != 0) &&
+      (option (OPT_SSLVERIFYPARTIAL)))
+    allow_skip = 1;
+
+  /* L10N:
+   * These four letters correspond to the choices in the next four strings:
+   * (r)eject, accept (o)nce, (a)ccept always, (s)kip.
+   * These prompts are the interactive certificate confirmation prompts for
+   * an OpenSSL connection.
+   */
+  menu->keys = _("roas");
   if (SslCertFile
       && (option (OPTSSLVERIFYDATES) == MUTT_NO
 	  || (X509_cmp_current_time (X509_get_notAfter (cert)) >= 0
 	      && X509_cmp_current_time (X509_get_notBefore (cert)) < 0)))
   {
-    menu->prompt = _("(r)eject, accept (o)nce, (a)ccept always");
-    menu->keys = _("roa");
+    allow_always = 1;
+    if (allow_skip)
+      menu->prompt = _("(r)eject, accept (o)nce, (a)ccept always, (s)kip");
+    else
+      menu->prompt = _("(r)eject, accept (o)nce, (a)ccept always");
   }
   else
   {
-    menu->prompt = _("(r)eject, accept (o)nce");
-    menu->keys = _("ro");
+    if (allow_skip)
+      menu->prompt = _("(r)eject, accept (o)nce, (s)kip");
+    else
+      menu->prompt = _("(r)eject, accept (o)nce");
   }
 
   helpstr[0] = '\0';
@@ -1105,6 +1179,8 @@
         done = 1;
         break;
       case OP_MAX + 3:		/* accept always */
+        if (!allow_always)
+          break;
         done = 0;
         if ((fp = fopen (SslCertFile, "a")))
 	{
@@ -1125,8 +1201,15 @@
         /* fall through */
       case OP_MAX + 2:		/* accept once */
         done = 2;
+        SSL_set_ex_data (ssl, SkipModeExDataIndex, NULL);
 	ssl_cache_trusted_cert (cert);
         break;
+      case OP_MAX + 4:          /* skip */
+        if (!allow_skip)
+          break;
+        done = 2;
+        SSL_set_ex_data (ssl, SkipModeExDataIndex, &SkipModeExDataIndex);
+        break;
     }
   }
   unset_option(OPTIGNOREMACROEVENTS);
